<?php

return [
    'host'=>env('ETH_HOST','127.0.0.1'),
    'port'=>env('ETH_PORT','30303')
];	