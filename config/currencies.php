<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Available crypto currencies
    |--------------------------------------------------------------------------
    */
    'crypto_currencies' => [
        'BTC' => [
            'name' => 'Bitcoin',
            'img-icon' => 'http://freeiconbox.com/icon/256/37054.png',
            'icon' => 'fab fa-bitcoin'
        ],
        'ETH' => [
            'name' => 'Ethereum',
            'img-icon' => 'https://crypto-coin.top/frontend/web/mt/img/ETH-256.png',
            'icon' => 'fab fa-ethereum'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Available fiat currencies
    |--------------------------------------------------------------------------
    */
    'fiat_currencies' => [
        'USD' => [
            'icon' => 'fas fa-dollar-sign'
        ],
        'EUR' => [
            'icon' => 'fas fa-euro-sign'
        ]
    ]
];

