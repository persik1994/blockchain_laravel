<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Auth::routes(['verify' => true]);

Route::post('2fa', function () {
    return redirect(route('wallet'));
})->name('2fa')->middleware('2fa');


/* Nexmo SMS */
Route::get('/sms/send/{to}', function(\Nexmo\Client $nexmo, $to){
    $message = $nexmo->message()->send([
        'to' => $to,
        'from' => env('NEXMO_NUMBER'),
        'text' => 'Sending SMS from Laravel. Woohoo!'
    ]);
    Log::info('sent message: ' . $message['message-id']);
});
Route::post('/sms/receive', function(\Nexmo\Client $nexmo){
    $message = \Nexmo\Message\InboundMessage::createFromGlobals();
    Log::info('got text: ' . $message->getBody());
});



Route::post('/admin/login', 'Auth\LoginController@adminLogin');
Route::get('/admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestFormAdmin')->name('admin.password.request');
Route::get('/admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetFormAdmin')->name('admin.password.reset');
Route::get('/testpage', 'IndexController@index'); //Guzzle route
Route::get('/trade/{tradeType}', 'HomeController@trade')->name('trade');
Route::get('/buy', 'HomeController@buy')->name('buy');
Route::get('/sell', 'HomeController@sell')->name('sell');
Route::get('/send', 'HomeController@send')->name('send');
Route::get('/receive', 'HomeController@receive')->name('receive');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::post('/contact-us', 'HomeController@sendContactUs')->name('send.contact-us');
Route::get('/page/{page}', 'PagesController@page')->name('admin.pages.page');

Route::prefix('support')->group(function () {
    Route::get('/', 'SupportController@index')->name('support');
    Route::post('/', 'SupportController@addTicket')->name('support.add.ticket');
    Route::get('/{id}', 'SupportController@userChat')->name('support.userchat');
    Route::post('/{id}', 'SupportController@addUserMessage')->name('support.user.chat');
});

Route::get('/test', 'PagesController@test');

Route::prefix('verification')->group(function () {
    Route::get('email/{hash}', 'Account\VerificationController@verifyEmail')->name('verification.email');
});

Route::namespace('Account')->prefix('account')->group(function () {
    Route::get('info', 'WalletController@currentAccountInformation');
    Route::get('info/support', 'WalletController@currentUserInformationSupport');

    Route::get('wallet/get-currency-prices', 'WalletController@getCurrencyPrices');
    Route::put('wallet/update-user-currencies', 'WalletController@updateUserCurrencies');

    Route::group(['middleware' => ['auth', 'privileged.user', '2fa', 'verified']], function () {
        Route::prefix('wallet')->group(function () {
            Route::get('/', 'WalletController@index')->name('wallet');
            Route::get('get-bitcoin-addresses', 'WalletController@getBitcoinAddresses');
            Route::post('create-address', 'WalletController@createAddress')->name('account.address.create');
            Route::prefix('modal')->group(function () {
                Route::get('receive-bitcoins', 'WalletController@receiveBitcoinsModal');
                Route::get('send-bitcoins', 'WalletController@sendBitcoinsModal');
                Route::get('create-address', 'WalletController@createBitcoinAddressModal');
            });
        });

        Route::get('/profile/edit', 'UsersController@editProfile')->name('profile.edit');
        Route::put('/profile/update', 'UsersController@updateProfile')->name('profile.update');
        Route::get('file/{filepath}', 'FileController@getUserFile')->where('filepath', '.*')->name('user.file');
        Route::get('/history', 'HistoryController@index')->name('history');

        Route::prefix('deposit-money')->group(function () {
            Route::get('/', 'DepositController@index')->name('deposit-money');
            Route::get('/payment-data', 'DepositController@getPaymentData');
            Route::post('save-deposit', 'DepositController@saveUserDeposit')->name('account.deposit.save');
            Route::get('/details/{id}', 'DepositController@details')->name('deposit-money.details');
        });

        Route::prefix('withdraw-money')->group(function () {
            Route::get('/', 'WithdrawController@index')->name('withdraw-money');
            Route::post('send-request', 'WithdrawController@sendRequest')->name('account.withdraw.request');
            Route::get('/details/{id}', 'WithdrawController@details')->name('withdraw-money.details');
        });

        Route::prefix('order')->group(function() {
            Route::post('send-request', 'OrderController@sendRequest')->name('order.request');
            /* new pages Auth */
            Route::get('buy', 'OrderController@buy')->name('account.order.buy');
            Route::get('sell', 'OrderController@sell')->name('account.order.sell');
            Route::get('details/{id}', 'OrderController@details')->name('account.order.details');
        });

        Route::prefix('transfer')->group(function () {
            Route::get('/', 'TransferController@index')->name('transfer');
            Route::post('send-request', 'TransferController@sendRequest')->name('account.transfer.request');
            Route::get('details/{id}', 'TransferController@details')->name('account.transfer.details');
        });

        Route::prefix('request')->group(function () {
            Route::get('/', 'RequestController@index')->name('request');
            Route::post('send-request', 'RequestController@sendRequest')->name('account.request.send');
        });

        Route::prefix('verification')->group(function () {
            Route::post('send-email', 'VerificationController@sendEmail');
            Route::post('send-documents', 'VerificationController@sendDocuments')->name('account.verification.documents');
            Route::post('save-phone-number', 'VerificationController@savePhoneNumber');
            Route::post('send-sms-code', 'VerificationController@sendSmsCode');
            Route::post('verify-sms-code', 'VerificationController@verifySmsCode');
        });

        Route::get('transactions', 'TransactionController@index')->name('transactions');

        Route::prefix('security')->group(function () {
            Route::get('/', 'SecurityController@index')->name('security');
            Route::post('change-password', 'SecurityController@changePassword')->name('account.security.password');
            Route::post('change-pin', 'SecurityController@changePin')->name('account.security.pin');
            Route::post('disable-2fa', 'SecurityController@disableTfa')->name('account.security.disable-2fa');
            Route::post('enable-2fa', 'SecurityController@enableTfa')->name('account.security.enable-2fa');
            Route::post('remove-pin', 'SecurityController@removePin');
        });
    });
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\LoginController@showAdminLoginForm')->name('login.admin');

    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['auth', 'privileged.admin']], function () {
            Route::get('/', 'DashboardController@index')->name('admin.dashboard');
            Route::get('file/{filepath}', 'FileController@getFile')->where('filepath', '.*')->name('admin.file');
            Route::get('/unviewed/count', 'NotificationsController@getUnviewedCount')->name('admin.unviewed.count');

            Route::group(['prefix' => 'faqs'], function () {
                Route::get('/', 'FaqController@index')->name('admin.faqs');
                Route::post('/add', 'FaqController@add');
                Route::put('/update/{id}', 'FaqController@update')->name('admin.faqs.update'); //new
                Route::delete('/delete/{id}', 'FaqController@delete')->name('admin.faqs.delete'); // new
            });

            // Admin USD Balance in dashboard
            Route::get('/edit-balance/{id}', 'DashboardController@editBalanceModal')->name('admin.balance.modal.edit');  
            Route::put('/update/{id}', 'DashboardController@update')->name('admin.balance.update');
  
            // Admin ETH Balance in dashboard
            Route::get('/edit-ethbalance/{id}', 'DashboardController@editBalanceEthereumModal')->name('admin.ethbalance.modal.edit');
            // Admin BTC Balance in dashboard
            Route::get('/edit-btcbalance/{id}', 'DashboardController@editBalanceBitcoinModal')->name('admin.btcbalance.modal.edit');
            // Admin ETH node balance
            Route::get('/edit-ethereum-node-balance/{id}', 'DashboardController@editBalanceEthereumNodeModal')->name('admin.ethnodebalance.modal.edit');


            /* ethereum node balance page */
            Route::group(['prefix' => 'ethereumadresses'], function () {
                Route::get('/', 'DashboardController@ethereumModalNodeBalance')->name('admin.ethereumadresses');
                Route::get('/details', 'DashboardController@detailsModal')->name('admin.ethereumadresses.modal.details');
            });


            Route::group(['prefix' => 'gateways'], function () {
                Route::get('/', 'GatewaysController@index')->name('admin.gateways');
                Route::post('/add', 'GatewaysController@add');
                Route::put('/update/{id}', 'GatewaysController@update')->name('admin.gateways.update');
               
                Route::delete('/delete/{id}', 'GatewaysController@delete')->name('admin.gateways.delete');
             
            });

            Route::group(['prefix' => 'transfers'], function () {
                Route::get('/', 'TransfersController@index')->name('admin.transfers');
                Route::post('confirm', 'TransfersController@confirmTransfer');
                Route::post('cancel', 'TransfersController@cancelTransfer');

                Route::group(['prefix' => 'modal'], function () {
                    Route::get('detail', 'TransfersController@detailModal');
                });
            });

            Route::group(['prefix' => 'users'], function () {
                Route::get('/', 'UsersController@index')->name('admin.users');

                Route::post('update', 'UsersController@update')->name('admin.users.update');
                Route::post('delete-document', 'UsersController@deleteDocument');
                Route::delete('delete/{id}', 'UsersController@delete')->name('admin.delete.user');

                Route::group(['prefix' => 'modal'], function () {
                    Route::get('transactions', 'UsersController@transactionsModal');
                    Route::get('edit', 'UsersController@editModal');
                });
            });

            Route::group(['prefix' => 'money'], function () {
                Route::get('/deposits', 'MoneyController@deposits')->name('admin.money.deposits');
                Route::post('/deposit/confirm', 'MoneyController@confirm')->name('admin.money.deposit.confirm');
                Route::post('/deposit/cancel', 'MoneyController@cancel')->name('admin.money.deposit.cancel');

                Route::get('/withdrawals', 'MoneyController@withdrawals')->name('admin.money.withdrawals');
                Route::post('/withdrawal/confirm', 'MoneyController@confirm')->name('admin.money.withdrawal.confirm');
                Route::post('/withdrawal/cancel', 'MoneyController@cancel')->name('admin.money.withdrawal.cancel');

                Route::put('/mark/viewed', 'MoneyController@markAsViewed')->name('admin.money.mark.viewed');

                Route::post('/upload-proof-image/{id}', 'MoneyController@uploadProofImage')
                    ->name('admin.withdrawal.proof-image.modal');
            });

            Route::group(['prefix' => 'trades'], function () {
                Route::get('/{type}', 'TradesController@index')->name('admin.trades');
                Route::post('confirm', 'TradesController@confirm');
                Route::post('cancel', 'TradesController@cancel');
                Route::put('/mark/viewed', 'TradesController@markAsViewed')->name('admin.trades.mark.viewed');
            });

            Route::get('/faq', 'FaqController@index')->name('admin.faq');


            Route::group(['prefix' => 'support'], function () {
                Route::get('/', 'SupportController@index')->name('admin.support'); // Support Admin Page
                Route::put('/mark/viewed', 'SupportController@markAsViewed')->name('admin.support.mark.viewed');
                Route::get('/{id}', 'SupportController@adminChat')->name('adminchat'); // Support Admin Chat Page
                Route::post('/{id}', 'SupportController@addAdminMessage')->name('admin.chat'); // Support Admin Sent Message
                Route::delete('/delete/{id}', 'SupportController@delete')->name('admin.support.delete');
                Route::get('/edit-ticket/{id}', 'SupportController@editSupportModal')->name('admin.support.modal.edit');
                Route::put('/update/{id}', 'SupportController@update')->name('admin.support.update');
            });

            Route::group(['prefix' => 'pages'], function () {
                Route::get('/', 'PagesController@index')->name('admin.pages');
                Route::get('/add', 'PagesController@add')->name('admin.pages.add');
                Route::post('/store', 'PagesController@store')->name('admin.pages.store');
                Route::get('/edit/{id}', 'PagesController@edit')->name('admin.pages.edit');
                Route::put('/update/{id}', 'PagesController@update')->name('admin.pages.update');
                Route::delete('/delete/{id}', 'PagesController@delete')->name('admin.pages.delete');
            });

            Route::group(['prefix' => 'settings'], function () {
                Route::group(['prefix' => 'bitcoin'], function () {
                    Route::get('/', 'SettingsController@bitcoin')->name('admin.settings.bitcoin');
                    Route::post('save', 'SettingsController@saveBitcoin')->name('admin.settings.bitcoin.save');
                    Route::post('generate-addresses', 'SettingsController@generateBitcoinAddresses')->name('admin.settings.bitcoin.generate');
                });
                // for Ethereum
                Route::group(['prefix' => 'ethereum'], function () {
                    Route::get('/', 'SettingsController@ethereum')->name('admin.settings.ethereum');
                    Route::post('save', 'SettingsController@saveEthereum')->name('admin.settings.ethereum.save');
                    Route::post('generate-addresses', 'SettingsController@generateEthereumAddresses')->name('admin.settings.ethereum.generate');
                });
                Route::group(['prefix' => 'web'], function () {
                    Route::get('/', 'SettingsController@web')->name('admin.settings.web');
                    Route::post('save', 'SettingsController@saveWeb')->name('admin.settings.web.save');
                });

                Route::get('/plugins', 'SettingsController@plugins')->name('admin.settings.plugins');
                Route::put('/', 'SettingsController@updatePlugins')->name('admin.settings.plugins.update');
            });

            Route::group(['prefix' => 'modal'], function () {
                Route::get('/add-gateway', 'GatewaysController@addGatewayModal');
                Route::get('/add-faq', 'FaqController@addFaqModal');
                Route::get('/edit-gateway/{id}', 'GatewaysController@editGatewayModal')->name('admin.gateways.modal.edit');
                Route::get('/edit-faq/{id}', 'FaqController@editFaqModal')->name('admin.faqs.modal.edit');

                Route::get('/trades/details', 'TradesController@detailsModal')->name('admin.trades.modal.details');

                Route::group(['prefix' => 'money'], function () {
                    Route::get('/deposit-details/{id}', 'MoneyController@depositsDetailsModal')
                        ->name('admin.deposit.details.modal');
                    Route::get('/withdrawal-details/{id}', 'MoneyController@withdrawalsDetailsModal')
                        ->name('admin.withdrawal.details.modal');
                    Route::get('/upload-proof-image/{id}', 'MoneyController@uploadProofImageModal')
                        ->name('admin.withdrawal.proof-image.modal');
                });
            });
        });
    });
});

