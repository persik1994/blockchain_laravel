<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('user.money.manage.{userId}', \App\Broadcasting\UserMoneyManageChannel::class);
Broadcast::channel('user.money.request', \App\Broadcasting\UserMoneyRequestChannel::class);

Broadcast::channel('user.order.request', \App\Broadcasting\UserOrderRequestChannel::class);
Broadcast::channel('user.order.manage.{userId}', \App\Broadcasting\UserOrderManageChannel::class);

Broadcast::channel('user.support.request', \App\Broadcasting\UserSupportRequestChannel::class);
Broadcast::channel('user.support.response.{ticketId}', \App\Broadcasting\UserSupportResponseChannel::class);
Broadcast::channel('unauthenticated.support.response.{ticketHash}', \App\Broadcasting\UnauthenticatedSupportResponseChannel::class);
Broadcast::channel('user.support.request.ticket', \App\Broadcasting\UserSupportTicketRequestChannel::class);

Broadcast::channel('user.transfer.request', \App\Broadcasting\UserTransferRequestChannel::class);
Broadcast::channel('user.transfer.response.{userId}', \App\Broadcasting\UserTransferResponseChannel::class);
