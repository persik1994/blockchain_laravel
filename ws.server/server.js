// https://www.youtube.com/watch?v=f59ddeP8qmg&t=213s
// var io = require('socket.io')(6001),
// 	Redis = require('ioredis'),
// 	redis = new Redis();

// redis.psubscribe("*", function(error, count) {
// 	// ...
// });

// redis.on("pmessage", function(pattern, channel, message) {

// 	message = JSON.parse(message);
// 	io.emit(channel + ':' + message.event, message.data.message);
// 	//channel:message.event
	
// });

var io = require('socket.io')(6001);
io.on('connection', function(socket) {
	console.log('New connection:', socket.id);
	socket.send('Message from server');
});