<?php

use Illuminate\Database\Seeder;
use App\Models\Gateway;

class GatwaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gateways = [
            [
                'name' => 'paypal',
                'currency' => 'USD',
                'allow' => 1,
                'gateway_information' => json_encode([
                    'playpal_account' => '4032036812536694'
                ])
            ],
            [
                'name' => 'credit_card',
                'currency' => 'USD',
                'allow' => 1,
                'gateway_information' => json_encode([
                    'credit_card_number' => '4012888888881881'
                ])
            ]
        ];

        foreach ($gateways as $gateway) {
            Gateway::create($gateway);
        }
    }
}
