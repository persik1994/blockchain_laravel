<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'title' => 'Terms of service',
                'prefix' => 'terms-of-services',
                'content' => 'Edit Terms of service from WebAdmin.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Privacy Policy',
                'prefix' => 'privacy-policy',
                'content' => 'Edit Privacy policy from WebAdmin.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        foreach ($pages as $page) {
            $pageEloquent = new Page();
            $pageEloquent->title = $page['title'];
            $pageEloquent->prefix = $page['prefix'];
            $pageEloquent->content = $page['content'];
            $pageEloquent->created_at = $page['created_at'];
            $pageEloquent->updated_at = $page['updated_at'];
            $pageEloquent->save();
        }
    }
}
