<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use \Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    use \Denpa\Bitcoin\Traits\Bitcoind;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Administrator',
                'email' => 'admin@test.com',
                'email_verified_at' => date('Y-m-d'),
                'password' => Hash::make('admin123'),
                'remember_token' => NULL,
                'secret_pin' => Hash::make('11111111'),
                'status' => 1,
                'balance' => 0.00,
                'crypto_currency' => 'BTC',
                'fiat_currency' => 'USD',
                'btc_balance' => 0.00000000,
                'eth_balance' => 0.000000000000000000,
                'ip' => NULL,
                'time_signin' => NULL,
                'time_activity' => NULL,
                'document_verified' => NULL,
                'document_1' => NULL,
                'document_2' => NULL,
                'mobile_verified' => NULL,
                'mobile_number' => NULL
            ],
            [
                'name' => 'Jhon Smith',
                'email' => 'smith@test.com',
                'email_verified_at' => date('Y-m-d'),
                'password' => Hash::make('smith123'),
                'remember_token' => NULL,
                'secret_pin' => Hash::make('22222222'),
                'status' => 2,
                'balance' => 0.00,
                'crypto_currency' => 'BTC',
                'fiat_currency' => 'USD',
                'btc_balance' => 0.00000000,
                'eth_balance' => 0.000000000000000000,
                'ip' => NULL,
                'time_signin' => NULL,
                'time_activity' => NULL,
                'document_verified' => NULL,
                'document_1' => NULL,
                'document_2' => NULL,
                'mobile_verified' => NULL,
                'mobile_number' => NULL
            ]
        ];

        foreach ($users as $user) {
            $user = User::create($user);

            $userCryptoAddresses['BTC'] = $this->bitcoind()->getaddressesbyaccount($user->email)->get();

            foreach ($userCryptoAddresses['BTC'] as $key => $userCryptoAddress) {
                $userAddress = new \App\Models\UserAddresses();
                $userAddress->uid = $user->id;
                $userAddress->label = 'Address ' . $key;
                $userAddress->crypto_currency = 'BTC';
                $userAddress->address = $userCryptoAddress;
                $userAddress->created = \DB::raw('NOW()');
                $userAddress->updated = \DB::raw('NOW()');
                $userAddress->save();
            }
        }

        $admin = User::where('email', 'admin@test.com')->first();
        $admin->type = 'admin';
        $admin->save();
    }
}
