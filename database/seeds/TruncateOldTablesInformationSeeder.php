<?php

use Illuminate\Database\Seeder;

class TruncateOldTablesInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('gateways')->truncate();
        \DB::table('faq')->truncate();
        \DB::table('orders')->truncate();
        \DB::table('sms_codes')->truncate();
       /* \DB::table('requests')->truncate(); */
        \DB::table('transfers')->truncate();
        \DB::table('users')->truncate();
        \DB::table('users_addresses')->truncate();
        \DB::table('users_bank_account')->truncate();
        \DB::table('users_money')->truncate();
        \DB::table('users_login_history')->truncate();
        \DB::table('users_transactions')->truncate();
        \DB::table('pages')->truncate();
        \DB::table('support_tickets')->truncate();
        \DB::table('support_chat')->truncate();
    }
}
