<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveLicenseStatusFromAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->dropColumn(['status', 'lid', 'created', 'updated', 'available_balance', 'pending_received_balance', 'archived']);
        });

        Schema::table('users_addresses', function (Blueprint $table) {
            $table->timestamp('created');
            $table->timestamp('updated');
            $table->decimal('available_balance', 30, 18)->default(0);
            $table->decimal('pending_received_balance', 30, 18)->default(0);
            $table->boolean('archived')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->dropColumn(['created', 'updated', 'available_balance', 'pending_received_balance', 'archived']);
        });

        Schema::table('users_addresses', function (Blueprint $table) {
            $table->integer('created');
            $table->integer('updated');
            $table->string('available_balance')->nullable()->default(null);
            $table->string('pending_received_balance')->nullable()->default(null);
            $table->integer('archived');
            $table->string('status')->after('pending_received_balance');
            $table->integer('lid')->after('address');
        });
    }
}
