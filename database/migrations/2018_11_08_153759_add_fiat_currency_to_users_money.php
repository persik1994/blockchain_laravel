<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiatCurrencyToUsersMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->char('user_fiat_currency', 3)->nullable()->after('current_amount');
            $table->decimal('user_amount', 12, 2)->nullable()->after('user_fiat_currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('user_fiat_currency', 'user_amount');
        });
    }
}
