<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusColumnForUserMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('users_money', function (Blueprint $table) {
            $table->enum('status', ['waiting', 'confirmed', 'canceled'])->after('transaction_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('users_money', function (Blueprint $table) {
            $table->boolean('status')->default(1)->after('transaction_type');
        });
    }
}
