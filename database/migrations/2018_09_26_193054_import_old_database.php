<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportOldDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $oldDatabaseContent = file_get_contents('./storage/common/wallet-dump.sql');

        \DB::unprepared($oldDatabaseContent);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP TABLE transfers, sms_codes, settings, requests, pages, orders, gateways, faq, blockio_licenses");
    }
}
