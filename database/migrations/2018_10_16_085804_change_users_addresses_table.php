<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->dropColumn(['available_balance', 'pending_received_balance', 'archived', 'created', 'updated']);
        });

        Schema::table('users_addresses', function (Blueprint $table) {
            $table->string('crypto_currency', 4)->after('uid');
            $table->softDeletes();
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->dropColumn(['deleted_at', 'crypto_currency']);
        });

        Schema::table('users_addresses', function (Blueprint $table) {
            $table->decimal('available_balance', 30, 18)->default(0);
            $table->decimal('pending_received_balance', 30, 18)->default(0);
            $table->boolean('archived')->default(0);
        });
    }
}
