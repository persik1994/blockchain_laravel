<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForEthereumInSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->boolean('autoupdate_ethereum_price')->default(0)->after('ethereum_gas_limit');
            $table->decimal('ethereum_fixed_price', 12, 2)->after('autoupdate_ethereum_price');
            $table->integer('ethereum_buy_fee')->after('ethereum_fixed_price');
            $table->integer('ethereum_sell_fee')->after('ethereum_buy_fee');
            $table->string('eth_fee_address')->after('ethereum_sell_fee');
            $table->string('eth_buy_address')->after('eth_fee_address');
            $table->string('eth_sell_address')->after('eth_buy_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn(
                'autoupdate_ethereum_price',
                'ethereum_fixed_price',
                'ethereum_buy_fee',
                'ethereum_sell_fee',
                'eth_fee_address',
                'eth_buy_address',
                'eth_sell_address'
            );
        });
    }
}
