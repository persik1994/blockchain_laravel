<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn([
                'u_field_1', 'u_field_2', 'u_field_3',
                'u_field_4', 'u_field_5', 'u_field_6',
                'u_field_7', 'u_field_8', 'u_field_9',
                'u_field_10', 'attachment', 'gateway_id',
                'type', 'from_address', 'bitcoins_released'
            ]);
        });

        Schema::table('requests', function (Blueprint $table) {
            $table->enum('type', ['buy', 'sell'])->after('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn(['type']);
        });

        Schema::table('requests', function (Blueprint $table) {
            $userColumns = [
                'u_field_1', 'u_field_2', 'u_field_3',
                'u_field_4', 'u_field_5', 'u_field_6',
                'u_field_7', 'u_field_8', 'u_field_9',
                'u_field_10'
            ];

            foreach ($userColumns as $column) {
                $table->string($column)->nullable();
            }

            $table->text('attachment')->nullable();
            $table->integer('gateway_id')->nullable();
            $table->integer('from_address')->nullable();
            $table->integer('bitcoins_released')->nullable();
            $table->integer('type');
        });
    }
}
