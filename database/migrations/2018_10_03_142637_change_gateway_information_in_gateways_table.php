<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGatewayInformationInGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gateways', function (Blueprint $table) {
            $table->dropColumn([
                'a_field_1', 'a_field_2', 'a_field_3',
                'a_field_4', 'a_field_5', 'a_field_6',
                'a_field_7', 'a_field_8', 'a_field_9',
                'a_field_10'
            ]);

            $table->text('gateway_information')
                ->default(null)
                ->after('allow_receive')
                ->comment('Information about for bank data or for service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gateways', function (Blueprint $table) {
            for($i = 1; $i <= 10; $i++) {
                $table->string('a_field_' . $i)->after('allow_receive');
            }

            $table->dropColumn('gateway_information');
        });
    }
}
