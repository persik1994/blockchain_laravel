<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('viewed');
        });

        Schema::table('users_money', function (Blueprint $table) {
            $table->boolean('viewed_by_admin')->after('time')->default(0);
            $table->boolean('viewed_by_owner')->after('viewed_by_admin')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('viewed_by_admin');
            $table->dropColumn('viewed_by_owner');
        });

        Schema::table('users_money', function (Blueprint $table) {
            $table->boolean('viewed')->after('time')->default(0);
        });
    }
}
