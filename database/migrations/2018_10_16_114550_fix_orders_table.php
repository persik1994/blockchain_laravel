<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders');

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('uid');
            $table->enum('strategy_type', ['market', 'limit', 'stop']);
            $table->enum('order_type', ['buy', 'sell']);
            $table->enum('status', ['waiting', 'confirmed', 'canceled']);
            $table->char('crypto_currency', 3);
            $table->decimal('crypto_amount', 30, 18)->default(0);
            $table->decimal('fiat_amount', 10, 2)->default(0);
            $table->decimal('target_amount', 30, 18)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('uid');
            $table->enum('order_type', ['market', 'limit', 'stop'])->default('market');
            $table->enum('transaction_type', ['sell_btc', 'buy_btc']);
            $table->tinyInteger('status');
            $table->decimal('amount', 20, 8);
            $table->decimal('target_price', 20, 8);
            $table->integer('time');
        });
    }
}
