<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusTxidOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE orders MODIFY status enum('waiting', 'processing', 'confirmed', 'canceled');");

        Schema::table('orders', function (Blueprint $table) {
            $table->text('txid')->after('target_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE orders MODIFY status enum('waiting', 'confirmed', 'canceled');");

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('txid');
        });
    }
}
