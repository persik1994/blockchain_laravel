<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->default('1');
            $table->integer('uid')->nullable();
            $table->string('title')->nullable();    
            $table->string('description')->nullable();
            $table->enum('status', ['opened', 'closed'])->default('opened');
            $table->string('email_address')->nullable();
            $table->string('ticket_hash')->nullable();  
            $table->tinyInteger('viewed_by_admin')->default('0');
            $table->tinyInteger('viewed_by_owner')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
