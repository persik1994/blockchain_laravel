<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledViewedToUsersMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_money', function (Blueprint $table) {

            //TODO will need to remove then will be implemented history functional for history table
            $table->boolean('viewed')->default(0)->after('time')->comment('If row was viewed by admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('viewed');
        });
    }
}
