<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllowFieldsInGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gateways', function (Blueprint $table) {
            $table->dropColumn('allow_send', 'allow_receive');
        });

        Schema::table('gateways', function (Blueprint $table) {
            $table->boolean('allow')->default(0)->after('currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gateways', function (Blueprint $table) {
            $table->dropColumn('allow');
        });

        Schema::table('gateways', function (Blueprint $table) {
            $table->boolean('allow_send')->default(0)->after('currency');
            $table->boolean('allow_receive')->default(0)->after('currency');
        });
    }
}
