<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldFieldsFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email_hash', 'time_signin', 'time_activity');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('time_signin')->nullable()->after('mobile_number');
            $table->timestamp('time_activity')->nullable()->after('time_signin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('time_signin', 'time_activity');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->text('email_hash');
            $table->integer('time_signin');
            $table->integer('time_activity');
        });
    }
}
