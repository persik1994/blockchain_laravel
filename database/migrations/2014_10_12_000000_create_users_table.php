<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('secret_pin');
            $table->text('email_hash');
            $table->string('status');
            $table->unsignedDecimal('btc_balance', 14, 8);
            $table->string('ip')->nullable();
            $table->unsignedInteger('time_signin')->nullable();
            $table->unsignedInteger('time_activity')->nullable();
            $table->unsignedInteger('document_verified')->nullable();
            $table->text('document_1')->nullable();
            $table->text('document_2')->nullable();
            $table->unsignedInteger('mobile_verified')->nullable();
            $table->text('mobile_number')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
