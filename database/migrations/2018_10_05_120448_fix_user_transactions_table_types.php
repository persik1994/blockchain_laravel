<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixUserTransactionsTableTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_transactions', function (Blueprint $table) {
            $table->dropColumn('amount', 'confirmations', 'time');
        });

        Schema::table('users_transactions', function (Blueprint $table) {
            $table->decimal('amount', 30, 18)->after('sender');
            $table->dateTime('time')->after('amount');
            $table->integer('confirmations', false, true)->after('time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_transactions', function (Blueprint $table) {
            $table->dropColumn('amount', 'confirmations', 'time');
        });

        Schema::table('users_transactions', function (Blueprint $table) {
            $table->string('amount')->after('sender');
            $table->integer('time')->after('amount');
            $table->integer('confirmations')->after('time');
        });
    }
}
