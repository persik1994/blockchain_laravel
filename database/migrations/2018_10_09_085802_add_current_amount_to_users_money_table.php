<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentAmountToUsersMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->decimal('current_amount', 12, 2)
                ->after('amount')
                ->nullable()
                ->comment('Ammount by currency gateway chosen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_money', function (Blueprint $table) {
            $table->dropColumn('current_amount');
        });
    }
}
