<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldIndexesInDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $needIndexTables = [
            'faq', 'gateways', 'orders',
            'pages', 'requests',
            'settings', 'sms_codes', 'transfers'
        ];

        foreach ($needIndexTables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('id');
            });

            Schema::table($table, function (Blueprint $table) {
                $table->increments('id')->first();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $needIndexTables = [
            'faq', 'gateways', 'orders',
            'pages', 'requests',
            'settings', 'sms_codes', 'transfers'
        ];

        foreach ($needIndexTables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('id');
            });

            Schema::table($table, function (Blueprint $table) {
                $table->integer('id')->first();
            });
        }
    }
}
