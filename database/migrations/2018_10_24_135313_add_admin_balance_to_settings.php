<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminBalanceToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->decimal('balance', 12, 2)->default(0)->after('url');
            $table->decimal('btc_balance', 14, 8)->default(0)->after('balance');
            $table->decimal('eth_balance', 24, 18)->default(0)->after('btc_balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('balance', 'btc_balance', 'eth_balance');
        });
    }
}
