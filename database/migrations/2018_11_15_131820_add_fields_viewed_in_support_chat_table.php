<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsViewedInSupportChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('support_chat', function (Blueprint $table) {
            $table->tinyInteger('viewed_by_admin')->default('0')->after('message');
            $table->tinyInteger('viewed_by_owner')->default('0')->after('viewed_by_admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('support_chat', function (Blueprint $table) {
            $table->dropColumn('viewed_by_admin', 'viewed_by_owner');
        });
    }
}
