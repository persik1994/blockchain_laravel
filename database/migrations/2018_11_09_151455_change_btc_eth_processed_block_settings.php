<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBtcEthProcessedBlockSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('eth_processed_block');
            $table->dropColumn('btc_processed_block');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->text('eth_processed_block')->after('eth_balance');
            $table->text('btc_processed_block')->after('eth_processed_block');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('eth_processed_block');
            $table->dropColumn('btc_processed_block');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->unsignedInteger('eth_processed_block')->after('eth_balance');
            $table->unsignedInteger('btc_processed_block')->after('eth_processed_block');
        });
    }
}
