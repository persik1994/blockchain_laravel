<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBlockioLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('blockio_licenses');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('blockio_licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account');
            $table->string('license');
            $table->string('secret_pin');
            $table->string('address');
            $table->integer('addresses');
            $table->integer('default_license');
        });
    }
}
