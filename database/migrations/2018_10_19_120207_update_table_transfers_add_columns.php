<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTransfersAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function(Blueprint $table) {
            $table->dropColumn('btc_amount');            
        });
        Schema::table('transfers', function (Blueprint $table) {
            $table->decimal('crypto_amount', 30, 18);
            $table->char('crypto_currency', 3);
        });               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropColumn(['crypto_amount', 'crypto_currency']);
        });
        Schema::table('transfers', function (Blueprint $table) {
            $table->decimal('btc_amount', 14, 8);
        });        
    }
}
