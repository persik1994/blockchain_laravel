<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldAndTablesNotUsed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('history');
        Schema::dropIfExists('users_credit_cards');
        Schema::dropIfExists('users_data');

        Schema::table('gateways', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action');
            $table->unsignedInteger('owner');
            $table->text('details');
            $table->dateTime('time');
            $table->timestamps();
        });

        Schema::table('gateways', function (Blueprint $table) {
            $table->integer('status')->nullable();
        });
    }
}
