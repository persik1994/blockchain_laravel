<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminAddressesToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('btc_fee_address')->after('url');
            $table->string('btc_sell_address')->after('btc_fee_address');
            $table->string('btc_buy_address')->after('btc_sell_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('btc_fee_address', 'btc_sell_address', 'btc_buy_address');
        });
    }
}
