export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('verification', {
            sendDocuments: 'submit'
        });
    },
    actionSendEmailConfirmation: function($btn) {
        $btn.attr('disabled', true);
        $btn.find('i').removeClass('fa fa-reply').addClass('fas fa-spinner fa-spin');

        $.ajax({
            type: 'post',
            url: '/account/verification/send-email',
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            $btn.attr('disabled', false);
            $btn.find('i').removeClass('fas fa-spinner fa-spin').addClass('fa fa-check');
        });
    },
    actionSendDocuments: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $form[0].reset();
        }).fail(GSCommon.processFailResponse);
    },
    actionSavePhoneNumber: function ($btn) {
        $.ajax({
            type: 'post',
            url: '/account/verification/save-phone-number',
            data: {
                'phone-number': $('#mobile-number').val()
            },
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $('#change-phone-btn').show();
            $('#phone-verification-block').show();
            $('#save-phone-btn').hide();
            $('#mobile-number').prop('disabled', true);
        }).fail(GSCommon.processFailResponse);
    },
    actionSendSmsCode: function ($btn) {
        $.ajax({
            type: 'post',
            url: '/account/verification/send-sms-code',
            data: {
                'phone-number': $('#mobile-number').val()
            },
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse);
    },
    actionVerifySmsCode: function ($btn) {
        $.ajax({
            type: 'post',
            url: '/account/verification/verify-sms-code',
            data: {
                'sms-code': $('#sms-code').val()
            },
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $('#phone-verification-block').hide();
            $('#sms-code').val('');
        }).fail(GSCommon.processFailResponse);
    }
};