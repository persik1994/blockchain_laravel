export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('security', {
            changePassword: 'submit',
        });
    },
    actionChangePin: function () {
        let $form = $('#pin-form');
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $form[0].reset();
            GSCommon.clearFormErrors();
            $('.show-after-save-pin').show();
            $('.hide-after-save-pin').hide();
        }).fail(GSCommon.processFailFormResponse($form));
    },
    actionRemovePin: function () {
        let $form = $('#pin-form');
        $.ajax({
            type: 'post',
            url: '/account/security/remove-pin',
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $form[0].reset();
            GSCommon.clearFormErrors();
            $('.show-after-save-pin').hide();
            $('.hide-after-save-pin').show();
        }).fail(GSCommon.processFailFormResponse($form));
    }
};