export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('deposit', {
            selectPaymentMethod: 'change',
            convertToBitcoins: 'input',
            submitDeposit: 'submit'
        }); 

        $.ajax({
            url: '/account/info',
            dataType: 'json'
        }).done((response) => {
            if (response.data) {
                let depostiRowTemplateHtml = $('#template-deposit-row').html();
                let depostiRowTemplate = _.template(depostiRowTemplateHtml);

                Echo.private(`user.money.manage.` + response.data.id)
                    .listen('UserMoneyManageEvent', (e) => {
                        if (e.data) {
                            let data = e.data;

                            if (data.user && data.order && data.orders && data.order.transaction_type === 'deposit') {
                                let depositHistoryPanelElement = $('#deposit-history-panel');
                                if (!depositHistoryPanelElement.hasClass('active')) depositHistoryPanelElement.addClass('active');
                                depositHistoryPanelElement.find('.timeline').html('');

                                data.orders.forEach((element) => {
                                    depositHistoryPanelElement.find('.timeline').append(depostiRowTemplate({
                                        deposit: element,
                                        user: data.user
                                    }));
                                });

                                if (data.order.status === 'confirmed') {
                                    $.notify('Your deposit with id: ' + data.order.id + ' was ' + data.order.status, 'success');
                                } else {
                                    $.notify('Your deposit with id: ' + data.order.id + ' was ' + data.order.status);
                                }

                                $('.menu__item-info-money-deposit').text(data.unviewed_deposits);
                                if($('#user-fiat-balance .number').length) $('#user-fiat-balance .number').text(data.user.balance_number);
                            }
                        }
                    });
            }
        }).fail(GSCommon.processFailResponse);
    },
    actionSelectPaymentMethod: function($select) {
        let gatewayId = $select.val();

        if (gatewayId === '') {
            $('#amount-input').prop('disabled', true);
            return;
        }

        $.ajax({
            url: '/account/deposit-money/payment-data?gateway-id=' + gatewayId,
            dataType: 'json'
        }).done(function (response) {
            $('#amount-input').prop('disabled', false);
            $('#currency').html(response.currency);

            let gatewayFields = '';
            Object.keys(response.fields).forEach(function (element) {
                gatewayFields += `
                    <span>` + element + `: </span><span>` + response.fields[element] + `</span></br>
                `;
            });

            $('#btc_account_fields').html(gatewayFields);
        });
    },
    actionConvertToBitcoins: function($input) {
        let btcBuyPrice = parseFloat($input.data('btcBuyPrice')),
            amount = parseFloat($input.val());

        btcBuyPrice = btcBuyPrice ? btcBuyPrice : 0;
        amount = amount ? amount : 0;

        let btcValue = (btcBuyPrice === 0 ? 0 : (amount / btcBuyPrice)).toFixed(8);

        $('#btc-amount-receive').val(btcValue);
    },
    actionSubmitDeposit: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            BootstrapDialog.show({
                message: response.message,
                type: BootstrapDialog.TYPE_SUCCESS
            });
            $form[0].reset();
            $('#amount-input').prop('disabled', true);
        })
        .fail(GSCommon.processFailFormResponse())
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    }
};