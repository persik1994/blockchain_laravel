export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('withdraw', {
            sendRequest: 'submit'
        });

        $.ajax({
            url: '/account/info',
            dataType: 'json'
        }).done((response) => {
            if (response.data) {
                let withdrawRowTemplateHtml = $('#template-withdraw-row').html();
                let withdrawRowTemplate = _.template(withdrawRowTemplateHtml);

                Echo.private(`user.money.manage.` + response.data.id)
                    .listen('UserMoneyManageEvent', (e) => {
                        if (e.data) {
                            let data = e.data;

                            if (data.user && data.order && data.orders && data.order.transaction_type === 'withdraw') {
                                let withdrawHistoryPanelElement = $('#withdraw-history-panel');

                                if (!withdrawHistoryPanelElement.hasClass('active')) withdrawHistoryPanelElement.addClass('active');

                                withdrawHistoryPanelElement.find('.timeline').html('');

                                console.log(data.orders);

                                data.orders.forEach((element, index) => {
                                    withdrawHistoryPanelElement.find('.timeline').append(withdrawRowTemplate({
                                        withdraw: element,
                                        user: data.user
                                    }));
                                });

                                if (data.order.status === 'confirmed') {
                                    $.notify('Your withdraw with id: ' + data.order.id + ' was ' + data.order.status, 'success');
                                    $('#current-balance').text(data.user_current_balance)
                                } else {
                                    $.notify('Your withdraw with id: ' + data.order.id + ' was ' + data.order.status);
                                }

                                console.log(data);

                                $('.menu__item-info-money-withdraw').text(data.unviewed_withdraws)
                            }
                        }
                    });
            }
        }).fail(GSCommon.processFailResponse);
    },
    actionSendRequest: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            BootstrapDialog.show({
                message: response.message,
                type: BootstrapDialog.TYPE_SUCCESS
            });
            $form[0].reset();
            GSCommon.clearFormErrors();
        })
        .fail(GSCommon.processFailFormResponse($form))
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    }
};