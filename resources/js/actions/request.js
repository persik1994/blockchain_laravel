export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('request', {
            send: 'submit'
        });
    },
    actionSend: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $('#success-message').html(response.message).show();
            $form[0].reset();
        }).fail(function (error) {
            let errorsList = error['responseJSON']['errors'];
            for (let _ in errorsList) {
                $.notify(errorsList[_], 'error');
            }
        });
    }
};