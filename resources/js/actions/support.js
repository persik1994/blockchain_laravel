export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('support', {
            'addTicket': 'submit',
            'addMessage': 'submit'
        });

        $.ajax({
            url: '/account/info/support',
            dataType: 'json'
        }).done((response) => {
            let ticketId = $('#ticket-chat').data('ticket-id');

            if (response.data) {
                Echo.private(`user.support.response.` + ticketId)
                    .listen('UserSupportResponseEvent', (e) => {
                        let messages = e.data.messages,
                            ticket = e.data.ticket;

                        this.refreshMessages(messages, ticket);

                        $.notify('You have a new message from Support', 'info');
                    });

            } else {
                let ticketHash = $('input[name="ticket_hash"]').val();

                Echo.channel(`unauthenticated.support.response.` + ticketHash)
                .listen('UnauthenticatedSupportResponseEvent', (e) => {
                    let messages = e.data.messages,
                        ticket = e.data.ticket;

                    this.refreshMessages(messages, ticket);

                    $.notify('You have a new message from Support', 'info');
                });
            }
        });
    },
    actionAddTicket: function($form) {
        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse);
    },

    actionAddMessage: function($form) {
        let btnForm = $form.find('button[type="submit"]'),
            self = this;

        GSCommon.formSubmitStart($form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            let messages = response.messages,
                ticket = response.ticket;

            if (messages) {
                self.refreshMessages(messages, ticket);
            }

            $form[0].reset();
        })
        .fail(GSCommon.processFailFormResponse())
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },
    refreshMessages(messages, ticket) {
        $('.messages-wrapper').html('');
        messages.forEach((message) => {
            if (message.user) {
                let messageUser = message.user;

                if (messageUser.type === 'admin') {
                    $('.messages-wrapper').append(`<div class="pull-right message"><b>`+ message.user.name +`:</b> ` + message.message + ` </div><br>`)
                } else {
                    $('.messages-wrapper').append(`<div class="message"><b>`+ message.user.name +`:</b> ` + message.message + ` </div><br>`)
                }
            } else {
                $('.messages-wrapper').append(`<div class="message"><b>`+ ticket.email_address +`:</b> ` + message.message + ` </div><br>`)
            }
        });
    }
};