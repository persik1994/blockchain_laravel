export default {
    init: function () {
        // Inherit GSCommon object
        GSCommon.makeParentOf(this);

        // Init listeners for all methods which begins with 'action'
        this.initListeners('user');

        /*
        'initListeners' method takes as first param namespace
        and can have second optional param 'exceptions', for ex.

        this.initListeners('user', {
            register: 'submit'
        });

        second param 'exceptions' is an object with `key` = action name, and `value` = event name (default event is click)

        OR, you also can use this format to indicate 'preventDefault' value

        this.initListeners('user', {
            register: {eventName: 'submit', preventDefault: false}
        });


        */
    },
    actionSignIn: function ($btn, e) {
        console.log($btn);
        console.log(e);
    },
    actionOpenModal: function ($btn, e) {
        // $btn - current clicked element
        // e - event object
        console.log($btn);
        console.log(e);
    },
    actionRegister: function ($btn, e) {
        // $btn - current clicked element
        // e - event object
        console.log($btn);
        console.log(e);
    }
};

/*
* In Html you can use like this

<button class=js-user-signIn>Test button</button>
<button class=js-user-openModal>Test button</button>

* where:
 * user - namespace indicated here "this.initListeners('user');"
 * openModal - action name (method which begins with 'action')

*/