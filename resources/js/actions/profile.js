export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('profile', {
            changePassword: 'submit',
            update: 'submit'
        });
    },

    actionChangePassword: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            $form[0].reset();
            GSCommon.clearFormErrors();
        }).fail(GSCommon.processFailFormResponse($form));
    },

    actionUpdate: function ($form) {
        let formData = new FormData($form[0]);

        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            GSCommon.clearFormErrors();
        }).fail(GSCommon.processFailFormResponse($form));
    }
};