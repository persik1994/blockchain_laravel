export default {
    selectedOrderType: 'buy',
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('order', {
            sendRequest: 'submit',
            convertToBitcoins: 'input'
        });

        $.ajax({
            url: '/account/info',
            dataType: 'json'
        }).done((response) => {
            if (response.data) {
                let rowTemplateHtml = $('#template-order-row').html();
                let rowTemplate = _.template(rowTemplateHtml);

                Echo.private(`user.order.manage.` + response.data.id)
                    .listen('UserOrderManageEvent', (e) => {
                        if (e.data) {
                            let data = e.data;

                            if (data.user && data.order && data.orders) {
                                let depositHistoryPanelElement = $('#deposit-history-panel');

                                // if (!depositHistoryPanelElement.hasClass('active')) depositHistoryPanelElement.addClass('active');

                                depositHistoryPanelElement.find('.timeline').html('');

                                console.log(data, data.orders);

                                data.orders.forEach((element, index) => {

                                    depositHistoryPanelElement.find('.timeline').append(rowTemplate({
                                        order: element,
                                        user: data.user
                                    }));
                                });

                                if (data.order.status === 'confirmed') {
                                    $.notify('Your deposit with id: ' + data.order.id + ' was ' + data.order.status, 'success');
                                } else {
                                    $.notify('Your deposit with id: ' + data.order.id + ' was ' + data.order.status);
                                }

                                $('.navbar-notifications-count__info-' + data.order.order_type)
                                    .text(data['unviewed_' + data.order.order_type + 's']);
                            }
                        }
                    });
            }
        }).fail(GSCommon.processFailResponse);
    },
    actionConvertToBitcoins: function($input) {
        let orderInfoWrapper = $('.order-type-wrapper'),
            amountReceive = $('#amount-receive'),
            feeValue = $('#fee-value'),
            userCryptoCurrency = orderInfoWrapper.data('user-crypto-currency'),
            userFiatCurrency = orderInfoWrapper.data('user-fiat-currency');

        if (this.selectedOrderType === 'buy') {
           let cryptoBuyPrice = $('input[name="buy-price-' + userCryptoCurrency + '-' + userFiatCurrency + '"]').val().toString().trim();
           cryptoBuyPrice = cryptoBuyPrice === '' ? 0 : parseFloat(cryptoBuyPrice);

           let cryptoBuyFee = $('input[name="buy-fee-' + userCryptoCurrency + '"]').val().toString().trim();
           cryptoBuyFee = cryptoBuyFee === '' ? 0 : parseFloat(cryptoBuyFee);

           let amount = $input.val().toString().trim();
           amount = amount === '' ? 0 : parseFloat(amount);

           let amountReceiveValue = cryptoBuyPrice === 0 ? 0 : (amount / cryptoBuyPrice);
           amountReceiveValue = GSCommon.convertToCrypto(amountReceiveValue, userCryptoCurrency);

           let amountFeeValue = cryptoBuyFee === 0 ? 0 : ((amount * Math.abs(cryptoBuyFee)) / 100);

           amountReceive.val(amountReceiveValue);
           feeValue.text(amountFeeValue);
        }

        if (this.selectedOrderType === 'sell') {
           let cryptoSellPrice = $('input[name="sell-price-' + userCryptoCurrency + '-' + userFiatCurrency + '"]').val().toString().trim();
           cryptoSellPrice = cryptoSellPrice === '' ? 0 : parseFloat(cryptoSellPrice);

           let cryptoSellFee = $('input[name="sell-fee-' + userCryptoCurrency + '"]').val().toString().trim();
           cryptoSellFee = cryptoSellFee === '' ? 0 : parseFloat(cryptoSellFee);

           let amount = $input.val().toString().trim();
           amount = amount === '' ? 0 : parseFloat(amount);

           let amountReceiveValue = cryptoSellPrice === 0 ? 0 : (amount * cryptoSellPrice);
           amountReceiveValue = amountReceiveValue.toFixed(2);

           let amountFeeValue = cryptoSellFee === 0 ? 0 : ((amount * Math.abs(cryptoSellFee)) / 100) * cryptoSellPrice;

           amountReceive.val(amountReceiveValue);
           feeValue.text(amountFeeValue.toFixed(2));
        }
    },
    actionSendRequest: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            BootstrapDialog.show({
                message: response.message,
                type: BootstrapDialog.TYPE_SUCCESS
            });
            $form[0].reset();
            $('#fee-value').text('0');
        }).fail(GSCommon.processFailResponse);
    },
    actionChangeOrderType: function($btn) {
        let userCryptoCurrency = $('.order-type-wrapper').data('user-crypto-currency'),
            userFiatCurrenncy = $('.order-type-wrapper').data('user-fiat-currency');

        this.selectedOrderType = $btn.data('type');
        $('#order-type-input').val(this.selectedOrderType);

        switch (this.selectedOrderType) {
            case 'buy':
                $('#place').text('PLACE BUY ORDER').addClass('qx').removeClass('qy');
                $('.kvYUlk').addClass('qx');
                $('.jHbvLz').removeClass('qy');
                $('#f3').text(userFiatCurrenncy);
                // $('#fee-value').text($('#buy-fee').val());
                $('#f2').text('Total (' + userCryptoCurrency + ') ≈');

                $('#amount-receive').val(GSCommon.convertToCrypto(0, userCryptoCurrency));
                break;
            case 'sell':
                $('#place').text('PLACE SELL ORDER').addClass('qy').removeClass('qx');
                $('.jHbvLz').addClass('qy');
                $('.kvYUlk').removeClass('qx');
                $('#f3').text(userCryptoCurrency);

                $('#f2').text('Total (' + userFiatCurrenncy + ') ≈');
                // $('#fee-value').text($('#sell-fee').val());
                $('#amount-receive').val('0.00');
                break;
        }

        $('#fee-value').text('0');
        $('#amount-input').val('');
    },
    actionChangeStrategyType: function($btn) {
        let strategy = $btn.data('type');
        $('#strategy-type-input').val(strategy);

        $('.strategy-type-item').removeClass('zx');
        $btn.addClass('zx');

        switch (strategy) {
            case 'market':
                $('#stop-price-field, #limit-price-field').hide();

                if(this.selectedOrderType === 'buy') {
                    $('#f3').text(GSCommon.fiatCurrency);
                } else if(this.selectedOrderType === 'sell') {
                    $('#f3').text(GSCommon.cryptoCurrency);
                }


                $('#f1').text('Fee (' + GSCommon.fiatCurrency + ') ≈');

                if(this.selectedOrderType === 'buy') {
                    $('#f2').text('Total (' + GSCommon.cryptoCurrency + ') ≈');
                } else {
                    $('#f2').text('Total (' + GSCommon.fiatCurrency + ') ≈');
                }
                break;
            case 'limit':
                $('#limit-price-field').show();
                $('#stop-price-field').hide();

                $('#f3').text(GSCommon.cryptoCurrency);
                $('#f5').text(GSCommon.fiatCurrency);

                $('#f1').text('Fee (' + GSCommon.fiatCurrency + ') ≈');
                $('#f2').text('Total (' + GSCommon.fiatCurrency + ') ≈');
                break;
            case 'stop':
                $('#limit-price-field').hide();
                $('#stop-price-field').show();

                $('#f3').text(GSCommon.fiatCurrency);
                $('#f4').text(GSCommon.fiatCurrency);

                if(this.selectedOrderType === 'sell') {
                    $('#f3').text(GSCommon.cryptoCurrency);
                }

                $('#f1').text('Fee (' + GSCommon.fiatCurrency + ') ≈');

                if(this.selectedOrderType === 'buy') {
                    $('#f2').text('Total (' + GSCommon.cryptoCurrency + ') ≈');
                } else {
                    $('#f2').text('Total (' + GSCommon.fiatCurrency + ') ≈');
                }
                break;
        }
    }
}