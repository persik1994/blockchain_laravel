export default {
    panelOpened: false,
    init: function () {
        GSCommon.makeParentOf(this);

        this.initListeners('account', {
            'openCurrencyPanel': 'mouseenter',
            'closeCurrencyPanel': 'mouseleave'
        });

        this.listenCurrencyPriceChange();
    },
    listenCurrencyPriceChange: function () {
        Echo.channel('update.crypto.currency.price').listen('UpdateCryptoCurrencyEvent', (e) => {
            let data = e.data;

            console.info('Update prices');

            $('.crt').each(function () {
                let $c = $(this),
                    d = $c.data();

                $c.html(data[d.type] + ' ' + d.suffix);
            });

            let orderInfo = data['order-info'];

            Object.keys(orderInfo).forEach((elementKey) => {
                let orderInfoElement = orderInfo[elementKey];

                Object.keys(orderInfoElement).forEach((orderInfoElementKey) => {
                    if (elementKey === 'buy-price' || elementKey === 'sell-price') {
                        let orderInfoPriceElement = orderInfoElement[orderInfoElementKey];

                        Object.keys(orderInfoPriceElement).forEach((orderInfoPriceElementKey) => {
                            $('.order-info')
                                .find('input[name="' + elementKey + '-' + orderInfoElementKey + '-' + orderInfoPriceElementKey + '"]')
                                .val(orderInfoPriceElement[orderInfoPriceElementKey]);
                        });
                    } else {
                        $('.order-info')
                            .find('input[name="' + elementKey + '-' + orderInfoElementKey +'"]')
                            .val(orderInfoElement[orderInfoElementKey]);
                    }
                });
            });
        });
    },
    actionSelectCurrency: function ($btn, e) {
        let tradeKeyInformationHtml = $btn.find('.fiat-price')[0].outerHTML + $btn.find('.status-level')[0].outerHTML,
            currenciesBtn = $btn.find('.x2'),
            tradeKeyHtml = $btn.find('.x1')[0].outerHTML + currenciesBtn[0].outerHTML,
            orderInfoWrapper = $('.order-type-wrapper'),
            userCryptoCurrency = orderInfoWrapper.data('user-crypto-currency'),
            userFiatCurrency = orderInfoWrapper.data('user-fiat-currency');

        $("#maincrypt").html(tradeKeyInformationHtml);
        $("#zzz .current-currency").html(tradeKeyHtml);

        this.actionCloseCurrencyPanel();

        let currentValue = $("#maincrypt span:nth-child(2)").html();
        let word = currentValue.split("-");

        GSCommon.fiatCurrency = word[1];
        GSCommon.cryptoCurrency = word[0];

        $('#f1').text('Fee ' + '(' + userFiatCurrency + ') ' + '≈');
        $('#f2').text('Total ' + '(' + userCryptoCurrency + ') ' + '≈');
        $("#f3").text(userFiatCurrency);

        let currenciesBtnURL = $btn.data('url');

        if (currenciesBtnURL) {
            window.location.href = currenciesBtnURL;
        } else {
            $.ajax({
                url: '/account/wallet/update-user-currencies',
                type: 'PUT',
                data: {
                    'crypto_currency': currenciesBtn.data('crypto-currency'),
                    'fiat_currency': currenciesBtn.data('fiat-currency')
                },
                dataType: 'json'
            }).done(function (response) {
                window.location.reload();
            }).fail(GSCommon.processFailResponse);
        }
    },
    actionOpenCurrencyPanel: function ($btn, $e) {
        if (this.panelOpened == false) {
            var self = this;
            $(".descr").slideDown("fast", function () {
                self.panelOpened = true
            });
            console.log('Open');
        }

    },
    actionCloseCurrencyPanel: function ($btn, $e) {
        if (this.panelOpened == true) {
            var self = this;

            $(".descr").hide();

            this.panelOpened = false;
            console.log('Close');
        }
    },
    // actionMenuItemClass: function ($btn, $e) {
    //     let angleUpIconExist = $('.fa-angle-up').length;
    //
    //     if ($('.fa-angle-down').length) {
    //         $('.fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-up');
    //     }
    //     $(".sc-cCVOAp").slideToggle("slow", function () {
    //         if (angleUpIconExist) {
    //             $('.fa-angle-up').removeClass('fa-angle-up').addClass('fa-angle-down');
    //         }
    //     });
    // }
};