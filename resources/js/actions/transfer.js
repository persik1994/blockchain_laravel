export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('transfer', {
            sendRequest: 'submit',
        });

        $.ajax({
            url: '/account/info',
            dataType: 'json'
        }).done((response) => {
            if (response.data) {
                let rowTemplateHtml = $('#template-transfer-row').html();
                let rowTemplate = _.template(rowTemplateHtml);

                console.log(response.data.id);

                Echo.private(`user.transfer.response.` + response.data.id)
                    .listen('UserTransferResponseEvent', (e) => {
                        if (e.data) {
                            let data = e.data;

                            if (data.transfer && data.transfers) {
                                let transferHistoryPanelElement = $('#transfer-history-panel');
                                if (!transferHistoryPanelElement.hasClass('active')) transferHistoryPanelElement.addClass('active');
                                transferHistoryPanelElement.find('.timeline').html('');

                                data.transfers.forEach((element) => {
                                    transferHistoryPanelElement.find('.timeline').append(rowTemplate({
                                        item: element
                                    }));
                                });

                                if (data.transfer.status === 1) {
                                    $.notify('Your deposit with id: ' + data.transfer.id + ' was confirmed', 'success');
                                } else {
                                    $.notify('Your deposit with id: ' + data.transfer.id + ' was canceled');
                                }

                                $('.navbar-notifications-count__info-transfer')
                                    .text(data['unviewed_transfers']);
                            }
                        }
                    });
            }
        }).fail(GSCommon.processFailResponse);
    },
    actionSendRequest: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            BootstrapDialog.show({
                message: response.message,
                type: BootstrapDialog.TYPE_SUCCESS
            });
            $form[0].reset();
            GSCommon.clearFormErrors();
        }).fail(GSCommon.processFailFormResponse($form))
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    }
};