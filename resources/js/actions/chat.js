export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('chat', {
            'addAdminMessage': 'submit',
            'addUserMessage': 'submit'
        }); 
    },
 
    actionAddAdminMessage: function($form) {
        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/support',  
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse);
    },
    actionAddUserMessage: function($form) {
        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/support', 
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse);
    }, 
};