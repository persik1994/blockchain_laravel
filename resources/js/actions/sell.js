export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('sell', {
            sendRequest: 'submit',
            orderTypeChange: 'change',
            convertToBitcoins: 'input',
            calculateStopAndLimit: 'input',
        });
    },
    actionOrderTypeChange: function($select) {
        let type = $select.val();

        switch (type) {
            case 'market':
                $('#stop-price-field, #limit-price-field').hide();
                break;
            case 'limit':
                $('#limit-price-field').show();
                $('#stop-price-field').hide();
                break;
            case 'stop':
                $('#limit-price-field').hide();
                $('#stop-price-field').show();
                break;
        }
    },
    actionConvertToBitcoins: function($input) {
        let btcSellPrice = $('#btc-sell-price').val().toString().trim();
        btcSellPrice = btcSellPrice === '' ? 0 : parseFloat(btcSellPrice);

        let amount = $input.val().toString().trim();
        amount = amount === '' ? 0 : parseFloat(amount);

        let bitcoins = btcSellPrice === 0 ? 0 : (amount * btcSellPrice);

        bitcoins = bitcoins.toFixed(6);

        $('#amount-receive').val(bitcoins);
    },
    actionCalculateStopAndLimit: function($input) {
        let $amountInput = $('#amount-input');

        let amount = $amountInput.val().toString().trim();
        amount = amount === '' ? 0 : parseFloat(amount);

        let currentValue = $input.val().toString().trim();
        currentValue = currentValue === '' ? 0 : parseFloat(currentValue);

        let bitcoins = currentValue === 0 ? 0 : (amount * currentValue);
        bitcoins = bitcoins.toFixed(6);

        $('#amount-receive').val(bitcoins);
    },
    actionSendRequest: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: new FormData($form[0]),
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (response) {
            $('#success-message').html(response.message).show();
            $form[0].reset();
        }).fail(function (error) {
            let errorsList = error['responseJSON']['errors'];
            for (let _ in errorsList) {
                $.notify(errorsList[_], 'error');
            }
        });
    }
};