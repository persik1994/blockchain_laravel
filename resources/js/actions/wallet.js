export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('wallet', {
            sendBitcoins: 'submit',
            createAddress: 'submit',
        });
    },
    actionCreateAddressModal: function ($btn) {
        this.modal.show({
            name: 'create-address-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/account/wallet/modal/create-address'
        });
    },
    actionCreateAddress: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            context: this
        }).done(function (response) {
            $.notify(response.message, 'success');
            GSCommon.modal.list['create-address-modal'].close();
            this.refreshWalletAddresses();
        }).fail(GSCommon.processFailFormResponse($form));
    }, 
    refreshWalletAddresses: function() {
        $.ajax({
            url: '/account/wallet/get-bitcoin-addresses',
            success: function (response) {
                $('#wallet-addresses').html(response);
            }
        });
    },
    actionOpenReceiveBitcoinsModal: function ($btn) {
        this.modal.show({
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/account/wallet/modal/receive-bitcoins',
            data: {
                address: $btn.data('address')
            }
        });
    },
    actionOpenNewTransactionModal: function ($btn) {
        this.modal.show({
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/account/wallet/modal/send-bitcoins'
        });
    },
    actionSendBitcoins: function ($form) {
        $.ajax({
            type: 'post',
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            BootstrapDialog.closeAll();
        }).fail(GSCommon.processFailFormResponse($form));
    },
    actionGenerateQRCode: function ($btn) {
        let amount = parseFloat($('#qr-code-amount').val());
        $('#qr-code-image').attr('src', 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=bitcoin:' + $btn.data('address') + '?amount=' + amount + '&choe=UTF-8').show();
    }
};