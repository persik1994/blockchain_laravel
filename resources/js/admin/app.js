
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require('./adminlte');

require('./actions/gateways').default.init();
require('./actions/transfer').default.init();
require('./actions/money').default.init();
require('./actions/trades').default.init();
require('./actions/faqs').default.init();
require('./actions/user').default.init();
require('./actions/settings').default.init();
require('./actions/tickets').default.init();
require('./actions/support').default.init();
require('./actions/adminbalance').default.init(); 


import tinymce from 'tinymce/tinymce'
import 'tinymce/themes/modern/theme'

import 'tinymce/plugins/paste/plugin'
import 'tinymce/plugins/link/plugin'
import 'tinymce/plugins/autoresize/plugin'
import 'tinymce/plugins/advlist/plugin'
import 'tinymce/plugins/anchor/plugin'
import 'tinymce/plugins/autolink/plugin'
import 'tinymce/plugins/autosave/plugin'
import 'tinymce/plugins/colorpicker/plugin'
import 'tinymce/plugins/bbcode/plugin'
import 'tinymce/plugins/emoticons/plugin'

$(document).on('click', '.delete-item', function () {
    let changeStatusBtn = $(this),
        route = $(this).data('url');

    changeStatusBtn.attr('disabled', true);

    BootstrapDialog.confirm({
        title: 'Are you sure ?',
        message: 'You will can not restore that item !',
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        btnCancelLabel: 'Cancel',
        closeByBackdrop: false,
        closeByKeyboard: false,
        btnOKLabel: 'Yes, remove',
        btnOKClass: 'btn-danger',
        callback: function(result) {
            if(result) {
                $.ajax({
                    type: 'DELETE',
                    url: route,
                    dataType: 'JSON'
                }).done(function(response) {
                    console.log(response);
                    $.notify(response.message, 'success');
                    window.location.reload();
                })
                .fail(GSCommon.processFailResponse);
            }
        },
        onhide: function(){
            changeStatusBtn.attr('disabled', false);
        }
    });
});

$(document).on('click', '.check-input', function () {
    if ($(this).hasClass('list-input') && !$('.list .row-item').length) {
        return;
    }

    if (!$(this).closest('div').find('input').is(':checked')){
        $(this).closest('div').find('input').attr('checked', true);
    } else {
        $(this).closest('div').find('input').attr('checked', false);
    }
});

$(document).on('click', '.list-input', function () {
    let checkboxList = $('.list').find('.check-input');

    if (checkboxList.length) {
        let checkListState = false;
        if (!$(this).closest('div').find('input').is(':checked')){
            checkListState = true;
        }

        checkboxList.each((index, element) => {
            $(element).find('input').attr('checked', !checkListState);
        });
    }

    GSCommon.checkForUnviewed();
});

$(document).on('click', '.row-input', function () {
    GSCommon.checkForUnviewed($(this));
});

$('.mark-selected-as-viewed').on('click', function () {
    let checkboxList = $('.list').find('.check-input input:checked');

    let checkedItemsIds = [];
    if (checkboxList.length) {
        checkboxList.each((index, element) => {
            checkedItemsIds.push($($(element).find('input').prevObject[0]).val());
        });
    }

    $(this).attr('disabled', true);

    $.ajax({
        type: 'PUT',
        url: $(this).data('url'),
        data: {
            'type': $(this).data('type'),
            'items': checkedItemsIds,
        },
        dataType: 'JSON'
    }).done(function(response) {
        console.log(response);
        // $.notify(response.message, 'success');
        window.location.reload();
    })
    .fail(GSCommon.processFailResponse)
    .always(() => {
        $(this).attr('disabled', false);
    });
});

tinymce.init({
    selector: '#tinymce',
    skin_url: '/css/skins/lightgray',
    plugins: [
        'paste', 'link', 'autoresize',
        'advlist', 'anchor', 'autolink',
        'autosave', 'colorpicker', 'bbcode',
        'emoticons'
    ]
}); 