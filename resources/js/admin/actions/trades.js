export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-users-trades');

        Echo.private(`user.order.request`)
            .listen('UserOrderRequestEvent', (e) => {
                let data = e.data,
                    menuBuyItemInfo = $('.menu__item-info-order-buy'),
                    menuSellItemInfo = $('.menu__item-info-order-sell'),
                    rowTemplateHtml = $('#template-order-row').html();


                if (data && data.order) {
                    if (data.order.order_type === 'buy') {
                        menuBuyItemInfo.data('val', (menuBuyItemInfo.data('val') || 0) + 1);
                        menuBuyItemInfo.text(menuBuyItemInfo.data('val'));

                        $.notify('You have new request for buy', 'info');

                        let rowTemplate = _.template(rowTemplateHtml);

                        console.log(rowTemplate(data));

                        if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                            if (data && data.pagination && data.pagination.total > 10) {
                                $('.buy.list tbody tr:last-child').remove();
                            }

                            $('.buy.list tbody').prepend(rowTemplate(data));
                        }
                    }

                    if (data.order.order_type === 'sell') {
                        menuSellItemInfo.data('val', (menuSellItemInfo.data('val') || 0) + 1);
                        menuSellItemInfo.text(menuSellItemInfo.data('val'));

                        $.notify('You have new request for sell', 'info');

                        let rowTemplate = _.template(rowTemplateHtml);

                        if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                            if (data && data.pagination && data.pagination.total > 10) {
                                $('.sell.list tbody tr:last-child').remove();
                            }

                            $('.sell.list tbody').prepend(rowTemplate(data));
                        }
                    }
                }
            });
    },
    actionOpenDetailsModal: function($btn) {
        let buttons = [];

        if ($btn.data('status') === 'waiting') {
            buttons.push({
                icon: 'fas fa-check',
                label: 'Confirm ' + $btn.data('type'),
                cssClass: 'btn btn-success',
                action: function(dialog) {
                    let message = '';
                    if ($btn.data('type') === 'sell') {
                        message = 'Are you sure you want to buy <b>' + $btn.data('amount') + ' </b> from user: ' + $btn.data('email') + '</b>?';
                    } else {
                        message = 'Are you sure you want to sell <b>' + $btn.data('amount') + ' </b> to user: ' + $btn.data('email') + '</b>?';
                    } 
 
                    BootstrapDialog.confirm(message, function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/trades/confirm',
                            data: { 
                                id: $btn.data('id'),
                                type: $btn.data('type'),
                                passphrase: $('.passphrase').val()
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            }, {
                icon: 'fas fa-times',
                label: 'Cancel ' + $btn.data('type'),
                cssClass: 'btn-danger',
                action: function(dialog) {
                    BootstrapDialog.confirm('Are you sure you want cancel this ' + $btn.data('type') + ' ?', function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/trades/cancel',
                            data: {
                                id: $btn.data('id'),
                                type: $btn.data('type')
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            });
        }

        let menuItemInfo = {
            buy: 0,
            sell: 0
        }, menuItemInfoElement = $('.menu__item-info-order-' + $btn.data('type'));

        this.modal.show({
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),
            data: {
                id: $btn.data('id'),
                type: $btn.data('type'),
                passphrase: $('.passphrase').val()
            },
            buttons: buttons,
            actionElement: $btn,
            onshown: function () {
                if ($btn.closest('tr').hasClass('item-unviewed')) {
                    $btn.closest('tr').removeClass('item-unviewed');

                    menuItemInfo[$btn.data('type')] = menuItemInfoElement.data('val') - 1;

                    if ($btn.data('type') === 'buy' || $btn.data('type') === 'sell') {
                        menuItemInfoElement.data('val', menuItemInfo[$btn.data('type')]);

                        if (menuItemInfo[$btn.data('type')] === 0)
                            menuItemInfoElement.text('');
                        else
                            menuItemInfoElement.text(menuItemInfo[$btn.data('type')]);
                    }
                }
            }
        });
    }
};