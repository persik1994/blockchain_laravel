export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-users-money');

        Echo.private(`user.money.request`)
            .listen('UserMoneyRequestEvent', (e) => {
                if (e.data.deposit) {
                    let menuDepositItemInfo = $('.menu__item-info-money-deposit');
                        menuDepositItemInfo.data('val', (menuDepositItemInfo.data('val') || 0) + 1);
                        menuDepositItemInfo.text(menuDepositItemInfo.data('val'));

                    $.notify('You have new request for deposit', 'info');

                    let rowTemplateHtml = $('#template-deposit-row').html();
                    let rowTemplate = _.template(rowTemplateHtml);

                    if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                        if (e.data && e.data.pagination && e.data.pagination.total > 10) {
                            $('.deposit.list tbody tr:last-child').remove();
                        }

                        $('.deposit.list tbody').prepend(rowTemplate(e.data));
                    }
                }

                if (e.data.withdraw) {
                    let menuDepositItemInfo = $('.menu__item-info-money-withdraw');
                    menuDepositItemInfo.data('val', (menuDepositItemInfo.data('val') || 0) + 1);
                    menuDepositItemInfo.text(menuDepositItemInfo.data('val'));

                    $.notify('You have new request for withdraw', 'info');

                    let rowTemplateHtml = $('#template-withdraw-row').html();
                    let rowTemplate = _.template(rowTemplateHtml);

                    if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                        if (e.data && e.data.pagination && e.data.pagination.total > 10) {
                            $('.withdraw.list tbody tr:last-child').remove();
                        }

                        $('.withdraw.list tbody').prepend(rowTemplate(e.data));
                    }
                }
            });
    },
    actionOpenDetailModal: function($btn) {
        let buttons = [];

        if ($btn.data('status') === 'waiting') {
            buttons.push({
                icon: 'fas fa-check',
                label: 'Confirm ' + $btn.data('type'),
                cssClass: 'btn btn-success',
                action: function(dialog) {
                    let withdrawConfirmDialog = dialog;

                    if ($btn.data('type') === 'withdraw') {
                        GSCommon.modal.show({
                            title: 'Are you sure you made payment? Upload payment proof to mark payment as made.',
                            dialogType: BootstrapDialog.TYPE_DEFAULT,
                            url: $btn.data('proof-image-upload-url'),
                            actionElement: $btn,
                            buttons: [{
                                icon: 'fas fa-check',
                                label: 'Upload',
                                cssClass: 'btn btn-success',
                                action: function(dialog) {
                                    let imageProofInput = $(dialog.$modalBody[0]).find('input[name="uploadFile"]');

                                    if (imageProofInput) {
                                        let image = imageProofInput[0].files[0];

                                        if (!image) {
                                            $.notify('Please upload at least one image', 'error');
                                            return;
                                        }

                                        let formData = new FormData();
                                        formData.append('imageProof', image);

                                        $.ajax({
                                            type: 'post',
                                            url: '/admin/money/upload-proof-image/' + $btn.data('id'),
                                            data: formData,
                                            dataType: 'json',
                                            processData: false,
                                            contentType: false,
                                        }).done(function (response) {
                                            $.notify(response.message, 'success');
                                            withdrawConfirmDialog.close();
                                            dialog.close();
                                            setTimeout(() => { document.location.reload() }, 2000);
                                        }).fail(GSCommon.processFailResponse);
                                    }
                                }
                            }]
                        });

                        return;
                    }

                    BootstrapDialog.confirm('Are you sure you want confirm that <b>' + $btn.data('amount') + ' USD</b> will be deposited to user: ' + $btn.data('email') + '</b>?', function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/money/deposit/confirm',
                            data: {
                                id: $btn.data('id'),
                                type: $btn.data('type')
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            }, {
                icon: 'fas fa-times',
                label: 'Cancel ' + $btn.data('type'),
                cssClass: 'btn-danger',
                action: function(dialog) {
                    BootstrapDialog.confirm('Are you sure you want cancel this deposit ' + $btn.data('type') + ' ?', function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/money/deposit/cancel',
                            data: {
                                id: $btn.data('id'),
                                type: $btn.data('type')
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            });
        }

        let menuItemInfo = {
            deposit: 0,
            withdraw: 0
        }, menuItemInfoElement = $('.menu__item-info-money-' + $btn.data('type'));

        this.modal.show({
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),
            buttons: buttons,
            actionElement: $btn,
            onshown: function () {
                if ($btn.closest('tr').hasClass('item-unviewed')) {
                    $btn.closest('tr').removeClass('item-unviewed');

                    menuItemInfo[$btn.data('type')] = menuItemInfoElement.data('val') - 1;

                    if ($btn.data('type') === 'deposit' || $btn.data('type') === 'withdraw') {
                        menuItemInfoElement.data('val', menuItemInfo[$btn.data('type')]);

                        if (menuItemInfo[$btn.data('type')] === 0)
                            menuItemInfoElement.text('');
                        else
                            menuItemInfoElement.text(menuItemInfo[$btn.data('type')]);
                    }
                }
            }
        });
    }
};