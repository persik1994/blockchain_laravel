export default {

    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-adminbalance', {
            'selectBalance': 'change',
            'editBalance': 'submit',
            'addBalance': 'submit',
        });
    },

    actionAddBalanceModal: function($btn) {
        this.modal.show({
            name: 'add-balance-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/modal/add-balance',
            onshown: function () {
                
            }
        });
    },

    actionEditBalanceModal: function($btn) {
        this.modal.show({
            name: 'add-balance-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),

        });
    },
    actionEditBalance: function($form) {
        GSCommon.clearFormErrors();
        $.ajax({
            type: 'PUT',
            url: $form.data('url'),
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse);
    },
    actionSelectBalance: function ($select) {
   
    },
    actionAddBalance: function($form) {
        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/admin/balance/add',
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse);
    },

};