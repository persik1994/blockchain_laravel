export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-transfer');

        Echo.private(`user.transfer.request`)
            .listen('UserTransferRequestEvent', (e) => {
                let data = e.data;

                if (data) {
                    let menuDepositItemInfo = $('.menu__item-info-money-transfer');
                    menuDepositItemInfo.data('val', (menuDepositItemInfo.data('val') || 0) + 1);
                    menuDepositItemInfo.text(menuDepositItemInfo.data('val'));

                    let rowTemplateHtml = $('#template-transfer-row').html();
                    let rowTemplate = _.template(rowTemplateHtml);

                    if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                        if (data && data.pagination && data.pagination.total > 10) {
                            $('.transfer.list tbody tr:last-child').remove();
                        }

                        $('.transfer.list tbody').prepend(rowTemplate(e.data));
                    }

                    $.notify('You have new request for transfer', 'info');
                }
            });
    },
    actionOpenDetailModal: function($btn) {
        let buttons = [];

        if (parseInt($btn.data('status')) === 0) {
            buttons.push({
                icon: 'fas fa-check',
                label: 'Confirm transfer',
                cssClass: 'btn btn-success',
                action: function(dialog) {
                    BootstrapDialog.confirm('Are you sure you want confirm that <b>' + $btn.data('amount') + ' BTC</b> will be transferred to address: ' + $btn.data('address') + '</b>?', function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/transfers/confirm',
                            data: {
                                id: $btn.data('id')
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            //setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            }, {
                icon: 'fas fa-times',
                label: 'Cancel transfer',
                cssClass: 'btn-danger',
                action: function(dialog) {
                    BootstrapDialog.confirm('Are you sure you want cancel this transfer?', function (response) {
                        if (!response) {
                            return;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/admin/transfers/cancel',
                            data: {
                                id: $btn.data('id')
                            },
                            dataType: 'json'
                        }).done(function (response) {
                            $.notify(response.message, 'success');
                            dialog.close();
                            setTimeout(() => { document.location.reload() }, 2000);
                        }).fail(GSCommon.processFailResponse);
                    });
                }
            });
        }

        let btnRow = $btn.closest('tr');
        let modalProperties = {
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/transfers/modal/detail',
            buttons: buttons,
            actionElement: $btn,
            data: {
                id: $btn.data('id')
            },
            onshown: function (d, data) {
                if (btnRow.hasClass('item-unviewed')) {
                    btnRow.removeClass('item-unviewed');
                    if (data) $('.menu__item-info-money-' + $btn.data('type')).text(data.count_unviewed || '');
                }
            }
        };

        if (btnRow.hasClass('item-unviewed')) {
            modalProperties.unievedType = $btn.data('type');
        }

        console.log(modalProperties);

        this.modal.show(modalProperties);
    }
};