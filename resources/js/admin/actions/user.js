export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-user', {
            update: 'submit'
        });
    },
    actionOpenTransactionsModal: function ($btn) {
        this.modal.show({
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/users/modal/transactions',
            actionElement: $btn,
            data: {
                id: $btn.data('id')
            }
        });
    },
    actionOpenEditModal: function ($btn) {
        this.modal.show({
            name: 'update-user',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/users/modal/edit',
            actionElement: $btn,
            data: {
                id: $btn.data('id')
            }
        });
    },
    actionDeleteDocument: function ($btn) {
        BootstrapDialog.confirm('Are you sure you want to delete this document?', function (response) {
            if (!response) {
                return;
            }

            $.ajax({
                url: '/admin/users/delete-document',
                type: 'post',
                data: {
                    id: $btn.data('id'),
                    type: $btn.data('type')
                },
                dataType: 'json'
            }).done(function (response) {
                $.notify(response.message, 'success');
                $btn.closest('tr').remove();
            }).fail(GSCommon.processFailResponse);
        });
    },
    actionUpdate: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            GSCommon.modal.list['update-user'].close();
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },
    actionDeleteUser: function ($btn) {
        BootstrapDialog.confirm('Are you sure you want to delete this user?', function (response) {
            if (!response) {
                return;
            }

            $.ajax({
                url: '/admin/users/delete',
                type: 'post',
                data: {
                    id: $btn.data('id')
                },
                dataType: 'json'
            }).done(function (response) {
                $.notify(response.message, 'success');
                $btn.closest('tr').remove();
            }).fail(GSCommon.processFailResponse);
        });
    }
};