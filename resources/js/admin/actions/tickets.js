export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-tickets', {
            'addTicket': 'submit',
            'editTicket': 'submit',
            'selectTicket': 'change'
        });
        /* listen Support Event */
        // Echo.private(`user.support.request`)
        //     .listen('UserSupportRequestEvent', (e) => {
        //         if (e.data.support) {   
        //             let menuSupportItemInfo = $('.menu__item-info-money-support');
        //                 menuSupportItemInfo.data('val', menuSupportItemInfo.data('val') + 1);
        //                 menuSupportItemInfo.text(menuSupportItemInfo.data('val'));
        //             $.notify('You have new messages for support', 'info');    
        //         }
        // });
        // Echo.private(`user.support.response`)
        //     .listen('UserSupportResponseEvent', (e) => {

        // });                    
    },
    actionAddTicket: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/admin/support/add',
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },
    actionEditTicketModal: function($btn) {
        this.modal.show({
            name: 'add-ticket-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),
            actionElement: $btn
        });
    },
    actionEditTicket: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'PUT',
            url: $form.data('url'),
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    }
};