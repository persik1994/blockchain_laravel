export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-faqs', {
            'addFaq': 'submit',
            'selectFaq': 'change',
            'editFaq': 'submit'
        });
    },

    actionAddFaqModal: function($btn) {
        this.modal.show({
            name: 'add-faq-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/modal/add-faq',
            actionElement: $btn,
        });
    },
 
    actionEditFaqModal: function($btn) {
        this.modal.show({
            name: 'add-faq-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),
            actionElement: $btn,
        });
    },

    actionAddFaq: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/admin/faqs/add',
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(function () {
            GSCommon.formSubmitEnd($form);
        });
    },

    actionEditFaq: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'PUT',
            url: $form.data('url'),
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(function () {
            GSCommon.formSubmitEnd($form);
        });
    }
};