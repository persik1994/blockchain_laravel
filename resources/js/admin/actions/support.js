export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-support', {
            'addMessage': 'submit'
        });

        let ticketId = $('#ticket-chat').data('ticket-id');

        Echo.private(`user.support.request.ticket`)
            .listen('UserSupportTicketRequestEvent', (e) => {
                let menuDepositItemInfo = $('.menu__item-info-ticket-request-support');
                let data = e.data;

                menuDepositItemInfo.data('val', data.unviewed_items);
                menuDepositItemInfo.text(data.unviewed_items);

                $.notify('You have new ticket request', 'info');

                let rowTemplateHtml = $('#template-ticket-row').html();
                let rowTemplate = _.template(rowTemplateHtml);

                if (data) {
                    if (GSCommon.getParameterByName('page') == null || GSCommon.getParameterByName('page')  == 1) {
                        if (data.pagination && data.pagination.total > 10) {
                            $('.ticket.list tbody tr:last-child').remove();
                        }

                        $('.ticket.list tbody').prepend(rowTemplate({
                            item: data.ticket
                        }));
                    }
                }
            });

            Echo.private(`user.support.request`)
            .listen('UserSupportRequestEvent', (e) => {
                let data = e.data,
                    messages = e.data.messages,
                    ticket = e.data.ticket;

                let menuDepositItemInfo = $('.menu__item-info-ticket-request-support');

                menuDepositItemInfo.data('val', data.unviewed_items);
                menuDepositItemInfo.text(data.unviewed_items);

                if (ticket.id === ticketId) {
                    this.refreshMessages(messages, ticket);
                }

                $.notify('You have new message for support', 'info')
            });
    },

    actionAddMessage: function($form) {
        let self = this;

        GSCommon.formSubmitStart($form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            let messages = response.messages,
                ticket = response.ticket;

            if (messages) {
                self.refreshMessages(messages, ticket)
            }

            $form[0].reset();
        })
        .fail(GSCommon.processFailFormResponse())
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },

    refreshMessages(messages, ticket) {
        $('.messages-wrapper').html('');
        messages.forEach((message) => {
            if (message.user) {
                let messageUser = message.user;

                if (messageUser.type === 'admin') {
                    $('.messages-wrapper').append(`<div class="pull-right message"><b>`+ message.user.name +`:</b> ` + message.message + ` </div><br>`)
                } else {
                    $('.messages-wrapper').append(`<div class="message"><b>`+ message.user.name +`:</b> ` + message.message + ` </div><br>`)
                }
            } else {
                $('.messages-wrapper').append(`<div class="message"><b>`+ ticket.email_address +`:</b> ` + message.message + ` </div><br>`)
            }
        })
    }
};