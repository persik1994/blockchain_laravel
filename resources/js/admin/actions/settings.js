export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-settings', {
            saveBitcoin: 'submit',
            saveEthereum: 'submit', 
            saveWeb: 'submit',
            autoUpdateChange: 'change',
            generateCryptoAddresses: 'submit',
        });
    },
    actionAutoUpdateChange: function ($c) {
        $('[name="ethereum-buy-fee"]').prop('disabled', !$c.is(':checked'));
        $('[name="ethereum-sell-fee"]').prop('disabled', !$c.is(':checked'));
        $('[name="ethereum-fixed-price"]').prop('disabled', $c.is(':checked'));

        $('[name="bitcoin-buy-fee"]').prop('disabled', !$c.is(':checked'));
        $('[name="bitcoin-sell-fee"]').prop('disabled', !$c.is(':checked'));
        $('[name="bitcoin-fixed-price"]').prop('disabled', $c.is(':checked'));
    },
    actionSaveBitcoin: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },
    actionSaveEthereum: function ($form) {
        GSCommon.formSubmitStart($form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },
    actionGenerateCryptoAddresses: function ($form) {
        let btnForm = $form.find('button[type="submit"]');

        GSCommon.formSubmitStart($form);
        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            GSCommon.clearFormErrors();

            $.notify(response.message, 'success');
            $('[name="fee_address"]').val(response['fee_address']);
            $('[name="sell_address"]').val(response['sell_address']);
            $('[name="buy_address"]').val(response['buy_address']);

            $form.find('.addresses-password-wrapper').each((index, element) => {
                $(element).remove();
            });
            btnForm.remove();
        }).fail(GSCommon.processFailFormResponse($form))
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    },    
    actionSaveWeb: function ($form) {
        GSCommon.formSubmitStart($form);
        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json'
        }).done(function (response) {
            $.notify(response.message, 'success');
            GSCommon.clearFormErrors();
        }).fail(GSCommon.processFailResponse)
        .always(() => {
            GSCommon.formSubmitEnd($form);
        });
    }
};