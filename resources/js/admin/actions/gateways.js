export default {
    init: function () {
        GSCommon.makeParentOf(this);
        this.initListeners('admin-gateways', {
            'addGateWay': 'submit',
            'selectGateWay': 'change',
            'editGateWay': 'submit'
        });
    },

    actionAddGateWayModal: function($btn) {
        this.modal.show({
            name: 'add-gateway-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: '/admin/modal/add-gateway',
            actionElement: $btn,
            onshown: function () {
                $('select[name="currency"]').selectpicker({
                    liveSearch: true
                });

                let gatewayFields = $('select[name="name"] option:first').data('fields');

                if (gatewayFields) {
                    let fields = '';

                    Object.keys(gatewayFields).forEach(function (element) {
                        fields += `
                            <div class="form-group">
                                <label>` + gatewayFields[element] + `</label>
                                <input type="text" class="form-field form-control" name="` + element + `">
                            </div>`;
                    });

                    $('#account_fields').html(fields)
                }
            }
        });
    },

    actionEditGateWayModal: function($btn) {
        this.modal.show({
            name: 'add-gateway-modal',
            title: $btn.data('title'),
            dialogType: BootstrapDialog.TYPE_DEFAULT,
            url: $btn.data('url'),
            actionElement: $btn,
            onshown: function () {
                $('select[name="currency"]').selectpicker({
                    liveSearch: true
                });
            }
        });
    },

    actionAddGateWay: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'post',
            url: '/admin/gateways/add',
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(function () {
            GSCommon.formSubmitEnd($form);
        });
    },

    actionEditGateWay: function($form) {
        GSCommon.formSubmitStart($form);

        GSCommon.clearFormErrors();
        $.ajax({
            type: 'PUT',
            url: $form.data('url'),
            data: $form.serializeArray(),
            dataType: 'json'
        }).done(function (response) {
            window.location.reload();
        }).fail(GSCommon.processFailResponse)
        .always(function () {
            GSCommon.formSubmitEnd($form);
        });
    },

    actionSelectGateWay: function ($select) {
        let fields = '';

        let gatewayFields = $('option:selected', $select).data('fields');

        if (gatewayFields) {
            Object.keys(gatewayFields).forEach(function (element) {
                fields += `
                    <div class="form-group">
                        <label>` + gatewayFields[element] + `</label>
                        <input type="text" class="form-field form-control" name="` + element + `">
                    </div>`;
            });
        }

        $('#account_fields').html(fields)
    }
};