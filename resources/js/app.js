
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Require actions
 */

require('./actions/user').default.init();
require('./actions/wallet').default.init();
require('./actions/deposit').default.init();
require('./actions/withdraw').default.init();
require('./actions/order').default.init();
require('./actions/account').default.init();
require('./actions/transfer').default.init();
require('./actions/request').default.init();
require('./actions/verification').default.init();
require('./actions/security').default.init();
require('./actions/profile').default.init();
require('./actions/support').default.init();
require('./actions/chat').default.init();


function offsetAnchor() {
    if (location.hash.length !== 0) {
        window.scrollTo(window.scrollX, window.scrollY - 100);
    }
}

$(document).on('click', 'a[href^="#"]', function(event) {
    window.setTimeout(function() {
        offsetAnchor();
    }, 0);
});

window.setTimeout(offsetAnchor, 0);

 