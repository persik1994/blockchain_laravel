@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <h2>
        <small>{{ __('account.deposit_money') }}</small>
    </h2>
    <h3>
        <small>
            {{ __('account.deposit_money_info') }}
            {{ $settings->process_time_to_buy === 1 ? __('account.1_hour') : ($settings->process_time_to_buy . ' ' . __('account.hours')) }}
        </small>
    </h3>
    <br>

    <div class="alert alert-success" style="display: none;" id="success-message"></div>

    <div style="background-color: #192126 !important" 
        id="deposit-history-panel"
        class="panel panel-default history-panel @if(!$deposits->isEmpty()) active @endif"
    >
        <div class="panel-body">
            <h2>
                <small>Deposit Notifications</small>
            </h2>
            <div class="timeline">
                @foreach($deposits as $deposit)
                    <div class="timeline-item" id="deposit-{{$deposit->id}}">
                        <div class="timeline-badge">
                            @if($deposit->status === \App\Models\UserMoney::STATUS_WAITING)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-arrow-circle-up fa-3x"></i>
                                </div>
                            @elseif($deposit->status === \App\Models\UserMoney::STATUS_CONFIRMED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                                    <i class="fas fa-arrow-circle-down fa-3x"></i>
                                </div>
                            @elseif($deposit->status === \App\Models\UserMoney::STATUS_CANCELED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-ban fa-3x"></i>
                                </div>
                            @endif
                        </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow"></div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <span class="timeline-body-title font-blue-madison">
                                        Amount: <b>{{ number_format($deposit->amount, 2) }} {{$userFiatCurrency}}</b>
                                    </span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                    <span class="font-grey-cascade"><br>
                                        Deposit id:
                                        <b>
                                            <a href="{{route('deposit-money.details', $deposit->id)}}">
                                                {{ $deposit->id }}
                                            </a>
                                        </b>
                                        <br/>
                                            Status: <b>
                                                        @if($deposit->status == \App\Models\UserMoney::STATUS_WAITING)
                                                            In Process
                                                        @elseif($deposit->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                                            Approved
                                                        @elseif($deposit->status == \App\Models\UserMoney::STATUS_CANCELED)
                                                            Canceled
                                                        @endif
                                                    </b>
                                        <br/>
                                            Time: <b>{{ date('Y-m-d H:i:s', $deposit->time) }}</b>
                                        <br/>
                                            Prove image: <a href="{{route('user.file', $deposit->attachment)}}" target="_blank"><small>Click to preview full size.</small></a>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
<!--             @if($depositsTotalCount > 5)
                <a href="{{route('history', 'type=deposit')}}" class="btn btn-default pull-right">See all deposits</a>
            @endif -->
        </div>
    </div>


    <form method="POST" action="{{ route('account.deposit.save') }}" class="js-deposit-submitDeposit" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>{{ __('account.select_payment_method') }}</label>
            <select class="form-control js-deposit-selectPaymentMethod" name="gateway-id">
                @if($gateways->isEmpty())
                    <option>{{ __('account.info_4') }}</option>
                @else
                    @foreach($gateways as $gateway)
                        <option @if($gateway->id == $firstGateway->id) selected @endif value="{{ $gateway->id }}">{{ \App\Models\Gateway::$gateways[$gateway->name]['name'] . ' ' . $gateway->currency }}</option>
                    @endforeach
                @endif
            </select>
            <small>{{ __('account.select_from_where_to_pay') }}</small>
        </div>

        @if(!$gateways->isEmpty())
            <div class="form-group">
                <label>{{ __('account.amount') }}</label>
                <div class="input-group">
                    {{--js-deposit-convertToBitcoins--}}
                    <input type="text" class="form-control"
{{--                           data-btc-buy-price="{{ $cryptoBuyPrice[$userCryptoCurrency][$userFiatCurrency] }}"--}}
                           id="amount-input"
                           name="amount" placeholder="0.00">
                    <span class="input-group-addon" id="currency">{{$firstGateway->currency}}</span>
                </div>
            </div>
        @endif

        <div id="btc_account_fields">
            @if(!$gateways->isEmpty())
                @foreach($firstGateway->gateway_information as $key => $infoItem)
                    <span>{{\App\Models\Gateway::$gateways[$firstGateway->name]['fields'][$key]}}:</span><span>{{$infoItem}}</span></br>
                @endforeach
            @endif
        </div>

        <br>

        {{--<div class="form-group">--}}
            {{--<label>{{ __('account.will_receive') }}</label>--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" class="form-control" id="btc-amount-receive" disabled placeholder="0.00">--}}
                {{--<span class="input-group-addon">BTC</span>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="form-group">
            <label>{{ __('account.attach_payment_proof') }}</label>
            <input type="file" name="payment-proof" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">{{ __('account.btn_6') }}</button>
    </form>

    <br>



    @include('account.deposit.templates')
@endsection
