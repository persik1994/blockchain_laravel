@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <legend>Deposit nr. {{$deposit->id}}</legend>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Status:</label>
                <span>
                    @if($deposit->status == \App\Models\UserMoney::STATUS_WAITING)
                        <span class="label label-info">Awaiting</span>
                    @elseif($deposit->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                        <span class="label label-success">Confirmed</span>
                    @elseif($deposit->status == \App\Models\UserMoney::STATUS_CANCELED)
                        <span class="label label-danger">Canceled</span>
                    @endif
                </span>
            </div>

            <div class="form-group">
                <label>Amount:</label>
                <span>
                   {{$deposit->current_amount}} {{$deposit->gateway->currency}}
                </span>
            </div>

            <div class="form-group">
                <label>Amount in {{$userFiatCurrency}}:</label>
                <span>
                    {{$deposit->amount}} {{$userFiatCurrency}}
                </span>
            </div>

            <div class="form-group">
                <label>Request Time</label>
                <span>
                    {{$deposit->time}}
                </span>
            </div>

            <div class="form-group">
                <label>Updated Time</label>
                <span>
                    {{$deposit->updated_time}}
                </span>
            </div>

            <div class="form-group">
                <label>Proof Image:</label>
                <div class="proof-image-block">
                    <img class="proof-image" src="{{route('user.file', $deposit->attachment)}}" alt="Proof Image">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            @if(isset($deposit->gateway))
                <div class="form-group">
                    <label>Payment method:</label>
                    <span>
                        {{\App\Models\Gateway::$gateways[$deposit->gateway->name]['name']}}
                    </span>
                    <br>
                    <label>Payment method information:</label>
                    @foreach($deposit->gateway_information as $key => $infoItem)
                        <br>
                        <span>
                            {{\App\Models\Gateway::$gateways[$deposit->gateway->name]['fields'][$key]}}: {{$infoItem}}
                        </span>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection