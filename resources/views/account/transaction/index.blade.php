@extends('layouts.account', [
    'settings' => $settings
])

@section('content')

    <h2>
        <small>{{ __('account.transactions') }}</small>
    </h2>

    <div class="timeline">
        @foreach($transactions as $transaction)
            <div class="timeline-item">
                <div class="timeline-badge">
                    @if($transaction['category'] === 'send')
                        <div style="margin-left: 20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                            <i class="fas fa-arrow-alt-circle-up fa-3x"></i>
                        </div>
                    @else
                        <div style="margin-left: 20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                            <i class="fas fa-arrow-alt-circle-down fa-3x"></i>
                        </div>
                    @endif
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <a href="#" class="timeline-body-title font-blue-madison">
                                {{ __($transaction['category'] === 'send' ? 'account.sent' : 'account.received') }}
                                {{ satoshitize($transaction['amount']) }} BTC
                            </a>
                        </div>
                    </div>
                    <div class="timeline-body-content">
                        <span class="font-grey-cascade"><br>
                            {{ __('account.transaction_id') }}:
                            <b>
                                <a href="https://live.blockcypher.com/btc-testnet/tx/{{ $transaction['txid'] }}" target="_blank">
                                    {{ $transaction['txid'] }}
                                </a>
                            </b>
                            <br/>
                            {{ __('account.address') }}: <b>{{ $transaction['address'] }}</b>
                            <br/>
                            {{ __('account.confirmations') }}: <b>{{ $transaction['confirmations'] }}</b>
                            <br/>
                            {{ __('account.time') }}: <b>{{ date("d/m/Y H:i", $transaction['time']) }}</b>
                        </span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
