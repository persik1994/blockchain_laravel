@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <legend>Withdraw nr. {{$withdraw->id}}</legend>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Status:</label>
                <span>
                    @if($withdraw->status == \App\Models\UserMoney::STATUS_WAITING)
                        <span class="label label-info">Awaiting</span>
                    @elseif($withdraw->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                        <span class="label label-success">Confirmed</span>
                    @elseif($withdraw->status == \App\Models\UserMoney::STATUS_CANCELED)
                        <span class="label label-danger">Canceled</span>
                    @endif
                </span>
            </div>

            <div class="form-group">
                <label>Amount in {{$userFiatCurrency}}:</label>
                <span>
                    {{$withdraw->amount}} {{$userFiatCurrency}}
                </span>
            </div>

            <div class="form-group">
                <label>Request Time</label>
                <span>
                    {{$withdraw->time}}
                </span>
            </div>

            <div class="form-group">
                <label>Updated Time</label>
                <span>
                    {{$withdraw->updated_time}}
                </span>
            </div>

            <div class="form-group">
                <label>Proof Image:</label>
                <span class="proof-image-block">
                    @if(!empty($withdraw->attachment))
                        <img class="proof-image" src="{{route('user.file', $withdraw->attachment)}}" alt="Proof Image">
                    @endif
                </span>
            </div>
        </div>
    </div>
@endsection