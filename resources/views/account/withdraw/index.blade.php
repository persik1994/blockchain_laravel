@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <h2>
        <small>{{ __('account.withdraw_money') }}</small>
    </h2>
    <h3>
        <small>{{ __('account.withdraw_money_info') }}</small>
    </h3>

    <br>

    <div class="alert alert-success" style="display: none;" id="success-message"></div>

    <div style="background-color: #192126 !important"
        id="withdraw-history-panel"
        class="panel panel-default history-panel @if(!$withdrawals->isEmpty()) active @endif"
    >
        <div class="panel-body">
            <h2>
                <small>Withdrawal Notifications</small>
            </h2>
            <div class="timeline">
                @foreach($withdrawals as $withdrawal)
                    <div class="timeline-item">
                        <div class="timeline-badge">
                            @if($withdrawal->status === \App\Models\UserMoney::STATUS_WAITING)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-arrow-circle-up fa-3x"></i>
                                </div>
                            @elseif($withdrawal->status === \App\Models\UserMoney::STATUS_CONFIRMED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                                    <i class="fas fa-arrow-circle-down fa-3x"></i>
                                </div>
                            @elseif($withdrawal->status === \App\Models\UserMoney::STATUS_CANCELED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-ban fa-3x"></i>
                                </div>
                            @endif
                        </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow"></div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <span class="timeline-body-title font-blue-madison">
                                        Amount: <b>{{ number_format($withdrawal->user_amount, 2) }} {{$withdrawal->user_fiat_currency}}</b>
                                    </span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                    <span class="font-grey-cascade"><br>
                                        Withdraw id:
                                        <b>
                                            <a href="{{route('withdraw-money.details', $withdrawal->id)}}">
                                                {{ $withdrawal->id }}
                                            </a>
                                        </b>
                                        <br/>
                                            Status: <b>
                                                        @if($withdrawal->status == \App\Models\UserMoney::STATUS_WAITING)
                                                            In Process
                                                        @elseif($withdrawal->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                                            Approved
                                                        @elseif($withdrawal->status == \App\Models\UserMoney::STATUS_CANCELED)
                                                            Canceled
                                                        @endif
                                                    </b>
                                        <br/>
                                            Time: <b>{{ date('Y-m-d H:i:s', $withdrawal->time) }}</b>
                                        @if(!empty($withdrawal->attachment))
                                            <br/>
                                                Prove image: <a href="{{route('user.file', $withdrawal->attachment)}}">Click to preview full size.</a>
                                        @endif
                                    </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
<!--             @if($withdrawalsTotalCount > 5)
                <a href="{{route('history', 'type=withdraw')}}" class="btn btn-default pull-right">See all withdrawals</a>
            @endif -->
        </div>
    </div>

    <form method="POST" action="{{ route('account.withdraw.request') }}" class="js-withdraw-sendRequest">
        <input type="hidden" name="user-fiat-currency" value="{{$userFiatCurrency}}">
        <div class="form-group">
            <label>{{ __('account.current_balance') }}: <span id="current-balance">{{ number_format($user->availableAmount($userFiatCurrency), 2) }} {{$userFiatCurrency}}</span></label>
        </div>
        <div class="form-group">
            <label>{{ __('account.amount') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="amount" placeholder="0.00">
                <span class="input-group-addon" id="basic-addon2">{{$userFiatCurrency}}</span>
            </div>
        </div>

        @if(!empty($user->secret_pin))
            <div class="form-group">
                <label>{{ __('account.your_secret_pin') }}</label>
                <input type="password" class="form-control" name="secret-pin" autocomplete="new-password">
            </div>
        @endif

        <div class="form-group">
            <label>{{ __('account.bank_holder_name') }}</label>
            <input type="text" class="form-control" name="bank-holder-name" value="{{ $bankAccount ? $bankAccount->u_field_1 : '' }}">
        </div>
        <div class="form-group">
            <label>{{ __('account.bank_account_number') }}</label>
            <input type="text" class="form-control" name="bank-account-number" value="{{ $bankAccount ? $bankAccount->u_field_2 : '' }}">
        </div>
        <div class="form-group">
            <label>{{ __('account.bank_swift') }}</label>
            <input type="text" class="form-control" name="bank-swift" value="{{ $bankAccount ? $bankAccount->u_field_3 : '' }}">
        </div>
        <div class="form-group">
            <label>{{ __('account.bank_address') }}</label>
            <input type="text" class="form-control" name="bank-address" value="{{ $bankAccount ? $bankAccount->u_field_4 : '' }}">
        </div>
        <button type="submit" class="btn btn-primary">{{ __('account.withdraw_money') }}</button>
    </form>

    <br>


    @include('account.withdraw.templates')
@endsection
