@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <h2><small>{{ __('account.transfer') }}</small></h2>
    <h3><small>{{ __('account.transfer_info', ['curypto_currency_name' => config('currencies.crypto_currencies')[$userCryptoCurrency]['name']]) }}</small></h3>

    <br>

    <div style="background-color: #192126 !important"
         id="transfer-history-panel"
         class="panel panel-default history-panel @if(!$transfers->isEmpty()) active @endif"
    >
        <div class="panel-body">
            <h2>
                <small>Transfer Notifications</small>
            </h2>
            <div class="timeline">
                @foreach($transfers as $transfer)
                    <div class="timeline-item" id="deposit-{{$transfer->id}}">
                        <div class="timeline-badge">
                            @if($transfer->status === 0)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-arrow-circle-up fa-3x"></i>
                                </div>
                            @elseif($transfer->status === 1)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                                    <i class="fas fa-arrow-circle-down fa-3x"></i>
                                </div>
                            @elseif($transfer->status === 2)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-ban fa-3x"></i>
                                </div>
                            @endif
                        </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow"></div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <span class="timeline-body-title font-blue-madison">
                                        Amount: <b> {{ convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency) }} {{ $transfer->crypto_currency }}</b>
                                    </span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                    <span class="font-grey-cascade"><br>
                                        Transfer id:
                                        <b>
                                            <a href="{{route('account.transfer.details', $transfer->id)}}">
                                                {{ $transfer->id }}
                                            </a>
                                        </b>
                                        <br/>
                                            Status: <b>
                                            @if($transfer->status === 0)
                                                In Process
                                            @elseif($transfer->status === 1)
                                                Approved
                                            @elseif($transfer->status === 2)
                                                Canceled
                                            @endif
                                                    </b>
                                        <br/>
                                            Time: <b>{{ $transfer->time }}</b>

                                        @if(!empty($transfer->from_address))
                                            <br/>
                                                Recipient Address: <b>{{ $transfer->from_address }}</b>
                                        @endif

                                        <br/>
                                            Recipient Address: <b>{{ $transfer->recipient_address }}</b>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <form action="{{ route('account.transfer.request') }}" class="js-transfer-sendRequest" method="POST">
        @if($userCryptoCurrency === 'ETH')
            <div class="form-group">
                <label>{{ __('account.from_address') }}</label>
                <select class="form-control js-transfer-selectFromAddress" name="from-address">
                    @if($addresses['ETH']->isEmpty())
                        <option>{{ __('account.info_no_address') }}</option>
                    @else
                        <option value="">From address</option>
                        @foreach($addresses['ETH'] as $address)
                            <option value="{{ $address->address }}">{{ $address->address }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">{{ __('account.to_passphrase_address') }}</label>
                <input autocomplete="off" type="password" class="form-control" name="passphrase">
            </div>
        @endif

        <div class="form-group">
            <label>{{ __('account.recipient_address') }}</label>
            <input type="text" class="form-control" name="address">
        </div>
        <div class="form-group">
            <label>{{ __('account.amount') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="amount" placeholder="0.0000000">
                <span class="input-group-addon" id="basic-addon2">@if($userCryptoCurrency === 'BTC') BTC @elseif($userCryptoCurrency === 'ETH') ETH @endif</span>
            </div>
        </div>

        @if(!empty($user->secret_pin))
            <div class="form-group">
                <label>{{ __('account.your_secret_pin') }}</label>
                <input type="password" class="form-control" name="secret-pin" autocomplete="new-password">
            </div>
        @endif

        <button type="submit" class="btn btn-primary">{{ __('account.btn_18') }}</button>
        <span class="pull-right">Transaction fee: <?php echo $txFee + $settings->withdrawal_comission; ?> @if($userCryptoCurrency === 'BTC') BTC @elseif($userCryptoCurrency === 'ETH') ETH @endif</span>
    </form>

    @include('account.transfer.templates')
@endsection
