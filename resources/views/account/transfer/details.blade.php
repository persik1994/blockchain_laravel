@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <legend>Transfer nr. {{$transfer->id}}</legend>

    <div class="row text-white">
        <div class="col-md-6">
            <div class="form-group">
                <label>Status:</label>
                <span>
                    @if($transfer->status === 0)
                        <span class="label label-info">Awaiting</span>
                    @elseif($transfer->status === 1)
                        <span class="label label-success">Confirmed</span>
                    @elseif($transfer->status === 2)
                        <span class="label label-danger">Canceled</span>
                    @endif
                </span>
            </div>

            <div class="form-group">
                <label>Crypto Amount:</label>
                <span>
                   {{ convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency) }} {{ $transfer->crypto_currency }}
                </span>
            </div>

            <div class="form-group">
                <label>Transfer Time:</label>
                <span>
                    {{$transfer->time}}
                </span>
            </div>

            @if(!empty($transfer->from_address))
                <div class="form-group">
                    <label>From Address:</label>
                    <span>
                    {{$transfer->from_address}}
                </span>
                </div>
            @endif

            <div class="form-group">
                <label>Recipient Address:</label>
                <span>
                    {{$transfer->recipient_address}}
                </span>
            </div>
        </div>
    </div>
@endsection