@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <h2><small>{{ __('account.request') }}</small></h2>
    <h3><small>@if($userCryptoCurrency === 'BTC') {{ __('account.request_info_bitcoin') }} @elseif($userCryptoCurrency === 'ETH') {{ __('account.request_info_ethereum') }} @endif</small></h3>

    <br>

    <div class="alert alert-success" style="display: none;" id="success-message"></div>

    <form method="POST" action="{{ route('account.request.send') }}" class="js-request-send">
        @if($userCryptoCurrency === 'BTC')
            <div class="form-group">
                <label>{{ __('account.select_wallet_address') }}</label>
                <select class="form-control" name="address">
                    @foreach($bitcoinAddresses as $bitcoinAddress)
                        <option value="{{ $bitcoinAddress }}">{{ $bitcoinAddress }}</option>
                    @endforeach
                </select>
            </div>
        @elseif($userCryptoCurrency === 'ETH')
            <div class="form-group">
                <label>{{ __('account.from_address') }}</label>
                <select class="form-control js-transfer-selectFromAddress" name="address">
                    @if($addresses['ETH']->isEmpty())
                        <option>{{ __('account.info_no_address') }}</option>
                    @else
                        <option value="">From address</option>
                        @foreach($addresses['ETH'] as $address)
                            <option value="{{ $address->address }}">{{ $address->address }}</option>
                        @endforeach
                    @endif
                </select>
            </div>     
            <div class="form-group">
                <label class="control-label">{{ __('account.to_passphrase_address') }}</label>
                <input autocomplete="off" type="password" class="form-control" name="passphrase">
            </div>
        @endif
        <div class="form-group">
            <label>{{ __('account.email') }}</label>
            <input type="text" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label>{{ __('account.amount') }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="amount" placeholder="0.0000000">
                <span class="input-group-addon">@if($userCryptoCurrency === 'BTC') BTC @elseif($userCryptoCurrency === 'ETH ') ETH @endif</span>
            </div>
        </div>
        <div class="form-group">
            <label>{{ __('account.note') }}</label>
            <textarea class="form-control" name="notes" rows="2"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">{{ __('account.btn_12') }}</button>
    </form>

@endsection
