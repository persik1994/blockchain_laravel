@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="edit-profile-points-wrapper">
                <ul class="nav nav-list well">
                    <li><a href="#basic_info">Basic information</a></li>
                    <li><a href="#change_password">Change password</a></li>

                    @if($settings->document_verification == 1)
                        <li><a href="#document_verification">Document Verification</a></li>
                    @endif

                    @if($settings->phone_verification == 1)
                        <li><a href="#mobile_verification">Mobile Verification</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <h1><small>Edit your profile</small> </h1>

    <br>

    <div class="row text-white" id="basic_info">
        <div class="col-md-12">
            <h4 >Basic information</h4>
            <form action="{{ route('profile.update') }}" class="js-profile-update" method="POST">
                <input type="hidden" name="_method" value="PUT">

                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                </div>
                <button type="submit" class="btn btn-primary">Save base information</button>
            </form>
            <br><br>
        </div>
    </div>

    <div class="row text-white" id="change_password">
        <div class="col-md-12">
            <h4>{{ __('account.password') }}</h4>
            <form action="{{ route('account.security.password') }}" class="js-profile-changePassword" method="POST">
                <div class="form-group">
                    <label class="control-label">{{ __('account.current_password') }}</label>
                    <input type="password" class="form-control" name="current-password">
                </div>
                <div class="form-group">
                    <label class="control-label">{{ __('account.new_password') }}</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group">
                    <label class="control-label">{{ __('account.confirm_new_password') }}</label>
                    <input type="password" class="form-control" name="confirm-password">
                </div>
                <button type="submit" class="btn btn-primary">{{ __('account.btn_13') }}</button>
            </form>
            <br><br>
        </div>
    </div>

    @if($settings->document_verification == 1 || $settings->phone_verification == 1)
        <h2><small>{{ __('account.account_verification') }}</small></h2>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success">
            {!! Session::get('success') !!}
        </div>
    @endif

    <?php

    /*if(isset($_POST['bit_send_sms_code'])) {
        include("includes/NexmoMessage.php");
        $nexmo_sms = new NexmoMessage($settings[nexmo_api_key],$settings[nexmo_api_secret]);
        // Step 2: Use sendText( $to, $from, $message ) method to send a message.
        $rand = rand(00000,99999);
        $number = idinfo($_SESSION['btc_uid'],"mobile_number");
        $insert = $db->query("INSERT btc_sms_codes (uid,sms_code,verified) VALUES ('$_SESSION[btc_uid]','$rand','0')");
        $message = 'Your code for '.$settings[name].' is: '.$rand.' ';
        $info = $nexmo_sms->sendText( '+'.$number, $settings[name], $message );
        $success_13 = str_ireplace("{{number}}",$number,$lang['success_13']);
        echo success($success_13);
    }*/

    /*if(isset($_POST['bit_verify_sms_code'])) {
        $sms_code = protect($_POST['sms_code']);
        $check_code = $db->query("SELECT * FROM btc_sms_codes WHERE uid='$_SESSION[btc_uid]' and sms_code='$sms_code' and verified='0'");
        if(empty($sms_code)) { echo error($lang['error_34']); }
        elseif($check_code->num_rows==0) { echo error($lang['error_35']); }
        else {
            $update = $db->query("UPDATE btc_sms_codes SET verified='1' WHERE uid='$_SESSION[btc_uid]' and sms_code='$sms_code'");
            $update = $db->query("UPDATE btc_users SET mobile_verified='1' WHERE id='$_SESSION[btc_uid]'");
            echo success($lang['success_14']);
        }
    }

    if(isset($_POST['bit_add_number'])) {
        $mobile_number = protect($_POST['mobile_number']);
        if(empty($mobile_number)) { echo error($lang['error_36']); }
        elseif(!is_numeric($mobile_number)) { echo error($lang['error_37']); }
        else {
            $update = $db->query("UPDATE btc_users SET mobile_number='$mobile_number' WHERE id='$_SESSION[btc_uid]'");
            echo success($lang['success_15']);
        }
    }*/

    ?>



    <div class="row text-white" id="document_verification">
        <div class="col-md-12">
            @if($settings->document_verification == 1)
                <br>
                <h3>{{ __('account.document_verification') }}</h3>
                @if($user->document_verified == 1)
                    <p>
                        <span class="text text-success">
                            <i class="fa fa-check"></i> {{ __('account.documents_are_accepted') }}
                        </span>
                    </p>
                @else
                    @if(!empty($user->document_1) && !empty($user->document_2))
                        <p>
                            <span class="text text-info">
                                <i class="fa fa-clock-o"></i> {{ __('account.documents_are_awaiting') }}
                            </span>
                        </p>
                    @else
                        <form action="{{ route('account.verification.documents') }}" class="js-verification-sendDocuments" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>{{ __('account.scanned_copy_id') }}</label>
                                <input type="file" class="form-control" name="document-1">
                            </div>
                            <div class="form-group">
                                <label>{{ __('account.scanned_copy_invoice') }}</label>
                                <input type="file" class="form-control" name="document-2">
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-upload"></i> {{ __('account.btn_20') }}
                            </button>
                        </form>
                    @endif
                @endif
            @endif
        </div>
    </div>

    <div class="row text-white" id="mobile_verification">
        <div class="col-md-12">
            @if($settings->phone_verification == 1)
                <br>
                <h3>{{ __('account.mobile_verification') }}</h3>
                @if($user->mobile_verified == 1)
                    <p>
                        <span class="text text-success">
                            <i class="fa fa-check"></i> {{ __('account.your_mobile_verified') }}
                        </span>
                    </p>
                @else
                    <p>{{ __('account.click_button_sms_code') }}</p>

                    <div>
                        <div>
                            <div class="form-group">
                                <label>{{ __('account.your_mobile_number') }}</label>
                                <input type="text" class="form-control" {{ empty($user->mobile_number) ? '' : 'disabled' }} value="{{ $user->mobile_number }}" id="mobile-number">
                                <small id="change-phone-btn" class="help-block" {!! empty($user->mobile_number) ? 'style="display: none;"' : '' !!}>
                                    <a href="#">(Change phone number)</a>
                                </small>
                            </div>

                            <button id="save-phone-btn" class="btn btn-primary btn-sm js-verification-savePhoneNumber" {!! empty($user->mobile_number) ? '' : 'style="display: none;"' !!}>
                                <i class="fa fa-plus"></i> {{ __('account.btn_23') }}
                            </button>
                        </div>

                        <div {!! empty($user->mobile_number) ? 'style="display: none;"' : '' !!} id="phone-verification-block">
                            <div class="form-group">
                                <label>{{ __('account.enter_sms_code') }}</label>
                                <input type="text" class="form-control" id="sms-code">
                            </div>
                            <button class="btn btn-primary btn-sm js-verification-sendSmsCode"><i class="fa fa-reply"></i> {{ __('account.btn_21') }}</button>
                            <button class="btn btn-primary btn-sm js-verification-verifySmsCode"><i class="fa fa-check"></i> {{ __('account.btn_22') }}</button>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
