@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <div
        id="deposit-history-panel"
        class="panel panel-default history-panel active"
        style="background-color: #192126 !important;"
    >
        <div class="panel-body">
            <h2>
                <small>Sell Notifications</small>
            </h2>
            <div class="timeline">
                @foreach($orders as $order)
                    <div class="timeline-item" id="sell-{{$order->id}}">
                        <div class="timeline-badge">
                            @if($order->status === \App\Models\Order::STATUS_WAITING)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-arrow-circle-up fa-3x"></i>
                                </div>
                            @elseif($order->status === \App\Models\Order::STATUS_PROCESSING)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-info">
                                    <i class="fas fa-arrow-circle-down fa-3x"></i>
                                </div>
                            @elseif($order->status === \App\Models\Order::STATUS_CONFIRMED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                                    <i class="fas fa-arrow-circle-down fa-3x"></i>
                                </div>
                            @elseif($order->status === \App\Models\Order::STATUS_CANCELED)
                                <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                    <i class="fas fa-ban fa-3x"></i>
                                </div>
                            @endif
                        </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow"></div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <span class="timeline-body-title font-blue-madison">
                                        Amount: <b>{{ convertToCrypto($order->crypto_amount, $order->crypto_currency) . ' ' . $order->crypto_currency }} / {{ number_format($order->fiat_amount, 2) . 'USD' }}</b>
                                    </span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                    <span class="font-grey-cascade">
                                        Order id:
                                        <b>
                                            <a href="{{route('account.order.details', $order->id)}}">
                                                {{$order->id}}
                                            </a>
                                        </b>
                                        @if (!empty($order->txid))
                                            <br/>
                                            Transaction id:
                                            <b>
                                                <a href="https://chain.so/tx/BTC/{{ $order->txid }}" target="_blank">
                                                    {{ $order->txid }}
                                                </a>
                                            </b>
                                        @endif
                                        <br/>
                                            Status: <b>
                                            @if($order->status == \App\Models\Order::STATUS_WAITING)
                                                In Admin Process
                                            @elseif($order->status == \App\Models\Order::STATUS_CONFIRMED)
                                                Approved
                                            @elseif($order->status == \App\Models\Order::STATUS_PROCESSING)
                                                Approved and waiting for miners confirmations
                                            @elseif($order->status == \App\Models\Order::STATUS_CANCELED)
                                                Canceled
                                            @endif
                                                    </b>
                                        <br/>
                                            Time: <b>{{ $order->created_at }}</b>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('account.order.templates')
@endsection