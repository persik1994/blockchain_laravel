@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <legend>{{ucfirst($order->order_type)}} Order nr. {{$order->id}}</legend>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Status:</label>
                <span>
                    @if($order->status == \App\Models\Order::STATUS_WAITING)
                        <span class="label label-info">Awaiting</span>
                    @elseif($order->status == \App\Models\Order::STATUS_PROCESSING)
                        <span class="label label-success">Processing</span>
                    @elseif($order->status == \App\Models\Order::STATUS_CONFIRMED)
                        <span class="label label-success">Confirmed</span>
                    @elseif($order->status == \App\Models\Order::STATUS_CANCELED)
                        <span class="label label-danger">Canceled</span>
                    @endif
                </span>
            </div>

            <div class="form-group">
                <label>Transaction ID:</label>
                <a href="https://chain.so/tx/BTC/{{ $order->txid }}">
                   {{$order->txid}}
                </a>
            </div>

            <div class="form-group">
                <label>Crypto Amount:</label>
                <span>
                   {{convertToCrypto($order->crypto_amount, $order->crypto_currency)}} {{$order->crypto_currency}}
                </span>
            </div>

            <div class="form-group">
                <label>Fiat amount:</label>
                <span>
                    {{$order->amount}} USD
                </span>
            </div>

            <div class="form-group">
                <label>Request Time</label>
                <span>
                    {{$order->created_at}}
                </span>
            </div>

            <div class="form-group">
                <label>Updated Time</label>
                <span>
                    {{$order->updated_at}}
                </span>
            </div>
        </div>
    </div>
@endsection