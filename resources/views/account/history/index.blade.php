@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="danger @if($historyType === 'deposit') active @endif">
                <a href="{{route('history', 'type=deposit')}}">Deposit</a>
            </li>
            <li class="danger @if($historyType === 'withdraw') active @endif">
                <a href="{{route('history', 'type=withdraw')}}">Withdraw</a>
            </li>
            <li class="danger @if($historyType === 'buy') active @endif">
                <a href="{{route('history', 'type=buy')}}">Buy</a>
            </li>
            <li class="danger @if($historyType === 'sell') active @endif">
                <a href="{{route('history', 'type=sell')}}">Sell</a>
            </li>
            <li class="danger @if($historyType === 'login') active @endif">
                <a href="{{route('history', 'type=login')}}">Login</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            @if($historyType === 'deposit' || $historyType === 'withdraw')
                                <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    @if($historyType === 'deposit')
                                        <th>Payment Method</th>
                                        <th>Amount</th>
                                    @endif
                                    <th>Amount in {{$userFiatCurrency}}</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($historyItems[$historyType]))
                                    @foreach($historyItems[$historyType] as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            @if($historyType === 'deposit')
                                                <td>@if(isset($item->gateway)) {{\App\Models\Gateway::$gateways[$item->gateway->name]['name']}} @endif</td>
                                                <td>{{$item->current_amount}} {{$item->gateway->currency}}</td>
                                            @endif
                                            <td>
                                                {{$item->amount}} {{$userFiatCurrency}}
                                            </td>
                                            <td>
                                                {{date('Y-m-d H:i:s', $item->time)}}
                                            </td>
                                            <td>
                                                @if($item->status == \App\Models\UserMoney::STATUS_WAITING)
                                                    <span class="label label-info">Awaiting</span>
                                                @elseif($item->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                                    <span class="label label-success">Confirmed</span>
                                                @elseif($item->status == \App\Models\UserMoney::STATUS_CANCELED)
                                                    <span class="label label-danger">Canceled</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{$item->details_route}}" class="btn btn-sm btn-default btn-block">Info</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">No entries</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            @endif

                            @if($historyType === 'buy' || $historyType === 'sell')
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Crypto Amount</th>
                                            <th>Fiat amount</th>
                                            <th>Request Time</th>
                                            <th>Status</th>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($historyItems[$historyType]))
                                        @foreach($historyItems[$historyType] as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>
                                                    {{convertToCrypto($item->crypto_amount, $item->crypto_currency)}} {{$item->crypto_currency}}
                                                </td>
                                                <td>
                                                    {{$item->amount}} USD
                                                </td>
                                                <td>
                                                    {{$item->created_at}}
                                                </td>
                                                <td>
                                                    @if($item->status == \App\Models\Order::STATUS_WAITING)
                                                        <span class="label label-info">Awaiting</span>
                                                    @elseif($item->status == \App\Models\Order::STATUS_PROCESSING)
                                                        <span class="label label-success">Confirmed</span>
                                                    @elseif($item->status == \App\Models\Order::STATUS_CONFIRMED)
                                                        <span class="label label-success">Confirmed</span>
                                                    @elseif($item->status == \App\Models\Order::STATUS_CANCELED)
                                                        <span class="label label-danger">Canceled</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{$item->details_route}}" class="btn btn-sm btn-default btn-block">Info</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">No entries</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            @endif

                            @if($historyType === 'login')
                                <div class="text-white">
                                    <h4 style="line-height: 1.4px;">Your location history for the last 30 days</h4>
                                    <small>If you do not recognize any of them, please, <a href="{{route('profile.edit')}}#change_password" target="_blank">change your password</a></small>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>IP address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($historyItems[$historyType]))
                                            @foreach($historyItems[$historyType] as $item)
                                                <tr>
                                                    <td>{{$item->time}}</td>
                                                    <td>
                                                        {{$item->ip_address}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">No entries</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div id="show-row-information">Show {{ $historyItems[$historyType]->firstItem() }} to {{ $historyItems[$historyType]->lastItem() }} from {{ $historyItems[$historyType]->total() }} rows</div>
                    </div>
                    <div class="col-sm-7">
                        <div class="pull-right">
                            {!! $historyItems[$historyType]->appends(Request::except('page'))->render() !!}
                        </div>
                    </div>

                    <div class="col-sm-12">
                        @if($historyType === 'login')
                            <br>
                            <div class="text-white">
                                <h4 style="line-height: 1.4px;">Log out on all devices</h4>
                                <small>
                                    If you've logged in to {{$settings->name}} from other devices and no longer have access to them, you can <a href="#">log out on all devices</a>
                                </small>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection