@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    <div class="row">
        <div class="col-md-6">
            <div class="count-info-solid clearfix">
                <i class="{{ config('currencies.crypto_currencies')[$userCryptoCurrency]['icon'] }}"></i>
                <p>
                    <span class="number">
                        {{ $userCryptoBalance }} {{$userCryptoCurrency}} /
                        @switch($userCryptoCurrency)
                            @case('BTC')
                            {{ number_format($user->getBitcoinMoneyBalance($userFiatCurrency), 2) }}
                            @break
                            @case('ETH')
                            {{ number_format($user->getEthereumMoneyBalance($userFiatCurrency), 2) }}
                            @break
                        @endswitch
                        {{$userFiatCurrency}}
                    </span>
                    <span class="text">{{ __('account.crypto_balance') }}</span>
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="count-info-solid clearfix" id="user-fiat-balance">
                <i class="{{ config('currencies.fiat_currencies')[$userFiatCurrency]['icon'] }}"></i>
                <p>
                    <span class="number">{{ number_format($user->availableAmount($userFiatCurrency), 2) }} {{$userFiatCurrency}}</span>
                    <span class="text">{{ __('account.fiat_balance') }}</span>
                </p>
            </div>
        </div>
    </div>

    @if($userCryptoCurrency === 'BTC')
        <div class="wallet_chart" style="height: 400px;">
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
                <div id="tradingview_3e98e" style="height: 400px;"></div>
                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                <script type="text/javascript">
                    new TradingView.widget(
                        {
                            "autosize": true,
                            "symbol": "COINBASE:BTCUSD",
                            "interval": "D",
                            "timezone": "Etc/UTC",
                            "theme": "Dark",
                            "style": "3",
                            "locale": "en",
                            "toolbar_bg": "#f1f3f6",
                            "enable_publishing": false,
                            "save_image": false,
                            "container_id": "tradingview_3e98e"
                        }
                    );
                </script>
            </div>
            <!-- TradingView Widget END -->
        </div>
    @endif

    @if($userCryptoCurrency === 'ETH')
        <div class="wallet_chart form-group">
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
                <div id="tradingview_3e98e" style="height: 400px;"></div>
                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                <script type="text/javascript">
                    new TradingView.widget(
                        {
                            "autosize": true,
                            "symbol": "COINBASE:ETHUSD",
                            "interval": "D",
                            "timezone": "Etc/UTC",
                            "theme": "Dark",
                            "style": "3",
                            "locale": "en",
                            "toolbar_bg": "#f1f3f6",
                            "enable_publishing": false,
                            "save_image": false,
                            "container_id": "tradingview_3e98e"
                        }
                    );
                </script>
            </div>
            <!-- TradingView Widget END -->
        </div>
    @endif


    @if($userCryptoCurrency === 'BTC')
        <div id="wallet-addresses">
            @include('account.wallet.btc-addresses', [
                'userAddresses' => $userAddresses
            ])
        </div>
    @elseif($userCryptoCurrency === 'ETH')
        <div id="wallet-addresses">
            @include('account.wallet.eth-addresses', [
                'userAddresses' => $userAddresses
            ])
        </div>    
    @endif


    @if(!empty($transactions))
        <div class="panel panel-default">
            <div class="panel-body">
                <h2>
                    <small>{{ __('account.latest_transactions') }}</small>
                </h2>

                <div class="timeline">
                    @foreach($transactions as $transaction)
                        <div class="timeline-item">
                            <div class="timeline-badge">
                                @if($transaction['category'] === 'send')
                                    <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-danger">
                                        <i class="fas fa-arrow-circle-up fa-3x"></i>
                                    </div>
                                @else
                                    <div style="margin-left:20px; margin-top: 35px; font-size: 16px;" class="text text-success">
                                        <i class="fas fa-arrow-circle-down fa-3x"></i>
                                    </div>
                                @endif
                            </div>
                            <div class="timeline-body">
                                <div class="timeline-body-arrow"></div>
                                <div class="timeline-body-head">
                                    <div class="timeline-body-head-caption">
                                        <a href="#" class="timeline-body-title font-blue-madison">
                                            {{ __($transaction['category'] === 'send' ? 'account.sent' : 'account.received') }}
                                            {{ satoshitize($transaction['amount']) }} BTC
                                        </a>
                                    </div>
                                </div>
                                <div class="timeline-body-content">
                                    <span class="font-grey-cascade">
                                        <br/>
                                        {{ __('account.transaction_id') }}: <b><a href="https://chain.so/tx/BTC/{{ $transaction['txid'] }}" target="_blank">{{ $transaction['txid'] }}</a></b>
                                        <br/>
                                        {{ __('account.address') }}: <b>{{ $transaction['address'] }}</b>
                                        <br/>
                                        {{ __('account.confirmations') }}: <b>{{ $transaction['confirmations'] }}</b>
                                        <br/>   
                                        {{ __('account.time') }}: <b>{{ date("d/m/Y H:i", $transaction['time']) }}</b>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endsection
