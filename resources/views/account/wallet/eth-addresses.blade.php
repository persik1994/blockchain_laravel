<div class="panel panel-default">
    <div class="panel-body">
        <h2>
            <small>{{ __('account.addresses') }}</small>
            <span class="pull-right">
                <small>
                    <a href="#" class="btn btn-default btn-sm js-wallet-createAddressModal btn_button"
                       data-title="{{ __('account.this_modal_create_eth_address') }}">
                        <i class="fas fa-plus"></i> {{ __('account.btn_5') }}
                    </a>    
                    <a class="btn btn-sm btn-default js-wallet-openNewTransactionModal btn_button"
                       href="#" data-toggle="tooltip" data-title="{{ __('account.new_transaction') }}"
                       data-placement="top" title="{{ __('account.btn_28') }}">
                            <i class="fas fa-arrow-circle-up" style="margin: 0;"></i> {{ __('account.btn_28') }}
                        </a>
                </small>
            </span>
        </h2>

        <table class="table">
            <thead>
            <tr>
                <th>{{ __('account.label') }}</th>
                <th>{{ __('account.address') }}</th>
                <th>{{ __('account.balance') }}</th>
                <th style="text-align: right;">{{ __('account.action') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($userAddresses as $userAddress)
                    @if($userAddress->crypto_currency === 'ETH')
                        <tr>
                            <td>{{ $userAddress->label }}</td>
                            <td>{{ $userAddress->address }}</td>
                            <td>{{ $userAddress->balance }}</td>
                            <td style="text-align: right;">
                                <a class="btn btn-circle btn-sm btn-icon btn-default js-wallet-openReceiveBitcoinsModal btn_button"
                                <a class="btn btn-circle btn-sm btn-icon btn-default js-wallet-openReceiveBitcoinsModal"
                                   data-address="{{ $userAddress->address }}" data-title="{{ __('account.receive_bitcoins') }}"
                                   href="#" data-toggle="tooltip" data-placement="top" title="{{ __('account.btn_7') }}">
                                    <i class="fas fa-arrow-circle-down" style="margin: 0;"></i>
                                </a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>