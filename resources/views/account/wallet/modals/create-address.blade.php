<form action="{{ route('account.address.create') }}" class="js-wallet-createAddress">
    <div class="form-group">
        <label class="control-label">{{ __('account.label') }}</label>
        <input type="text" class="form-control" id="input-address-label" name="label" placeholder="{{ __('account.label_info') }}">
    </div>
    @if($userCryptoCurrency === 'ETH')
        <div class="form-group">
            <label class="control-label">{{ __('account.label_password') }}</label>
            <input type="password" class="form-control" id="input-password-label" name="password" placeholder="{{ __('account.password_info') }}">
        </div>
    @endif
    <p>{!! __('account.label_info_2') !!}</p>
    <button class="btn btn-primary"><i class="fas fa-plus"></i> {{ __('account.btn_26') }}</button>
</form>