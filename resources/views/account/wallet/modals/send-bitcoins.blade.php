<div class="row">
    <div class="col-md-12">
        <form class="js-wallet-sendBitcoins" method="post" action="{{ route('account.transfer.request') }}">
            @if($userCryptoCurrency === 'ETH')
                <div class="form-group">
                    <label>{{ __('account.from_address') }}</label>
                    <select class="form-control js-transfer-selectFromAddress" name="from-address">
                        @if($addresses['ETH']->isEmpty())
                            <option>{{ __('account.info_no_address') }}</option>
                        @else
                            <option value="">From address</option>
                            @foreach($addresses['ETH'] as $address)
                                <option value="{{ $address->address }}">{{ $address->address }}</option>
                            @endforeach
                        @endif
                    </select>
                    <small>{{ __('account.select_from_where_to_select_address') }}</small>
                </div>     
                <div class="form-group">
                    <label class="control-label">{{ __('account.to_passphrase_address') }}</label>
                    <input autocomplete="off" type="password" class="form-control" name="passphrase">
                    <small>{{ __('account.select_passphrase') }}</small>
                </div>                   
            @endif
            <div class="form-group">
                <label class="control-label">{{ __('account.to_wallet_address') }}</label>
                <input autocomplete="off" type="text" class="form-control" name="address">
            </div>
            <div class="form-group">
                <label class="control-label">{{ __('account.amount') }}</label>
                <input autocomplete="off" type="text" class="form-control" name="amount" placeholder="0.0000000">
            </div>
            @if(!empty($user->secret_pin))
                <div class="form-group">
                    <label class="control-label">{{ __('account.your_secret_pin') }}</label>
                    <input type="password" class="form-control" name="secret-pin" autocomplete="new-password">
                </div>
            @endif            
            <button type="submit" class="btn btn-primary">{{ __('account.btn_6') }}</button>
            <span class="pull-right">
                @if($userCryptoCurrency === 'BTC')
                    {{ __('account.error_30', ['amount' => formatBitcoin($balance), 'currency' => 'BTC']) }}
                @elseif($userCryptoCurrency === 'ETH')
                    {{ __('account.error_30', ['amount' => formatEthereum($txFeeEthereum), 'currency' => 'ETH']) }}
                @endif
            </span>
        </form>
    </div>
</div>