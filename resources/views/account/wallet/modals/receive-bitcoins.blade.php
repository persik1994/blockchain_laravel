<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label>{{ __('account.wallet_address') }}</label>
            <input type="text" class="form-control" disabled value="{{ $address }}">
        </div>
        <div class="form-group">
            <label>{{ __('account.amount') }}</label>
            <input type="text" class="form-control" id="qr-code-amount" name="amount" placeholder="0.000000">
        </div>
        <button type="button" class="btn btn-primary js-wallet-generateQRCode" data-address="{{ $address }}">{{ __('account.btn_27') }}</button>
    </div>
    <div class="col-md-4">
        <div style="text-align: center;">
            <img id="qr-code-image" style="display: none;">
        </div>
    </div>
</div>