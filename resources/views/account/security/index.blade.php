@extends('layouts.account', [
    'settings' => $settings
])

@section('content')
    <h1><small>{{ __('account.security') }}</small> </h1>

    <div class="row">
        <div class="col-md-12">
            <div class="text-white">
                <h4>{{ __('account.2fa_title') }}</h4>
                <p><small>{{ __('account.secret_pin_info') }}</small></p>
            </div>
            @if(empty($user->google2fa_secret))
                <div class="alert alert-danger" style="line-height: normal;">
                    <p>Currently two-factor authentication is disabled on your account. To increase the protection level we recommend to enable it!</p>
                    <small style="margin-top: 15px; display: block;">Notice: Every time you disable and then enable this option, you will need to re-scan the QR code or enter it manually in <strong>Authenticator</strong> application</small>

                    <img src="{{ $qrImage }}" alt="QR code" style="margin: 15px 0;">

                    <br>

                    You can download this application by following the link:
                    <ul style="margin-top: 10px;">
                        <li><a target="_blank" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8">Authenticator for iOS</a></li>
                        <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Authenticator for Android</a></li>
                    </ul>

                    <small style="margin-top: 15px; display: block;">Notice: Every time you disable and then enable this option, you will need to re-scan the QR code or enter it manually in <strong>Authenticator</strong> application</small>
                </div>

                <form action="{{ route('account.security.enable-2fa') }}" method="post" class="text-right">
                    <input type="hidden" name="google-2fa-secret" value="{{$google_2fa_secret}}">
                    @csrf
                    <button class="btn btc-primary" type="submit">Enable two-factor authentication</button>
                </form>
            @else
                <div class="alert alert-success" style="line-height: normal;">
                    <strong>Two-factor authentication is enabled.</strong>
                    Bellow you can scan the QR code with <strong>Authenticator</strong> application on your mobile phone.

                    <img src="{{ $qrImage }}" alt="QR code" style="margin: 15px 0;">

                    <p>Or you can enter it manually: <strong>{{ $user->google2fa_secret }}</strong></p>

                    <br>

                    You can download this application by following the link:
                    <ul style="margin-top: 10px;">
                        <li><a target="_blank" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8">Authenticator for iOS</a></li>
                        <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Authenticator for Android</a></li>
                    </ul>

                    <small style="margin-top: 15px; display: block;">Notice: Every time you disable and then enable this option, you will need to re-scan the QR code or enter it manually in <strong>Authenticator</strong> application</small>
                </div>

                <form action="{{ route('account.security.disable-2fa') }}" method="post" class="text-right">
                    @csrf
                    <button class="btn btc-primary" type="submit">Disable two-factor authentication</button>
                </form>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-white" style="margin-top: 70px;">
            <h4>{{ __('account.secret_pin') }}</h4>
            <p>{{ __('account.secret_pin_info') }}</p>

            <form action="{{ route('account.security.pin') }}" id="pin-form" method="POST">
                <div class="form-group show-after-save-pin" {!! empty($user->secret_pin) ? 'style="display: none;"' : '' !!}>
                    <label class="control-label">{{ __('account.current_secret_pin') }}</label>
                    <input type="password" class="form-control" name="current-pin">
                </div>
                <div class="form-group">
                    <label class="control-label">{{ __('account.new_secret_pin') }}</label>
                    <input type="password" class="form-control" name="new-pin">
                </div>
                <div class="form-group">
                    <label class="control-label">{{ __('account.confirm_secret_pin') }}</label>
                    <input type="password" class="form-control" name="confirm-pin">
                </div>
            </form>
            <button {!! empty($user->secret_pin) ? '' : 'style="display: none;"' !!} class="btn btn-primary js-security-changePin hide-after-save-pin">{{ __('account.btn_16') }}</button>
            <button {!! empty($user->secret_pin) ? 'style="display: none;"' : '' !!} class="btn btn-primary js-security-changePin show-after-save-pin">{{ __('account.btn_14') }}</button>
            <button {!! empty($user->secret_pin) ? 'style="display: none;"' : '' !!} class="btn btn-danger js-security-removePin show-after-save-pin">{{ __('account.btn_15') }}</button>
        </div>
    </div>

    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="text-white">
                <h4>Other Security Measures</h4>
            </div>

            <br>

            <div class="text-white">
                <p>
                    <small><strong>Do not use the same password on different websites.</strong></small>
                </p>
                <p>
                    <small>Do not use <strong>Tor browser</strong>. <a href="/faq#ulbc-tor-browser">Using a Tor browser puts you in the risk getting your bitcoins stolen.</a></small>
                </p>
                <p>
                    <small>Do not get involved in transactions outside the LocalBitcoins.</small>
                </p>
                <p>
                    <small>Do not use the website from a <strong>shared computers or devices, like ones in public internet cafes</strong>, as they may have keyloggers installed to steal your user credentials.</small>
                </p>
                <p>
                    <small>When logging in to the website, <strong>read the browser address bar</strong> and check that you are logging into <strong>trade.otceed.com</strong> and not a phishing domain. Make sure the spelling is trade.otceed.com exactly, as the phishers, especially email phishers, often register domain names resembling trade.otceed.com domain name. (ie: trade.otced.com). </small>
                </p>
                <p class="text-center">
                    <img alt="" src="/img/otceed_link.png">
                </p>
                <p><small>Do not click LocalBitcoins Google advertisements. </small></p>
                <p><small>If possible, when accessing Bitcoin wallets, do this from a dedicated computer you have reserved for financial tasks only. Do not use this computer for other tasks. </small></p>
                <p><small>Do not install third party software, pirated software or browser addons you cannot trust 100%. This greatly reduces the risk of getting Bitcoin stealing malware infection on the computer.</small></p>
            </div>
        </div>
    </div>

@endsection
