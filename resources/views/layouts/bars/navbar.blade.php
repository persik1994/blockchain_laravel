<nav class="navbar navbar-default navbar-fixed-top">
    <div class="nav-topbar clearfix ">
                <div class="col-md-3">
                    <a href="{{ route('home') }}" class="header_logo_link">
                        <span class="header_logo_link_text">
                            {{$settings->name}}
                            {{--<img src="/public/img/login-invert2.png" class="header_logo">--}}
                        </span>
                    </a>
                </div>
                <div class="col-md-9">
                    <ul class="nav navbar-nav navbar-right menu_left">
                        @if(\Auth::user() && $user2FaAuthenticated)
{{--                        <li class="left_menu"><a href="{{ route('trade', \Auth::user()->crypto_currency . '-' . \Auth::user()->fiat_currency) }}"--}}
{{--                               class="menu_left_item {{ Route::is('trade') ? 'active' : '' }}">TRADE</a>--}}
                            {{--</li>--}}

                            @php
                                $userUnviewedItems['buys'] = \Auth::user()->getCountUnviewedItems('buys');
                                $userUnviewedItems['sells'] = \Auth::user()->getCountUnviewedItems('sells');
                                $userUnviewedItems['transfers'] = \Auth::user()->getCountUnviewedItems('transfers');
                            @endphp

                            <li class="left_menu"><a href="{{ route('wallet') }}" class="menu_left_item">{{ __('account.menu_dashboard') }}</a></li>
                            <li class="left_menu">
                                <a href="{{ route('account.order.buy') }}" class="menu_left_item">
                                    {{ __('account.menu_buy_bitcoins') }}

                                    <span class="pull-right-container navbar-notifications-count">
                                        @if($userUnviewedItems['buys'])
                                            <span
                                                class="label label-primary pull-right navbar-notifications-count__info-buy"
                                                data-val="{{$userUnviewedItems['buys']}}"
                                            >{{$userUnviewedItems['buys'] != 0 ? $userUnviewedItems['buys'] : ''}}</span>
                                        @else
                                            <span class="label label-primary pull-right navbar-notifications-count__info-buy"></span>
                                        @endif
                                    </span>
                                </a>
                            </li>
                            <li class="left_menu">
                                <a href="{{ route('account.order.sell') }}" class="menu_left_item">
                                    {{ __('account.menu_sell_bitcoins') }}

                                    <span class="pull-right-container navbar-notifications-count">
                                        @if($userUnviewedItems['sells'])
                                            <span
                                                    class="label label-primary pull-right navbar-notifications-count__info-sell"
                                                    data-val="{{$userUnviewedItems['sells']}}"
                                            >{{$userUnviewedItems['sells'] != 0 ? $userUnviewedItems['sells'] : ''}}</span>
                                        @else
                                            <span class="label label-primary pull-right navbar-notifications-count__info-sell"></span>
                                        @endif
                                    </span>
                                </a>
                            </li>
                            <li class="left_menu">
                                <a href="{{ route('transfer') }}" class="menu_left_item">
                                    {{ __('account.menu_transfer_bitcoins') }}

                                    <span class="pull-right-container navbar-notifications-count">
                                        @if($userUnviewedItems['transfers'])
                                            <span
                                                    class="label label-primary pull-right navbar-notifications-count__info-transfer"
                                                    data-val="{{$userUnviewedItems['buys']}}"
                                            >{{$userUnviewedItems['transfers'] != 0 ? $userUnviewedItems['transfers'] : ''}}</span>
                                        @else
                                            <span class="label label-primary pull-right navbar-notifications-count__info-transfer"></span>
                                        @endif
                                    </span>
                                </a>
                            </li>
                            <li class="left_menu"><a href="{{ route('request') }}" class="menu_left_item">{{ __('account.menu_request_bitcoins') }}</a></li>
                            <li class="left_menu"><a href="{{ route('transactions') }}" class="menu_left_item">{{ __('account.menu_transactions') }}</a></li>

                        @else
                            {{--<li class="left_menu"><a href="{{ route('trade', 'BTC-USD') }}" class="menu_left_item {{ Route::is('trade') ? 'active' : '' }}">TRADE</a></li>--}}
                            <li class="left_menu"><a href="{{ route('home') }}" class="menu_left_item {{ Route::is('home') ? 'active' : '' }}">{{ __('account.menu_home') }}</a></li>
                            <li class="left_menu"><a href="{{ url('/buy') }}" class="menu_left_item {{ Route::is('buy') ? 'active' : '' }}">{{ __('account.menu_buy_bitcoins') }}</a></li>
                            <li class="left_menu"><a href="{{ url('/sell') }}" class="menu_left_item {{ Route::is('sell') ? 'active' : '' }}">{{ __('account.menu_sell_bitcoins') }}</a></li>
                            <li class="left_menu"><a href="{{ url('/send') }}" class="menu_left_item {{ Route::is('send') ? 'active' : '' }}">{{ __('account.menu_transfer_bitcoins') }}</a></li>
                            <li class="left_menu"><a href="{{ url('/receive') }}" class="menu_left_item {{ Route::is('receive') ? 'active' : '' }}">{{ __('account.menu_request_bitcoins') }}</a></li>
                        @endif


                    </ul>

                    @if(\Auth::user() && $user2FaAuthenticated && \Auth::user()->type == 'user')
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-item dropdown left_menu">
                                <a class="nav-link dropdown-toggle menu_left_item" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"><i class="fa fa-user fa-fw"></i>  {{\Auth::user()->name}} <span class="caret"></span></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <div class="user_dropdown_list"><a href="{{route('profile.edit')}}" class="menu_left_item"><i class="fa fa-user fa-fw"></i> Edit profile</a></div>
                                    <div class="user_dropdown_list"><a href="{{route('security')}}" class="menu_left_item"><i class="fa fa-lock"></i> Security</a></div>
                                    <div class="user_dropdown_list"><a href="{{route('history')}}" class="menu_left_item"><i class="fas fa-history"></i> History</a></div>
                                    <div class="user_dropdown_list"><a href="{{ url('/support') }}" class="menu_left_item"><i class="fa fa-ambulance fa-fw"></i> Support</a></div>
                                    <hr>
                                    <div class="dropdown-divider">
                                    </div><a class="dropdown-item menu_left_item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}</a>
                                </div>
                            </li>
                        </ul>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <ul class="nav navbar-nav navbar-right">
                            <li class="right_menu_item"><a href="{{ route('login') }}" class="menu_right_item">SIGN IN</a></li>
                            <li class="right_menu_item"><a href="{{ route('register') }}" class="menu_right_item">SIGN UP</a></li>
                        </ul>
                    @endauth
                </div>
    </div>
</nav>