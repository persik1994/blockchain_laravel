@php
    $userUnviewedItems['deposits'] = \Auth::user()->getCountUnviewedItems('deposits');
    $userUnviewedItems['withdraws'] = \Auth::user()->getCountUnviewedItems('withdraws');
@endphp
<div class="col-md-3 main_sidebar" style="position: fixed; z-index: 999; margin-top: 90px;" >

    @auth
        <a style="color: #fff" href="{{ route('deposit-money') }}"
           class="sc-hAXbOi ppkAR sc-bdVaJa fapzTF {{ Route::currentRouteName() === 'deposit-money' ? 'active' : '' }}">
            {{ mb_strtoupper(__('account.deposit_money')) }}

            <span class="pull-right-container">
                @if($userUnviewedItems['deposits'])
                    <span
                        class="label label-primary pull-right menu__item-info-money-deposit"
                        data-val="{{$userUnviewedItems['deposits']}}"
                    >{{$userUnviewedItems['deposits'] != 0 ? $userUnviewedItems['deposits'] : ''}}</span>
                @else
                    <span class="label label-primary pull-right menu__item-info-money-deposit"></span>
                @endif
            </span>
        </a>
        <a style="color: #fff" href="{{ route('withdraw-money') }}"
           class="sc-hAXbOi ppkAR sc-bdVaJa fapzTF {{ Route::currentRouteName() === 'withdraw-money' ? 'active' : '' }}">
            {{ mb_strtoupper(__('account.withdraw_money')) }}

            <span class="pull-right-container">
                @if($userUnviewedItems['withdraws'])
                    <span
                        class="label label-primary pull-right menu__item-info-money-withdraw"
                        data-val="{{$userUnviewedItems['withdraws']}}"
                    >{{$userUnviewedItems['withdraws'] != 0 ? $userUnviewedItems['withdraws'] : ''}}</span>
                @else
                    <span class="label label-primary pull-right menu__item-info-money-withdraw"></span>
                @endif
            </span>
        </a>
    @else
        <a href="{{route('login')}}" class="sc-hAXbOi ppkAR sc-bdVaJa fapzTF">SIGN IN</a>
        <a href="{{route('register')}}" class="sc-hAXbOi ppkAR sc-bdVaJa fapzTF">SIGN UP</a>
    @endauth

    @if($settings->plugin_buy_bitcoins || $settings->plugin_sell_bitcoins)
        <div class="forma">
            {{--js-account-menuItemClass--}}
            <div class="sc-hAXbOi ppkAR sc-bdVaJa fapzTF">
                ORDER FORM

                {{--<span style="position: absolute; right: 20px;">--}}
                  {{--<i class="fas fa-angle-down"></i>--}}
                {{--</span>--}}

            </div>
            <div class="sc-cCVOAp kefjuS sc-bdVaJa fapzTF">
                <form id="order-form" action="{{ route('order.request') }}" method="post" class="js-order-sendRequest">
                    <div class="sc-guztPN gPfxFb sc-jtRfpW jjiJAo order-type-wrapper"
                         data-user-crypto-currency="{{$userCryptoCurrency}}"
                         data-user-fiat-currency="{{$userFiatCurrency}}"
                    >
                        @if($settings->plugin_buy_bitcoins)
                            <div data-type="buy" class="sc-kTUwUJ kvYUlk qx js-order-changeOrderType">BUY</div>
                        @endif
                        @if($settings->plugin_sell_bitcoins)
                            <div data-type="sell" class="sc-kTUwUJ jHbvLz js-order-changeOrderType
                                {{ !$settings->plugin_buy_bitcoins ? 'qy' : '' }}">SELL
                            </div>
                        @endif
                        <input type="hidden" name="order-type"
                               value="{{ $settings->plugin_buy_bitcoins ? 'buy' : 'sell' }}" id="order-type-input">
                        <input type="hidden" name="order-crypto-currency"
                               value="{{ $userCryptoCurrency }}">
                        <input type="hidden" name="order-fiat-currency"
                               value="{{ $userFiatCurrency }}">
                    </div>

                    <div class="sc-bdVaJa fapzTF">
                        <div class="sc-bdVaJa keODMS">
                            <div class="sc-cvbbAY exGoF sc-bdVaJa cbNbaj">
                                <div class="sc-brqgnP kYlebK sc-bdVaJa fapzTF">
                                    {{--<span role="button" class="sc-eHgmQL hVfXXr zx strategy-type-item js-order-changeStrategyType"--}}
                                          {{--data-type="market">Market</span>--}}
                                    {{--<span role="button" class="sc-eHgmQL hVfXXr strategy-type-item js-order-changeStrategyType" data-type="limit">Limit</span>--}}
                                    {{--<span role="button" class="sc-eHgmQL hVfXXr strategy-type-item js-order-changeStrategyType" data-type="stop">Stop</span>--}}
                                    <div class="sc-jWBwVP kMfcDZ" style="transform: transateX(0px); width: 100%;"></div>
                                    <input type="hidden" name="strategy-type" value="market" id="strategy-type-input">
                                </div>
                                <div class="sc-bdVaJa keODMS"></div>
                            </div>
                        </div>
                    </div>
                    <div class="sc-fvLVrH cBptqf sc-bdVaJa cbNbaj">
                        <div class="sc-bdVaJa bjBPca">
                            <span class="sc-jwKygS brJNHi sc-htoDjs jndoRX">{{ __('account.amount') }}</span>
                            <div class="sc-lhVmIH gHjlyg sc-bdVaJa fapzTF">
                                <a class="sc-btzYZH smLZv sc-EHOje cXwJgs">Max</a>
                                <input id="amount-input" name="amount"
                                       class="sc-bYSBpT dkseXk js-order-convertToBitcoins" autocomplete="off"
                                       placeholder="0.00" data-pup="830884269" type="text">
                                <div class="sc-elJkPf fVqAZl sc-bdVaJa jnkYAA" id="f3">{{$userFiatCurrency}}</div>
                            </div>
                        </div>
                        <div class="sc-bdVaJa bjBPca" id="stop-price-field" style="display: none;">
                            <span class="sc-jwKygS brJNHi sc-htoDjs jndoRX">{{ __('account.stop_price') }}</span>
                            <div class="sc-lhVmIH gHjlyg sc-bdVaJa fapzTF">
                                <input name="stop-price" class="sc-bYSBpT dkseXk" autocomplete="off" placeholder="0.00"
                                       data-pup="830884269" value="" type="number">
                                <div class="sc-elJkPf fVqAZl sc-bdVaJa jnkYAA" id="f4">USD</div>
                            </div>
                        </div>
                        <div class="sc-bdVaJa bjBPca" id="limit-price-field" style="display: none;">
                            <span class="sc-jwKygS brJNHi sc-htoDjs jndoRX">{{ __('account.limit_price') }}</span>
                            <div class="sc-lhVmIH gHjlyg sc-bdVaJa fapzTF">
                                <input name="limit-price" class="sc-bYSBpT dkseXk " autocomplete="off" placeholder="0.00"
                                       data-pup="830884269" value="" type="number">
                                <div class="sc-elJkPf fVqAZl sc-bdVaJa jnkYAA" id="f5">{{$userFiatCurrency}}</div>
                            </div>
                        </div>
                        <div class="sc-eSePXt hQhBLc"></div>
                        <div class="sc-bdVaJa cbNbaj js-buy-typeValues" id="order-info-wrapper">
                            <div class="sc-lnrBVv hsSNKo sc-bdVaJa iCiZvJ">
                                <span class="sc-htoDjs iZQVXZ" id="f1">Fee ({{$userFiatCurrency}}) ≈</span>
                                <span class="sc-htoDjs gnfXdV">
                                    <span id="fee-value" class="sc-rBLzX fhUrxz sc-htoDjs gnfXdV">0</span>
                                </span>
                            </div>
                            <div class="sc-lnrBVv hsSNKo sc-bdVaJa iCiZvJ">
                                <span class="sc-htoDjs iZQVXZ" id="f2">Total ({{$userCryptoCurrency}}) ≈</span>
                                <span class="sc-htoDjs gnfXdV">
                                    <!--<span class="sc-rBLzX fhUrxz sc-htoDjs gnfXdV">0.00000000</span>-->
                                    <input style="color: #fff; text-align: right; border:0; background:#3b3b3b"
                                           disabled value="{{convertToCrypto(0, $userCryptoCurrency)}}"
                                           id="amount-receive" type="text">
                                </span>
                            </div>
                        </div>
                        <button type="submit"
                                class="sc-jlyJG hfLvHd {{ $settings->plugin_buy_bitcoins ? 'qx' : 'qy' }}"
                                id="place">
                            {{ $settings->plugin_buy_bitcoins ? 'PLACE BUY ORDER' : 'PLACE SELL ORDER' }}
                        </button>

                        <div class="order-info">
                            @foreach ($cryptoCurrencies as $cryptoKey => $cryptoCurrency)
                                <input type="hidden" name="buy-fee-{{$cryptoKey}}" value="{{$cryptoBuyFee[$cryptoKey]}}">
                                <input type="hidden" name="sell-fee-{{$cryptoKey}}" value="{{$cryptoSellFee[$cryptoKey]}}">
                                @foreach ($fiatCurrencies as $fiatKey => $fiatCurrency)
                                    <input type="hidden" name="buy-price-{{$cryptoKey}}-{{$fiatKey}}" value="{{$cryptoBuyPrice[$cryptoKey][$fiatKey]}}">
                                    <input type="hidden" name="sell-price-{{$cryptoKey}}-{{$fiatKey}}" value="{{$cryptoSellPrice[$cryptoKey][$fiatKey]}}">
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
</div>