<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="mirak.no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$settings->name}} - @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    @if(Request::is('admin', 'admin/*'))
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @else
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @endif

    @yield('styles')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div id="app" class="wrapper">
        @include('layouts.admin.bars.navbar')
        @include('layouts.admin.bars.sidebar')

        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/admin.js') }}" defer></script>

    @yield('scripts')
</body>
</html>