<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            {{--<div class="pull-left image">--}}
                {{--<img src="{{ \Auth::user()->urlUserImage() }}" class="img-circle" alt="User Image">--}}
            {{--</div>--}}
            <div class="pull-left info" style="height: 20px;position: static;margin-bottom: 20px;">
                <h5>Welcome, {{ \Auth::user()->name }}</h5>
                {{--<a href="#"><i class="fa fa-user text-success"></i> Role</a>--}}
            </div>
        </div>
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-btn">--}}
                    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                    {{--</button>--}}
                {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}

        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">General</li>
                <li class="sidebar-menu__item @if(Route::is('admin.dashboard')) active @endif">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="fas fa-tachometer-alt"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.gateways')) active @endif">
                    <a href="{{route('admin.gateways')}}">
                        <i class="far fa-credit-card"></i> <span>Gateways</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.transfers')) active @endif">
                    <a href="{{route('admin.transfers')}}">
                        <i class="far fa-credit-card"></i> <span>Transfers</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('transfers_unviewed_by_admin'))
                              <span
                                class="label label-primary pull-right menu__item-info-money-transfer"
                                data-val="{{\Illuminate\Support\Facades\Cache::get('transfers_unviewed_by_admin')}}"
                              >{{\Illuminate\Support\Facades\Cache::get('transfers_unviewed_by_admin') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('transfers_unviewed_by_admin') : ''
                                }}</span>
                            @else
                                <span class="label label-primary pull-right menu__item-info-money-transfer"></span>
                            @endif
                        </span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.money.deposits')) active @endif">
                    <a href="{{route('admin.money.deposits')}}">
                        <i class="far fa-credit-card"></i> <span>Money Deposits</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('deposits_unviewed_by_admin'))
                              <span
                                class="label label-primary pull-right menu__item-info-money-deposit"
                                data-val="{{\Illuminate\Support\Facades\Cache::get('deposits_unviewed_by_admin')}}"
                              >{{\Illuminate\Support\Facades\Cache::get('deposits_unviewed_by_admin') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('deposits_unviewed_by_admin') : ''
                                }}</span>
                            @else
                                <span class="label label-primary pull-right menu__item-info-money-deposit"></span>
                            @endif
                        </span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.money.withdrawals')) active @endif">
                    <a href="{{route('admin.money.withdrawals')}}">
                        <i class="far fa-credit-card"></i> <span>Money withdrawals</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('withdraws_unviewed'))
                                <span
                                        class="label label-primary pull-right menu__item-info-money-withdraw"
                                        data-val="{{\Illuminate\Support\Facades\Cache::get('withdraws_unviewed')}}"
                                >{{\Illuminate\Support\Facades\Cache::get('withdraws_unviewed') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('withdraws_unviewed') : ''
                                }}</span>
                            @else
                                <span class="label label-primary pull-right menu__item-info-money-withdraw"></span>
                            @endif
                        </span>
                    </a>
                </li>
                <li class="sidebar-menu__item
                    @if(Route::is('admin.trades') && Route::current()->parameters['type'] === 'buy') active @endif"
                >
                    <a href="{{route('admin.trades', 'buy')}}">
                        <i class="fas fa-exchange-alt"></i> <span>Buy Trades</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('buys_unviewed_by_admin'))
                                <span
                                        class="label label-primary pull-right menu__item-info-order-buy"
                                        data-val="{{\Illuminate\Support\Facades\Cache::get('buys_unviewed_by_admin')}}"
                                >{{\Illuminate\Support\Facades\Cache::get('buys_unviewed_by_admin') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('buys_unviewed_by_admin') : ''
                                }}</span>
                            @else
                                <span class="label label-primary pull-right menu__item-info-order-buy"></span>
                            @endif
                        </span>
                    </a>
                </li>
                <li class="sidebar-menu__item
                    @if(Route::is('admin.trades') && Route::current()->parameters['type'] === 'sell') active @endif"
                >
                    <a href="{{route('admin.trades', 'sell')}}">
                        <i class="fas fa-exchange-alt"></i> <span>Sell Trades</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('sells_unviewed_by_admin'))
                                <span
                                        class="label label-primary pull-right menu__item-info-order-sell"
                                        data-val="{{\Illuminate\Support\Facades\Cache::get('sells_unviewed_by_admin')}}"
                                >{{\Illuminate\Support\Facades\Cache::get('sells_unviewed_by_admin') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('sells_unviewed_by_admin') : ''
                                }}</span>
                            @else
                                <span class="label label-primary pull-right menu__item-info-order-sell"></span>
                            @endif
                        </span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.users')) active @endif">
                    <a href="{{route('admin.users')}}">
                        <i class="fas fa-users"></i> <span>Users</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.faq')) active @endif">
                    <a href="{{route('admin.faq')}}">
                        <i class="fas fa-question-circle"></i> <span>FAQ</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.pages')) active @endif">
                    <a href="{{route('admin.pages')}}">
                        <i class="far fa-folder"></i> <span>Pages</span>
                    </a>
                </li>
                <!-- Support -->
                <li class="sidebar-menu__item @if(Route::is('admin.support')) active @endif">
                    <a href="{{route('admin.support')}}">
                        <i class="far fa-life-ring"></i> <span>Support</span>
                        <span class="pull-right-container">
                            @if(\Illuminate\Support\Facades\Cache::has('tickets_unviewed_by_admin'))
                              <span
                                class="label label-primary pull-right menu__item-info-ticket-request-support"
                                data-val="{{\Illuminate\Support\Facades\Cache::get('tickets_unviewed_by_admin')}}"
                              >{{\Illuminate\Support\Facades\Cache::get('tickets_unviewed_by_admin') != 0 ?
                                    \Illuminate\Support\Facades\Cache::get('tickets_unviewed_by_admin') : ''
                                }}</span>
                            @endif
                        </span>                        
                    </a>
                </li>                   
            <li class="header">Settings</li>
                <li class="sidebar-menu__item @if(Route::is('admin.settings.plugins')) active @endif">
                    <a href="{{route('admin.settings.plugins')}}">
                        <i class="fas fa-cog"></i> <span>Plugins</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.settings.bitcoin')) active @endif">
                    <a href="{{route('admin.settings.bitcoin')}}">
                        <i class="fab fa-bitcoin"></i> <span>Bitcoin Settings</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.settings.ethereum')) active @endif">
                    <a href="{{route('admin.settings.ethereum')}}">
                        <i class="fab fa-ethereum"></i> <span>Ethereum Settings</span>
                    </a>
                </li>
                <li class="sidebar-menu__item @if(Route::is('admin.settings.web')) active @endif">
                    <a href="{{route('admin.settings.web')}}">
                        <i class="fas fa-cogs"></i> <span>Web Settings</span>
                    </a>
                </li>             
        </ul>
    </section>
</aside>
