<header class="main-header">
    <a href="{{ url('/') }}" class="logo">
        <span class="logo-mini">{{ config('app.short_name', 'APP') }}</span>
        <span class="logo-lg">{{$settings->name}}</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <i class="fas fa-bars"></i>
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if($globalNotificationsCount)
                    <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" disabled>
                        <i class="fas fa-bell"></i>
                        <span class="label label-warning" id="global-notification-count">{{$globalNotificationsCount}}</span>
                    </a>
                    @if ($globalNotifications)
                        <ul class="dropdown-menu">
                            <li class="header">You have {{$globalNotificationsCount}} notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    @foreach($globalNotifications as $globalNotification)
                                        @foreach($globalNotification['messages'] as $message)
                                            <li>
                                                <a href="{{$message['url']}}">
                                                    <i class="{{$globalNotification['icon']}}"></i>
                                                    {{$message['message']}}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </li>
                            {{--<li class="footer"><a href="#">View all</a></li>--}}
                        </ul>
                    @endif
                </li>
                @endif
                <li class="dropdown user user-menu">
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<img src="{{ \Auth::user()->urlUserImage() }}" class="user-image" alt="User Image">--}}
                        {{--<span class="hidden-xs">{{ \Auth::user()->name }}</span>--}}
                    {{--</a>--}}
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit(); ">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="user-header">--}}
                            {{--<img src="{{ \Auth::user()->urlUserImage() }}" class="img-circle" alt="User Image">--}}

                            {{--<p>--}}
                                {{--{{ \Auth::user()->name }}--}}

                                {{--<small><b>Group</b></small>--}}
                            {{--</p>--}}
                        {{--</li>--}}
                        {{--<li class="user-footer">--}}
                            {{--<div class="pull-left">--}}
                                {{--<a href="{{ url('/users/edit/' . \Auth::user()->id) }}" class="btn btn-default btn-flat">Profile</a>--}}
                            {{--</div>--}}
                            {{--<div class="pull-right">--}}
                                {{--<a href="{{ url('/logout') }}"--}}
                                   {{--onclick="event.preventDefault();--}}
                                    {{--document.getElementById('logout-form').submit(); " class="btn btn-default btn-flat">--}}
                                    {{--Logout--}}
                                {{--</a>--}}

                                {{--<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
                                    {{--{{ csrf_field() }}--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </li>
            </ul>
        </div>
    </nav>
</header>