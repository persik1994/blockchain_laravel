    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Account</title>
    <meta charset="utf-8">

    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="mirak.no">


    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$settings->title}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Styles -->
    <link rel="stylesheet" href="/plugins/fontawesome/css/all.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


</head>

<body class="main_body">
    <div class="main_body_top_header">
        @include('layouts.bars.navbar')

        @if(Request::is('account/*'))
            @include('layouts.cryptoheader')
        @endif

        <div class="alert alert-success" style="display: none;" id="success-message"></div>
            <div>
                @yield('content')
            </div>
<script type="text/javascript">
    var socket = io(':6001');
    socket.on('message', function(data) {
        console.log('From server: ', data);
    });
</script>

@if(!Route::is('trade'))
    <footer class="main">
        <div class="container">
            <div class="footer-bottom">
                <div class="left">
                    <nav class="clearfix">
                        <ul class="list-inline">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="{{url('page/terms-of-services')}}">Terms of services</a></li>
                            <li><a href="{{url('page/privacy-policy')}}">Privacy policy</a></li>
                            <li><a href="{{url('faq')}}">FAQ</a></li>
                            <li><a href="{{route('support')}}">Contact Support</a></li>
                        </ul>
                    </nav>
                    <p class="copyright-text">© 2018 <a href="http://mirak.no">www.mirak.no</a>. All Rights Reserved.</p>
                </div>
                <ul class="right list-inline social-icons social-icons-bordered social-icons-small social-icons-fullrounded">
                    <li><a href="@if(empty($settings->fb_link)) javascript:void(0) @else {{$settings->fb_link}} @endif" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="@if(empty($settings->tw_link)) javascript:void(0) @else {{$settings->tw_link}} @endif" target="_blank"><i class="fab fa-twitter"></i></a></li>
                </ul>
            </div>
        </div>
    </footer>
@endif

        <div class="back-to-top">
            <a href="#top"><i class="fas fa-chevron-up"></i></a>
        </div>
    </div>
<!-- END WRAPPER -->
<!-- JAVASCRIPT -->
{{--<script src="/assets/js/jquery-2.1.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-notify.min.js"></script>
<script src="/assets/js/plugins/easing/jquery.easing.min.js"></script>
<script src="/assets/js/plugins/morphext/morphext.min.js"></script>
<script src="/assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="/assets/js/bravana.js"></script>
<script src="/assets/js/bootstrap-select.min.js"></script>--}}
</body>



</html> 