<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="{{$settings->description}}">
    <meta name="keywords" content="{{$settings->keywords}}">
    <meta name="author" content="Mirak.no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>{{$settings->title}}</title>

    <link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>

<body style="padding-top: 3%;">
    <div id="wrapper">
        @include('layouts.bars.navbar')
        @include('layouts.cryptoheader')

        <div class="container-fluid">
            <div class="row">
                @include('layouts.bars.sidebar')
                <div class="page-content">
                    <div style="float: right;width: 78%;padding-right: 15px;margin-top: 30px;">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <footer class="main">
            <div class="container">
                <div class="col-lg-offset-3 col-sm-9">
                   <div class="footer-bottom">
                        <div class="left">
                            <nav class="clearfix">
                                <ul class="list-inline">
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li><a href="{{url('page/terms-of-services')}}">Terms of services</a></li>
                                    <li><a href="{{url('page/privacy-policy')}}">Privacy policy</a></li>
                                    <li><a href="{{url('faq')}}">FAQ</a></li>
                                    <li><a href="{{route('support')}}">Contact Support</a></li>
                                </ul>
                            </nav>
                            <p class="copyright-text">&copy; {{ date('Y') }} <a href="http://mirak.no">www.mirak.no</a>. All Rights Reserved.</p>
                        </div>
                        <ul class="right list-inline social-icons social-icons-bordered social-icons-small social-icons-fullrounded">
                            <li><a href="@if(empty($settings->fb_link)) javascript:void(0) @else {{$settings->fb_link}} @endif" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="@if(empty($settings->tw_link)) javascript:void(0) @else {{$settings->tw_link}} @endif" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                   </div>
                </div>
            </div>
        </footer>

        <div class="back-to-top">
            <a href="#top"><i class="fas fa-chevron-up"></i></a>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html> 