<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$settings->title}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/plugins/fontawesome/css/all.min.css">

</head>
<body class="auth_register_body">
    <div id="app">
        @include('layouts.bars.navbar')

        <br><br>

        <main class="py-4">
            @yield('content')
        </main>


    </div>
    @if(!Route::is('login') && !Route::is('register'))
        <footer class="main">
            <div class="container">
                <div class="footer-bottom">
                    <div class="left">
                        <nav class="clearfix">
                            <ul class="list-inline">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><a href="{{url('page/terms-of-services')}}">Terms of services</a></li>
                                <li><a href="{{url('page/privacy-policy')}}">Privacy policy</a></li>
                                <li><a href="{{url('faq')}}">FAQ</a></li>
                                <li><a href="{{route('support')}}">Contact Support</a></li>
                            </ul>
                        </nav>
                        <p class="copyright-text">© 2018 <a href="http://mirak.no">www.mirak.no</a>. All Rights Reserved.</p>
                    </div>
                    <ul class="right list-inline social-icons social-icons-bordered social-icons-small social-icons-fullrounded">
                        <li><a href="@if(empty($settings->fb_link)) javascript:void(0) @else {{$settings->fb_link}} @endif" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="@if(empty($settings->tw_link)) javascript:void(0) @else {{$settings->tw_link}} @endif" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </footer>
    @endif
</body>
</html>
