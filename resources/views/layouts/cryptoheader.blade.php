<div class="navbar navbar-default" style="
            position: fixed;
            z-index: 1000;
            background-color: #192126 !important;
            border-top: 1px solid rgb(20, 24, 28);
            border-bottom: 1px solid rgb(20, 24, 28);
            margin-top: 0px;
            "
>
    <div class="col-md-3">
        <div class="leftx">
            <div style="font-weight: bold;" class="js-account-openCurrencyPanel js-account-closeCurrencyPanel">
                <div id="zzz">
                    @if(\Auth::user())
                        <span class="current-currency">
                            <span><i class="{{\App\Models\User::$availableCryptoCurrencies[$userCryptoCurrency]['icon']}}"></i></span>
                            <span>{{$userCryptoCurrency}}-{{$userFiatCurrency}}</span>
                        </span>
                    @else
                        <span class="current-currency">
                            <span><i class="{{\App\Models\User::$availableCryptoCurrencies[$cryptoCurrency]['icon']}}"></i></span>
                            <span>{{$cryptoCurrency}}-{{$fiatCurrency}}</span>
                        </span>
                    @endif
                    <span class="pull-right">
                        Select Market
                        <i class="fas fa-angle-down"></i>
                    </span> 
                </div>

                <div class="descr">
                    <p style="font-size: 12px;margin-top: 10px">Fiat markets</p>
                    <ul class="list1b">
                        <li class="list-item">
                            <table width="100%" class="js-account-selectCurrency" @if(!\Auth::user() && Route::is('trade')) data-url="{{route('trade', 'BTC-USD')}}" @endif>
                                <tr class="list_current_item">
                                    <td rowspan="2"><span id="listcrypto" class="x1"><i class="fab fa-2x fa-bitcoin"></i></span></td>
                                    <td><span class="x2" id="listcrypto" data-crypto-currency="BTC" data-fiat-currency="USD">BTC-USD</span></td>
                                    <td rowspan="2"></td>
                                    <td><span id="listcrypto" class="crt fiat-price" data-type="priceBTCUSD" data-suffix="USD">{{$cryptoFiatPrice['BTC']['USD']}} USD</span></td>
                                    <td class="@if($cryptoFiatLevelBy24H['BTC']['USD'] < 0) minus @else plus @endif"><span id="listcrypto" class="crt status-level" data-type="currencyTimeBTCUSD" data-suffix="%">{{$cryptoFiatLevelBy24H['BTC']['USD']}} %</span></td>
                                </tr>
                                <tr class="list_current_item">
                                    <td>Bitcoin</td>
                                    <td>Trade price</td>
                                    <td>24h price</td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-item">
                            <table width="100%" class="js-account-selectCurrency" @if(!\Auth::user() && Route::is('trade')) data-url="{{route('trade', 'BTC-EUR')}}" @endif>
                                <tr class="list_current_item">
                                    <td rowspan="2"><span id="listcrypto" class="x1"><i class="fab fa-2x fa-bitcoin"></i></span></td>
                                    <td><span class="x2" id="listcrypto" data-crypto-currency="BTC" data-fiat-currency="EUR">BTC-EUR</span></td>
                                    <td rowspan="2"></td>
                                    <td><span id="listcrypto" class="crt fiat-price" data-type="priceBTCEUR" data-suffix="EUR">{{$cryptoFiatPrice['BTC']['EUR']}} EUR</span></td>
                                    <td class="@if($cryptoFiatLevelBy24H['BTC']['EUR'] < 0) minus @else plus @endif"><span id="listcrypto" class="crt status-level" data-type="currencyTimeBTCEUR" data-suffix="%">{{$cryptoFiatLevelBy24H['BTC']['EUR']}} %</span></td>
                                </tr>
                                <tr class="list_current_item">
                                    <td>Bitcoin</td>
                                    <td>Trade price</td>
                                    <td>24h price</td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-item">
                            <table width="100%" class="js-account-selectCurrency" @if(!\Auth::user() && Route::is('trade')) data-url="{{route('trade', 'ETH-USD')}}" @endif>
                                <tr class="list_current_item">
                                    <td rowspan="2"><span id="listcrypto" class="x1">&nbsp;<i class="fab fa-2x fa-ethereum"></i></span></td>
                                    <td><span class="x2" id="listcrypto" data-crypto-currency="ETH" data-fiat-currency="USD">ETH-USD</span></td>
                                    <td rowspan="2"></td>
                                    <td><span id="listcrypto" class="crt fiat-price" data-type="priceETHUSD" data-suffix="USD">{{$cryptoFiatPrice['ETH']['USD']}} USD</span></td>
                                    <td class="@if($cryptoFiatLevelBy24H['ETH']['USD'] < 0) minus @else plus @endif"><span id="listcrypto" class="crt status-level" data-type="currencyTimeETHUSD" data-suffix="%">{{$cryptoFiatLevelBy24H['ETH']['USD']}} %</span></td>
                                </tr>
                                <tr class="list_current_item">
                                    <td>Ethereum</td>
                                    <td>Trade price</td>
                                    <td>24h price</td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-item">
                            <table width="100%" class="js-account-selectCurrency" @if(!\Auth::user() && Route::is('trade')) data-url="{{route('trade', 'ETH-EUR')}}" @endif>
                                <tr class="list_current_item">
                                    <td rowspan="2"><span id="listcrypto" class="x1">&nbsp;<i class="fab fa-2x fa-ethereum"></i></span></td>
                                    <td><span class="x2" id="listcrypto" data-crypto-currency="ETH" data-fiat-currency="EUR">ETH-EUR</span></td>
                                    <td rowspan="2"></td>
                                    <td><span id="listcrypto" class="crt fiat-price" data-type="priceETHEUR" data-suffix="EUR">{{$cryptoFiatPrice['ETH']['EUR']}} EUR</span></td>
                                    <td class="@if($cryptoFiatLevelBy24H['ETH']['EUR'] < 0) minus @else plus @endif"><span id="listcrypto" class="crt status-level" data-type="currencyTimeETHEUR" data-suffix="%">{{$cryptoFiatLevelBy24H['ETH']['EUR']}} %</span></td>
                                </tr>
                                <tr class="list_current_item">
                                    <td>Ethereum</td>
                                    <td>Trade price</td>
                                    <td>24h price</td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9" style="padding-right: 0;margin-top: 55px">
        <div style="font-weight: bold;" id="maincrypt">
            {{--<span id="listcrypto">--}}
            {{--<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bitcoin.svg/600px-Bitcoin.svg.png" width="30px">--}}
            {{--</span>--}}
            {{--<span id="listcrypto" style="font-weight: bold;">BTC-USD</span>--}}

            @if(\Auth::user())
                <span id="listcrypto"
                      style="font-weight: bold;"
                      data-type="price{{$userCryptoCurrency . $userFiatCurrency}}"
                      data-suffix="{{\Auth::user()->fiat_currency}}" class="crt"
                >
                    {{$cryptoFiatPrice[$userCryptoCurrency][$userFiatCurrency]}} {{$userFiatCurrency}}
                </span>
                <span id="listcrypto" class="crt" style="font-weight: bold;" data-type="currencyTime{{$userCryptoCurrency . $userFiatCurrency}}" data-suffix="%">
                    {{$cryptoFiatLevelBy24H[$userCryptoCurrency][$userFiatCurrency]}} %
                </span>
            @else
                @if(Route::is('trade'))
                    <span id="listcrypto" style="font-weight: bold;" data-type="price{{$cryptoCurrency . $fiatCurrency}}" data-suffix="{{$fiatCurrency}}" class="crt">
                        {{$cryptoFiatPrice[$cryptoCurrency][$fiatCurrency]}} {{$fiatCurrency}}
                    </span>
                    <span id="listcrypto" class="crt @if($cryptoFiatLevelBy24H[$cryptoCurrency][$fiatCurrency] < 0) minus @else plus @endif" style="font-weight: bold;" data-type="currencyTime{{$cryptoCurrency . $fiatCurrency}}" data-suffix="%">
                        @if($cryptoFiatLevelBy24H[$cryptoCurrency][$fiatCurrency] > 0)
                           + {{$cryptoFiatLevelBy24H[$cryptoCurrency][$fiatCurrency]}} %
                        @else
                            {{$cryptoFiatLevelBy24H[$cryptoCurrency][$fiatCurrency]}} %
                        @endif
                    </span>
                @endif
            @endif
        </div>
    </div>
</div>
