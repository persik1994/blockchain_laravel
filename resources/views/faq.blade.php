@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">FREQUENTLY ASKED QUESTIONS</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom">
        <section>
            <div class="container">
                @if(!$faqItems->isEmpty())
                    @foreach($faqItems as $item)
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Q: {{$item->question}}</h4>
                                <h5>A: {{$item->answer}}</h5>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <span>No items yet</span>
                        </div>
                    </div>
                @endif
            </div>
        </section>
    </div>
@endsection