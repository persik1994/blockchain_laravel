@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">TRANSFER BITCOINS</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom">
                <!-- SERVICES -->
                <section>
                    <div class="container">
                        <h2 class="section-heading">Service transfer Bitcoins</h2>
                        <p class="buy_text">Any registered user can deposit or withdrawal Bitcoins every time when want. You can pay to friends, customers or family for goods, services or gift.</p>
                        <div class="row margin-top-50">
                            <div class="col-md-6">
                                <img src="{{url('/img/transfer-bitcoins.png')}}" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="icon-info icon-info-left">
                                        <i class="fas fa-clock fa-2x text-primary"></i>
                                        <div class="text">
                                            <h2 class="title">Fast transfer</h2>
                                            <p>{{$settings->name}} processes transfers instantly. Our generated wallet addresses are with GREEN FLAG, that mean need only 1 confirmation to recipient get your Bitcoins.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END SERVICES -->
        </div>
@endsection
