@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">SELL BITCOINS</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom">
                <!-- SERVICES -->
                <section>
                    <div class="container">
                        <h2 class="section-heading">Service selling Bitcoins</h2>
                        <p class="buy_text">Any registered user may sell Bitcoin to us, by using website easily and safely. The system allows the sell Bitcoins with Wire Transfer, Credit Card and many other ways if allowed by the administrator. The processing is fast and safe, directly from your Bitcoin wallet.</p>
                        <div class="row margin-top-50">
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-chart-bar fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Sell to the Market</h2>
                                        <p>Our platform provides a market place for individuals looking to buy and sell Bitcoins. While we monitor this platform to ensure security, we do not set prices, the market does. So the price of a Bitcoin is the market value of a Bitcoin. It is as simple as that. There are no hidden fees. {{$settings->name}} will tell you exactly what obtaining Bitcoins will cost.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-clock fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Sell Fast</h2>
                                        <p>{{$settings->name}} processes trades instantly. While the high level cryptography of the protocol can take a few minutes to verify that the bitcoins are not counterfeit, your order once placed, requires no additional work on your part. Sit back, relax, and we’ll transfer your money to your account.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-shield-alt fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Sell Securely</h2>
                                        <p>We have spent a lot of time ensuring our system is safe for everyone. With the added feature of two-factor Authentication, {{$settings->name}} ensures that no transaction will ever be made without your express authorization.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END SERVICES -->
        </div>
@endsection
