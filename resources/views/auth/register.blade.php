@extends('layouts.app')

@section('content')
    <div id="wrapper">
        <div class="table-wrap">
            <div class="table-cell">
                <div class="auth-box">
                    <div class="container">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <br>
                            {{--<div class="header">--}}
                                {{--<a href="http://crypto-market/"><img src="/public/img/login-invert2.png" class="logo" alt="Anspot Logo"></a>--}}
                                {{--<h1 class="heading">Register for free</h1>--}}
                            {{--</div>--}}
                            <form class="form-auth-small" style="width:100%;" action="{{ route('register') }}" method="POST">
                                @csrf
                                <h2 class="heading">SIGN UP TO {{ $settings->title }}</h2>

                                <div class="form-group">
                                    <label for="name" class="control-label sr-only">Name</label>
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" placeholder="Full Name" type="text" required>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="signup-email" class="control-label sr-only">Email address</label>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Email address" type="email" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif                                    
                                </div>
                                <div class="form-group">
                                    <label for="signup-password" class="control-label sr-only">Password</label>
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="Password" type="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif                                    
                                </div>
                                <div class="form-group">
                                    <label for="signup-password2" class="control-label sr-only">Confirm Password</label>
                                    <input class="form-control" name="password_confirmation" id="password-confirm" placeholder="Confirm Password" type="password" required>

                                </div>
                                <div class="form-group">
                                    <label for="mobile_number" class="control-label sr-only">Phone number</label>
                                    <input class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" id="mobile_number" placeholder="Phone number" type="text" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" name="btc_sign_up">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{url('/')}}" class="btn btn-default btn-lg btn-block">
                                            Cancel
                                        </a>
                                    </div>
                                </div>

                                <br>
                                <p class="buy_text">Already have an account? Sign in <a href="{{ route('login') }}">here</a>.</p>
                            </form>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
