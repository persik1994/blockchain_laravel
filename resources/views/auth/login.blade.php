@extends('layouts.app')

@section('content')
<div id="wrapper">
    <div class="table-wrap">
        <div class="table-cell">
            <div class="auth-box">
                <div class="container">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <form class="form-auth-small" style="width:100%;" method="POST" action="{{ route('login') }}">
                            @csrf
                            <h2 class="heading">SIGN IN TO {{$settings->name}}</h2>
                            <div class="form-group">
                                <label for="signup-email" class="control-label sr-only">Email address</label>
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Email address" type="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">    
                                <label for="signup-password" class="control-label sr-only">Password</label>
                                <input name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Password" type="password" required="">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="clearfix">
                                <label>
                                    <input class="form-check-input" name="remember" id="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{url('/')}}" class="btn btn-default btn-lg btn-block">
                                        Cancel
                                    </a>
                                </div>
                            </div>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </form>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
