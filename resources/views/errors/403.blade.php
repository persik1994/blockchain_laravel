@extends('layouts.illustrated-layout')

@section('code', '403')
@section('title', __('Unauthorized'))

@section('left_content')
    @if(Request::is('admin', 'admin/*'))
        <div class="login-box" style="margin-top: 30%;">
            @if ($errors->any())
                <div class="callout callout-danger">
                    @foreach ($errors->any() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            <div class="login-box-body">
                <p class="login-box-msg">Log Into Dashboard</p>

                <form action="{{ url('/admin/login') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group has-feedback">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}" required placeholder="Email" autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password" required placeholder="Password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <input type="checkbox" name="remember" id="remember" class="checkbox-custom">
                            <label for="remember" class="checkbox-custom-label">Remomber me</label>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
                        </div>
                    </div>
                </form>

                <a href="{{ route('admin.password.request') }}">Lost your password ?</a><br>
            </div>
        </div>
    @elseif (Request::is('account', 'account/*'))
        <div class="col-md-3"></div>
        <div class="col-md-6" style="margin-top: 25%;">
            @if ($errors->any())
                <div class="callout callout-danger">
                    @foreach ($errors->any() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <br>
            <form class="form-auth-small" style="width:100%;" method="POST" action="{{ route('login') }}">
                @csrf
                <h2 class="heading">SIGN IN</h2>
                <div class="form-group">
                    <label for="signup-email" class="control-label sr-only">Email address</label>
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Email address" type="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="signup-password" class="control-label sr-only">Password</label>
                    <input name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Password" type="password" required="">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
                    @endif
                </div>
                <div class="clearfix">
                    <label>
                        <input class="form-check-input" name="remember" id="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </label>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            {{ __('Login') }}
                        </button>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('/')}}" class="btn btn-default btn-lg btn-block">
                            Cancel
                        </a>
                    </div>
                </div>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </form>
        </div>
        <div class="col-md-3"></div>
    @else
        <div style="background-image: url('/svg/403.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
        </div>
    @endif
@endsection

@section('message', __('Sorry, you are not authorized to access this page.'))
