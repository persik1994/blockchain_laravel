@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">{{$page->title}}</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom">
        <section>
            <div class="container text-white">
                <div class="row">
                    <div class="col-md-12">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection