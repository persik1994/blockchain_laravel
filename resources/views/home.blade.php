@extends('layouts.main')

@section('content')
    <div id="wrapper">
            <!-- HERO UNIT -->
            <section class="hero-unit hero-fullwidth fullwidth-image hero-rotating-words">
                <div class="hero-content">
                    <div class="container">
                        <h2 class="heading">{{$settings->name}}</h2>
                        <p class="lead">Made <span id="rotating-words" class="rotating-words morphext"><span class="animated fadeInUp">to protect your wallet</span></span></p>
                        <div class="row">
                            <div class="col-md-5">
                                <a href="{{route('register')}}" class="btn btn-primary btn-lg" name="btc_get_wallet"><i class="fab fa-bitcoin"></i> Get Bitcoin Wallet</a>
                            </div>
                            <div class="col-md-7"></div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END HERO UNIT -->

            <!-- WHAT WE DO -->
            <section>
                <div class="container">
                    <h2 class="section-heading">Secure and safe<br>
                    <small>Our bank level security keeps all your transactions and personal information secure.</small></h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="icon-info icon-info-left">
                                        <i class="fab fa-bitcoin text-primary fa-3x"></i>
                                        <div class="text">
                                            <h2 class="title">Your Online Hosted Bitcoin Wallet.</h2>
                                            <p>No more thefts. We keep your Bitcoin safe and secure.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="icon-info icon-info-left">
                                        <i class="fa fa-lock text-primary fa-3x"></i>
                                        <div class="text">
                                            <h2 class="title">Two-factor Wallet Decrypt provides an extra level of security to you.</h2>
                                            <p>Only you have the access to your account, and this means that your money is always safe.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="icon-info icon-info-left">
                                        <i class="fas fa-shield-alt fa-3x text-primary"></i>
                                        <div class="text">
                                            <h2 class="title">Your Bitcoins are in safe hands.</h2>
                                            <p>We dont take any security risks. 95% of Bitcoin data is stored offline.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-6">
                            <img src="assets/img/bitcoin2_complete.png" class="img-responsive">
                        </div>-->
                    </div>
                </div>
            </section>
            <!-- END WHAT WE DO -->
        </div>
@endsection
