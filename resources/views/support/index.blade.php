@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">CONTACT SUPPORT</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom"> 
        <section>
            <div class="container">
                <h2 class="section-heading">@if(!Auth::user()) Contact support @else My support tickets @endif</h2>

                <hr>

                @if (!empty($usertickets))
                    @foreach($usertickets as $userticket)
                        @if(\Auth::user())
                        <div class="row">
                            <div class="col-md-12">
                                @if($userticket->uid == \Auth::user()->id)
                                <h4><a href="{{ route('support.userchat', $userticket->id) }}">  {{ $userticket->title }} </a></h4>
                                @endif
                            </div>
                        </div>
                        @endif
                    @endforeach
                @endif

                <br>

                <form method="POST" action="{{ route('support.add.ticket', 'string') }}">
                    @csrf 
                    <div class="form-group">
                        <label class="support">Title</label>
                        <input class="form-field form-control" name="support_title">
                    </div>
                    @if(!\Auth::user())
                    <div class="form-group">
                        <label class="support">Email</label>
                        <input class="form-field form-control" name="support_email">
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="support">Description</label>
                        <textarea class="form-field form-control" name="support_message" rows="7"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" name="btn_add"> Send request</button>
                </form>
               
            </div>
        </section>
    </div>
@endsection

