@extends('layouts.main')

@section('content')


<div id="page-header-services" class="page-header has-background-image">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title">SUPPORT CHAT</h1>
    </div>
</div>

<div class="page-content no-margin-bottom" id="ticket-chat" data-ticket-id="{{$ticket->id}}">
            @if(\Auth::guest())
                <input type="hidden" name="ticket_hash" value="{{$ticket->ticket_hash}}">
            @endif

            <section>
                <div class="container">
                    <h4 class="pull-right"><a href="{{route('support')}}"><i class="fas fa-arrow-left"></i> back to tickets list</a></h4>

                    <br>

                    <h2 class="section-heading"></h2>
                            <div class="text-white">
                                <h3>
                                    Ticket title: {{ $ticket->title }}
                                </h3>
                                <h4>
                                    Title Description: {{ $ticket->description }}
                                </h4>
                            </div>

                            <br>
                            <hr>
                            <br>

                            <div class="text-white messages-wrapper">
                                @if(!$ticket->messages->isEmpty())
                                    @foreach($ticket->messages as $message)
                                        @if($message->user && $message->user->type == 'admin')
                                            <div class="pull-right message"><b>{{ $message->user->name }}:</b> {{ $message->message }} </div>
                                         @elseif($message->user)
                                            <div class="message"><b>{{ $message->user->name }}:</b> {{ $message->message }} </div>
                                         @else
                                            <div class="message"><b>{{ $ticket->email_address }}:</b> {{ $message->message }} </div>
                                         @endif
                                        <br>
                                    @endforeach
                                @endif
                            </div>

                        <!-- Chat Form -->
                        <form method="POST" action="{{ route('support.user.chat', $id ) }}" class="js-support-addMessage">
                            @csrf
                            <div class="form-group">
                                <label class="support">Message</label>
                                <textarea class="form-field form-control" name="user_chat_message" rows="7"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary" name="btn_add"> Send message</button>
                        </form>
                </div>
            </section>

            <!-- END SERVICES -->
</div>


@endsection

