<style type="text/css">
    .table {
        border:1px solid #c1c1c1;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        padding:10px;
    }
</style>
<table border="0" width="600px" align="center" class="table">
    <tr>
        <td><img src="{{ url('/') }}/public/img/login-invert.png"><br><br></td>
    </tr>
    <td>
        <center>
            <b>{{ $params['user']->email }}</b> request <b>{{ $params['amount'] }} @if($userCryptoCurrency === 'BTC') BTC @else($userCryptoCurrency == 'ETH') ETH @endif</b> from you.
            <br><br>
            <img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=bitcoin:{{ $params['address'] }}?amount={{ $params['amount'] }}&choe=UTF-8"><br>
            @if($userCryptoCurrency === 'BTC') Bitcoin @else($userCryptoCurrency == 'ETH') Ethereum @endif address: <b>{{ $params['address'] }}</b><br/>
            Note: <b>{{ $params['notes'] }}</b>
        </center>
    </td>
    </tr>
</table>