<style type="text/css">
    .table {
        border:1px solid #c1c1c1;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        padding:10px;
    }
</style>

<table border="0" width="600px" align="center" class="table">
    <tr>
        <td><img src="{{ url('/') }}img/login-invert.png"><br><br></td>
    </tr>
    <tr>
        <td>
            To unlock your {{ $settings->name }} account, need to activate it from the following link:<br/>
            <a href="{{ route('verification.email', ['hash' => $user->email_hash]) }}" target="_blank">
                {{ route('verification.email', ['hash' => $user->email_hash]) }}
            </a>
        </td>
    </tr>
</table>