@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">CONTACT US</h1>
        </div>
    </div>
    <div class="page-content no-margin-bottom">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="heading-border-left">GET IN TOUCH</h2>
                        <p>Feel free to contact us anytime when you have a problem, we will try in the shortest possible time to solve your problem or answer your questions..</p>

                        @include('layouts.messages')

                        <form method="post" action="{{route('contact-us')}}" id="contact-form" class="form-horizontal form-minimal margin-top-30" novalidate="">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="contact-name" class="control-label sr-only">Name</label>
                                        <input type="text" class="form-control" id="contact-name" name="name" placeholder="Name*" required="">

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="contact-email" class="control-label sr-only">Email address</label>
                                        <input type="email" class="form-control" id="contact-email" name="email" placeholder="Email address*" required="">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contact-subject" class="control-label sr-only">Subject</label>
                                        <input type="text" class="form-control" id="contact-subject" name="subject" placeholder="Subject*">

                                        @if ($errors->has('subject'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contact-message" class="control-label sr-only">Message</label>
                                        <textarea class="form-control" id="contact-message" name="message" rows="5" cols="30" placeholder="Message*" required=""></textarea>

                                        @if ($errors->has('message'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('message') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button id="submit-button" type="submit" name="bit_send" class="btn btn-primary"><i class="fa loading-icon"></i> <span>Submit Message</span></button>
                                </div>
                            </div>
                            <input type="hidden" name="msg-submitted" id="msg-submitted" value="true">
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection