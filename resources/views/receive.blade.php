@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title">REQUEST BITCOINS</h1>
        </div>
    </div>

    <div class="page-content no-margin-bottom">
        <!-- SERVICES -->
        <section>
                <div class="container">
                    <h2 class="section-heading">Service request Bitcoins</h2>
                    <p class="buy_text">Any registered user can request Bitcoins from user by email, recipient will receive your email, requested amount and qr code for fast payment.</p>
                    <div class="row margin-top-50">
                        <div class="col-md-6">
                            <style type="text/css">
                            .tab_bitcoinwallet {
                                border:1px solid #c1c1c1;
                                -webkit-border-radius: 5px;
                                -moz-border-radius: 5px;
                                border-radius: 5px;
                            }
                            .tab_bitcoinwallet tbody {
                                padding:20px;
                            }
                            </style>
                            <table class="tab_bitcoinwallet" width="100%" align="center">
                                <tbody style="padding:20px;">
                                <tr>
                                    <td><img src="{{url('/img/login-invert.png')}}"><br><br></td>
                                </tr>
                                <tr>
                                    <td class="buy_text">
                                        <center>
                                            <b>admin@me4onkof.info</b> request <b>0.098576 BTC</b> from you.
                                            <br><br>
                                            <img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl=bitcoin:1PRPdegR2hZmb5GNb4z6C2xq9APbnBWpUa?amount=0.098576&amp;choe=UTF-8"><br>
                                            Bitcoin address: <b>1PRPdegR2hZmb5GNb4z6C2xq9APbnBWpUa</b><br>
                                            Note: <b>Pay Digital Goods</b>
                                        </center>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-clock fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Easy &amp; Fast Request</h2>
                                        <p>{{$settings->name}} give chance to user to request Bitcoins from users by email, the process takes few seconds. This is most popular way to get paid with Bitcoins.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </div>
@endsection
