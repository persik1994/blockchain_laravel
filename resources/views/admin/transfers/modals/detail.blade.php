<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>User info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>User</b> <span class="pull-right"><a>{{ $transfer->user->email }}</a></span></td>
            </tr>
            <tr>
                <td><b>Balance</b> <span class="pull-right">{{ formatBitcoin($transfer->user->btc_balance) }} BTC</span></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>Trade info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b>Transfer ID:</b> <span class="pull-right">{{ $transfer->id }}</span></td>
                </tr>
                <tr>
                    <td><b>Trade type:</b> <span class="pull-right">User transfers BTC</span></td>
                </tr>
                <tr>
                    <td><b>Amount :</b> <span class="pull-right">{{ $transfer->crypto_amount }} BTC</span></td>
                </tr>
                <tr>
                    <td><b>Recipient address :</b> <span class="pull-right">{{ $transfer->recipient_address }}</span></td>
                </tr>
                <tr>
                    <td>
                        <b>Status</b>
                        <span class="pull-right">
                            @if($transfer->status == 0)
                                <span class="label label-info">Awaiting</span>
                            @elseif($transfer->status == 1)
                                <span class="label label-success">Processed</span>
                            @elseif($transfer->status == 2)
                                <span class="label label-danger">Canceled</span>
                            @else
                                <span class="label label-defualt">Unknown</span>
                            @endif
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>