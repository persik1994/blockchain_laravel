@extends('layouts.admin.app')

@section('title')
    Transfers
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5px">
                                    <div class="check-input list-input">
                                        <input type="checkbox" class="checkbox-custom">
                                        <span class="checkbox-custom-label text"></span>
                                    </div>
                                </th>
                                <th>
                                    <botton
                                            class="btn btn-sm btn-default mark-selected-as-viewed hidden-element"
                                            data-type="deposit"
                                            data-url="{{route('admin.money.mark.viewed')}}"
                                    >
                                        <i class="fas fa-eye"></i>
                                    </botton>
                                </th>
                                {{--<th>--}}
                                {{--<botton class="btn btn-sm btn-default">--}}
                                {{--<i class="fas fa-trash"></i>--}}
                                {{--</botton>--}}
                                {{--</th>--}}
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered transfer list">
                                <thead>
                                <tr>
                                    <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                    <th>ID</th>
                                    <th>Address</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($transfers))
                                    @foreach($transfers as $transfer)
                                        <tr class="row-item @if(!$transfer->viewed_by_admin) item-unviewed @endif">
                                            <td class="text-center">
                                                <div class="check-input row-input">
                                                    <input
                                                            type="checkbox"
                                                            class="checkbox-custom"
                                                            value="{{$transfer->id}}"
                                                    >
                                                    <span class="checkbox-custom-label text"></span>
                                                </div>
                                            </td>
                                            <td>{{ $transfer->id }}</td>
                                            <td>{{ $transfer->recipient_address }}</td>
                                            <td>
                                                {{ convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency) }}
                                                {{ $transfer->crypto_currency }}
                                            </td>
                                            <td>
                                                @if($transfer->status == 0)
                                                    <span class="label label-info">Awaiting</span>
                                                @elseif($transfer->status == 1)
                                                    <span class="label label-success">Processed</span>
                                                @elseif($transfer->status == 2)
                                                    <span class="label label-danger">Canceled</span>
                                                @endif
                                            </td>
                                            <td width="120px">
                                                <a href="#"
                                                   data-title="Transfer detail"
                                                   data-id="{{ $transfer->id }}"
                                                   data-status="{{ $transfer->status }}"
                                                   data-address="{{ $transfer->recipient_address }}"
                                                   data-type="transfer"
                                                   data-amount="{{ convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency) }}"
                                                   class="btn btn-default btn-small js-admin-transfer-openDetailModal">
                                                    <i class="fas fa-search"></i> Explore
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No transfers</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                    <th>ID</th>
                                    <th>Address</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $transfers->firstItem() }} to {{ $transfers->lastItem() }} from {{ $transfers->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {{ $transfers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.transfers.templates')
@endsection
