@foreach($user->transactions as $transaction)
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 text-center">
                    @if($transaction->type == 'sent')
                        <span class="text text-danger text-center"><i class="fas fa-arrow-circle-up fa-2x"></i><br/>Sent</span>
                    @else
                        <span class="text text-success text-center"><i class="fas fa-arrow-circle-down fa-2x"></i><br/>Received</span>
                    @endif
                    <br><br>
                    <span class="text-muted"><small>{{ $transaction->confirmations }} confirmations</small></span>
                </div>
                <div class="col-md-10">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>User:</td>
                            <td><a href="#">{{ $user->name }}</a></td>
                        </tr>
                        <tr>
                            <td>Transaction:</td>
                            <td><a target="_blank" href="https://chain.so/tx/BTC/{{ $transaction->txid }}">{{ $transaction->txid }}</a></td>
                        </tr>
                        <tr>
                            <td>Sender:</td>
                            <td>{{ $transaction->sender }}</td>
                        </tr>
                        <tr>
                            <td>Recipient:</td>
                            <td>{{ $transaction->recipient }}</td>
                        </tr>
                        <tr>
                            <td>Amount:</td>
                            <td>{{ $transaction->amount }} BTC</td>
                        </tr>
                        <tr>
                            <td>Time:</td>
                            <td>{{ date('d/m/Y H:i', strtotime($transaction->time)) }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endforeach