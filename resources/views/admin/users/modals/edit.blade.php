<form action="{{ route('admin.users.update') }}" class="js-admin-user-update" method="POST">
    <input type="hidden" name="id" value="{{ $user->id }}">
    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control" name="name" value="{{ $user->name }}">
    </div>
    <div class="form-group">
        <label>Email address</label>
        <input type="text" class="form-control" name="email" value="{{ $user->email }}">
    </div>
    <div class="form-group">
        <label>New password</label>
        <input type="text" class="form-control" name="password" placeholder="Leave empty if do not want to change it.">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="status">
            <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Not verified</option>
            <option value="2" {{ $user->status == 2 ? 'selected' : '' }}>Banned</option>
            <option value="3" {{ $user->status == 3 ? 'selected' : '' }}>Verified</option>
            <option value="666" {{ $user->status == 666 ? 'selected' : '' }}>Administrator</option>
            <option value="777" {{ $user->status == 777 ? 'selected' : '' }}>Operator</option>
        </select>
    </div>
    <div class="form-group">
        <label>Mobile number</label>
        <input type="text" class="form-control" name="mobile-number" value="{{ $user->mobile_number }}">
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="email-verified" {{ !empty($user->email_verified_at) ? 'checked' : '' }}> Email
            verified
        </label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="document-verified" {{ $user->document_verified == 1 ? 'checked' : '' }}>
            Document verified
        </label>
    </div>
    @if(!empty($user->document_1) || !empty($user->document_2))
        <table class="table table-striped">
            <thead>
            <tr>
                <th colspan="2"><b>Attached files:</b></th>
            </tr>
            <tr>
                <th>Filename</th>
                <th>File size</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($user->document_1))
                <tr>
                    <td>
                        <a href="{{ route('admin.file', ['filepath' => $user->document_1]) }}" target="_blank">{{ basename($user->document_1) }}</a>
                    </td>
                    <td>
                        {{ formatBytes(Storage::size($user->document_1)) }}
                    </td>
                    <td>
                        <a href="#" class="js-admin-user-deleteDocument" data-id="{{ $user->id }}" data-type="document-1">
                            <i class="fa fa-times"></i> Delete
                        </a>
                    </td>
                </tr>
            @endif
            @if(!empty($user->document_2))
                <tr>
                    <td>
                        <a href="{{ route('admin.file', ['filepath' => $user->document_2]) }}" target="_blank">{{ basename($user->document_2) }}</a>
                    </td>
                    <td>
                        {{ formatBytes(Storage::size($user->document_2)) }}
                    </td>
                    <td>
                        <a href="#" class="js-admin-user-deleteDocument" data-id="{{ $user->id }}" data-type="document-2">
                            <i class="fa fa-times"></i> Delete
                        </a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    @endif
    <div class="checkbox">
        <label>
            <input type="checkbox" name="mobile-verified"
                   value="yes" {{ $user->mobile_verified == 1 ? 'checked' : '' }}> Mobile verified
        </label>
    </div>
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-check"></i> Save changes
    </button>
</form>