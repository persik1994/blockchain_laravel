@extends('layouts.admin.app')

@section('title')
    Users
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Email verified</th>
                                    <th>Document verified</th>
                                    <th>Mobile verified</th>
                                    <th>Available balance</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if($user->status == 1)
                                                <span class="label label-warning">Not verified</span>
                                            @elseif($user->status == 2)
                                                <span class="label label-danger">Banned</span>
                                            @elseif($user->status == 3)
                                                <span class="label label-success">Verified</span>
                                            @elseif($user->status == 666)
                                                <span class="label label-info">Administrator</span>
                                            @elseif($user->status == 777)
                                                <span class="label label-primary">Operator</span>
                                            @else
                                                <span class="label label-default">Unknown</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->type === 'admin')
                                                <span class="label label-info">Administrator</span>
                                            @elseif($user->type === 'user')
                                                <span class="label label-info">User</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($user->email_verified))
                                                <span class="label label-success"><i class="fa fa-check"></i> Yes</span>
                                            @else
                                                <span class="label label-danger"><i class="fa fa-times"></i> No</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->document_verified == 1)
                                                <span class="label label-success"><i class="fa fa-check"></i> Yes</span>
                                            @else
                                                <span class="label label-danger"><i class="fa fa-times"></i> No</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->mobile_verified == 1)
                                                <span class="label label-success"><i class="fa fa-check"></i> Yes</span>
                                            @else
                                                <span class="label label-danger"><i class="fa fa-times"></i> No</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" title="Open transaction" data-title="User transactions"
                                               data-id="{{ $user->id }}"
                                               class="js-admin-user-openTransactionsModal">
                                                {{ formatBitcoin($user->btc_alance) }} BTC
                                            </a>
                                        </td>
                                        <td width="120px">
                                            <button class="btn btn-sm btn-primary js-admin-user-openEditModal"
                                                    data-id="{{ $user->id }}">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger delete-item" data-url="{{ route('admin.delete.user', ['id' => $user->id]) }}">
                                                <i class="fa fa-eraser"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Email verified</th>
                                    <th>Document verified</th>
                                    <th>Mobile verified</th>
                                    <th>Avaiable balance</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $users->firstItem() }} to {{ $users->lastItem() }} from {{ $users->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
