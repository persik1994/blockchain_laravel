<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>Addresses info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>User info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>