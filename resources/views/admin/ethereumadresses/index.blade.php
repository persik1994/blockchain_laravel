@extends('layouts.admin.app')

@section('title')
    Ethereum addresses
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
<section class="content">    
    <div class="box box-info">
            <div class="box-body">
                <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Address</th>
                                    <th>Balance</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
    
    @php 
        $ethereumAccountsInformation = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_accounts();
        foreach ($ethereumAccountsInformation as $ethereum_infokey => $ethereumAccountInformation) 
        {
             $acctInfo = $ethereumAccountsInformation[$ethereum_infokey];
             $acctBal = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($acctInfo);
             $decimal = pow(10, 18);
             $acctBalHexToDec = hexdec($acctBal) / $decimal;
             echo "<tr>
                        <td>User</td>
                        <td>$acctInfo</td>
                        <td>$acctBalHexToDec</td>
                        <td>
                            <a href='#'
                               data-title='js-admin-users-trades-openDetailsModals'
                               data-id=''
                               data-url='route(\"admin.ethereumadresses.modal.details\")'
                               data-status=''
                               data-email=''
                               data-amount=''
                               data-type=''
                               class='btn btn-default btn-small js-admin-users-trades-openDetailsModal'>
                                <i class='fas fa-search'></i> Explore
                            </a>
                        </td>
                    </tr>";
        }
    @endphp                                
                                
                                    

                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
</section>
@endsection
