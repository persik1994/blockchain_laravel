<div>
    <i class="fa fa-info-circle"></i> {{ $buyBalanceNotification['message'] }}
</div>

<div>
    <i class="fa fa-info-circle"></i> {{ $sellBalanceNotification['message'] }}
</div>

<div>
    <i class="fa fa-info-circle"></i> {{ $feeBalanceNotification['message'] }}
</div>