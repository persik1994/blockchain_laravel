@extends('layouts.admin.app')

@section('title')
    Pages
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <a href="{{route('admin.pages.add')}}" class="btn btn-default pull-right">
                            <i class="fa fa-plus"></i>
                            Add page
                        </a>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Prefix</th>
                                        <th>Link</th>
                                        <th>Created on</th>
                                        <th>Updated on</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($pages))
                                        @foreach($pages as $page)
                                            <tr>
                                                <td>{{$page->title}}</td>
                                                <td>{{$page->prefix}}</td>
                                                <td><a href="{{url('/') . '/page/' . $page->prefix}}">{{url('/') . '/page/' . $page->prefix}}</a></td>
                                                <td>{{$page->created_at}}</td>
                                                <td>{{$page->updated_at}}</td>
                                                <td width="120px">
                                                    <a href="{{route('admin.pages.edit', $page->id)}}" class="btn btn-sm btn-primary">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    @if($page->prefix != 'terms-of-services' && $page->prefix != 'privacy-policy')
                                                        <button type="button" class="btn btn-sm btn-danger delete-item"
                                                                data-url="{{route('admin.pages.delete', $page->id)}}">
                                                            <i class="fa fa-eraser"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">No entries</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Prefix</th>
                                        <th>Link</th>
                                        <th>Created on</th>
                                        <th>Updated on</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $pages->firstItem() }} to {{ $pages->lastItem() }} from {{ $pages->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $pages->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
