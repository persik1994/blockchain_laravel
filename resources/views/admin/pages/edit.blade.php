@extends('layouts.admin.app')

@section('title')
    Pages
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <form action="{{route('admin.pages.update', $page->id)}}" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title" value="{{$page->title}}">
            </div>
            <div class="form-group">
                <label>Prefix</label>
                <div class="input-group">
                    <span class="input-group-addon">{{url('/page')}}</span>
                    <input type="text" class="form-control" name="prefix" value="{{$page->prefix}}">
                </div>
                <small>Use latin characters and symbols - and _. Do not make spaces between words.</small>
            </div>
            <div class="form-group">
                <label>Content</label>
                <textarea class="cleditor form-control" id="tinymce" rows="15" name="page_content">{{$page->content}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary" name="btn_add"><i class="fas fa-pencil-alt"></i> Update</button>
        </form>
    </section>
@endsection
