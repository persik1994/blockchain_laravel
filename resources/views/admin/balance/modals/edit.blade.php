<form class="js-admin-adminbalance-editBalance"  data-url="{{route('admin.balance.update', $balanceAdminUSD->id )}}">
    <div class="form-group">
        <label>Admin USD balance</label>
        <input class="form-field form-control" name="balance" value="{{ $balanceAdminUSD->balance }}">
    </div>
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fas fa-pencil-alt"></i> Update</button>
</form>