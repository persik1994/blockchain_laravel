<form class="js-admin-gateways-editGateWay" data-url="{{route('admin.gateways.update', $gatewayToEdit->id)}}">
    <div class="form-group">
        <label>Gateway</label>
        <select class="form-field form-control js-admin-gateways-selectGateWay" name="name">
            @foreach(\App\Models\Gateway::$gateways as $key => $gateway)
                <option
                    @if($gatewayToEdit->name == $key) selected @endif
                    value="{{$key}}" data-fields="{{json_encode($gateway['fields'])}}"
                >{{$gateway['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-field form-control" name="currency">
            @foreach(\App\Models\Gateway::$currencies as $key => $gateway)
                <option
                    @if($gatewayToEdit->currency == $key) selected @endif
                    value="{{$key}}"
                >
                    {{$key}}: {{$gateway}}
                </option>
            @endforeach
        </select>
    </div>
    <div id="account_fields">
        @foreach($gatewayToEdit->gateway_information as $key => $information)
            <div class="form-group">
                <label>{{\App\Models\Gateway::$gateways[$gatewayToEdit->name]['fields'][$key]}}</label>
                <input type="text" class="form-field form-control" name="{{$key}}" value="{{$information}}">
            </div>
        @endforeach
    </div>
    <div class="checkbox">
        <label>
            <input class="form-field" type="checkbox" name="allow" value="yes" @if ($gatewayToEdit->allow) checked @endif>
            Allow customers to deposit money through this gateway
        </label>
    </div>
    {{--<div class="checkbox">--}}
        {{--<label>--}}
            {{--<input class="form-field" type="checkbox" name="allow_receive" value="yes" @if ($gatewayToEdit->allow_receive) checked @endif>--}}
            {{--Allow customers to sell Bitcoins to this gateway--}}
        {{--</label>--}}
    {{--</div>--}}
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fas fa-pencil-alt"></i> Update</button>
</form>