<form class="js-admin-gateways-addGateWay">
    <div class="form-group">
        <label>Gateway</label>
        <select class="form-field form-control js-admin-gateways-selectGateWay" name="name">
            @foreach(\App\Models\Gateway::$gateways as $key => $gateway)
                <option value="{{$key}}" data-fields="{{json_encode($gateway['fields'])}}">{{$gateway['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Currency</label>
        <select class="form-field form-control" name="currency">
            @foreach(\App\Models\Gateway::$currencies as $key => $gateway)
                <option value="{{$key}}">{{$key}}: {{$gateway}}</option>
            @endforeach
        </select>
    </div>
    <div id="account_fields"></div>
    <div class="form-group checkbox">
        <label>
            <input class="form-field" type="checkbox" name="allow" value="yes"> Allow customers to deposit money through this gateway
        </label>
    </div>
    {{--<div class="form-group checkbox">--}}
        {{--<label>--}}
            {{--<input class="form-field" type="checkbox" name="allow_receive" value="yes"> Allow customers to sell Bitcoins to this gateway--}}
        {{--</label>--}}
    {{--</div>--}}
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fa fa-plus"></i> Add</button>
</form>