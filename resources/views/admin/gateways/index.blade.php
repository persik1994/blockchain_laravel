@extends('layouts.admin.app')

@section('title')
    Gateways
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <button class="btn btn-default pull-right js-admin-gateways-addGateWayModal" data-title="Add Gateway">
                            <i class="fa fa-plus"></i>
                            Add gateway
                        </button>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Gateway name</th>
                                        <th>Currency</th>
                                        <th>Allow</th>
                                        {{--<th>Allow buy Bitcoins</th>--}}
                                        {{--<th>Allow sell Bitcoins</th>--}}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($gateways))
                                        @foreach($gateways as $gateway)
                                            <tr>
                                                <td>{{\App\Models\Gateway::$gateways[$gateway->name]['name']}}</td>
                                                <td>{{$gateway->currency}}</td>
                                                <td>@if($gateway->allow)<i class="fas fa-check"></i>@else<i class="fas fa-times"></i>@endif</td>
                                                {{--<td>--}}
{{--                                                    @if($gateway->allow_receive)<i class="fas fa-check"></i>@else<i class="fas fa-times"></i>@endif--}}
                                                {{--</td>--}}
                                                <td width="120px">
                                                    <button
                                                        class="btn btn-sm btn-primary js-admin-gateways-editGateWayModal"
                                                        data-url="{{route('admin.gateways.modal.edit', $gateway->id)}}"
                                                        data-title="Edit Gateway"
                                                    >
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger delete-item"
                                                            data-url="{{route('admin.gateways.delete', $gateway->id)}}">
                                                        <i class="fa fa-eraser"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">No entries</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Gateway name</th>
                                        <th>Currency</th>
                                        <th>Allow</th>
                                        {{--<th>Allow buy Bitcoins</th>--}}
                                        {{--<th>Allow sell Bitcoins</th>--}}
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $gateways->firstItem() }} to {{ $gateways->lastItem() }} from {{ $gateways->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $gateways->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
