@extends('layouts.admin.app')

@section('title')
    Support
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5px">
                                    <div class="check-input list-input">
                                        <input type="checkbox" class="checkbox-custom">
                                        <span class="checkbox-custom-label text"></span>
                                    </div>
                                </th>
                                <th>
                                    <botton
                                            class="btn btn-sm btn-default mark-selected-as-viewed hidden-element"
                                            data-type="deposit"
                                            data-url="{{route('admin.support.mark.viewed')}}"
                                    >
                                        <i class="fas fa-eye"></i>
                                    </botton>
                                </th>
                                {{--<th>--}}
                                {{--<botton class="btn btn-sm btn-default">--}}
                                {{--<i class="fas fa-trash"></i>--}}
                                {{--</botton>--}}
                                {{--</th>--}}
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered ticket list">
                                <thead>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>Ticket</th>
                                        <th>User</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($tickets))
                                            @foreach($tickets as $ticket)
                                                <tr class="row-item @if(!$ticket->viewed_by_admin) item-unviewed @endif" >
                                                    <td class="text-center">
                                                        <div class="check-input row-input">
                                                            <input
                                                                    type="checkbox"
                                                                    class="checkbox-custom"
                                                                    value="{{$ticket->id}}"
                                                            >
                                                            <span class="checkbox-custom-label text"></span>
                                                        </div>
                                                    </td>
                                                    <td>{{ $ticket->id }}</td>
                                                    <td>{{ $ticket->title }}</td>
                                                    <td>@if($ticket->user) {{ $ticket->user->name }} @else {{ $ticket->email_address }} @endif</td>
                                                    <td>{{ $ticket['created_at'] }}</td>
                                                    <td>{{ $ticket['updated_at'] }}</td>
                                                    <td width="160px" style="text-align: center;">
                                                        <button
                                                                class="btn btn-sm btn-primary js-admin-tickets-editTicketModal"
                                                                data-url="{{route('admin.support.modal.edit', $ticket->id)}}"
                                                                data-title="Edit Ticket"
                                                        >
                                                            <i class="fas fa-edit"></i>
                                                        </button>
                                                        <a href="{{ route('admin.chat', $ticket->id) }}" class="btn btn-sm btn-info js-admin-tickets-chatTicket"
                                                        >
                                                            <i class="far fa-comments"></i>
                                                            @if(\Illuminate\Support\Facades\Cache::get('ticket_' . $ticket->id . '_messages_unviewed_by_admin') > 0)
                                                                <span class="label label-warning" id="global-notification-count">
                                                                    {{\Illuminate\Support\Facades\Cache::get('ticket_' . $ticket->id . '_messages_unviewed_by_admin')}}
                                                                </span>
                                                            @endif
                                                        </a>
                                                        <button type="button" data-url="{{route('admin.support.delete', $ticket->id)}}" class="btn btn-sm btn-danger delete-item"
                                                        >
                                                            <i class="fa fa-eraser"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">No tickets</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>Ticket</th>
                                        <th>User</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $tickets->firstItem() }} to {{ $tickets->lastItem() }} from {{ $tickets->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $tickets->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.support.templates')
@endsection
