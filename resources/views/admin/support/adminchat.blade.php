@extends('layouts.admin.app')

@section('title')
    Support
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content" id="ticket-chat" data-ticket-id="{{ $ticket->id }}">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        @include('layouts.messages')
                        <div class="table-responsive">
                            <h1>Admin Chat</h1>
                            <hr>
                            Ticket title: {{ $ticket->title }}<br>
                            Title Description: {{ $ticket->description }}  <br>
                            <hr>

                            <div class="messages-wrapper">
                                @if(!$ticket->messages->isEmpty())
                                    @foreach($ticket->messages as $message)
                                        @if($message->user && $message->user->type == 'admin')
                                            <div class="pull-right message"><b>{{ $message->user->name }}:</b> {{ $message->message }} </div>
                                        @elseif($message->user)
                                            <div class="message"><b>{{ $message->user->name }}:</b> {{ $message->message }} </div>
                                        @else
                                            <div class="message"><b>{{ $ticket->email_address }}:</b> {{ $message->message }} </div>
                                        @endif
                                        <br>
                                    @endforeach
                                @endif
                            </div>

                            <!-- Chat Form -->
                            <form method="POST" action="{{ route('admin.chat', $ticket->id ) }}" class="js-admin-support-addMessage">
                                @csrf
                                <div class="form-group">
                                    <label class="support">Message</label>
                                    <textarea class="form-field form-control" name="admin_chat_message" rows="7"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary" name="btn_add"> Send message</button>
                            </form>  
                                                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
