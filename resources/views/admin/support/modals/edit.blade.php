<form class="js-admin-tickets-editTicket" data-url="{{route('admin.support.update', $supportToEdit->id)}}">
    <div class="form-group">
        <label>Title</label>
        <input class="form-field form-control js-admin-tickets-selectTicket" name="title" value="{{$supportToEdit->title}}">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-field form-control js-admin-tickets-selectTicket" name="description">{{$supportToEdit->description}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fas fa-pencil-alt"></i> Update</button>
</form>