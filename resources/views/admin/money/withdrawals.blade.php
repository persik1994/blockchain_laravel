@extends('layouts.admin.app')

@section('title')
    Money withdrawals
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5px">
                                        <div class="check-input list-input">
                                            <input type="checkbox" class="checkbox-custom">
                                            <span class="checkbox-custom-label text"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <botton
                                            class="btn btn-sm btn-default mark-selected-as-viewed hidden-element"
                                            data-type="withdraw"
                                            data-url="{{route('admin.money.mark.viewed')}}"
                                        >
                                            <i class="fas fa-eye"></i>
                                        </botton>
                                    </th>
                                    {{--<th>--}}
                                    {{--<botton class="btn btn-sm btn-default">--}}
                                    {{--<i class="fas fa-trash"></i>--}}
                                    {{--</botton>--}}
                                    {{--</th>--}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered withdraw list">
                                <thead>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>User Req. Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($userMoneyWithdrawals))
                                        @foreach($userMoneyWithdrawals as $moneyWithdrawal)
                                            <tr class="row-item @if(!$moneyWithdrawal->viewed_by_admin) item-unviewed @endif">
                                                <td class="text-center">
                                                    <div class="check-input row-input">
                                                        <input
                                                            type="checkbox"
                                                            class="checkbox-custom"
                                                            value="{{$moneyWithdrawal->id}}"
                                                        >
                                                        <span class="checkbox-custom-label text"></span>
                                                    </div>
                                                </td>
                                                <td>{{$moneyWithdrawal->id}}</td>
                                                <td>@if(isset($moneyWithdrawal->user)) {{$moneyWithdrawal->user->email}} @endif</td>
                                                <td>{{number_format($moneyWithdrawal->amount, 2)}} USD</td>
                                                <td>{{number_format($moneyWithdrawal->user_amount, 2)}} {{$moneyWithdrawal->user_fiat_currency}}</td>
                                                <td>
                                                    @if($moneyWithdrawal->status == \App\Models\UserMoney::STATUS_WAITING)
                                                        <span class="label label-info">Awaiting</span>
                                                    @elseif($moneyWithdrawal->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                                        <span class="label label-success">Confirmed</span>
                                                    @elseif($moneyWithdrawal->status == \App\Models\UserMoney::STATUS_CANCELED)
                                                        <span class="label label-danger">Canceled</span>
                                                    @endif
                                                <td width="120px">
                                                    <a href="#"
                                                       data-title="Withdrawal details"
                                                       data-id="{{ $moneyWithdrawal->id }}"
                                                       data-url="{{ route('admin.withdrawal.details.modal', $moneyWithdrawal->id) }}"
                                                       data-proof-image-upload-url="{{ route('admin.withdrawal.proof-image.modal', $moneyWithdrawal->id) }}"
                                                       data-status="{{ $moneyWithdrawal->status }}"
                                                       data-email="{{ $moneyWithdrawal->user->email }}"
                                                       data-amount="{{ $moneyWithdrawal->amount }}"
                                                       data-type="withdraw"
                                                       class="btn btn-default btn-small js-admin-users-money-openDetailModal">
                                                        <i class="fas fa-search"></i> Explore
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">No entries</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>User Req. Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $userMoneyWithdrawals->firstItem() }} to {{ $userMoneyWithdrawals->lastItem() }} from {{ $userMoneyWithdrawals->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $userMoneyWithdrawals->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.money.templates')
@endsection
