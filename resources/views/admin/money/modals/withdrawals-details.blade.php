<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>Trade info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>Order ID: </b> <span class="pull-right">{{ $userWithdraw->id }}</span></td>
            </tr>
            <tr>
                <td><b>Trade type: </b> <span class="pull-right">{{ $userWithdraw->transaction_type  }}</span></td>
            </tr>
            <tr>
                <td><b>Amount: </b> <span class="pull-right">{{ $userWithdraw->amount }} USD</span></td>
            </tr>
            <tr><td><b>Available balance: </b> <span class="pull-right">{{ $userWithdraw->user->availableAmount('USD') }} USD</span></td></tr>
            <tr>
                <td>
                    <b>Status</b>
                    <span class="pull-right">
                        @if($userWithdraw->status === \App\Models\UserMoney::STATUS_WAITING)
                            <span class="label label-info">Awaiting</span>
                        @elseif($userWithdraw->status === \App\Models\UserMoney::STATUS_CONFIRMED)
                            <span class="label label-success">Processed</span>
                        @elseif($userWithdraw->status === \App\Models\UserMoney::STATUS_CANCELED)
                            <span class="label label-danger">Canceled</span>
                        @endif
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>User info</small>
                    </h3>
                </td>
            </tr>
            </thead>
                <tbody>
                    <tr>
                        <td><b>User</b> <span class="pull-right"><a>{{ $userWithdraw->user->email }}</a></span></td>
                    </tr>
                    @if(!empty($userWithdraw->user->bankAccount))
                        <tr><td><b>Bank Account Holder Name:</b> <span class="pull-right">{{$userWithdraw->user->bankAccount->u_field_1}}</span></td></tr>
                        <tr><td><b>Bank Account Number/IBAN:</b> <span class="pull-right">{{$userWithdraw->user->bankAccount->u_field_2}}</span></td></tr>
                        <tr><td><b>Bank Swift Code:</b> <span class="pull-right">{{$userWithdraw->user->bankAccount->u_field_3}}</span></td></tr>
                        <tr><td><b>Bank Country,City,Address:</b> <span class="pull-right">{{$userWithdraw->user->bankAccount->u_field_4}}</span></td></tr>
                    @endif
                </tbody>
        </table>
    </div>
</div>