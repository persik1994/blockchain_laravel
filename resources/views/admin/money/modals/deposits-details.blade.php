<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>Trade info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b>Order ID: </b> <span class="pull-right">{{ $userDeposit->id }}</span></td>
                </tr>
                <tr>
                    <td><b>Trade type: </b> <span class="pull-right">{{ $userDeposit->transaction_type  }}</span></td>
                </tr>
                <tr>
                    <td><b>Payment method: </b> <span class="pull-right">{{\App\Models\Gateway::$gateways[$userDeposit->gateway->name]['name']}}</span></td>
                </tr>
                <tr>
                    <td><b>Amount in Credit Card USD: </b> <span class="pull-right">{{ $userDeposit->amount }} USD</span></td>
                </tr>
                <tr>
                    <td>
                        <b>Status</b>
                        <span class="pull-right">
                                @if($userDeposit->status === \App\Models\UserMoney::STATUS_WAITING)
                                <span class="label label-info">Awaiting</span>
                            @elseif($userDeposit->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                <span class="label label-success">Confirmed</span>
                            @elseif($userDeposit->status == \App\Models\UserMoney::STATUS_CANCELED)
                                <span class="label label-danger">Canceled</span>
                            @endif
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>User info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b>User:</b> <span class="pull-right"><a>{{ $userDeposit->user->email }}</a></span></td>
                </tr>
                <tr>
                    <td>
                        <b>Document for payment:</b>
                        <br>
                        <a href="{{route('admin.file', $userDeposit->attachment)}}" target="_blank">
                            <small>Click to preview full size.</small>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>