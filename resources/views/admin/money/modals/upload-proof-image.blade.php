<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Attach payment proof (allowed: jpg,png and pdf, max 5MB)</label>
            <input type="file" name="uploadFile" class="form-control">
        </div>
    </div>
</div>