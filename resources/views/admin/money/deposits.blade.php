@extends('layouts.admin.app')

@section('title')
    Money deposits
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5px">
                                        <div class="check-input list-input">
                                            <input type="checkbox" class="checkbox-custom">
                                            <span class="checkbox-custom-label text"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <botton
                                            class="btn btn-sm btn-default mark-selected-as-viewed hidden-element"
                                            data-type="deposit"
                                            data-url="{{route('admin.money.mark.viewed')}}"
                                        >
                                            <i class="fas fa-eye"></i>
                                        </botton>
                                    </th>
                                    {{--<th>--}}
                                        {{--<botton class="btn btn-sm btn-default">--}}
                                            {{--<i class="fas fa-trash"></i>--}}
                                        {{--</botton>--}}
                                    {{--</th>--}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered deposit list">
                                <thead>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Payment Method</th>
                                        <th>Payment Amount</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!$userMoneyDeposits->isEmpty())
                                        @foreach($userMoneyDeposits as $moneyDeposit)
                                            <tr class="row-item @if(!$moneyDeposit->viewed_by_admin) item-unviewed @endif" >
                                                <td class="text-center">
                                                    <div class="check-input row-input">
                                                        <input
                                                            type="checkbox"
                                                            class="checkbox-custom"
                                                            value="{{$moneyDeposit->id}}"
                                                        >
                                                        <span class="checkbox-custom-label text"></span>
                                                    </div>
                                                </td>
                                                <td>{{$moneyDeposit->id}}</td>
                                                <td>@if(isset($moneyDeposit->user)) {{$moneyDeposit->user->email}} @endif</td>
                                                <td>@if(isset($moneyDeposit->gateway)) {{\App\Models\Gateway::$gateways[$moneyDeposit->gateway->name]['name']}} @endif</td>
                                                <td>
                                                    {{$moneyDeposit->current_amount}} {{$moneyDeposit->gateway->currency}}
                                                </td>
                                                <td>
                                                    {{$moneyDeposit->amount}} USD
                                                </td>
                                                <td>
                                                    @if($moneyDeposit->status == \App\Models\UserMoney::STATUS_WAITING)
                                                        <span class="label label-info">Awaiting</span>
                                                    @elseif($moneyDeposit->status == \App\Models\UserMoney::STATUS_CONFIRMED)
                                                        <span class="label label-success">Confirmed</span>
                                                    @elseif($moneyDeposit->status == \App\Models\UserMoney::STATUS_CANCELED)
                                                        <span class="label label-danger">Canceled</span>
                                                    @endif
                                                </td>
                                                <td width="120px">
                                                    <a href="#"
                                                       data-title="Deposit details"
                                                       data-id="{{ $moneyDeposit->id }}"
                                                       data-url="{{ route('admin.deposit.details.modal', $moneyDeposit->id) }}"
                                                       data-status="{{ $moneyDeposit->status }}"
                                                       data-email="{{ $moneyDeposit->user->email }}"
                                                       data-amount="{{ $moneyDeposit->amount }}"
                                                       data-type="deposit"
                                                       class="btn btn-default btn-small js-admin-users-money-openDetailModal">
                                                        <i class="fas fa-search"></i> Explore
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">No entries</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Payment Method</th>
                                        <th>Payment Amount</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $userMoneyDeposits->firstItem() }} to {{ $userMoneyDeposits->lastItem() }} from {{ $userMoneyDeposits->total() }} rows</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $userMoneyDeposits->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.money.templates')
@endsection
