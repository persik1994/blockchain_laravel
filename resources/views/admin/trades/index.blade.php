@extends('layouts.admin.app')

@section('title')
    {{ucfirst($type)}} Trades
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5px">
                                        <div class="check-input list-input">
                                            <input type="checkbox" class="checkbox-custom">
                                            <span class="checkbox-custom-label text"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <botton
                                            class="btn btn-sm btn-default mark-selected-as-viewed hidden-element"
                                            data-type="{{$type}}"
                                            data-url="{{route('admin.trades.mark.viewed')}}"
                                        >
                                            <i class="fas fa-eye"></i>
                                        </botton>
                                    </th>
                                    {{--<th>--}}
                                    {{--<botton class="btn btn-sm btn-default">--}}
                                    {{--<i class="fas fa-trash"></i>--}}
                                    {{--</botton>--}}
                                    {{--</th>--}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered {{$type}} list">
                                <thead>
                                    <tr>
                                        <th width="5px" class="text-center"><i class="fas fa-bell"></i></th>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Crypto Amount</th>
                                        <th>Fiat Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(!empty($trades))
                                    @foreach($trades as $trade)
                                        <tr class="row-item @if(!$trade->viewed_by_admin) item-unviewed @endif">
                                            <td class="text-center">
                                                <div class="check-input row-input">
                                                    <input
                                                            type="checkbox"
                                                            class="checkbox-custom"
                                                            value="{{$trade->id}}"
                                                    >
                                                    <span class="checkbox-custom-label text"></span>
                                                </div>
                                            </td>
                                            <td>{{$trade->id}}</td>
                                            <td>
                                                <a href="javascript:void(0)">{{$trade->user->name}}</a>
                                            </td>
                                            <td>{{convertToCrypto($trade->crypto_amount, $trade->crypto_currency)}} {{$trade->crypto_currency}}</td>
                                            <td>{{$trade->fiat_amount}} USD</td>
                                            <td>
                                                @if($trade->status === \App\Models\Order::STATUS_WAITING)
                                                    <span class="label label-info">Awaiting</span>
                                                @elseif($trade->status === \App\Models\Order::STATUS_PROCESSING)
                                                    <span class="label label-success">Processing</span>
                                                @elseif($trade->status === \App\Models\Order::STATUS_CONFIRMED)
                                                    <span class="label label-success">Confirmed</span>
                                                @elseif($trade->status === \App\Models\Order::STATUS_CANCELED)
                                                    <span class="label label-danger">Canceled</span>
                                                @else
                                                    <span class="label label-defualt">Unknown</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="#"
                                                   data-title="{{ ucfirst($type) }} details"
                                                   data-id="{{ $trade->id }}"
                                                   data-url="{{ route('admin.trades.modal.details') }}"
                                                   data-status="{{ $trade->status }}"
                                                   data-email="{{ $trade->user->email }}"
                                                   data-amount="{{ $trade->crypto_currency == 'BTC' ? formatBitcoin($trade->crypto_amount) : formatEthereum($trade->crypto_amount) }}"
                                                   data-type="{{ $type }}"
                                                   class="btn btn-default btn-small js-admin-users-trades-openDetailsModal">
                                                    <i class="fas fa-search"></i> Explore
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No entries</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class="text-center"><i class="fas fa-bell"></i></th>
                                    <th>ID</th>
                                    <th>User</th>
                                    <th>Crypto Amount</th>
                                    <th>Fiat Amount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <div id="show-row-information">Show {{ $trades->firstItem() }} to {{ $trades->lastItem() }}
                                from {{ $trades->total() }} rows
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="pull-right">
                                {!! $trades->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.trades.templates')
@endsection
