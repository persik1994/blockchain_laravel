<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>Trade info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>Order ID: </b> <span class="pull-right">{{ $trade->id }}</span></td>
            </tr>
            <tr>
                <td><b>Trade type: </b> <span class="pull-right">User {{ $trade->order_type }} @if($trade->crypto_currency == 'BTC') Bitcoins @elseif($trade->crypto_currency == 'ETH') Ethereums @endif from you</span>
                </td>
            </tr>
            <tr>
                <td><b>Amount in {{ $trade->crypto_currency}}: </b> <span class="pull-right">{{ formatBitcoin($trade->crypto_amount) }}</span></td>
            </tr>
            <tr>
                <td><b>Amount in USD: </b> <span class="pull-right">{{ $trade->fiat_amount }}</span></td>
            </tr>
            <tr>
                <td>
                    <b>Status</b>
                    <span class="pull-right">
                            @if($trade->status === \App\Models\Order::STATUS_WAITING)
                            <span class="label label-info">Awaiting</span>
                        @elseif($trade->status === \App\Models\Order::STATUS_PROCESSING)
                            <span class="label label-success">Processing</span>
                        @elseif($trade->status === \App\Models\Order::STATUS_CONFIRMED)
                            <span class="label label-success">Confirmed</span>
                        @elseif($trade->status === \App\Models\Order::STATUS_CANCELED)
                            <span class="label label-danger">Canceled</span>
                        @else
                            <span class="label label-defualt">Unknown</span>
                        @endif
                        </span>
                </td>
            </tr>
            @if($trade->crypto_currency == 'ETH')
            <tr>
                <td>
                    <b>Enter passphrase</b>
                    <span>
                        <input type="password" class="form-field form-control passphrase" name="passphrase">
                    </span>
                </td>
            </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>
                    <h3>
                        <small>User info</small>
                    </h3>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>User:</b> <span class="pull-right"><a>{{ $trade->user->email }}</a></span></td>
            </tr>
            <tr>
                <td>
                    <b>User wallet addresses:</b>


                    @if(!empty($trade->user) && !empty($trade->user->addresses) )
                        <div class="pull-right">
                            @foreach($trade->user->addresses as $address)
                                <div><label>{{$address->label}}: </label><span>{{$address->address}}</span></div>
                            @endforeach
                        </div>
                    @endif


                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>