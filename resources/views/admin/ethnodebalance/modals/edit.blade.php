<div>
	@php 
		$ethereumAccountsInformation = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_accounts();
        foreach ($ethereumAccountsInformation as $ethereum_infokey => $ethereumAccountInformation) 
        {
             $acctInfo = $ethereumAccountsInformation[$ethereum_infokey];
             $acctBal = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($acctInfo);
             $decimal = pow(10, 18);
             $acctBalHexToDec = hexdec($acctBal) / $decimal;
             echo "<p><i class='fa fa-info-circle'></i> Your current balance in your address <b>($acctInfo)</b>: $acctBalHexToDec ETH</p>";
        }
	@endphp
</div>

