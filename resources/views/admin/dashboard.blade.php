@extends('layouts.admin.app')

@section('title')
    Dashboard
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$users->count()}}</h3>

                        <p>USERS</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <a href="{{route('admin.users')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{formatBitcoin($walletInfo['balance'])}}</h3>

                        <p>BITCOIN NODE BALANCE</p>
                    </div>
                    <div class="icon">
                        <i class="fab fa-bitcoin"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ formatEthereum($ethereumAllNodeBalance) }}</h3>

                        <p>ETHEREUM NODE BALANCE</p>
                    </div>
                    <div class="icon">
                        <i class="fab fa-ethereum"></i>
                    </div>
                    <a style="cursor: pointer;" href="{{ route('admin.ethereumadresses') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> 
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>${{ number_format($adminUsdBalance, 2) }}</h3>
                        <p>ADMIN USD BALANCE</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <a style="cursor: pointer;" 
                    class="small-box-footer js-admin-adminbalance-editBalanceModal"
                    data-url="{{ route('admin.balance.modal.edit', $settings->id) }}"
                    >Edit info <i class="fas fa-edit"></i></a> 
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ formatBitcoin($adminBtcBalance) }}</h3>
                        <p>ADMIN BITCOIN BALANCE</p>
                    </div>
                    <div class="icon">
                        <i class="fab fa-bitcoin"></i>
                    </div>
                    <a style="cursor: pointer;" 
                    class="small-box-footer js-admin-adminbalance-editBalanceModal"
                    data-url="{{ route('admin.btcbalance.modal.edit', $settings->id) }}"
                    >More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ formatEthereum($adminEthBalance) }}</h3>

                        <p>ADMIN ETHEREUM BALANCE</p>
                    </div>
                    <div class="icon">
                        <i class="fab fa-ethereum"></i>
                    </div>
                    <a style="cursor: pointer;" 
                    class="small-box-footer js-admin-adminbalance-editBalanceModal"
                    data-url="{{ route('admin.ethbalance.modal.edit', $settings->id) }}"
                    >More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            
        </div>


        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">New Buy &amp; Sell Bitcoins Requests</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Type</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($cryptoCurrenciesRequests))
                                @foreach($cryptoCurrenciesRequests as $cryptoCurrenciesRequest)
                                    <tr>
                                        <td>
                                            <a>{{$cryptoCurrenciesRequest->user->name}}</a>
                                        </td>
                                        <td>
                                            {{ $cryptoCurrenciesRequest->order_type === 'sell' ? 'Buy Bitcoins' : 'Sell Bitcoins' }}
                                        </td>
                                        <td>{{$cryptoCurrenciesRequest->crypto_amount}} {{$cryptoCurrenciesRequest->crypto_currency}} ({{$cryptoCurrenciesRequest->fiat_amount}} USD)</td>
                                        <td>
                                            @if($cryptoCurrenciesRequest->status === \App\Models\Order::STATUS_WAITING)
                                                <span class="label label-info">Awaiting</span>
                                            @elseif($cryptoCurrenciesRequest->status === \App\Models\Order::STATUS_PROCESSING)
                                                <span class="label label-success">Processing</span>
                                            @elseif($cryptoCurrenciesRequest->status == \App\Models\Order::STATUS_CONFIRMED)
                                                <span class="label label-success">Confirmed</span>
                                            @elseif($cryptoCurrenciesRequest->status == \App\Models\Order::STATUS_CANCELED)
                                                <span class="label label-danger">Canceled</span>
                                            @else
                                                <span class="label label-defualt">Unknown</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#"
                                               data-title="{{ ucfirst($cryptoCurrenciesRequest->order_type) }} details"
                                               data-id="{{ $cryptoCurrenciesRequest->id }}"
                                               data-url="{{ route('admin.trades.modal.details') }}"
                                               data-status="{{ $cryptoCurrenciesRequest->status }}"
                                               data-email="{{ $cryptoCurrenciesRequest->user->email }}"
                                               data-amount="{{ $cryptoCurrenciesRequest->crypto_amount }}"
                                               data-type="{{ $cryptoCurrenciesRequest->order_type }}"
                                               class="btn btn-default btn-small js-admin-users-trades-openDetailsModal">
                                                <i class="fas fa-search"></i> Explore
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">No entries</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{--<div class="box box-info">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Latest transactions</h3>--}}

                {{--<div class="box-tools pull-right">--}}
                    {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                    {{--</button>--}}
                    {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--<div class="table-responsive">--}}
                    {{--<table class="table no-margin">--}}
                        {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>Transaction</th>--}}
                                {{--<th>User</th>--}}
                                {{--<th>Address</th>--}}
                                {{--<th>Amount</th>--}}
                                {{--<th>Time</th>--}}
                                {{--<th>Status</th>--}}
                                {{--<th>Confirmations</th>--}}
                            {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                            {{--<tr>--}}
                                {{--<td>--}}
                                    {{--<span title="63bd0c221192e7cea564ff5293c2f0209c0e0b65c4c3656f7ff5979fb73d4469">63bd0c221192e...</span>--}}
                                {{--</td>--}}
                                {{--<td>anspotadmin</td>--}}
                                {{--<td>2NAzaG3JBMUCNFpcdiB4upQo4ERHaCTzV89</td>--}}
                                {{--<td>0.1719438 BTC</td>--}}
                                {{--<td>28/09/2018 04:53</td>--}}
                                {{--<td>--}}
                                    {{--<span class="text-success"><i class="far fa-arrow-alt-circle-down"></i> Received</span>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--0 Confirmations--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="box-footer clearfix">--}}
                {{--<a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Transactions</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>
@endsection
