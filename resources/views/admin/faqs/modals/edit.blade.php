<form class="js-admin-faqs-editFaq" data-url="{{route('admin.faqs.update', $faqToEdit->id)}}">
    <div class="form-group">
        <label>Question</label>
        <input class="form-field form-control js-admin-faqs-selectFaq" name="question" value="">
    </div>
    <div class="form-group">
        <label>Answer</label>
        <textarea class="form-field form-control js-admin-faqs-selectFaq" name="answer"></textarea>
    </div>
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fas fa-pencil-alt"></i> Update</button>
</form>