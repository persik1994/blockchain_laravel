<form class="js-admin-faqs-addFaq">
    <div class="form-group">
        <label>Question</label>
        <input class="form-field form-control" name="question">
    </div>
    <div class="form-group">
        <label>Answer</label>
        <textarea class="form-field form-control" name="answer"></textarea>
    </div>
    <button type="submit" class="btn btn-primary" name="btn_add"><i class="fa fa-plus"></i> Add</button>
</form>