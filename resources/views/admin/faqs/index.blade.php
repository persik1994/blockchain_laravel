@extends('layouts.admin.app')

@section('title')
    Faq
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <button  class="btn btn-default pull-right js-admin-faqs-addFaqModal" data-title="Add Faq">
                            <i class="fa fa-plus"></i>
                            Add FAQ
                        </button>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Created on</th>
                                        <th>Updated on</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                            @if(!empty($faqs))
                                    @foreach($faqs as $faq)                                    
                                    <tr>
                                        <td>{{ $faq['question'] }}</td>
                                        <td>{{ $faq['created_at'] }}</td>
                                        <td>{{ $faq['updated_at'] }}</td>
                                        <td width="120px">
                                            <button
                                                class="btn btn-sm btn-primary js-admin-faqs-editFaqModal"
                                                data-url="{{route('admin.faqs.modal.edit', $faq->id)}}"
                                                data-title="Edit Faq"
                                            >
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger delete-item"
                                                    data-url="{{route('admin.faqs.delete', $faq->id)}}">
                                                <i class="fa fa-eraser"></i>    
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No entry</td>
                                </tr>
                            @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Question</th>
                                        <th>Created on</th> 
                                        <th>Updated on</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
