@extends('layouts.admin.app')

@section('title')
    Bitcoin settings
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Bitcoin Settings
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.bitcoin.save') }}" class="js-admin-settings-saveBitcoin" method="POST">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="auto-update-bitcoin-price" value="0">
                            <input type="checkbox" name="auto-update-bitcoin-price" class="js-admin-settings-autoUpdateChange" value="1" {{ $settings->autoupdate_bitcoin_price == 1 ? 'checked' : '' }}> Auto update Bitcoin price
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Buy fee</label>
                        <input type="text" class="form-control" name="bitcoin-buy-fee" {{ $settings->autoupdate_bitcoin_price == 1 ? '' : 'disabled' }} value="{{ $settings->bitcoin_buy_fee }}">
                        <small>If autoupdate is turned on, enter fee percentage without <b>%</b>, for buy fee enter percentage with minus. For example: -3</small>
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Sell fee</label>
                        <input type="text" class="form-control" name="bitcoin-sell-fee" {{ $settings->autoupdate_bitcoin_price == 1 ? '' : 'disabled' }} value="{{ $settings->bitcoin_sell_fee }}">
                        <small>If autoupdate is turned on, enter fee percentage without <b>%</b>, for sell fee enter percentage without minus. For example: 3</small>
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Fixed Price</label>
                        <input type="text" class="form-control" name="bitcoin-fixed-price" {{ $settings->autoupdate_bitcoin_price == 1 ? 'disabled' : '' }} value="{{ $settings->bitcoin_fixed_price }}">
                        <small>If autoupdate is turned off, Enter your bitcoin price will be used for buy and sell. If autoupdae is turned on, leave empty.</small>
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Network Transaction Fee</label>
                        <input type="text" class="form-control" name="btc-fee" value="{{ $txFee }}">
                        <small>Enter the transaction fee which Bitcoin Node will charge clients for their transaction. The bitcoin fee should be tailored to the current cost of Bitcoin. This will be important factor for transaction confirmation. For example if Bitcoin price is 8000 USD, and client send 1 BTC, the fee must be 0.001 to confirm transaction from ~1 up to ~3 hours. The 0.0003 is best solution for transaction fee.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i> Save changes
                    </button>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Generate Admin Bitcoind addresses
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.bitcoin.generate') }}" class="js-admin-settings-generateCryptoAddresses" method="POST">
                    <div class="form-group">
                        <label>Bitcoin Fee Address</label>
                        <input type="text" class="form-control" name="fee_address" disabled value="{{ $settings->btc_fee_address }}">
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Buy Address</label>
                        <input type="text" class="form-control" name="sell_address" disabled value="{{ $settings->btc_sell_address }}">
                    </div>
                    <div class="form-group">
                        <label>Bitcoin Sell Address</label>
                        <input type="text" class="form-control" name="buy_address" disabled value="{{ $settings->btc_buy_address }}">
                    </div>
                    @if(empty($settings->btc_fee_address) || empty($settings->btc_sell_address) || empty($settings->btc_buy_address))
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> Generate addresses
                        </button>
                    @endif
                </form>
            </div>
        </div>
    </section>
@endsection