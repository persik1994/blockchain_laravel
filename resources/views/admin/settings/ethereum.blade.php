@extends('layouts.admin.app')

@section('title')
    Ethereum settings
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ethereum Settings
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.ethereum.save') }}" class="js-admin-settings-saveEthereum" method="POST">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="auto-update-ethereum-price" value="0">
                            <input type="checkbox" name="auto-update-ethereum-price" class="js-admin-settings-autoUpdateChange" value="1" {{ $settings->autoupdate_ethereum_price == 1 ? 'checked' : '' }}> Auto update Ethereum price
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Ethereum Buy fee</label>
                        <input type="text" class="form-control" name="ethereum-buy-fee" {{ $settings->autoupdate_ethereum_price == 1 ? '' : 'disabled' }} value="{{ $settings->ethereum_buy_fee }}">
                        <small>If autoupdate is turned on, enter fee percentage without <b>%</b>, for buy fee enter percentage with minus. For example: -3</small>
                    </div>
                    <div class="form-group">
                        <label>Ethereum Sell fee</label>
                        <input type="text" class="form-control" name="ethereum-sell-fee" {{ $settings->autoupdate_ethereum_price == 1 ? '' : 'disabled' }} value="{{ $settings->ethereum_sell_fee }}">
                        <small>If autoupdate is turned on, enter fee percentage without <b>%</b>, for sell fee enter percentage without minus. For example: 3</small>
                    </div>
                    <div class="form-group">
                        <label>Ethereum Fixed Price</label>
                        <input type="text" class="form-control" name="ethereum-fixed-price" {{ $settings->autoupdate_ethereum_price == 1 ? 'disabled' : '' }} value="{{ $settings->ethereum_fixed_price }}">
                        <small>If autoupdate is turned off, Enter your bitcoin price will be used for buy and sell. If autoupdae is turned on, leave empty.</small>
                    </div>
                    <div class="form-group">
                        <label>Ethereum Gas Price</label>
                        <input type="text" class="form-control" name="ethereum-gas-price" value="{{ $settings->ethereum_gas_price }}">
                        <small>Gas price refers to the amount of Ether you’re willing to pay for every unit of gas, and is usually measured in "Gwei"</small>
                    </div>
                    <div class="form-group">
                        <label>Ethereum Gas Limit</label>
                        <input type="text" class="form-control" name="ethereum-gas-limit" value="{{ $settings->ethereum_gas_limit  }}">
                        <small>Gas limit refers to the maximum amount of gas you’re willing to spend on a particular transaction</small>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i> Save changes
                    </button>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Generate Admin Ethereum addresses
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.ethereum.generate') }}" class="js-admin-settings-generateCryptoAddresses" method="POST">
                    <div class="form-group">
                        <label>Ethereum Fee Address</label>
                        <input type="text" class="form-control" name="fee_address" disabled value="{{ $settings->eth_fee_address }}">

                        @if(empty($settings->eth_fee_address))
                            <div class="addresses-password-wrapper">
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="fee_address_password" placeholder="Fee address password">
                                    </div>
                                    <div class="col-md-9">
                                        <br>
                                        <small class="text-center">Plase save your password before generate, we will not keep it.</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="fee_address_password_confirmation" placeholder="Repeat password">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Ethereum Buy Address</label>
                        <input type="text" class="form-control" name="sell_address" disabled value="{{ $settings->eth_sell_address }}">

                        @if(empty($settings->eth_sell_address))
                            <div class="addresses-password-wrapper">
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="sell_address_password" placeholder="Sell address password">
                                    </div>
                                    <div class="col-md-9">
                                        <br>
                                        <small>Plase save your password before generate, we will not keep it.</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="sell_address_password_confirmation" placeholder="Repeat password">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Ethereum Sell Address</label>
                        <input type="text" class="form-control" name="buy_address" disabled value="{{ $settings->eth_buy_address }}">

                        @if(empty($settings->eth_buy_address))
                            <div class="addresses-password-wrapper">
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="buy_address_password" placeholder="Buy address password">
                                    </div>
                                    <div class="col-md-9">
                                        <br>
                                        <small>Plase save your password before generate, we will not keep it.</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="password" class="form-control" name="buy_address_password_confirmation" placeholder="Repeat password">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    @if(empty($settings->eth_buy_address) || empty($settings->eth_sell_address) || empty($settings->eth_fee_address))
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i>

                            Generate addresses
                        </button>
                    @endif
                </form>
            </div>
        </div>

    </section>
@endsection