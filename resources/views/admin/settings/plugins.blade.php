@extends('layouts.admin.app')

@section('title')
    Plugins settings
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Plugins
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.plugins.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="checkbox">
                        <label>

                            <input type="checkbox" name="plugin_buy_bitcoins" @if($settings->plugin_buy_bitcoins) checked @endif>
                            Enable/disable Buy Plugin

                        </label>
                    </div>
                    <div class="checkbox">
                        <label>

                            <input type="checkbox" name="plugin_sell_bitcoins" @if($settings->plugin_sell_bitcoins) checked @endif>
                            Enable/disable Sell Plugin

                        </label>
                    </div>
                    <div class="checkbox">
                        <label>

                            <input type="checkbox" name="plugin_transfer_bitcoins" @if($settings->plugin_transfer_bitcoins) checked @endif>
                            Enable/disable Transfer Plugin

                        </label>
                    </div>
                    <div class="checkbox">
                        <label>

                            <input type="checkbox" name="plugin_request_bitcoins" @if($settings->plugin_request_bitcoins) checked @endif>
                            Enable/disable Request Plugin

                        </label>
                    </div>
                    <div class="form-group">
                        <label>Process time in hours when client make request to buy</label>
                        <input type="text" class="form-control" name="process_time_to_buy" value="{{$settings->process_time_to_buy}}">
                    </div>
                    <div class="form-group">
                        <label>Process time in hours when client make request to sell</label>
                        <input type="text" class="form-control" name="process_time_to_sell" value="{{$settings->process_time_to_sell}}">
                    </div>
                    <input type="submit" class="btn btn-primary" name="btn_save" value="Save changes">
                </form>
            </div>
        </div>
    </section>
@endsection
