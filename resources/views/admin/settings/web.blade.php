@extends('layouts.admin.app')

@section('title')
    Web settings
@endsection

@php
    $languages = [
        'en' => 'English'
    ];
@endphp

@section('content')
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
    </section>
    <section class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Web Settings
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.settings.web.save') }}" class="js-admin-settings-saveWeb" method="POST">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" value="{{ $settings->title }}">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description"
                                  rows="2">{{ $settings->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Keywords</label>
                        <textarea class="form-control" name="keywords" rows="2">{{ $settings->keywords }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Site name</label>
                        <input type="text" class="form-control" name="name" value="{{ $settings->name }}">
                    </div>
                    <div class="form-group">
                        <label>Site url address</label>
                        <input type="text" class="form-control" name="url" value="{{ $settings->url }}">
                    </div>
                    <div class="form-group">
                        <label>Info email address</label>
                        <input type="text" class="form-control" name="infoemail" value="{{ $settings->infoemail }}">
                    </div>
                    <div class="form-group">
                        <label>Support email address</label>
                        <input type="text" class="form-control" name="supportemail"
                               value="{{ $settings->supportemail }}">
                    </div>
                    <div class="form-group">
                        <label>Default language</label>
                        <select class="form-control" name="default_language">
                            @foreach($languages as $languageCode => $languageName)
                                <option {{ $settings->default_language == $languageCode ? 'selected' : '' }} value="{{ $languageCode }}">{{ $languageName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Default currency</label>
                        <select class="form-control" name="default_currency" disabled>
                            @foreach(\App\Models\Gateway::$currencies as $currencyCode => $currencyName)
                                <option {{ $settings->default_currency == $currencyCode ? 'selected' : '' }} value="{{ $currencyCode }}">{{ $currencyName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Commission when client withdrawal or send Bitcoins to other address</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="withdrawal_comission"
                                   value="{{ $settings->withdrawal_comission }}">
                            <span class="input-group-addon">BTC</span>
                        </div>
                        <small>This commission automatically will be transferred in your Bitcoin address</small>
                    </div>
                    <div class="form-group">
                        <label>Max wallet addresses per account</label>
                        <input type="text" class="form-control" name="max_addresses_per_account"
                               value="{{ $settings->max_addresses_per_account }}">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="document_verification" value="0">
                            <input type="checkbox" name="document_verification"
                                   value="1" {{ $settings->document_verification == 1 ? 'checked' : '' }}> Require user to
                            upload documents and you verify it before exchange
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="email_verification" value="0">
                            <input type="checkbox" name="email_verification"
                                   value="1" {{ $settings->email_verification == 1 ? 'checked' : '' }}> Require user to
                            verify their email address before exchange
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="phone_verification" value="0">
                            <input type="checkbox" name="phone_verification"
                                   value="1" {{ $settings->phone_verification == 1 ? 'checked' : '' }}> Require user to
                            verify their mobile number before exchange
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Nexmo API Key</label>
                        <input type="text" class="form-control" name="nexmo_api_key"
                               value="{{ $settings->nexmo_api_key }}">
                        <small>Type Nexmo API Key if you turned on mobile verification. Get api key form <a
                                    href="http://nexmo.com" target="_blank">www.nexmo.com</a></small>
                    </div>
                    <div class="form-group">
                        <label>Nexmo API Secret</label>
                        <input type="text" class="form-control" name="nexmo_api_secret"
                               value="{{ $settings->nexmo_api_secret }}">
                        <small>Type Nexmo API Secret if you turned on mobile verification. Get api key form <a
                                    href="http://nexmo.com" target="_blank">www.nexmo.com</a></small>
                    </div>
                    <div class="form-group">
                        <label>Facebook profile url</label>
                        <input type="text" class="form-control" name="fb_link" value="{{ $settings->fb_link }}">
                    </div>
                    <div class="form-group">
                        <label>Twitter profile url</label>
                        <input type="text" class="form-control" name="tw_link" value="{{ $settings->tw_link }}">
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i> Save changes
                    </button>
                </form>
            </div>
        </div>
    </section>
@endsection