@extends('layouts.admin.auth')

@section('styles')
    <style>
        body {
            background: #d2d6de;
        }

        /* Login checkbox */

        .checkbox-custom {
            opacity: 0;
            position: absolute;
        }

        .checkbox-custom, .checkbox-custom-label {
            display: inline-block;
            vertical-align: middle;
            cursor: pointer;
        }

        .checkbox-custom-label {
            position: relative;
            font-weight: normal;
        }

        .checkbox-custom + .checkbox-custom-label:before {
            content: '';
            background: #fff;
            border: 1px solid #ddd;
            display: inline-block;
            vertical-align: middle;
            width: 20px;
            height: 20px;
            padding: 2px;
            margin-right: 10px;
            text-align: center;
        }

        .checkbox-custom:checked + .checkbox-custom-label:before {
            background: #777;
            box-shadow: inset 0px 0px 0px 4px #fff;
        }

        .checkbox-custom:focus + .checkbox-custom-label {
            outline: 1px solid #ddd;
        }
    </style>
@endsection

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{$settings->name}}</b></a>
        </div>

        @if ($errors->any())
            <div class="callout callout-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif


        <div class="login-box-body">
            <p class="login-box-msg">Log Into Dashboard</p>

            <form action="{{ url('/admin/login') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" required placeholder="Email" autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" required placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <input type="checkbox" name="remember" id="remember" class="checkbox-custom">
                        <label for="remember" class="checkbox-custom-label">Remomber me</label>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
                    </div>
                </div>
            </form>

            <a href="{{ route('admin.password.request') }}">Lost your password ?</a><br>
        </div>
    </div>
@endsection
