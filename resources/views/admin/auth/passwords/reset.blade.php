@extends('layouts.admin.auth')

@section('styles')
    <style>
        body {
            background: #d2d6de;
        }
    </style>
@endsection

@section('content')
    <div class="login-box">
        @if ($errors->has('failed') || $errors->has('throttle'))
            <div class="callout callout-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg">Reset password</p>

            <form action="{{ url('/password/reset') }}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                @if ($errors->has('token'))
                    <span class="help-block server-error error-message">
                        {{ $errors->first('token') }}
                    </span>
                @endif

                <div class="form-group has-feedback">
                    <input type="email" name="email" value="{{ $email or old('email') }}" class="form-control" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="help-block server-error error-message">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block server-error error-message">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm password">

                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-5">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
