@extends('layouts.admin.auth')

@section('styles')
    <style>
        body {
            background: #d2d6de;
        }
    </style>
@endsection

@section('content')
    <div class="login-box">
        @if (Session::has('status'))
            <div class="callout callout-success">
                <p>{{ Session::get('status') }}</p>
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="callout callout-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg">Reset password</p>

            <form action="{{ url('/password/email') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="help-block server-error error-message">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Send reset link</button>
                    </div>
                    <div class="col-xs-4 pull-right">
                        <a href="{{ url('/admin/login') }}" class="btn btn-default btn-block btn-flat">Login page</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
