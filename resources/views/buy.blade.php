@extends('layouts.main')

@section('content')
    <div id="page-header-services" class="page-header has-background-image">
            <div class="overlay"></div>
            <div class="container">
                <h1 class="page-title">BUY BITCOINS</h1>
            </div>
    </div>

    <div class="page-content no-margin-bottom">
                <!-- SERVICES -->
                <section>
                    <div class="container">
                        <h2 class="section-heading">Service buying Bitcoins</h2>
                        <p class="buy_text">Any registered user may buy Bitcoin from our website easily and safely. The system allows the purchase by Wire Transfer, Credit Card and many other ways if allowed by the administrator. The processing is fast and depositing in your Bitcoin wallet becomes easy.</p>
                        <div class="row margin-top-50">
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-chart-bar fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Buy from the Market</h2>
                                        <p>Our platform provides a market place for individuals looking to buy and sell Bitcoins. While we monitor this platform to ensure security, we do not set prices, the market does. So the price of a Bitcoin is the market value of a Bitcoin. It is as simple as that. There are no hidden fees. {{$settings->name}} will tell you exactly what obtaining Bitcoins will cost.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-clock fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Buy Fast</h2>
                                        <p>{{$settings->name}} processes purchases instantly. While the high level cryptography of the protocol can take a few minutes to verify that the bitcoins are not counterfeit, your order once placed, requires no additional work on your part. Sit back, relax, and we�ll manage your Bitcoin transactions.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fas fa-shield-alt fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Buy Securely</h2>
                                        <p>We have spent a lot of time ensuring our system is safe for everyone. With the added feature of two-factor Authentication, {{$settings->name}} ensures that no transaction will ever be made without your express authorization.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icon-info icon-info-left">
                                    <i class="fa fa-university fa-2x text-primary"></i>
                                    <div class="text">
                                        <h2 class="title">Buy and Store</h2>
                                        <p>With our bank level encryption and security protocols, storing your Bitcoins on our platform is the safest way to ensure they are protected. Alternatively, you can store your Bitcoins on your computer, cell phone, or even your iPad. Just make sure you do not lose those devices. Just like cash, if you lose your device, your Bitcoin is gone. But if you store your Bitcoin on {{$settings->name}}, we will guarantee it is always safe.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END SERVICES -->
        </div>
@endsection

