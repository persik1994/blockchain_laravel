@extends('layouts.app')

@section('content')
    <div id="wrapper">
        <div class="table-wrap">
            <div class="table-cell">
                <div class="auth-box">
                    <div class="container">
                        <div class="text-center text-white">
                            <h2 class="section-heading">Two-Factor authentication</h2>

                            <p>Attempting to log in as {{Auth::user()->email}}</p>
                        </div>

                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <form class="form-auth-small" style="width: 100%; margin-top: 50px;" action="{{ route('2fa') }}" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label class="control-label sr-only">Two-factor authentication code</label>
                                    <input class="form-control" name="one_time_password" placeholder="000000" type="text">
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                                            Verify
                                        </button>
                                    </div>

                                    <div class="col-lg-6">
                                        <a href="{{route('login')}}" class="btn btn-default btn-lg btn-block">
                                            Change account
                                        </a>
                                    </div>
                                </div>
                            </form>

                            {{--<a data-toggle="collapse" href="#detail_div" aria-expanded="false" aria-controls="detail_div" class="collapsed">--}}
                                {{--Problems with two-factor login?--}}
                            {{--</a>--}}

                            @include('layouts.messages')
                        </div>
                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
