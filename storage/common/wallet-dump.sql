-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 26, 2018 at 10:00 PM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wallet`
--

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `allow_send` int(11) DEFAULT NULL,
  `allow_receive` int(11) DEFAULT NULL,
  `a_field_1` varchar(255) DEFAULT NULL,
  `a_field_2` varchar(255) DEFAULT NULL,
  `a_field_3` varchar(255) DEFAULT NULL,
  `a_field_4` varchar(255) DEFAULT NULL,
  `a_field_5` varchar(255) DEFAULT NULL,
  `a_field_6` varchar(255) DEFAULT NULL,
  `a_field_7` varchar(255) DEFAULT NULL,
  `a_field_8` varchar(255) DEFAULT NULL,
  `a_field_9` varchar(255) DEFAULT NULL,
  `a_field_10` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `name`, `currency`, `allow_send`, `allow_receive`, `a_field_1`, `a_field_2`, `a_field_3`, `a_field_4`, `a_field_5`, `a_field_6`, `a_field_7`, `a_field_8`, `a_field_9`, `a_field_10`, `status`) VALUES
(1, 'Credit Card', 'USD', 1, 1, 'creditcard', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Wire Transfer', 'USD', 1, 1, 'mybankname', 'bankcity', 'holdername', 'ibanbanknumber', '12344', NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `order_type` enum('market','limit','stop') NOT NULL DEFAULT 'market',
  `transaction_type` enum('sell_btc','buy_btc') DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `amount` decimal(20,8) DEFAULT NULL,
  `target_price` decimal(20,8) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `order_type`, `transaction_type`, `status`, `amount`, `target_price`, `time`) VALUES
(1, 5, 'limit', 'buy_btc', 0, '100.00000000', '5000.00000000', 1517797030),
(4, 5, 'limit', 'sell_btc', 0, '0.01500000', '4000.00000000', 1517800134),
(3, 5, 'stop', 'buy_btc', 0, '50.00000000', '8000.00000000', 1517797146),
(5, 5, 'stop', 'sell_btc', 0, '0.01500000', '3000.00000000', 1517800183);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `content` text,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `btc_amount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `from_address` int(11) DEFAULT NULL,
  `bitcoins_released` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `u_field_1` varchar(255) DEFAULT NULL,
  `u_field_2` varchar(255) DEFAULT NULL,
  `u_field_3` varchar(255) DEFAULT NULL,
  `u_field_4` varchar(255) DEFAULT NULL,
  `u_field_5` varchar(255) DEFAULT NULL,
  `u_field_6` varchar(255) DEFAULT NULL,
  `u_field_7` varchar(255) DEFAULT NULL,
  `u_field_8` varchar(255) DEFAULT NULL,
  `u_field_9` varchar(255) DEFAULT NULL,
  `u_field_10` varchar(255) DEFAULT NULL,
  `attachment` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `uid`, `type`, `btc_amount`, `amount`, `gateway_id`, `from_address`, `bitcoins_released`, `status`, `time`, `u_field_1`, `u_field_2`, `u_field_3`, `u_field_4`, `u_field_5`, `u_field_6`, `u_field_7`, `u_field_8`, `u_field_9`, `u_field_10`, `attachment`) VALUES
(1, 5, 1, '0.001453', '12', 1, 1, 1, 2, 1517606577, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517606577_attachment.jpg'),
(2, 5, 1, '0.000674', '21', 2, 1, 1, 2, 1517612318, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517612318_attachment.jpg'),
(3, 5, 1, '0.001221', '10', 1, 1, 1, 1, 1517767154, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517767154_attachment.jpg'),
(4, 5, 1, '0.013861', '111', 1, 1, 1, 1, 1517786693, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517786693_attachment.jpg'),
(5, 5, 1, '0.013861', '111', 1, 1, 1, 1, 1517786762, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517786762_attachment.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `infoemail` varchar(255) DEFAULT NULL,
  `supportemail` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `withdrawal_comission` varchar(255) DEFAULT NULL,
  `max_addresses_per_account` int(11) DEFAULT NULL,
  `profits` varchar(255) DEFAULT NULL,
  `document_verification` int(11) DEFAULT NULL,
  `email_verification` int(11) DEFAULT NULL,
  `phone_verification` int(11) DEFAULT NULL,
  `recaptcha_verification` int(11) DEFAULT NULL,
  `recaptcha_publickey` varchar(255) DEFAULT NULL,
  `recaptcha_privatekey` varchar(255) DEFAULT NULL,
  `nexmo_api_key` varchar(255) DEFAULT NULL,
  `nexmo_api_secret` varchar(255) DEFAULT NULL,
  `autoupdate_bitcoin_price` int(11) DEFAULT NULL,
  `bitcoin_sell_fee` int(11) DEFAULT NULL,
  `bitcoin_buy_fee` int(11) DEFAULT NULL,
  `bitcoin_fixed_price` varchar(255) DEFAULT NULL,
  `plugin_buy_bitcoins` int(11) DEFAULT NULL,
  `plugin_sell_bitcoins` int(11) DEFAULT NULL,
  `plugin_transfer_bitcoins` int(11) DEFAULT NULL,
  `plugin_request_bitcoins` int(11) DEFAULT NULL,
  `process_time_to_buy` int(11) DEFAULT NULL,
  `process_time_to_sell` int(11) DEFAULT NULL,
  `default_language` varchar(255) DEFAULT NULL,
  `default_currency` varchar(255) DEFAULT NULL,
  `fb_link` text,
  `tw_link` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `description`, `keywords`, `name`, `infoemail`, `supportemail`, `url`, `withdrawal_comission`, `max_addresses_per_account`, `profits`, `document_verification`, `email_verification`, `phone_verification`, `recaptcha_verification`, `recaptcha_publickey`, `recaptcha_privatekey`, `nexmo_api_key`, `nexmo_api_secret`, `autoupdate_bitcoin_price`, `bitcoin_sell_fee`, `bitcoin_buy_fee`, `bitcoin_fixed_price`, `plugin_buy_bitcoins`, `plugin_sell_bitcoins`, `plugin_transfer_bitcoins`, `plugin_request_bitcoins`, `process_time_to_buy`, `process_time_to_sell`, `default_language`, `default_currency`, `fb_link`, `tw_link`) VALUES
(1, 'Mazdex', 'Mazdex desc', 'Mazdex kw', 'Mazdex', 'info@mazdex.com', 'support@mazdex.com', 'http://mazdex.com/', '0.001', 10, NULL, 0, 0, 0, NULL, NULL, NULL, '', '', 1, 5, 3, '', 1, 1, 1, 1, 1, 1, 'English', 'USD', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sms_codes`
--

CREATE TABLE `sms_codes` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `sms_code` varchar(255) DEFAULT NULL,
  `verified` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transfers`
--

CREATE TABLE `transfers` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '0',
  `recipient_address` varchar(255) DEFAULT NULL,
  `btc_amount` decimal(14,8) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transfers`
--

INSERT INTO `transfers` (`id`, `uid`, `status`, `recipient_address`, `btc_amount`, `time`) VALUES
(1, 5, 0, 'rgwbrwthtrh', '0.01000000', 1517804611),
(2, 5, 0, 'ergtrgtrgtrg', '0.01000000', 1517805673),
(3, 5, 0, 'ergtrgtrgtrg', '0.01000000', 1517805698),
(4, 5, 2, 'ergtrgtrgtrg', '0.01000000', 1517805753);

-- -------------------------------------------------------

--
-- Table structure for table `users_addresses`
--

CREATE TABLE `users_addresses` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `lid` int(11) DEFAULT NULL,
  `available_balance` varchar(255) DEFAULT NULL,
  `pending_received_balance` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  `archived` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_bank_account`
--

CREATE TABLE `users_bank_account` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `u_field_1` varchar(255) DEFAULT NULL,
  `u_field_2` varchar(255) DEFAULT NULL,
  `u_field_3` varchar(255) DEFAULT NULL,
  `u_field_4` varchar(255) DEFAULT NULL,
  `u_field_5` varchar(255) DEFAULT NULL,
  `u_field_6` varchar(255) DEFAULT NULL,
  `u_field_7` varchar(255) DEFAULT NULL,
  `u_field_8` varchar(255) DEFAULT NULL,
  `u_field_9` varchar(255) DEFAULT NULL,
  `u_field_10` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_bank_account`
--

INSERT INTO `users_bank_account` (`id`, `uid`, `time`, `u_field_1`, `u_field_2`, `u_field_3`, `u_field_4`, `u_field_5`, `u_field_6`, `u_field_7`, `u_field_8`, `u_field_9`, `u_field_10`) VALUES
(1, 5, 1517850891, 'aaaa', 'bff', 'cggg', 'dtyjtuyjutj', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_money`
--

CREATE TABLE `users_money` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `transaction_type` enum('deposit','sell_btc','buy_btc','withdraw') DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `amount` decimal(12,2) DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `u_field_1` varchar(255) DEFAULT NULL,
  `u_field_2` varchar(255) DEFAULT NULL,
  `u_field_3` varchar(255) DEFAULT NULL,
  `u_field_4` varchar(255) DEFAULT NULL,
  `u_field_5` varchar(255) DEFAULT NULL,
  `u_field_6` varchar(255) DEFAULT NULL,
  `u_field_7` varchar(255) DEFAULT NULL,
  `u_field_8` varchar(255) DEFAULT NULL,
  `u_field_9` varchar(255) DEFAULT NULL,
  `u_field_10` varchar(255) DEFAULT NULL,
  `attachment` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_money`
--

INSERT INTO `users_money` (`id`, `uid`, `transaction_type`, `status`, `amount`, `gateway_id`, `time`, `u_field_1`, `u_field_2`, `u_field_3`, `u_field_4`, `u_field_5`, `u_field_6`, `u_field_7`, `u_field_8`, `u_field_9`, `u_field_10`, `attachment`) VALUES
(1, 5, 'deposit', 0, '50.00', 1, 1517705366, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517705366_attachment.jpg'),
(2, 5, 'buy_btc', 1, '-50.00', 1, 1517705479, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517705479_attachment.jpg'),
(3, 5, 'deposit', 1, '50.00', 1, 1517705485, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517705485_attachment.jpg'),
(4, 5, 'deposit', 2, '50.00', 1, 1517705522, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517705522_money.jpg'),
(5, 5, 'deposit', 1, '50.00', 1, 1517706662, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517706662_money.jpg'),
(6, 5, 'deposit', 1, '50.00', 1, 1517706696, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517706696_money.jpg'),
(7, 5, 'deposit', 1, '50.00', 1, 1517706710, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517706710_money.jpg'),
(8, 5, 'deposit', 1, '50.00', 1, 1517706944, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517706944_money.jpg'),
(9, 5, 'deposit', 1, '50.00', 1, 1517706978, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517706978_money.jpg'),
(10, 5, 'deposit', 1, '50.00', 1, 1517707205, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517707205_money.jpg'),
(11, 5, 'deposit', 1, '50.00', 1, 1517707293, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '/uploads/attachments/5_1517707293_money.jpg'),
(12, 5, 'deposit', 1, '50.00', 1, 1517707332, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517707332_money.jpg'),
(13, 5, 'deposit', 1, '13.00', 1, 1517768688, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517768688_money.jpg'),
(14, 5, 'deposit', 1, '13.00', 1, 1517768740, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517768740_money.jpg'),
(15, 5, 'deposit', 2, '10.00', 1, 1517783088, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'uploads/attachments/5_1517783088_money.jpg'),
(21, 5, 'buy_btc', 1, '-100.00', NULL, 1517788488, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 5, 'buy_btc', 1, '-100.00', NULL, 1517788478, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 5, 'sell_btc', 1, '78.90', NULL, 1517789901, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 5, 'withdraw', 0, '-10.00', NULL, 1517849820, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 5, 'withdraw', 0, '-10.00', NULL, 1517849960, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 5, 'withdraw', 0, '-10.00', NULL, 1517850196, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 5, 'withdraw', 0, '-10.00', NULL, 1517850272, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 5, 'withdraw', 0, '-300.00', NULL, 1517850320, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 5, 'withdraw', 1, '-10.00', NULL, 1517850364, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 5, 'withdraw', 1, '-10.00', NULL, 1517850746, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 5, 'withdraw', 2, '-21.00', NULL, 1517850879, 'a', 'b', 'c', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 5, 'withdraw', 1, '-12.00', NULL, 1517850891, 'aaaa', 'bff', 'cggg', 'dtyjtuyjutj', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_transactions`
--

CREATE TABLE `users_transactions` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `sender` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `confirmations` int(11) DEFAULT NULL,
  `txid` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_codes`
--
ALTER TABLE `sms_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfers`
--
ALTER TABLE `transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_addresses`
--
ALTER TABLE `users_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_bank_account`
--
ALTER TABLE `users_bank_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_money`
--
ALTER TABLE `users_money`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_transactions`
--
ALTER TABLE `users_transactions`
  ADD PRIMARY KEY (`id`);



--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sms_codes`
--
ALTER TABLE `sms_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transfers`
--
ALTER TABLE `transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_addresses`
--
ALTER TABLE `users_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_bank_account`
--
ALTER TABLE `users_bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_money`
--
ALTER TABLE `users_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `users_transactions`
--
ALTER TABLE `users_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
