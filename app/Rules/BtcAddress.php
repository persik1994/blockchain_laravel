<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Helpers\AddressValidator;

class BtcAddress implements Rule
{

    private $mode = null;

    /**
     * Create a new rule instance.
     *
     * @param $mode
     */
    public function __construct($mode = null)
    {
        $this->mode = $mode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return AddressValidator::isValid($value, $this->mode);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a valid bitcoin address.';
    }
}
