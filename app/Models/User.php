<?php

namespace App\Models;

use App\Notifications\MailResetPasswordTokenAdmin;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HelperTrait;
    use Bitcoind;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'email_verified_at',
        'password', 'secret_pin', 'email_hash',
        'status', 'balance', 'crypto_currency', 'fiat_currency',
        'btc_balance', 'eth_balance', 'ip',
        'time_signin', 'time_activity',
        'document_verified', 'document_1',
        'document_2', 'mobile_verified',
        'mobile_number', 'google2fa_secret'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'type'
    ];

    /**
     * Available crypto currencies
     *
     * @var array
     */
    public static $availableCryptoCurrencies = [
        'BTC' => [
            'name' => 'Bitcoin',
            'img-icon' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bitcoin.svg/600px-Bitcoin.svg.png',
            'icon' => 'fab fa-bitcoin'
        ],
        'ETH' => [
            'name' => 'Ethereum',
            'img-icon' => 'https://jirasupport.files.wordpress.com/2018/01/27623000-a0e75b2a-5bda-11e7-89e6-651029bf2d52.png?w=256',
            'icon' => 'fab fa-ethereum'
        ]
    ];

    /**
     * Available fiat currencies
     *
     * @var array
     */
    public static $availableFiatCurrencies = [
        'USD' => [
            'icon' => 'fas fa-dollar-sign'
        ],
        'EUR' => [
            'icon' => 'fas fa-euro-sign'
        ]
    ];

    /**
     * Check user image and return url to it.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function urlUserImage()
    {
        if (is_file(public_path() . "/img/users/" . $this->image)) {
            return url("/img/users/" . $this->image);
        }

        return url('/img/users/default_user.png');
    }


    /**
     * User money relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function money()
    {
        return $this->hasMany(UserMoney::class, 'uid');
    }


    /**
     * User transactions relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(UserTransaction::class, 'uid');
    }


    /**
     * Get user bitcoin value in currency
     * @param $currency
     * @return float
     */
    public function getBitcoinMoneyBalance($currency) {
        return (float)($this->btc_balance * $this->getBitcoinPrice($currency));
    }


    /**
     * Get user ethereum value in currency
     * @param $currency
     * @return float
     */
    public function getEthereumMoneyBalance($currency) {
        return (float)($this->eth_balance * $this->getEthereumPrice($currency));
    }


    /**
     * Get user available amount from $fromFiatCurrency to $currency
     *
     * @param $fromFiatCurrency
     * @param $currency
     * @return float
     */
    public function availableAmount($currency) {
        $amount = $this->balance;

        if ($currency === 'USD') {
            return (float)$amount;
        }

        return (float)$this->convertCurrency($amount, 'USD', $currency);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordTokenAdmin($token, $this));
    }

    /**
     * Has one relationship with users_bank_account table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankAccount()
    {
        return $this->hasOne(UserBankAccount::class, 'uid', 'id');
    }

    /**
     * Has many relationship with users_addresses table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(UserAddresses::class, 'uid');
    }

    public function bitcoinAddresses() 
    {
        return $this->hasMany(UserAddresses::class, 'uid')->where('crypto_currency', 'BTC');
    }

    public function ethereumAddresses() 
    {
        return $this->hasMany(UserAddresses::class, 'uid')->where('crypto_currency', 'ETH');
    }
    
    /**
     * Get user crypto balance
     *
     * @return int|string
     */
    public function getCryptoBalance()
    {
        $balance = 0;
        if ($this->crypto_currency == 'BTC') {
            $balance = formatBitcoin($this->btc_balance);
        }

        if ($this->crypto_currency == 'ETH') {
            $balance = formatEthereum($this->eth_balance);
        }
        
        return $balance;
    }

    /**
     * Get unviewed items cache by owner
     *
     * @param $type - items type example deposits, withdraws
     * @return bool|mixed
     */
    public function getCountUnviewedItems($type)
    {
        if (!Cache::has($type . '_unviewed_by_owner_' . $this->id)) {
            return false;
        }

        return Cache::get($type . '_unviewed_by_owner_' . $this->id);
    }

    /**
     * Update unviewed items cache count for owner
     *
     * @param $type - items type example deposit, withdraw, buy, sell
     * @throws \Exception
     */
    public function updateUnviewedItemsCache($type)
    {
        switch($type) {
            case 'deposit':
                $countOwnerUnviewed = UserMoney::where('uid', $this->id)
                    ->where('transaction_type', 'deposit')
                    ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
                    ->where('viewed_by_owner', 0)
                    ->count();

                Cache::forever('deposits_unviewed_by_owner_' . $this->id, $countOwnerUnviewed);
                break;

            case 'withdraw':
                $countOwnerUnviewed = UserMoney::where('uid', $this->id)
                    ->where('transaction_type', 'withdraw')
                    ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
                    ->where('viewed_by_owner', 0)
                    ->count();

                Cache::forever('withdraws_unviewed_by_owner_' . $this->id, $countOwnerUnviewed);
                break;

            case 'buy':
                $countOwnerUnviewed = Order::where('uid', $this->id)
                    ->where('order_type', 'buy')
                    ->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING])
                    ->where('viewed_by_owner', 0)
                    ->count();

                Cache::forever('buys_unviewed_by_owner_' . $this->id, $countOwnerUnviewed);
                break;

            case 'sell':
                $countOwnerUnviewed = Order::where('uid', $this->id)
                    ->where('order_type', 'sell')
                    ->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING])
                    ->where('viewed_by_owner', 0)
                    ->count();

                Cache::forever('sells_unviewed_by_owner_' . $this->id, $countOwnerUnviewed);
                break;

            case 'transfer':
                $countOwnerUnviewed = Transfer::where('uid', $this->id)
                    ->whereIn('status', [1, 2])
                    ->where('viewed_by_owner', 0)
                    ->count();

                Cache::forever('transfers_unviewed_by_owner_' . $this->id, $countOwnerUnviewed);
                break;

            default:
                throw new \Exception('Incorrect type value');
        }
    }

    public function checkIf2FaAuthenticated(Request $request)
    {
        $authenticator = app(Authenticator::class)->boot($request);

        if (empty($this->google2fa_secret)) {
            return true;
        }

        return $authenticator->isAuthenticated();
    }
}
