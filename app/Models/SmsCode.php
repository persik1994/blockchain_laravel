<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    protected $table = 'sms_codes';
    public $timestamps = false;

    protected $fillable = ['uid', 'sms_code', 'verified'];

}
