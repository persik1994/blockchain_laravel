<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    protected $table = 'users_transactions';
    public $timestamps = false;

    protected $fillable = [
        'uid',
        'crypto_currency',
        'type',
        'recipient',
        'sender',
        'amount',
        'time',
        'confirmations',
        'txid'
    ];

}
