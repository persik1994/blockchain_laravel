<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Support extends Model {


	public $timestamps = true;
	protected $table = 'support_tickets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id', 'uid', 'title', 'description', 'status', 'email_address', 'ticket_hash', 'viewed_by_admin', 'viewed_by_owner'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function messages()
    {
        return $this->hasMany(SupportChat::class, 'ticket_id');
    }
    
}
