<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gateway extends Model
{
    protected $table = 'gateways';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'currency', 'allow', 'gateway_information', 'status'
//        'allow_send', 'allow_receive'
    ];

    public $timestamps = false;

    public static $gateways = [
        'paypal' => [
            'name' => 'PayPal',
            'fields' => [
                'playpal_account' => 'PayPal account'
            ]
        ],
        'credit_card' => [
            'name' => 'Credit Card',
            'fields' => [
                'credit_card_number' => 'Credit Card Number'
            ]
        ],
        'skrill' => [
            'name' => 'Skrill',
            'fields' => [
                'scrill_acount' => 'Skrill account'
            ]
        ],
        'web_money' => [
            'name' => 'WebMoney',
            'fields' => [
                'webmoney_account' => 'WebMoney account'
            ]
        ],
        'payeer' => [
            'name' => 'Payeer',
            'fields' => [
                'payeer_account' => 'Payeer account'
            ]
        ],
        'perfect_money' => [
            'name' => 'Perfect Money',
            'fields' => [
                'perfect_money_acount' => 'Perfect Money account'
            ]
        ],
        'advCash' => [
            'name' => 'AdvCash',
            'fields' => [
                'advcash_account' => 'AdvCash account'
            ]
        ],
        'okpay' => [
            'name' => 'OKPay',
            'fields' => [
                'okpay_account' => 'OKPay account'
            ]
        ],
        'entromoney' => [
            'name' => 'Entromoney',
            'fields' => [
                'entromoney_account' => 'Entromoney account'
            ]
        ],
        'solidtrust_pay' => [
            'name' => 'SolidTrust Pay',
            'fields' => [
                'solidtrust_pay_account' => 'SolidTrust Pay account'
            ]
        ],
        'neteller' => [
            'name' => 'Neteller',
            'fields' => [
                'neteller_account' => 'Neteller account'
            ]
        ],
        'uquid' => [
            'name' => 'UQUID',
            'fields' => [
                'uquid_account' => 'UQUID account'
            ]
        ],
        'btc_e' => [
            'name' => 'BTC-e',
            'fields' => [
                'btc_e_account' => 'BTC-e account'
            ]
        ],
        'yandex_money' => [
            'name' => 'Yandex Money',
            'fields' => [
                'yandex_money_account' => 'Yandex Money account'
            ]
        ],
        'qiwi' => [
            'name' => 'QIWI',
            'fields' => [
                'qiwi_account' => 'QIWI account'
            ]
        ],
        'payza' => [
            'name' => 'Payza',
            'fields' => [
                'payza_account' => 'Payza account'
            ]
        ],
        'the_billion_coin' => [
            'name' => 'TheBillioncoin',
            'fields' => [
                'thebillioncoin_account' => 'TheBillioncoin account'
            ]
        ],
        'wire_transfer' => [
            'name' => 'Wire Transfer',
            'fields' => [
                'wt_bank_name' => 'Bank Name (for Wire Transfer transfer)',
                'wt_bank_address' => 'Bank Country,City,Address (for Wire Transfer transfer)',
                'wt_bank_hollder_name' => 'Bank Account Holder Name (for Wire Transfer transfer)',
                'wt_iban' => 'Bank Account Number/IBAN (for Wire Transfer transfer)',
                'wt_swift_code' => 'Bank Swift Code (for Wire Transfer transfer)'
            ]
        ],
        'western_union' => [
            'name' => 'Western Union',
            'fields' => [
                'wu_name' =>'Name (for Western Union transfer)',
                'wu_location' => 'Location (for Western Union transfer)'
            ]
        ],
        'moneygram' => [
            'name' => 'Moneygram',
            'fields' => [
                'moneygram_name' => 'Name (for Moneygram transfer)',
                'moneygram_location' => 'Location (for Moneygram transfer)'
            ]
        ]
    ];

    public static $currencies = [
        'AED'=> 'United Arab Emirates dirham',
        'AFN'=> 'Afghan afghani',
        'ALL'=> 'Albanian lek',
        'AMD'=> 'Armenian dram',
        'ANG'=> 'Netherlands Antillean guilder',
        'AOA'=> 'Angolan kwanza',
        'ARS'=> 'Argentine peso',
        'AUD'=> 'Australian dollar',
        'AWG'=> 'Aruban florin',
        'AZN'=> 'Azerbaijani manat',
        'BAM'=> 'Bosnia and Herzegovina convertible mark',
        'BBD'=> 'Barbados dollar',
        'BDT'=> 'Bangladeshi taka',
        'BGN'=> 'Bulgarian lev',
        'BHD'=> 'Bahraini dinar',
        'BIF'=> 'Burundian franc',
        'BMD'=> 'Bermudian dollar',
        'BND'=> 'Brunei dollar',
        'BOB'=> 'Boliviano',
        'BRL'=> 'Brazilian real',
        'BSD'=> 'Bahamian dollar',
        'BTN'=> 'Bhutanese ngultrum',
        'BWP'=> 'Botswana pula',
        'BYN'=> 'New Belarusian ruble',
        'BYR'=> 'Belarusian ruble',
        'BZD'=> 'Belize dollar',
        'CAD'=> 'Canadian dollar',
        'CDF'=> 'Congolese franc',
        'CHF'=> 'Swiss franc',
        'CLF'=> 'Unidad de Fomento',
        'CLP'=> 'Chilean peso',
        'CNY'=> 'Renminbi|Chinese yuan',
        'COP'=> 'Colombian peso',
        'CRC'=> 'Costa Rican colon',
        'CUC'=> 'Cuban convertible peso',
        'CUP'=> 'Cuban peso',
        'CVE'=> 'Cape Verde escudo',
        'CZK'=> 'Czech koruna',
        'DJF'=> 'Djiboutian franc',
        'DKK'=> 'Danish krone',
        'DOP'=> 'Dominican peso',
        'DZD'=> 'Algerian dinar',
        'EGP'=> 'Egyptian pound',
        'ERN'=> 'Eritrean nakfa',
        'ETB'=> 'Ethiopian birr',
        'EUR'=> 'Euro',
        'FJD'=> 'Fiji dollar',
        'FKP'=> 'Falkland Islands pound',
        'GBP'=> 'Pound sterling',
        'GEL'=> 'Georgian lari',
        'GHS'=> 'Ghanaian cedi',
        'GIP'=> 'Gibraltar pound',
        'GMD'=> 'Gambian dalasi',
        'GNF'=> 'Guinean franc',
        'GTQ'=> 'Guatemalan quetzal',
        'GYD'=> 'Guyanese dollar',
        'HKD'=> 'Hong Kong dollar',
        'HNL'=> 'Honduran lempira',
        'HRK'=> 'Croatian kuna',
        'HTG'=> 'Haitian gourde',
        'HUF'=> 'Hungarian forint',
        'IDR'=> 'Indonesian rupiah',
        'ILS'=> 'Israeli new shekel',
        'INR'=> 'Indian rupee',
        'IQD'=> 'Iraqi dinar',
        'IRR'=> 'Iranian rial',
        'ISK'=> 'Icelandic króna',
        'JMD'=> 'Jamaican dollar',
        'JOD'=> 'Jordanian dinar',
        'JPY'=> 'Japanese yen',
        'KES'=> 'Kenyan shilling',
        'KGS'=> 'Kyrgyzstani som',
        'KHR'=> 'Cambodian riel',
        'KMF'=> 'Comoro franc',
        'KPW'=> 'North Korean won',
        'KRW'=> 'South Korean won',
        'KWD'=> 'Kuwaiti dinar',
        'KYD'=> 'Cayman Islands dollar',
        'KZT'=> 'Kazakhstani tenge',
        'LAK'=> 'Lao kip',
        'LBP'=> 'Lebanese pound',
        'LKR'=> 'Sri Lankan rupee',
        'LRD'=> 'Liberian dollar',
        'LSL'=> 'Lesotho loti',
        'LYD'=> 'Libyan dinar',
        'MAD'=> 'Moroccan dirham',
        'MDL'=> 'Moldovan leu',
        'MGA'=> 'Malagasy ariary',
        'MKD'=> 'Macedonian denar',
        'MMK'=> 'Myanmar kyat',
        'MNT'=> 'Mongolian tögrög',
        'MOP'=> 'Macanese pataca',
        'MRO'=> 'Mauritanian ouguiya',
        'MUR'=> 'Mauritian rupee',
        'MVR'=> 'Maldivian rufiyaa',
        'MWK'=> 'Malawian kwacha',
        'MXN'=> 'Mexican peso',
        'MXV'=> 'Mexican Unidad de Inversion',
        'MYR'=> 'Malaysian ringgit',
        'MZN'=> 'Mozambican metical',
        'NAD'=> 'Namibian dollar',
        'NGN'=> 'Nigerian naira',
        'NIO'=> 'Nicaraguan córdoba',
        'NOK'=> 'Norwegian krone',
        'NPR'=> 'Nepalese rupee',
        'NZD'=> 'New Zealand dollar',
        'OMR'=> 'Omani rial',
        'PAB'=> 'Panamanian balboa',
        'PEN'=> 'Peruvian Sol',
        'PGK'=> 'Papua New Guinean kina',
        'PHP'=> 'Philippine peso',
        'PKR'=> 'Pakistani rupee',
        'PLN'=> 'Polish złoty',
        'PYG'=> 'Paraguayan guaraní',
        'QAR'=> 'Qatari riyal',
        'RON'=> 'Romanian leu',
        'RSD'=> 'Serbian dinar',
        'RUB'=> 'Russian ruble',
        'RWF'=> 'Rwandan franc',
        'SAR'=> 'Saudi riyal',
        'SBD'=> 'Solomon Islands dollar',
        'SCR'=> 'Seychelles rupee',
        'SDG'=> 'Sudanese pound',
        'SEK'=> 'Swedish krona',
        'SGD'=> 'Singapore dollar',
        'SHP'=> 'Saint Helena pound',
        'SLL'=> 'Sierra Leonean leone',
        'SOS'=> 'Somali shilling',
        'SRD'=> 'Surinamese dollar',
        'SSP'=> 'South Sudanese pound',
        'STD'=> 'São Tomé and Príncipe dobra',
        'SVC'=> 'Salvadoran colón',
        'SYP'=> 'Syrian pound',
        'SZL'=> 'Swazi lilangeni',
        'THB'=> 'Thai baht',
        'TJS'=> 'Tajikistani somoni',
        'TMT'=> 'Turkmenistani manat',
        'TND'=> 'Tunisian dinar',
        'TOP'=> 'Tongan paʻanga',
        'TRY'=> 'Turkish lira',
        'TTD'=> 'Trinidad and Tobago dollar',
        'TWD'=> 'New Taiwan dollar',
        'TZS'=> 'Tanzanian shilling',
        'UAH'=> 'Ukrainian hryvnia',
        'UGX'=> 'Ugandan shilling',
        'USD'=> 'United States dollar',
        'UYI'=> 'Uruguay Peso en Unidades Indexadas',
        'UYU'=> 'Uruguayan peso',
        'UZS'=> 'Uzbekistan som',
        'VEF'=> 'Venezuelan bolívar',
        'VND'=> 'Vietnamese đồng',
        'VUV'=> 'Vanuatu vatu',
        'WST'=> 'Samoan tala',
        'XAF'=> 'Central African CFA franc',
        'XCD'=> 'East Caribbean dollar',
        'XOF'=> 'West African CFA franc',
        'XPF'=> 'CFP franc',
        'XXX'=> 'No currency',
        'YER'=> 'Yemeni rial',
        'ZAR'=> 'South African rand',
        'ZMW'=> 'Zambian kwacha',
        'ZWL'=> 'Zimbabwean dollar'
    ];
}
