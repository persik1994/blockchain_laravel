<?php

namespace App\Models;

use App\Traits\HelperTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_WAITING = 'waiting';
    const STATUS_PROCESSING = 'processing';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELED = 'canceled';

    const TYPE_SELL = 'sell';
    const TYPE_BUY = 'buy';

    const STRATEGY_MARKET = 'market';
    const STRATEGY_LIMIT = 'limit';
    const STRATEGY_STOP = 'stop';

    protected $table = 'orders';

    protected $fillable = [
        'uid',
        'strategy_type',
        'order_type',
        'status',
        'crypto_currency',
        'crypto_amount',
        'fiat_currency',
        'fiat_amount',
        'current_amount',
        'target_amount',
        'txid',
    ];


    /**
     * Relation belongs to with users table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    /**
     * Extract items by order type
     *
     * @param $orderType
     * @param $takeCount
     * @param $user
     * @param bool $unviewed_by_owner
     * @param bool $statusIn
     * @return mixed
     */
    public static function takeItemsByOrderType($orderType, $takeCount, $user, $unviewed_by_owner = false, $statusIn = false)
    {
        $itemsQuery = Order::where('uid', $user->id)
            ->where('order_type', $orderType);

        if ($statusIn) {
            $itemsQuery->whereIn('status', $statusIn);
        }

        if ($unviewed_by_owner) {
            $itemsQuery->where('viewed_by_owner', 0);
        }

        $itemsQuery->orderBy('updated_at', 'desc');

        if ($takeCount) {
            $itemsQuery->take($takeCount);
        }

        $items = $itemsQuery->get();

        foreach ($items as $item) {
            $item->converted_crypto_amount = convertToCrypto($item->crypto_amount, $item->crypto_currency);
            $item->fiat_amount = number_format($item->fiat_amount, 2);
            $item->details_page_url = route('account.order.details', $item->id);
        }

        return $items;
    }
}
