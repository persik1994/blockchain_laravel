<?php

namespace App\Models;

use App\Traits\HelperTrait;
use Illuminate\Database\Eloquent\Model;

class UserMoney extends Model
{
    use HelperTrait;

    public $timestamps = false;

    const STATUS_WAITING = 'waiting';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELED = 'canceled';

    protected $table = 'users_money';

    protected $fillable = [
        'uid',
        'transaction_type',
        'status',
        'amount',
        'gateway_id',
        'current_amount',
        'user_fiat_currency',
        'user_amount',
        'time',
        'u_field_1',
        'u_field_2',
        'u_field_3',
        'u_field_4',
        'u_field_5',
        'attachment',
        'updated_at'
    ];

    /**
     * Belongs to relation with users table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    /**
     * Belongs to relation with gateways table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }

    /**
     * Extract items by transaction type
     *
     * @param $transactionType
     * @param $takeCount
     * @param $user
     * @param bool $unviewed_by_owner
     * @param bool $statusIn
     * @return mixed
     */
    public static function takeItemsByTransactionType($transactionType, $takeCount, $user, $unviewed_by_owner = false, $statusIn = false)
    {
        $itemsQuery = UserMoney::where('uid', $user->id)
            ->where('transaction_type', $transactionType);

        if ($statusIn) {
            $itemsQuery->whereIn('status', $statusIn);
        }

        if ($unviewed_by_owner) {
            $itemsQuery->where('viewed_by_owner', 0);
        }

        $itemsQuery->orderBy('updated_at', 'desc');

        if ($takeCount) {
            $itemsQuery->take($takeCount);
        }

        $items = $itemsQuery->get();

        if($transactionType == 'deposit') {
            foreach ($items as $item) {
                $item->time = date('Y-m-d H:i:s', $item->time);
                $item->attachment_path = route('user.file', $item->attachment);
                $item->current_amount = number_format($item->current_amount, 2);
                $item->amount = HelperTrait::convertCurrency($item->amount, 'USD', $user->fiat_currency);
            }
        }

        if($transactionType == 'withdraw') {
            foreach ($items as $item) {
                $item->time = date('Y-m-d H:i:s', $item->time);
                $item->amount = number_format($item->user_amount, 2);

                if(!empty($item->attachment)) {
                    $item->attachment_path = route('user.file', $item->attachment);
                }
            }
        }

        return $items;
    }
}
