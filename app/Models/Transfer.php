<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    public $timestamps = false;
    protected $table = 'transfers';

    protected $fillable = [
        'uid',
        'status',
        'recipient_address',
        'from_address',
        'crypto_amount',
        'crypto_currency',
        'time'
    ];


    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'uid');
    }
}
