<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SupportChat extends Model {
	public $timestamps = true;
	protected $table = 'support_chat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'ticket_id', 'message', ''
    ];
  
    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function ticket()
    {
        return $this->belongsTo(Support::class, 'ticket_id');
    }
}
