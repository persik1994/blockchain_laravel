<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{


	public $timestamps = false;
    protected $table = 'settings';

    protected $fillable = [
        'plugin_buy_bitcoins',
        'plugin_sell_bitcoins',
        'plugin_transfer_bitcoins',
        'plugin_request_bitcoins',
        'process_time_to_buy',
        'process_time_to_sell',
        'title',
        'description',
        'keywords',
        'name',
        'url',
        'infoemail',
        'supportemail',
        'default_language',
//        'default_currency',
        'withdrawal_comission',
        'max_addresses_per_account',
        'autoupdate_bitcoin_price',
        'bitcoin_buy_fee',
        'bitcoin_sell_fee',
        'bitcoin_fixed_price',
        'ethereum_gas_limit',
        'ethereum_gas_price',
        'eth_fee_address',
        'eth_sell_address',
        'eth_buy_address',
        'autoupdate_ethereum_price',
        'ethereum_buy_fee',
        'ethereum_sell_fee',
        'ethereum_fixed_price',
        'document_verification',
        'email_verification',
        'phone_verification',
        'nexmo_api_key',
        'nexmo_api_secret',
        'fb_link',
        'tw_link',
        'btc_fee_address',
        'btc_sell_address',
        'btc_buy_address',
        'btc_balance',
        'eth_balance',
        'balance',
        'eth_processed_block',
        'btc_processed_block',
    ];
}
