<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddresses extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'users_addresses';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'uid',
        'label',
        'crypto_currency',
        'address',
        'balance',
        'created',
        'updated',
    ];

}
