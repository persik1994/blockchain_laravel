<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankAccount extends Model
{

    public $timestamps = false;
    protected $table = 'users_bank_account';

    protected $fillable = [
        'uid',
        'u_field_1',
        'u_field_2',
        'u_field_3',
        'u_field_4',
        'time',
    ];

}
