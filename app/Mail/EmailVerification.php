<?php

namespace App\Mail;

use App\Models\Settings;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;


    public $user = null;
    public $settings = null;


    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Settings $settings
     */
    public function __construct(User $user, Settings $settings)
    {
        $this->user = $user;
        $this->settings = $settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verification.email');
    }
}
