<?php

namespace App\Providers;

use App\Events\UnauthenticatedSupportResponseEvent;
use App\Events\UpdateCryptoCurrencyEvent;
use App\Events\UserMoneyRequestEvent;
use App\Events\UserOrderRequestEvent;
use App\Events\UserTransferRequestEvent;
use App\Events\UserTransferResponseEvent;
use App\Events\WithdrawalEvent;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\UserMoneyManageEvent;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\Events\UserSupportRequestEvent;
use App\Events\UserSupportResponseEvent;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserMoneyManageEvent::class => [],
        UserMoneyRequestEvent::class => [],
        UpdateCryptoCurrencyEvent::class => [],
        UserOrderRequestEvent::class => [],
        UserSupportRequestEvent::class => [],
        UserSupportResponseEvent::class => [],
        UnauthenticatedSupportResponseEvent::class => [],
        UserTransferRequestEvent::class => [],
        UserTransferResponseEvent::class => []
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
