<?php

namespace App;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UsersLoginHistory extends Model
{
    protected $table = 'users_login_history';

    protected $fillable = ['uid', 'time', 'ip_address'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }
}
