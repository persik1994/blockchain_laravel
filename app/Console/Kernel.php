<?php

namespace App\Console;

use App\Traits\HelperTrait;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    use HelperTrait;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cron:update-user-balance')
            ->name('update-user-balance-cron')
            ->everyMinute()
            ->appendOutputTo('storage/logs/update-user-balance-cron.log')
            ->withoutOverlapping();

        $schedule->command('cron:update-order-status')
            ->name('update-order-status-cron')
            ->appendOutputTo('storage/logs/update-order-status-cron.log')
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->command('cron:remove-user-login-history')
            ->name('remove-user-login-history-cron')
            ->daily()
            ->withoutOverlapping();

        //TODO First it need to be finished
//        $schedule->command('cron:sync-user-transactions')
//            ->name('sync-user-transactions')
//            ->appendOutputTo('storage/logs/sync-user-transactions-cron.log')
//            ->everyMinute()
//            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
