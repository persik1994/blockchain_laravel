<?php

namespace App\Console\Commands;

use App\Traits\HelperTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use App\Models\Settings;
use App\Events\UpdateCryptoCurrencyEvent;

class UpdateCryptoInfo extends Command
{
    use HelperTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:update-crypto-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update info about cront';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        $cryptoCurrencies = config('currencies.crypto_currencies');
        $fiatCurrencies = config('currencies.fiat_currencies');

        while (true) {
            $bitcoinPriceUSD = $this->getBitcoinPrice('USD');
            $bitcoinPriceEUR = $this->getBitcoinPrice('EUR');
            $ethereumPriceUSD = $this->getEthereumPrice('USD');
            $ethereumPriceEUR = $this->getEthereumPrice('EUR');

            $priceChangeBTC['USD'] = $this->getCryptoCurrencyPriceChange24h(1, 'USD');
            $priceChangeBTC['EUR'] = $this->getCryptoCurrencyPriceChange24h(1, 'EUR');
            $priceChangeETH['USD'] = $this->getCryptoCurrencyPriceChange24h(1027, 'USD');
            $priceChangeETH['EUR'] = $this->getCryptoCurrencyPriceChange24h(1027, 'EUR');

            $data = [
                'priceBTCUSD' => $bitcoinPriceUSD ? number_format($bitcoinPriceUSD, 2) : '',
                'priceBTCEUR' => $bitcoinPriceEUR ? number_format($bitcoinPriceEUR, 2) : '',
                'priceETHUSD' =>  $ethereumPriceUSD ? number_format($ethereumPriceUSD, 2) : '',
                'priceETHEUR' => $ethereumPriceEUR ? number_format($ethereumPriceEUR, 2) : '',
                'currencyTimeBTCUSD' => $priceChangeBTC['USD'],
                'currencyTimeBTCEUR' => $priceChangeBTC['EUR'],
                'currencyTimeETHUSD' => $priceChangeETH['USD'],
                'currencyTimeETHEUR' => $priceChangeETH['EUR'],
            ];

            if (Schema::hasTable('settings')) {
                app()->bind('settings', function ($app) {
                    return Settings::first();
                });

                $data['order-info']['buy-fee']['BTC'] = app()['settings']->bitcoin_buy_fee;
                $data['order-info']['sell-fee']['BTC'] = app()['settings']->bitcoin_sell_fee;
                $data['order-info']['buy-fee']['ETH'] = app()['settings']->ethereum_buy_fee;
                $data['order-info']['sell-fee']['ETH'] = app()['settings']->ethereum_sell_fee;

                foreach ($cryptoCurrencies as $cryptoKey => $cryptoCurrency) {
                    foreach ($fiatCurrencies as $fiatKey => $fiatCurrency) {
                        $data['order-info']['buy-price'][$cryptoKey][$fiatKey] = $this->getCryptoBuyPrice($cryptoKey, $fiatKey);
                        $data['order-info']['sell-price'][$cryptoKey][$fiatKey] = $this->getCryptoSellPrice($cryptoKey, $fiatKey);
                    }
                }
            }

            event(new UpdateCryptoCurrencyEvent($data));

            $this->info('Send update event');
            sleep(5);
        }
    }
}
