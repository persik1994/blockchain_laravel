<?php

namespace App\Console\Commands;

use App\Models\User;
use App\UsersLoginHistory;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveUserLoginHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:remove-user-login-history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove user login history that are more than 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usersLoginHistoryQuery = UsersLoginHistory::whereDate('time', '<', Carbon::now()->subDays(30));

        $usersLoginHistoryQuery->delete();
    }
}
