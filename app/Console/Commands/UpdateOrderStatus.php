<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Settings;
use App\Models\User;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Console\Command;

class UpdateOrderStatus extends Command
{

    use Bitcoind;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:update-order-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d H:i:s');
        $orders = Order::with('user')->where('status', Order::STATUS_PROCESSING)->get();

        foreach ($orders as $order) {
            $txConfirmations = $this->bitcoind()->getRawTransaction($order->txid, true)->get('confirmations');
            if ($txConfirmations < 6) {
                continue;
            }

            $order->update([
                'status' => Order::STATUS_CONFIRMED
            ]);


            $settings = Settings::first();

            if ($order->order_type == Order::TYPE_BUY) {
                $order->user->update([
                    'balance' => $order->user->balance - $order->fiat_amount
                ]);

                $settings->update([
                    'balance' => $settings->balance + $order->fiat_amount
                ]);

                $this->info("[$date] Transferred  \${$order->fiat_amount} from user '{$order->user->email}' to 'Admin'");
            }

            if ($order->order_type == Order::TYPE_SELL) {
                $settings->update([
                    'balance' => $settings->balance - $order->fiat_amount
                ]);

                $order->user->update([
                    'balance' => $order->user->balance + $order->fiat_amount
                ]);

                $this->info("[$date] Transferred  \${$order->fiat_amount} from user 'Admin' to '{$order->user->email}'");
            }
        }
    }
}
