<?php

namespace App\Console\Commands;

use App\Models\Settings;
use App\Models\User;
use App\Models\UserAddresses;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Console\Command;
use Jcsofts\LaravelEthereum\Facade\Ethereum;


class UpdateUserBalance extends Command
{

    use Bitcoind;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:update-user-balance {--user= : ID of the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update user balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d H:i:s');
        $user = (int)$this->option('user');

        if ($user === 0) {
            $users = User::all();
        } else {
            $users = User::where('id', $user)->get();
        }

        foreach ($users as $user) {
            // UPDATE BITCOIN BALANCE
            $bitcoinBalance = $this->bitcoind()->getBalance($user->email, 6)->get();

            // UPDATE ETHEREUM BALANCE
            $ethereumTotalBalance = 0;
            $ethAddresses = UserAddresses::where('crypto_currency', 'ETH')->where('uid', $user->id)->get();
            foreach ($ethAddresses as $ethAddress) {
                $ethWai = Ethereum::eth_getBalance($ethAddress->address, 'latest', true);
                $ethereumBalance = ($ethWai / pow(10, 18));
                $ethereumTotalBalance = $ethereumTotalBalance + $ethereumBalance;
                $ethAddress->update([
                    'balance' => $ethereumBalance
                ]);

                $this->info("[$date] Updated ETH address [{$ethAddress->address}] of user {$user->id} : {$user->email} to amount: $ethereumBalance");
            }

            $user->update([
                'btc_balance' => $bitcoinBalance,
                'eth_balance' => $ethereumTotalBalance
            ]);

            $this->info("[$date] Updated balance of user {$user->id} : {$user->email} [BTC amount: $bitcoinBalance] [ETH amount: $ethereumTotalBalance]");
        }

        $settings = Settings::first();

        $adminEthereumAddresses = [
            $settings->eth_fee_address,
            $settings->eth_buy_address,
            $settings->eth_sell_address
        ];

        // UPDATE ADMIN BITCOIN AND ETHEREUM BALANCE
        $ethereumTotalBalance = 0;
        foreach ($adminEthereumAddresses as $address) {
            $ethWai = Ethereum::eth_getBalance($address, 'latest', true);
            $ethereumBalance = ($ethWai / pow(10, 18));
            $ethereumTotalBalance = $ethereumTotalBalance + $ethereumBalance;
        }

        $bitcoinBalance = $this->bitcoind()->getBalance('admin', 6)->get();
        $settings->update([
            'btc_balance' => $bitcoinBalance,
            'eth_balance' => $ethereumTotalBalance
        ]);

        $this->info("[$date] Updated admin balance: [BTC amount: $bitcoinBalance] [ETH amount: $ethereumTotalBalance]");
    }
}
