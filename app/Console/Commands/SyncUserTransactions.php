<?php

namespace App\Console\Commands;

use App\Models\Settings;
use App\Models\UserAddresses;
use App\Models\UserTransaction;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Console\Command;
use Jcsofts\LaravelEthereum\Facade\Ethereum;

class SyncUserTransactions extends Command
{

    use Bitcoind;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:sync-user-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync all user transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncEthereumBlocks();
        //$this->syncBitcoinBlocks();
    }



    private function syncBitcoinBlocks() {
        $settings = Settings::first();
        $lastProcessedBlock = $settings->btc_processed_block;
        $latestBlock = $this->bitcoind()->getBestBlockHash()->get();

        if (empty($settings->btc_processed_block)) {
            $settings->update([
                'btc_processed_block' => $latestBlock
            ]);

            $this->info('Updating bitcoind last processed block hash');

            return;
        }

        //TODO Finish sync for BTC (Check about confirmations number)
        $transactions = $this->bitcoind()->listSinceBlock($lastProcessedBlock)->get();

        dd($transactions);
    }



    private function syncEthereumBlocks() {
        $settings = Settings::first();
        $lastProcessedBlock = hexdec(Ethereum::eth_getBlockByHash($settings->eth_processed_block)->number);
        $latestBlock = Ethereum::eth_getBlockByNumber('latest');
        $latestBlockNumber = hexdec(Ethereum::eth_getBlockByNumber('latest')->number);

        if (empty($settings->eth_processed_block)) {
            $settings->update([
                'eth_processed_block' => $latestBlock->hash
            ]);

            $this->info('Updating ethereum last processed block hash');

            return;
        }


        $addresses = [];
        $transactions = [];
        $blocks = [];

        for ($i = $lastProcessedBlock; $i <= $latestBlockNumber; $i++) {
            $hex = dechex($i);
            $block = Ethereum::eth_getBlockByNumber("0x$hex", true);


            $blocks[$block->number] = hexdec($block->timestamp);

            foreach ($block->transactions as $transaction) {
                $addresses[] = $transaction->from;
                $addresses[] = $transaction->to;

                if (!isset($transactions[$transaction->from])) {
                    $transactions[$transaction->from] = [];
                }

                $transactions[$transaction->from][] = $transaction;


                if (!isset($transactions[$transaction->to])) {
                    $transactions[$transaction->to] = [];
                }

                $transactions[$transaction->to][] = $transaction;
            }
        }


        $addresses = array_unique($addresses);
        $addresses = array_filter($addresses, function ($element) {
            return !empty($element);
        });

        $addresses = UserAddresses::whereIn('address', $addresses)->where('crypto_currency', 'ETH')->get()->keyBy('address');


        $transactionsData = [];
        foreach ($addresses as $address => $addressData) {
            if (empty($transactions[$address])) {
                continue;
            }

            foreach ($transactions[$address] as $transaction) {
                $time = empty($transaction->blockNumber) ? time() : $blocks[$transaction->blockNumber];

                $transactionsData[] = [
                    'uid' => $addressData->uid,
                    'type' => $transaction->from == $address ? 'sent' : 'received',
                    'crypto_currency' => 'ETH',
                    'sender' => $transaction->from,
                    'recipient' => $transaction->to,
                    'txid' => $transaction->hash,
                    'amount' => hexdec($transaction->value) / pow(10, 18),
                    'time' => date('Y-m-d H:i:s', $time),
                    'confirmations' => empty($transaction->blockNumber) ? 0 : $latestBlockNumber - hexdec($transaction->blockNumber)
                ];

                $this->info("Insert transaction [{$transaction->hash}] of user [{$addressData->uid}]");
            }
        }


        UserTransaction::insert($transactionsData);

        $settings->update([
            'eth_processed_block' => $latestBlock->hash
        ]);

        $this->info('Updating ethereum last processed block hash');
    }
}
