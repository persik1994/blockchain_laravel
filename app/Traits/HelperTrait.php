<?php

namespace App\Traits;


use App\Models\Settings;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

trait HelperTrait
{
    /**
     * Get transactions by account.
     *
     * @param $account
     * @return array
     */
    public function getTransactionsByAccount($account)
    {
        $tx = array();

        $endBlockNumber = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBlockByNumber('latest', true);
        $endBlockNumber = hexdec($endBlockNumber->number);

        $startBlockNumber = $endBlockNumber - 100;

        for ($i = $startBlockNumber; $i <= $endBlockNumber; $i++) {
            $hex = dechex($i);
            $block = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBlockByNumber("0x$hex", true);
            if ($block != null) {
                $c = count($block->transactions);

                for ($j = 0; $j < $c; $j++) {
                    if ($account == $block->transactions[$j]->from || $account == $block->transactions[$j]->to || $account == '*') {
                        $tx[] = array(
                            'from' => $block->transactions[$j]->from,
                            'to' => $block->transactions[$j]->to,
                            'hash' => $block->transactions[$j]->hash,
                            'block_hash' => $block->transactions[$j]->blockHash,
                            'type' => ($account == $block->transactions[$j]->from ? 'send' : 'receive')
                        );
                    }
                }
            }
        }

        return $tx;
    }


    /**
     * Get current BTC/ETH price (default in USD currency)
     *
     * @param int $id
     * @param string $currency crypto currency
     * @return float
     */
    public function getCryptoCurrencyPriceChange24h($id = 1, $currency = 'USD')
    {
            $pairCode = $id . '_' . $currency;
//
//            if (!Cache::has($pairCode)) {
//                 $url = "https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=3";
//                 $ch = curl_init();
//                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//                 // Will return the response, if false it print the response
//                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                 curl_setopt($ch, CURLOPT_URL, $url);
//                 curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
//                 $result = curl_exec($ch);
//                 curl_close($ch);
//
//                 $data = json_decode($result);
//
//                 if (!is_object($data) || empty($data->data)) {
//                     Log::emergency('Error in getCryptoCurrencyPriceChange24h from https://api.coinmarketcap.com with data: ' . json_encode($data));
//                 }
//
//                 if (!isset($data->data->{$id})) {
//                     Log::emergency('Error in getCryptoCurrencyPriceChange24h from https://api.coinmarketcap.com with data: ' . json_encode($data->data));
//                 }
//
//                 if (is_object($data) && !empty($data->data) && isset($data->data->{$id})) {
//                     Cache::put($pairCode, (float)$data->data->{$id}->quotes->{$currency}->percent_change_24h, 1);
//                 }
//            }

            return Cache::get($pairCode);
    }


    /**
     * Get current Bitcoin price (default in USD currency)
     *
     * @param string $currency
     * @return float
     */
    public function getBitcoinPrice($currency = 'USD')
    {
        $pairCode = "BTC_{$currency}";

        if (!Cache::has($pairCode)) {
            $url = "https://api.gdax.com/products/BTC-$currency/ticker";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $result = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result);

            if (!is_object($data) && empty($data->price)) {
                Log::emergency('Error in getBitcoinPrice from https://api.gdax.com with data: ' . json_encode($data));
            }

            if (is_object($data) && !empty($data->price)) {
                Cache::put($pairCode, (float)$data->price, now()->addSecond(1));
                Cache::forever($pairCode . '_forever', (float)$data->price);
            }
        }

        return Cache::has($pairCode) ? Cache::get($pairCode) : Cache::get($pairCode . '_forever');
    }


    /**
     * Get current Ethereum price (default in USD currency)
     * @param string $currency
     * @return float
     */
    public function getEthereumPrice($currency = 'USD')
    {
        $pairCode = "ETH_{$currency}";

        if (!Cache::has($pairCode)) {
            $url = "https://api.gdax.com/products/ETH-$currency/ticker";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $result = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result);

            if (!is_object($data) && empty($data->price)) {
                Log::emergency('Error in getEthereumPrice from https://api.gdax.com with data: ' . json_encode($data));
            }

            if (is_object($data) && !empty($data->price)) {
                Cache::put($pairCode, (float)$data->price, now()->addSecond(1));
                Cache::forever($pairCode . '_forever', (float)$data->price);
            }
        }

        return Cache::has($pairCode) ? Cache::get($pairCode) : Cache::get($pairCode . '_forever');
    }


    /**
     * Convert currency
     * @param $amountconvertCurrency(
     * @param $fromCurrency
     * @param $toCurrency
     * @return float
     */
    public static function convertCurrency($amount, $fromCurrency, $toCurrency)
    {
        $pairCode = "{$fromCurrency}_{$toCurrency}";

        if (!Cache::has($pairCode)) {
            $url = "https://free.currencyconverterapi.com/api/v6/convert?q=$pairCode&compact=y";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $result = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result, true);

            if (!is_array($data) || empty($data[$pairCode])) {
                Log::emergency('Error in convertCurrency from https://free.currencyconverterapi.com with data: ' . json_encode($data));
            }

            Cache::put($pairCode, (float)$data[$pairCode]['val'], 60);
        }

        return Cache::get($pairCode) * $amount;
    }


    /**
     * Get Crypto buy price counting our fee
     *
     * @param $cryptoShortName - example BTC, ETH
     * @param string $fiatCurrency
     * @return float
     * @throws \Exception
     */
    public function getCryptoBuyPrice($cryptoShortName, $fiatCurrency = 'USD')
    {
        $settings = app()['settings'];

        switch($cryptoShortName) {
            case 'BTC':
                $cryptoBuyPrice = $settings->bitcoin_fixed_price;

                if ($settings->autoupdate_bitcoin_price == 1) {
                    $cryptoPrice = $this->getBitcoinPrice($fiatCurrency);
                    $cryptoBuyPrice = $cryptoPrice - (($cryptoPrice * $settings->bitcoin_buy_fee) / 100);
                }
                break;
            case 'ETH':
                $cryptoBuyPrice = $settings->ethereum_fixed_price;

                if ($settings->autoupdate_ethereum_price == 1) {
                    $cryptoPrice = $this->getEthereumPrice($fiatCurrency);
                    $cryptoBuyPrice = $cryptoPrice - (($cryptoPrice * $settings->ethereum_buy_fee) / 100);
                }
                break;
            default:
                throw new \Exception('Incorrect type value');
        }

        if ($settings->default_currency !== 'USD') {
            $cryptoBuyPrice = $this->convertCurrency($cryptoBuyPrice, 'USD', $settings->default_currency);
        }

        return (float)$cryptoBuyPrice;
    }


    /**
     * Get Crypto sell price counting our fee
     *
     * @param $cryptoShortName - example BTC, ETH
     * @return float
     * @throws \Exception
     */
    public function getCryptoSellPrice($cryptoShortName, $fiatCurrency = 'USD')
    {
        $settings = app()['settings'];

        switch($cryptoShortName) {
            case 'BTC':
                $cryptoSellPrice = $settings->bitcoin_fixed_price;

                if ($settings->autoupdate_bitcoin_price == 1) {
                    $cryptoPrice = $this->getBitcoinPrice($fiatCurrency);
                    $cryptoSellPrice = $cryptoPrice - (($cryptoPrice * $settings->bitcoin_sell_fee) / 100);
                }
                break;
            case 'ETH':
                $cryptoSellPrice = $settings->ethereum_fixed_price;

                if ($settings->autoupdate_ethereum_price == 1) {
                    $cryptoPrice = $this->getEthereumPrice($fiatCurrency);
                    $cryptoSellPrice = $cryptoPrice - (($cryptoPrice * $settings->bitcoin_sell_fee) / 100);
                }
                break;
            default:
                throw new \Exception('Incorrect type value');
        }

        if ($settings->default_currency !== 'USD') {
            $cryptoSellPrice = $this->convertCurrency($cryptoSellPrice, 'USD', $settings->default_currency);
        }

        return (float)$cryptoSellPrice;
    }


    /**
     * Get Crypto buy fee
     *
     * @param $cryptoShortName - example BTC, ETH
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public function getCryptoBuyFee($cryptoShortName, $currency = 'USD')
    {
        $settings = app()['settings'];

        $cryptoBuyFee = 0;
        switch($cryptoShortName) {
            case 'BTC':
                if ($settings->autoupdate_bitcoin_price == 1) {
                    $cryptoPrice = $this->getBitcoinPrice();
                    $cryptoBuyFee = number_format(abs(($cryptoPrice * $settings->bitcoin_buy_fee) / 100), 2);

                    if ($settings->default_currency !== 'USD') {
                        $cryptoBuyFee = $this->convertCurrency($cryptoBuyFee, 'USD', $currency);
                    }
                }
                break;
            case 'ETH':
                if ($settings->autoupdate_ethereum_price == 1) {
                    $cryptoPrice = $this->getEthereumPrice();
                    $cryptoBuyFee = number_format(abs(($cryptoPrice * $settings->ethereum_buy_fee) / 100), 2);

                    if ($settings->default_currency !== 'USD') {
                        $cryptoBuyFee = $this->convertCurrency($cryptoBuyFee, 'USD', $currency);
                    }
                }
                break;
            default:
                throw new \Exception('Incorrect type value');
        }

        return (float)$cryptoBuyFee;
    }


    /**
     * Get Crypto sell fee
     *
     * @param $cryptoShortName - example BTC, ETH
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public function getCryptoSellFee($cryptoShortName, $currency = 'USD')
    {
        $settings = app()['settings'];

        $cryptoSellFee = 0;
        switch($cryptoShortName) {
            case 'BTC':
                if ($settings->autoupdate_bitcoin_price == 1) {
                    $cryptoPrice = $this->getBitcoinPrice();
                    $cryptoSellFee = number_format(abs(($cryptoPrice * $settings->bitcoin_sell_fee) / 100), 2);

                    if ($settings->default_currency !== 'USD') {
                        $cryptoSellFee = $this->convertCurrency($cryptoSellFee, 'USD', $currency);
                    }
                }
                break;
            case 'ETH':
                if ($settings->autoupdate_ethereum_price == 1) {
                    $cryptoPrice = $this->getEthereumPrice();
                    $cryptoSellFee = number_format(abs(($cryptoPrice * $settings->ethereum_sell_fee) / 100), 2);

                    if ($settings->default_currency !== 'USD') {
                        $cryptoSellFee = $this->convertCurrency($cryptoSellFee, 'USD', $currency);
                    }
                }
                break;
            default:
                throw new \Exception('Incorrect type value');
        }


        return (float)$cryptoSellFee;
    }


    /**
     * Get address balance using blockchain info API
     *
     * @param $address - BTC address
     * @return bool|string - information about balance
     */
    public function getAddressBalance($address)
    {
        $address = urlencode($address);

        if (config('app.env') === 'local') {
            $get = "https://testnet.blockchain.info/q/addressbalance/$address?confirmations=6";
        } else {
            $get = "https://blockchain.info/q/addressbalance/$address?confirmations=6";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $get);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}