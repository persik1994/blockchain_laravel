<?php
/**
 * Created by PhpStorm.
 * User: besliu
 * Date: 11/15/18
 * Time: 5:48 PM
 */

namespace App\Traits;


use Doctrine\Common\Cache\Cache;

class HelperNotificationsTrait
{
    public function getGlobalNotifications()
    {
        $globalNotifications = [];
        $globalNotificationsCount = 0;

        if(Cache::has('tickets_unviewed_by_admin')) {
            $globalNotificationsCount += (int) Cache::get('tickets_unviewed_by_admin');

            $globalNotifications['unviewed_ticket'] = [
                'icon' => 'far fa-life-ring',
                'messages' => []
            ];

            $unviewedTickets = Support::with('user', 'messages')->where('viewed_by_admin', 0)->get();

            if (!$unviewedTickets->isEmpty()) {
                foreach ($unviewedTickets as $unviewedTicket) {
                    $globalNotifications['unviewed_ticket']['messages'][$unviewedTicket->id]['url']
                        = route('admin.chat', $unviewedTicket->id);

                    $message = 'You have a new  ticket #' . $unviewedTicket->id;
                    if (!$unviewedTicket->messages->isEmpty()) {
                        $message = 'You have new messages for ticket #' . $unviewedTicket->id;
                    }

                    $globalNotifications['unviewed_ticket']['messages'][$unviewedTicket->id]['message'] = $message;
                }
            }
        }

        $viewsShareArray['globalNotificationsCount'] = $globalNotificationsCount;
        $viewsShareArray['globalNotifications'] = $globalNotifications;

        return $viewsShareArray;
    }
}