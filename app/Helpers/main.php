<?php

function satoshiToBitcoin($value)
{
    if ($value < 0) {
        $value = 0;
    }

    return $value / 100000000;
}

function convertToCrypto($value, $type) {
    if ($type === 'BTC') {
        return formatBitcoin($value);
    }

    if ($type === 'ETH') {
        return formatEthereum($value);
    }
}

function formatBitcoin($value)
{
    return number_format($value, 8);
}

function formatEthereum($value)
{
    return number_format($value, 18);
}

function formatBytes($bytes, $precision = 2)
{
    if ($bytes > pow(1024, 3)) return round($bytes / pow(1024, 3), $precision) . "GB";
    else if ($bytes > pow(1024, 2)) return round($bytes / pow(1024, 2), $precision) . "MB";
    else if ($bytes > 1024) return round($bytes / 1024, $precision) . "KB";
    else return ($bytes) . "B";
}

function satoshitize($satoshitize)
{
    return sprintf('%.8f', $satoshitize);
}

function satoshitrim($satoshitrim)
{
    return rtrim(rtrim($satoshitrim, '0'), '.');
}