<?php

namespace App\Broadcasting;

use App\Models\Support;
use App\Models\User;

class UserSupportResponseChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\Models\User  $user
     * @return array|bool
     */
    public function join(User $user, $ticketId)
    {
        $ticket = Support::find($ticketId);

        if (empty($ticket)) {
            return false;
        }

        return (int) $ticket->uid === (int) $user->id;
    }
}
