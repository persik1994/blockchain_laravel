<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->type === 'admin' && ($request->route()->uri === 'login' || $request->route()->uri === 'register')) {
                return $next($request);
            }

            if ($request->route()->uri !== 'admin/login' && $request->route()->methods[0] !== 'POST') {
                if (Auth::user()->type === 'admin') {
                    return redirect('/admin');
                } else {
                    return redirect('/account/wallet');
                }
            }
        }

        return $next($request);
    }
}
