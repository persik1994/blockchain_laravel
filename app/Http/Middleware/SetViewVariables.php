<?php

namespace App\Http\Middleware;

use App\Models\SupportChat;
use App\Models\Support;
use App\Traits\HelperTrait;
use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

use App\Models\Settings;
use Laravel\Horizon\Horizon;
use Illuminate\Support\Facades\Cache;

class SetViewVariables
{
    use HelperTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $explodedRequestPath = explode('/', $request->path());

        if (Schema::hasTable('settings')) {
            app()->bind('settings', function () {
                return Settings::first();
            });
        }

        if ($explodedRequestPath[0] === 'admin') {
            Horizon::auth(function ($request) {
                if ($request->user() && $request->user()->type == 'admin') return true;
                return false;
            });

            $viewsShareArray = $this->backEndShareData();
        } else {
            $viewsShareArray = $this->frontEndShareData();
        }

        $viewsShareArray['user2FaAuthenticated'] = \Auth::user() && \Auth::user()->checkIf2FaAuthenticated($request);

        View::share($viewsShareArray);

        return $next($request);
    }

    protected function frontEndShareData() {
        $cryptoFiatPrice['BTC']['USD'] = $this->getBitcoinPrice('USD');
        $cryptoFiatPrice['BTC']['EUR'] = $this->getBitcoinPrice('EUR');
        $cryptoFiatPrice['ETH']['USD'] = $this->getEthereumPrice('USD');
        $cryptoFiatPrice['ETH']['EUR'] = $this->getEthereumPrice('EUR');

        $cryptoFiatLevelBy24H['BTC']['USD'] = $this->getCryptoCurrencyPriceChange24h(1, 'USD');
        $cryptoFiatLevelBy24H['BTC']['EUR'] = $this->getCryptoCurrencyPriceChange24h(1, 'EUR');
        $cryptoFiatLevelBy24H['ETH']['USD'] = $this->getCryptoCurrencyPriceChange24h(1027, 'USD');
        $cryptoFiatLevelBy24H['ETH']['EUR'] = $this->getCryptoCurrencyPriceChange24h(1027, 'EUR');

        $viewsShareArray = [
            'cryptoFiatPrice' => $cryptoFiatPrice,
            'cryptoFiatLevelBy24H' => $cryptoFiatLevelBy24H
        ];

        $user = \Auth::user();

        if (Schema::hasTable('settings')) {
            $viewsShareArray['settings'] = app()['settings'];

            $cryptoCurrencies = config('currencies.crypto_currencies');
            $fiatCurrencies = config('currencies.fiat_currencies');

            $viewsShareArray['cryptoBuyFee']['BTC'] = $viewsShareArray['settings']->bitcoin_buy_fee;
            $viewsShareArray['cryptoSellFee']['BTC'] = $viewsShareArray['settings']->bitcoin_sell_fee;
            $viewsShareArray['cryptoBuyFee']['ETH'] = $viewsShareArray['settings']->ethereum_buy_fee;
            $viewsShareArray['cryptoSellFee']['ETH'] = $viewsShareArray['settings']->ethereum_sell_fee;

            foreach ($cryptoCurrencies as $cryptoKey => $cryptoCurrency) {
                foreach ($fiatCurrencies as $fiatKey => $fiatCurrency) {
                    $viewsShareArray['cryptoBuyPrice'][$cryptoKey][$fiatKey] = $this->getCryptoBuyPrice($cryptoKey, $fiatKey);
                    $viewsShareArray['cryptoSellPrice'][$cryptoKey][$fiatKey] = $this->getCryptoSellPrice($cryptoKey, $fiatKey);
                }
            }

            $viewsShareArray['cryptoCurrencies'] = $cryptoCurrencies;
            $viewsShareArray['fiatCurrencies'] = $fiatCurrencies;
        }

        if (!empty($user)) {
            $viewsShareArray['userCryptoCurrency'] = $user->crypto_currency;
            $viewsShareArray['userFiatCurrency'] = $user->fiat_currency;
        }

        return $viewsShareArray;
    }

    protected function backEndShareData() {
        $globalNotifications = [];
        $globalNotificationsCount = 0;

        if (Schema::hasTable('settings')) {
            $viewsShareArray['settings'] = app()['settings'];
        }

        if(Cache::has('tickets_unviewed_by_admin')) {
            $globalNotificationsCount += (int) Cache::get('tickets_unviewed_by_admin');

            $globalNotifications['unviewed_ticket'] = [
                'icon' => 'far fa-life-ring',
                'messages' => []
            ];

            $unviewedTickets = Support::with('user', 'messages')->where('viewed_by_admin', 0)->get();

            if (!$unviewedTickets->isEmpty()) {
                foreach ($unviewedTickets as $unviewedTicket) {
                    $globalNotifications['unviewed_ticket']['messages'][$unviewedTicket->id]['url']
                        = route('admin.chat', $unviewedTicket->id);

                    $message = 'You have a new  ticket #' . $unviewedTicket->id;
                    if (!$unviewedTicket->messages->isEmpty()) {
                        $message = 'You have new messages for ticket #' . $unviewedTicket->id;
                    }

                    $globalNotifications['unviewed_ticket']['messages'][$unviewedTicket->id]['message'] = $message;
                }
            }
        }

        $viewsShareArray['globalNotificationsCount'] = $globalNotificationsCount;
        $viewsShareArray['globalNotifications'] = $globalNotifications;

        return $viewsShareArray;
    }
}
