<?php

namespace App\Http\Middleware;

use Closure;

class CheckForUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->type === 'user') {
            return $next($request);
        }

        return abort(403, 'Sorry, you are not authorized to access this page.');
    }
}
