<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\UserAddresses;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use Bitcoind;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account/wallet';
    private $passwordUser = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|min:6|max:255',
            'mobile_number' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registrcreateCryptoDefaultAddressation.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'name' => $data['name'],
            'secret_pin' => '',
            'status' => 0,
            'btc_balance' => 0,
            'eth_balance' => 0,
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        $this->passwordUser = $data['password'];
        $this->createCryptoDefaultAddress('BTC', $user); 
        $this->createCryptoDefaultAddress('ETH', $user); 

        return $user;
    }


    protected function createCryptoDefaultAddress($crypto, User $user) {
        if ($crypto == 'BTC') {
            $response = $this->bitcoind()->getNewAddress($user->email);
            UserAddresses::create([
                'uid' => $user->id,
                'label' => 'Default BTC address',
                'crypto_currency' => $crypto,
                'address' => $response->get(),
                'created' => \DB::raw('NOW()'), 
                'updated' => \DB::raw('NOW()')
            ]);
        }

        if ($crypto == 'ETH') { 
            $addressEthereum = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($this->passwordUser);
            UserAddresses::create([
                'uid' => $user->id,
                'label' => 'Default ETH address',
                'crypto_currency' => $crypto,
                'address' => $addressEthereum,
                'created' => \DB::raw('NOW()'), 
                'updated' => \DB::raw('NOW()'),
            ]);
        }
    }
}
