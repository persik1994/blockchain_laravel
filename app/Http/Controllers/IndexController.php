<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
// use App\Libs\Eth\Ethereum;
// use Web3p\EthereumTx\Transaction;
use GuzzleHttp\Exception\GuzzleException;	
use GuzzleHttp\Client;
use Jcsofts\LaravelEthereum\Facade\Ethereum;
use Jcsofts\LaravelEthereum\Lib\EthereumTransaction;


class IndexController extends Controller
{
    public function index()	
    {
    	echo 'Ethereum protocol version:';
		try {
		        $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_protocolVersion();
		        print_r($ret);
		    } catch (Exception $e){
		        echo $e->getMessage();
		    }	
		echo '<br>';
		echo 'Ethereum get Balance:';
		try {
		        $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance('0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76', 'latest', TRUE);
		        $ret = $ret/pow(10, 18);
		        print_r($ret);
		    } catch (Exception $e){
		        echo $e->getMessage();
		    }
		echo '<br>';
		echo '<br>';



		// echo 'Ethereum accounts:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_accounts();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum Block number:';
		// try {	
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_blockNumber(TRUE);
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }	
		// echo '<br>';
		// echo 'Ethereum Gas Price:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_gasPrice();
		//         echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum Hashrate:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_hashrate();
		//         echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum Mining:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_mining();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }		    
		// echo '<br>';
		// echo 'Ethereum Coinbase:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_coinbase();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }	
		// echo '<br>';
		// echo 'Ethereum Transaction Count:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getTransactionCount('0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76');
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }	
		// echo '<br>';
		// echo 'Ethereum Sign:';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_sign('0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76', '');
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }	
		// echo '<br>';
		// echo '<hr>';
		// echo 'Ethereum Send Transaction:';

	 //    $from = '0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76';
	 //    $to = '0x4d669dfc51a24406de0c67532998d5c3395a7a8c';
	 //    $gas = "0x76c0"; // 30400
	 //    $gasPrice = "0x9184e72a000"; // 10000000000000
	 //    $value = '0x9184e72a'; // 2441406250
	 //    $data = '0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675';
	 //    $transaction = new EthereumTransaction($from, $to, $value, $gas, $gasPrice, $data);
				
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_sendTransaction($transaction);
		//         echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum Unlock account:';
		// $address = '0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76';
		// $passphrase = '';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_unlockAccount($address,$passphrase,$duration=300);
		//         echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo "<br>";
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_listAccounts();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo "<br>";
		// echo "Ethereum personal Send Transaction: ";
	 //    $from = '0x8bdbde9df7e4e2778a497e84e5ea4115bda4fb76';
	 //    $to = '0x4d669dfc51a24406de0c67532998d5c3395a7a8c';
	 //    $gas = "0x76c0"; // 30400
	 //    $gasPrice = "0x9184e72a000"; // 10000000000000
	 //    $value = '0x9184e72a';
	 //    $data = '0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675';
	 //    $transaction = new EthereumTransaction($from, $to, $value, $gas, $gasPrice, $data);	
	 //    $passphrase = '';	
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_sendTransaction($transaction,$passphrase);
		//          echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }		
		// echo '<br>';	
		// echo '<hr>';
		// echo "Ethereum personal New Account: ";
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($passphrase = '');
		//          echo $ret;
		//     } catch (Exception $e){	
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum personal List Accounts: ';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_listAccounts();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }	
		// echo '<br>';
		// echo "Ethereum personal New Account_2: ";
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($passphrase = '');
		//          echo $ret;
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }
		// echo '<br>';
		// echo 'Ethereum personal List Accounts: ';
		// try {
		//         $ret = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_listAccounts();
		//         print_r($ret);
		//     } catch (Exception $e){
		//         echo $e->getMessage();
		//     }

	    //$host = "localhost";
	    //$port = 8545;
		// $httpClient = new GuzzleClient(new GuzzleClientFactory(), $host, $port);
		// $client = new Client($httpClient);
		// $result = $client->callMethod('eth_getBalance', ['0x73E745783237ee4EaeB321fbC9A9f5821255486D', 'latest']);
		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";
		

		// $transaction = new Transaction([	
		//     'nonce' => '0x01',
		//     'from' => '0x73E745783237ee4EaeB321fbC9A9f5821255486D',
		//     'to' => '0xC96d52Fc0c5BC091A2438A1c965de142eF1d433C',
		//     'gas' => '0x76c0',
		//     'gasPrice' => '0x9184e72a000',
		//     'value' => '0x9184e72a',
		//     'chainId' => 1,
		//     'data' => '0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675'
		// ]);

		// $hashedTx = $transaction;
		// echo "It's var dump of hashed txns:";
		// echo "<pre>";
		// print_r($hashedTx);
		// echo "</pre>";
	
		// echo "Hex encoded transaction:";
		// $transaction = new Transaction('0xf86c098504a817c800825208943535353535353535353535353535353535353535880de0b6b3a76400008025a028ef61340bd939bc2195fe537567866003e1a15d3c71ff63e1590620aa636276a067cbe9d8997f761aecb703304b3800ccf555c9f3dc64214b297fb1966a3b6d83');
		// echo $transaction;
		// echo "<br>";

		// echo "Sign a transaction:";
		// $signedTransaction = $transaction->sign('def8a2e2e6ff3dbe243180ff729853f05acd6f591170419b73aea5080e474e4a');
		// echo "<pre>";
		// print_r($signedTransaction);
		// echo "</pre>";
		// echo "</br>";

		// echo "It's var dump of hashed txns serialized:";
		// $serializedTx = $transaction->serialize();
		// echo "<pre>";	
		// print_r($serializedTx);
		// echo "</pre>";

        //$eth = new Ethereum(Config::get('eth.node_host'), Config::get('eth.node_port'));
        //dd($eth->eth_accounts());
    }	
}	