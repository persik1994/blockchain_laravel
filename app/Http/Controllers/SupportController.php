<?php

namespace App\Http\Controllers;

use App\Broadcasting\UnauthenticatedSupportResponseChannel;
use App\Broadcasting\UserSupportTicketRequestChannel;
use App\Events\UnauthenticatedSupportResponseEvent;
use App\Events\UserSupportTicketRequestEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SupportChat;
use App\Models\User;
use App\Models\UserMoney;
use App\Models\Support;
use App\Models\Order;
use App\Models\Settings;
use App\Traits\HelperTrait;
use Illuminate\Support\Facades\Cache;
use App\Events\UserSupportRequestEvent;
use App\Events\UserSupportResponseEvent;


class SupportController extends Controller
{

    public function index() 
    {
        $user = \Auth::user();

        $usertickets = [];
        if (!empty($user)) {
            $usertickets = Support::where('uid', $user->id)->get();
        }

        return view('support.index', compact('support'), [
            'usertickets' => $usertickets
        ]);
    }

    public function addTicket(Request $request) {
        if(\Auth::user()) {
            $validateFieldRules = [
                'support_title' => 'required',
                'support_message' => 'required'
            ];
        } else {
            $validateFieldRules = [
                'support_title' => 'required',
                'support_email' => 'required',
                'support_message' => 'required'
            ];            
        }

        $request->validate($validateFieldRules);

        if(\Auth::user()) {
            $ticket = Support::create([
                'uid' => \Auth::user()->id,
                'title' => $request->get('support_title'),
                'description' => $request->get('support_message')
            ]);
        } else {
            $email = $request->get('support_email');
            $microtime = microtime(true);
            $salt = uniqid(mt_rand(), true);
            $hash = $email . $microtime . $salt;
            $hash_decrypt =  md5($hash);
            $ticket = Support::create([
                'ticket_hash' => $hash_decrypt,
                'title' => $request->get('support_title'),
                'email_address' => $request->get('support_email'),
                'description' => $request->get('support_message')
            ]);
        }

        $countUnviewed = Support::where('viewed_by_admin', 0)->count();
        Cache::forever('tickets_unviewed_by_admin', $countUnviewed);

        $ticketPagination = Support::paginate(10);

        $ticket->load('user');
        $ticket->edit_url = route('admin.support.modal.edit', $ticket->id);
        $ticket->delete_url = route('admin.support.delete', $ticket->id);

        event(new UserSupportTicketRequestEvent([
            'ticket' => $ticket,
            'pagination' => $ticketPagination,
            'unviewed_items' => $countUnviewed
        ]));

        if(\Auth::user()) {
            return redirect("/support/$ticket->id");
        } else {
            return redirect("/support/$hash_decrypt");
        }
    }

    public function addUserMessage(Request $request, $id) {

        $validateFieldRules = [
            'user_chat_message' => 'required'
        ];

        $request->validate($validateFieldRules);

        $user = \Auth::user();
        $ticketQuery = Support::with('messages', 'user');

        if(filter_var($id, FILTER_VALIDATE_INT) !== false) {
            if(!empty($user)) {
                $messageData = ['uid' => \Auth::user()->id];
                $ticket = $ticketQuery->where('id', $id)->where('uid', $user->id)->first();
            }
        }

        if(preg_match('/^[a-f0-9]{32}$/', $id)) {
            $ticket = $ticketQuery->where('ticket_hash', $id)->first();
        }  

        if(empty($ticket)) {
            return response()->json([
                'message' => 'Ticket was not found'
            ], 404);
        }

        $messageData['ticket_id'] = $ticket->id;
        $messageData['message'] = $request->get('user_chat_message');

        SupportChat::create($messageData);

        $ticket->viewed_by_admin = 0;
        $ticket->save();

        $countUnviewed = SupportChat::where('ticket_id', $ticket->id)
            ->where('viewed_by_admin', 0)->count();
        Cache::forever('ticket_' . $ticket->id . '_messages_unviewed_by_admin', $countUnviewed);

        $countUnviewed = Support::where('viewed_by_admin', 0)->count();
        Cache::forever('tickets_unviewed_by_admin', $countUnviewed);

        $messages = SupportChat::with('user')->where('ticket_id', $ticket->id)->get();

        foreach ($messages as $message) {
            if ($message->user) {
                $message->user->makeVisible('type');
            }
        }

        event(new UserSupportRequestEvent([
            'messages' => $messages,
            'ticket' => $ticket,
            'unviewed_items' => $countUnviewed
        ]));

        return response()->json([
            'messages' => $messages,
            'ticket' => $ticket
        ]);
    }

    public function userChat(Request $request, $id) {
        $ticketQuery = Support::with('messages', 'user');

        if(filter_var($id, FILTER_VALIDATE_INT) !== false) {
            $user = \Auth::user();

            if(!empty($user)) {
                $ticket = $ticketQuery->where('id', $id)->where('uid', $user->id)->first();
                if(empty($ticket)) {
                    return redirect('/support')->withErrors(['Ticket was not found']);
                } 
            }
        }

        if(preg_match('/^[a-f0-9]{32}$/', $id)) {
            $ticket = $ticketQuery->where('ticket_hash', $id)->first();
            if(empty($ticket)) {
                return redirect('/support')->withErrors(['Ticket was not found']);
            }             
        }
        
        if(empty($ticket)) {
            abort('404');
        }  

        return view('support.userchat', compact('ticket', 'id'));

    }
}
