<?php

namespace App\Http\Controllers\Admin;

use App\Events\UnauthenticatedSupportResponseEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SupportChat;
use App\Models\User;
use App\Models\UserMoney;
use App\Models\Support;
use App\Models\Order;
use App\Models\Settings;
use App\Traits\HelperTrait;
use Illuminate\Support\Facades\Cache;
use App\Events\UserSupportRequestEvent;
use App\Events\UserSupportResponseEvent;


class SupportController extends Controller
{


    public function index()
    {
        $tickets = Support::with('user')
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('admin.support.index', compact('tickets'));
    }

    public function adminChat(Request $request, $id) {
        $ticket = Support::find($id);

        if (empty($ticket)) {
            abort('404');
        }

        $ticket->load('messages', 'user');

        $ticket->viewed_by_admin = 1;
        $ticket->save();

        SupportChat::where('ticket_id', $ticket->id)->update([
            'viewed_by_admin' => 1
        ]);

        $countUnviewed = SupportChat::where('ticket_id', $ticket->id)
            ->where('viewed_by_admin', 0)->count();

        Cache::forever('ticket_' . $ticket->id . '_messages_unviewed_by_admin', $countUnviewed);

        $cointUnviewed = Support::where('viewed_by_admin', 0)->count();
        Cache::forever('tickets_unviewed_by_admin', $cointUnviewed);

        return view('admin.support.adminchat', compact('ticket', 'id'));
    }

    /**
     * Add new row in support
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $validateFieldRules = [
            'title' => 'required',
            'description' => 'required'
        ];

        $request->validate($validateFieldRules);

        $ticket = Support::where('title', $request->title)
            ->where('description', $request->description)->first();

        if (!empty($ticket)) {
            return response()->json([
                'message' => 'Ticket ' . $request->title . ' already exist'
            ], 404);
        } else {

            $ticket = Support::create($request->all());

            return response()->json([
                'message' => 'Ticket ' . $ticket->title . ' was added successfully.'
            ]);
        }
    }

    public function addAdminMessage(Request $request, $id) {
        $validateFieldRules = [
            'admin_chat_message' => 'required'
        ];
        
        $request->validate($validateFieldRules);

        $ticket = Support::find($id);

        if(empty($ticket)) {
            return response()->json([
                'message' => 'Ticket was not found. Please refresh page.'
            ], 400);
        }

        SupportChat::create([
          'uid' => \Auth::user()->id,
          'ticket_id' => $id,
          'message' => $request->get('admin_chat_message')
        ]);

        $messages = SupportChat::with('user')->where('ticket_id', $id)->get();

        foreach ($messages as $message) {
            if ($message->user) {
                $message->user->makeVisible('type');
            }
        }

        if ($ticket->uid != null) {
            event(new UserSupportResponseEvent([
                'messages' => $messages,
                'ticket' => $ticket
            ]));
        } else {
            event(new UnauthenticatedSupportResponseEvent([
                'messages' => $messages,
                'ticket' => $ticket
            ]));
        }

        return response()->json([
             'messages' => $messages,
             'ticket' => $ticket
         ]);
    }

    public function markAsViewed(Request $request)
    {
        $request->validate([
            'items' => 'array',
            'items.*' => 'integer'
        ]);

        if (empty($request->items)) {
            return response()->json([
                'message' => 'Please select at least one field'
            ], 500);
        }

        foreach ($request->items as $id) {
            $ticket = Support::with('messages')->find($id);

            if (!empty($ticket)) {
                $ticket->viewed_by_admin = 1;
                $ticket->save();

                SupportChat::where('ticket_id', $ticket->id)->update([
                    'viewed_by_admin' => 1
                ]);

                Cache::forever('ticket_' . $ticket->id . '_messages_unviewed_by_admin', 0);
            }
        }

        $countUnviewed = Support::where('viewed_by_admin', 0)->count();
        Cache::forever('tickets_unviewed_by_admin', $countUnviewed);

        return response()->json([]);
    }

    /**
     * Delete gateway item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $item = Support::with('messages')->find($id);

        if ($item) {
            $item->messages()->delete();
            $item->delete();

            return response()->json([
                'message' => 'Ticket was deleted successfully.',
            ]);
        }

        $countUnviewed = Support::where('viewed_by_admin', 0)->count();
        Cache::forever('tickets_unviewed_by_admin', $countUnviewed);

        Cache::forever('ticket_' . $item->id . '_messages_unviewed_by_admin', 0);

        return response()->json([
            'message' => 'Gateway was not found.'
        ], 404);
    }

    /**
     * Update ticket
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validateFieldRules = [
            'title' => 'required',
            'description' => 'required'
        ];

        $request->validate($validateFieldRules);

        $ticket = Support::find($id);

        if (empty($ticket)) {
            return response()->json([
                'message' => 'Ticket was not found'
            ], 404);
        }

        if ($ticket->title != $request->title && $ticket->description != $request->description) {
            $ticketExist = Support::where('title', $request->title)
                ->where('description', $request->description)->first();

            if (!empty($ticketExist)) {
                return response()->json([
                    'message' => 'Ticket ' . $request->title . ' already exist'
                ], 404);
            }
        }


        $ticket->update($request->all());

        return response()->json([
            'message' => 'Ticket ' . $ticket->title . ' was added successfully.'
        ]);
    }

    /**
     * Get "Edit support" modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editSupportModal($id) {

        $supportToEdit = Support::find($id);

        if (!empty($ticket)) {
            return response()->json([
                'message' => 'Ticket was not found! Please refresh page.'
            ], 404);
        }

        return view('admin.support.modals.edit', compact('supportToEdit'));
    }


}
