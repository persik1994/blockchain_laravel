<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserTransferResponseEvent;
use App\Models\Settings;
use App\Models\Transfer;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class TransfersController extends Controller
{

    use Bitcoind;

    public function index()
    {
        $transfers = Transfer::orderBy('id', 'desc')->paginate(10);

        return view('admin.transfers.index', [
            'transfers' => $transfers
        ]);
    }

    public function detailModal(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $transfer = Transfer::with('user')->findOrFail($request->id);

        $transfer->viewed_by_admin = 1;
        $transfer->save();

        $cointUnviewed = Transfer::where('viewed_by_admin', 0)->count();
        Cache::forever('transfers_unviewed_by_admin', $cointUnviewed);

        return view('admin.transfers.modals.detail', [
            'transfer' => $transfer
        ]);
    }


    /**
     * Mark user transfer row as viewed
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsViewed(Request $request)
    {
        $request->validate([
            'items' => 'array',
            'items.*' => 'integer'
        ]);

        if (empty($request->items)) {
            return response()->json([
                'message' => 'Please select at least one field'
            ], 500);
        }

        foreach ($request->items as $id) {
            $transferMoney = Transfer::where('id', $id)->first();

            if (!empty($transferMoney)) {
                if ($transferMoney->viewed_by_admin == 0) {
                    $transferMoney->viewed_by_admin = 1;
                    $transferMoney->save();
                }
            }
        }

        $cointUnviewed = Transfer::where('viewed_by_admin', 0)->count();
        Cache::forever('transfers_unviewed_by_admin', $cointUnviewed);

        return response()->json([]);
    }

    /**
     * Confirm user transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmTransfer(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $transfer = Transfer::with('user')->findOrFail($request->id);

        $txFee = $this->bitcoind()->getWalletInfo()->get('paytxfee');
        $settings = Settings::first();

        $userAmount = (float)$transfer->user->btc_balance - (float)$txFee - (float)$settings->withdrawal_comission;
        $userAmount = $userAmount < 0 ? 0 : $userAmount;

        $userAmount = number_format($userAmount, 8);
        $transferAmount = (float)$transfer->crypto_amount;

        if ($transferAmount > $userAmount) {
            return response()->json([
                'message' => __('account.error_30', ['amount' => $userAmount, 'currency' => 'BTC'])
            ], 400);
        }


        $this->bitcoind()->sendMany($transfer->user->email, [
            $transfer->recipient_address => $transferAmount,
            $settings->btc_fee_address => (float)$settings->withdrawal_comission
        ]);


        $transfer->user->update([
            'btc_balance' => $userAmount - $transferAmount
        ]);

        $transfer->update([
            'status' => 1
        ]);

        $transfers = Transfer::where('uid', $transfer->user->id)->orderBy('time', 'desc')->get();

        $transfer->user->updateUnviewedItemsCache('transfer');

        $transfer->crypto_amount = convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency);

        event(new UserTransferResponseEvent([
            'transfer' => $transfer,
            'transfers' => $transfers,
            'unviewed_transfers' => $transfer->user->getCountUnviewedItems('transfers')
        ]));

        return response()->json([
            'message' => "You successfully transferred {$transfer->crypto_amount} BTC to address: {$transfer->recipient_address}"
        ]);
    }


    /**
     * Cancel user transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelTransfer(Request $request) {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $transfer = Transfer::with('user')->findOrFail($request->id);

        $transfer->update([
            'status' => 2
        ]);

        $transfers = Transfer::where('uid', $transfer->user->id)->orderBy('time', 'asc')->get();

        $transfer->user->updateUnviewedItemsCache('transfer');
        $transfer->details_page_url = route('account.transfer.details', $transfer->id);

        event(new UserTransferResponseEvent([
            'transfer' => $transfer,
            'transfers' => $transfers,
            'unviewed_transfers' => $transfer->user->getCountUnviewedItems('transfers')
        ]));

        return response()->json([
            'message' => 'Transfer request was canceled'
        ]);
    }

}
