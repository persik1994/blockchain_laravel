<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::paginate(10);

        return view('admin.pages.index', compact('pages'));
    }

    public function add()
    {
        return view('admin.pages.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'prefix' => 'required|regex:/^[a-zA-Z0-9-_]+$/'
        ]);

        Page::create($request->all());

        return redirect(route('admin.pages'));
    }

    public function edit($id, Request $request)
    {
        $page = Page::findOrFail($id);

        return view('admin.pages.edit', compact('page'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
           'title' => 'required',
           'prefix' => 'required|regex:/^[a-zA-Z0-9-_]+$/',
           'page_content' => ''
        ]);

        $page = Page::find($id);

        if (empty($page)) {
            abort('404', 'Page was not found');
        }

        $page->title = $request->title;
        $page->prefix = $request->prefix;
        $page->content = $request->page_content;
        $page->save();

        return redirect(route('admin.pages'));
    }

    public function delete($id)
    {
        $item = Page::find($id);

        if ($item) {
            $item->delete();

            return response()->json([
                'message' => 'Pages was deleted successfully.',
            ]);
        }

        return response()->json([
            'message' => 'Page was not found.'
        ], 404);
    }
}
