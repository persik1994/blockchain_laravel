<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AddressValidator;
use App\Models\Order;
use App\Models\Settings;
use App\Models\User;
use App\Models\Support;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Cache;
use App\Events\UserSupportRequestEvent;
use App\Events\UserSupportResponseEvent;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    use Bitcoind;
    use HelperTrait;



    public function detailsModal(Request $request)
    {
        return view('admin.ethereumadresses.modals.ethereum-details');
    }
    /**
     *  Ethereum node balance page
     *
     * @return view page
     */
    public function ethereumModalNodeBalance()
    {
        return view('admin.ethereumadresses.index');
    }
    /**
     * Get ethereum node balance
     *
     * @return float - balance amount
     */
    public function ethereumBalanceNode() 
    {
        global $totalBal;
        $totalBal = 0;
        $ethereumAccounts = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_listAccounts();

        foreach ($ethereumAccounts as $ethereum_key => $ethereumAccount) 
        {
            $acct = $ethereumAccounts[$ethereum_key];
            $acctBal = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($acct);
            $decimal = pow(10, 18);
            $acctBalHexToDec = hexdec($acctBal) / $decimal;
            $totalBal += $acctBalHexToDec;
        }
        return $totalBal;
    }

    /**
     * Get ethereum node balance in Modal
     *
     * @return float - balance amount
     */
    // public function ethereumBalanceNodeModal()
    // {

    //     $ethereumAccountsInformation = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_accounts();

    //     foreach ($ethereumAccountsInformation as $ethereum_infokey => $ethereumAccountInformation) 
    //     {
    //         $acctInfo = $ethereumAccountsInformation[$ethereum_infokey];
    //         $acctBal = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($acctInfo);
    //         $decimal = pow(10, 18);
    //         $acctBalHexToDec = hexdec($acctBal) / $decimal;
    //         return "Your current balance in your address ($acctInfo): $acctBalHexToDec ETH";
    //     }

    // }

    public function index()
    {
        $users = User::all();

        $response = $this->bitcoind()->getWalletInfo();
        $walletInfo = $response->get(); 

        $settings = Settings::first();
        $adminUsdBalance = $settings->balance;
        $adminBtcBalance = $settings->btc_balance;
        $adminEthBalance = $settings->eth_balance;
        $btcBuyAddress = $settings->btc_buy_address;
        $btcSellAddress = $settings->btc_sell_address;
        $btcFeeAddress = $settings->btc_fee_address;

        /* Total Balance function */
        $ethereumAllNodeBalance = $this->ethereumBalanceNode();


        /* All Unviewed Support Tickets */
        $cointUnviewed = Support::where('status', 'opened')->where('viewed_by_admin', 0)->count();
        Cache::forever('supports_unviewed_by_admin', $cointUnviewed);


        if (!AddressValidator::isValid($btcBuyAddress)) {
            $buyBalanceNotification = [
                'valid' => false,
                'message' => 'Please set a valid buy BTC address'
            ];
        }

        if (!AddressValidator::isValid($btcSellAddress)) {
            $sellBalanceNotification = [
                'valid' => false,
                'message' => 'Please set a valid sell BTC address'
            ];
        }

        if (!AddressValidator::isValid($btcFeeAddress)) {
            $feeBalanceNotification = [
                'valid' => false,
                'message' => 'Please set a valid sell BTC address'
            ];
        }

        $cryptoCurrenciesRequests = Order::with('user')
            ->orderBy('id', 'desc')->take(10)->get();

        return view('admin.dashboard', compact(
            'users',
            'walletInfo',
            'adminUsdBalance',
            'adminBtcBalance',
            'adminEthBalance',
            'buyBalanceNotification',
            'sellBalanceNotification',
            'feeBalanceNotification',
            'cryptoCurrenciesRequests',
            'settings',
            'ethereumAllNodeBalance'
        ));
    }

    /**
     * Calculate admin profit by fee, sell and buy addresses
     *
     * @return int - balance amount
     */
    protected function adminProfit()
    {
        $settings = Settings::first();
        /*$addressesArray = [
            $settings->btc_fee_address,
            $settings->btc_sell_address,
            $settings->btc_buy_address
        ];

        $addressesArray = array_unique($addressesArray);

        $balance = 0;
        foreach ($addressesArray as $address) {
            if (AddressValidator::isValid($address)) {
                $response = $this->bitcoind()->getReceivedByAddress($address);
                $balance += $response->get();
            }
        }*/

        return $settings->btc_balance;
    }

    /**
     * Get "Edit Admin USD Balance" Modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editBalanceModal(Request $request, $id) 
    {
        $balance = Settings::where('balance', $request->balance)->first();
        $balanceAdminUSD = Settings::find($id);
        return view('admin.balance.modals.edit', [
            'balanceAdminUSD' => $balanceAdminUSD
        ]);
    }

    /**
     * Get "Edit Ethereum Node Balance" Modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editBalanceEthereumNodeModal(Request $request, $id)
    {  
        return view('admin.ethnodebalance.modals.edit');   
    }

    /**
     * Get "Edit Admin Bitcoin Balance" Modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editBalanceBitcoinModal(Request $request, $id) 
    {
        $balance = Settings::where('btc_balance', $request->btc_balance)->first();
        $balanceAdminBTC = Settings::find($id);
        $settings = Settings::first();      

        $btcBuyAddress = $settings->btc_buy_address;
        $btcSellAddress = $settings->btc_sell_address;
        $btcFeeAddress = $settings->btc_fee_address;

        // BUY
        $buyBalance = $this->bitcoind()->getReceivedByAddress($btcBuyAddress)->get();
        $buyBalance = formatBitcoin(satoshiToBitcoin($buyBalance));
        $buyBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your BUY address (' . $btcBuyAddress . '): ' . $buyBalance . ' BTC'
        ];  
        // SELL
        $sellBalance = $this->bitcoind()->getReceivedByAddress($btcBuyAddress)->get();
        $sellBalance = formatBitcoin(satoshiToBitcoin($sellBalance));
        $sellBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your SELL address (' . $btcSellAddress . '): ' . $sellBalance . ' BTC'
        ];
        // FEE
        $feeBalance = $this->bitcoind()->getReceivedByAddress($btcBuyAddress)->get();
        $feeBalance = formatBitcoin(satoshiToBitcoin($feeBalance));
        $feeBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your FEE address (' . $btcFeeAddress . '): ' . $feeBalance . ' BTC'
        ];

        return view('admin.btcbalance.modals.edit', [
            'balanceAdminBTC' => $balanceAdminBTC,
            'buyBalanceNotification' => $buyBalanceNotification,
            'sellBalanceNotification' => $sellBalanceNotification,
            'feeBalanceNotification' => $feeBalanceNotification
        ]);
    }
    /**
     * Get "Edit Admin Ethereum Balance" Modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editBalanceEthereumModal(Request $request, $id) 
    {
        $balance = Settings::where('eth_balance', $request->eth_balance)->first();
        $balanceAdminETH = Settings::find($id);
        $settings = Settings::first();

        // BUY
        $ethBuyAddress = $settings->eth_buy_address;
        $buyBalance = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($ethBuyAddress, 'latest', true);
        $buyBalance = $buyBalance / pow(10, 18);
        $buyBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your BUY address (' . $ethBuyAddress . '): ' . $buyBalance . ' ETH'
        ];
        // SELL
        $ethSellAddress = $settings->eth_sell_address;
        $sellBalance = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($ethSellAddress, 'latest', true);
        $sellBalance = $sellBalance / pow(10, 18);
        $sellBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your SELL address (' . $ethSellAddress . '): ' . $sellBalance . ' ETH'
        ];
        //FEE
        $ethFeeAddress = $settings->eth_fee_address;
        $feeBalance = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($ethFeeAddress, 'latest', true);
        $feeBalance = $feeBalance / pow(10, 18);
        $feeBalanceNotification = [
            'valid' => true,
            'message' => 'Your current balance in your FEE address (' . $ethFeeAddress . '): ' . $feeBalance . ' ETH'
        ];        

        return view('admin.ethbalance.modals.edit', [
            'balanceAdminETH' => $balanceAdminETH,
            'buyBalanceNotification' => $buyBalanceNotification,
            'sellBalanceNotification' => $sellBalanceNotification,
            'feeBalanceNotification' => $feeBalanceNotification
        ]);
    }
    /**
     * Update Balance
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validateFieldRules = [
            'balance' => 'required'
        ];
        $request->validate($validateFieldRules);
        $balance = Settings::find($id);
        $balance->update($request->all());

        return response()->json([
            'message' => 'Balance ' . $balance . ' was updated successfully.'
        ]);
    }


}
