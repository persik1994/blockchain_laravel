<?php

namespace App\Http\Controllers\Admin;

use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Settings;
use Illuminate\Support\Facades\Validator;



class SettingsController extends Controller
{

    use Bitcoind;


    public function plugins()
    {
        $settings = Settings::first();

        return view('admin.settings.plugins', compact('settings'));
    }


    public function bitcoin()
    {
        $settings = Settings::first();
        $txFee = $this->bitcoind()->getWalletInfo()->get('paytxfee');

        return view('admin.settings.bitcoin', [
            'settings' => $settings,
            'txFee' => $txFee
        ]);
    }

    public function ethereum()
    {
        $settings = Settings::first();

        return view('admin.settings.ethereum', [
            'settings' => $settings,
        ]);
    }


    /**
     * Save Bitcoin settings
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveBitcoin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'auto-update-bitcoin-price' => 'required|in:0,1',
            'btc-fee' => 'required|numeric',
        ]);

        $validator->sometimes('bitcoin-fixed-price', 'required|numeric', function ($request) {
            return $request->get('auto-update-bitcoin-price') == 0;
        });

        $validator->sometimes('bitcoin-buy-fee', 'required|numeric|max:0', function ($request) {
            return $request->get('auto-update-bitcoin-price') == 1;
        });

        $validator->sometimes('bitcoin-sell-fee', 'required|numeric|min:0', function ($request) {
            return $request->get('auto-update-bitcoin-price') == 1;
        });

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $txFee = $this->bitcoind()->getWalletInfo()->get('paytxfee');
        if($request->get('btc-fee') !== $txFee) {
            $this->bitcoind()->settxfee($request->get('btc-fee'));
        }

        $settings = Settings::first();
        $settings->update([
            'autoupdate_bitcoin_price' => $request->get('auto-update-bitcoin-price'),
            'bitcoin_buy_fee' => $request->get('bitcoin-buy-fee'),
            'bitcoin_sell_fee' => $request->get('bitcoin-sell-fee'),
            'bitcoin_fixed_price' => $request->get('bitcoin-fixed-price')
        ]);

        return response()->json([
            'message' => 'Your changes was saved successfully'
        ]);
    }
    /**
     * Save Ethereum settings
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveEthereum(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'auto-update-ethereum-price' => 'required|in:0,1',
            'ethereum-gas-price' => 'required|numeric',
            'ethereum-gas-limit' => 'required|numeric',
        ]);

        $validator->sometimes('ethereum-fixed-price', 'required|numeric', function ($request) {
            return $request->get('auto-update-ethereum-price') == 0;
        });

        $validator->sometimes('ethereum-buy-fee', 'required|numeric|max:0', function ($request) {
            return $request->get('auto-update-ethereum-price') == 1;
        });

        $validator->sometimes('ethereum-sell-fee', 'required|numeric|min:0', function ($request) {
            return $request->get('auto-update-ethereum-price') == 1;
        });

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);    
        }

        $settings = Settings::first();

        $ethereumBuyFee = $request->get('ethereum-buy-fee');
        $ethereumBuyFee = empty($ethereumBuyFee) ? 0 : $ethereumBuyFee;
        $ethereumSellFee = $request->get('ethereum-sell-fee');
        $ethereumSellFee = empty($ethereumSellFee) ? 0 : $ethereumSellFee;
        $ethereumFixedPrice = $request->get('ethereum-fixed-price');
        $ethereumFixedPrice = empty($ethereumFixedPrice) ? 0 : $ethereumFixedPrice;

        $settings->update([
            'autoupdate_ethereum_price' => $request->get('auto-update-ethereum-price'),
            'ethereum_buy_fee' => $ethereumBuyFee,
            'ethereum_sell_fee' => $ethereumSellFee,
            'ethereum_fixed_price' => $ethereumFixedPrice,
            'ethereum_gas_price' => $request->get('ethereum-gas-price'),
            'ethereum_gas_limit' => $request->get('ethereum-gas-limit'),
        ]);

        return response()->json([
            'message' => 'Your changes was saved successfully'
        ]);
    }

    public function generateBitcoinAddresses() {
        $settings = Settings::first();

        if (empty($settings->btc_fee_address)) {

            $address = $this->bitcoind()->getNewAddress('admin')->get();
            $settings->update([
                'btc_fee_address' => $address
            ]);
        }

        if (empty($settings->btc_sell_address)) {
            $address = $this->bitcoind()->getNewAddress('admin')->get();
            $settings->update([
                'btc_sell_address' => $address
            ]);
        }

        if (empty($settings->btc_buy_address)) {
            $address = $this->bitcoind()->getNewAddress('admin')->get();
            $settings->update([
                'btc_buy_address' => $address
            ]);
        }

        return response()->json([
            'message' => 'Addresses generated successfully',
            'fee_address' => $settings->btc_fee_address,
            'sell_address' => $settings->btc_sell_address,
            'buy_address' => $settings->btc_buy_address,
        ]);
    }

    public function generateEthereumAddresses(Request $request) {
        $request->validate([
            'fee_address_password' => 'required|string|min:8|max:255|regex:/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/|confirmed',
            'buy_address_password' => 'required|string|min:8|max:255|regex:/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/|confirmed',
            'sell_address_password' => 'required|string|min:8|max:255|regex:/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/|confirmed',
        ], [
            'fee_address_password.regex' => 'You password should have at least 1(a-z), 1(A-Z) and 1 special character from (!@#$%^&*)',
            'buy_address_password.regex' => 'You password should have at least 1(a-z), 1(A-Z) and 1 special character from (!@#$%^&*)',
            'sell_address_password.regex' => 'You password should have at least 1(a-z), 1(A-Z) and 1 special character from (!@#$%^&*)'
        ]);

        $settings = Settings::first();

        if (empty($settings->eth_fee_address)) {
            $address = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($request->fee_address_password);

            $settings->update([
                'eth_fee_address' => $address
            ]);
        }

        if (empty($settings->eth_sell_address)) {
            $address = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($request->sell_address_password);

            $settings->update([
                'eth_sell_address' => $address
            ]);
        }

        if (empty($settings->eth_buy_address)) {
            $address = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($request->buy_address_password);

            $settings->update([
                'eth_buy_address' => $address
            ]);
        }

        return response()->json([
            'message' => 'Addresses generated successfully',
            'fee_address' => $settings->eth_fee_address,
            'sell_address' => $settings->eth_sell_address,
            'buy_address' => $settings->eth_buy_address
        ]);
    }


    public function web()
    {
        $settings = Settings::first();

        return view('admin.settings.web', [
            'settings' => $settings
        ]);
    }

    public function saveWeb(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'keywords' => 'required',
            'name' => 'required',
            'url' => 'required',
            'infoemail' => 'required',
            'supportemail' => 'required',
            'default_language' => 'required',
//            'default_currency' => 'required',
            'withdrawal_comission' => 'required',
            'max_addresses_per_account' => 'required',
            'document_verification' => 'required',
            'email_verification' => 'required',
            'phone_verification' => 'required',
//            'nexmo_api_key' => 'required',
//            'nexmo_api_secret' => 'required',
//            'fb_link' => 'required',
//            'tw_link' => 'required',
        ]);

        $settings = Settings::first();

        $settings->update($request->all());

        return response()->json([
            'message' => 'Settings successfully saved'
        ]);
    }


    public function updatePlugins(Request $request) {
        $request->merge(['plugin_buy_bitcoins' => !empty($request->plugin_buy_bitcoins) ? 1 : 0]);
        $request->merge(['plugin_sell_bitcoins' => !empty($request->plugin_sell_bitcoins) ? 1 : 0]);
        $request->merge(['plugin_transfer_bitcoins' => !empty($request->plugin_transfer_bitcoins) ? 1 : 0]);
        $request->merge(['plugin_request_bitcoins' => !empty($request->plugin_request_bitcoins) ? 1 : 0]);

        $settings = Settings::first();

        $settings->plugin_buy_bitcoins = $request->plugin_buy_bitcoins;
        $settings->plugin_sell_bitcoins = $request->plugin_sell_bitcoins;
        $settings->plugin_transfer_bitcoins = $request->plugin_transfer_bitcoins;
        $settings->plugin_request_bitcoins = $request->plugin_request_bitcoins;
        $settings->process_time_to_buy = $request->process_time_to_buy;
        $settings->process_time_to_sell = $request->process_time_to_sell;

        $settings->save();

        return redirect(route('admin.settings.plugins'));
    }
}
