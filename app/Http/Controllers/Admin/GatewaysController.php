<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gateway;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GatewaysController extends Controller
{
    public function index()
    {
        $gateways = Gateway::paginate(10);

        return view('admin.gateways.index', compact('gateways'));
    }

    /**
     * Get "Add gateway" modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addGatewayModal() 
    {
        return view('admin.gateways.modals.add');
    }

    /**
     * Get "Edit gateway" modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGatewayModal($id) {
        $gatewayToEdit = Gateway::find($id);

        $gatewayToEdit->gateway_information = json_decode($gatewayToEdit->gateway_information, true);

        if (!empty($gateway)) {
            return response()->json([
                'message' => 'Gateway was not found! Please refresh page.'
            ], 404);
        }

        return view('admin.gateways.modals.edit', compact('gatewayToEdit'));
    }

    /**
     * Add new row in gateways
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $validateFieldRules = [
            'name' => 'required',
            'currency' => 'required'
        ];

        $request->validate($validateFieldRules);

        $request->merge(['allow' => !empty($request->allow) ? 1 : 0]);
//        $request->merge(['allow_send' => !empty($request->allow_send) ? 1 : 0]);
//        $request->merge(['allow_receive' => !empty($request->allow_receive) ? 1 : 0 ]);

        $gateway = Gateway::where('name', $request->name)
            ->where('currency', $request->currency)->first();

        if (!empty($gateway)) {
            return response()->json([
                'message' => 'Gateway ' . $request->name . ' already exist'
            ], 404);
        } else {
            $gatewayInformation = $request->except(['name', 'currency', 'allow']);

            $request->merge(['gateway_information' => json_encode($gatewayInformation)]);

            $gateway = Gateway::create($request->all());

            return response()->json([
                'message' => 'Gateway ' . $gateway->name . ' was added successfully.'
            ]);
        }
    }

    /**
     * Update gateway
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validateFieldRules = [
            'name' => 'required',
            'currency' => 'required'
        ];

        $request->validate($validateFieldRules);

        $request->merge(['allow' => !empty($request->allow) ? 1 : 0]);
//        $request->merge(['allow_send' => !empty($request->allow_send) ? 1 : 0]);
//        $request->merge(['allow_receive' => !empty($request->allow_receive) ? 1 : 0]);
//
        $gateway = Gateway::find($id);

        if (empty($gateway)) {
            return response()->json([
                'message' => 'Gateway was not found'
            ], 404);
        }

        if ($gateway->name != $request->name && $gateway->currency != $request->currency) {
            $gatewayExist = Gateway::where('name', $request->name)
                ->where('currency', $request->currency)->first();

            if (!empty($gatewayExist)) {
                return response()->json([
                    'message' => 'Gateway ' . $request->name . ' already exist'
                ], 404);
            }
        }

        $gatewayInformation = $request->except(['name', 'currency', 'allow']);

        $request->merge(['gateway_information' => json_encode($gatewayInformation)]);

        $gateway->update($request->all());

        return response()->json([
            'message' => 'Gateway ' . $gateway->name . ' was added successfully.'
        ]);
    }

    /**
     * Delete gateway item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $item = Gateway::find($id);

        if ($item) {
            $item->delete();

            return response()->json([
                'message' => 'Gateway was deleted successfully.',
            ]);
        }

        return response()->json([
            'message' => 'Gateway was not found.'
        ], 404);
    }
}
