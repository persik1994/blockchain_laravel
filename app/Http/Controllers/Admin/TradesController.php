<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserOrderManageEvent;
use App\Models\Order;
use App\Models\Settings;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;
use Jcsofts\LaravelEthereum\Lib\EthereumTransaction;
    
class TradesController extends Controller
{
    use HelperTrait;
    use Bitcoind;

    public function index($type) {
        if (!in_array($type, [Order::TYPE_BUY, Order::TYPE_SELL])) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $trades = Order::with('user')
            ->where('order_type', $type)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.trades.index', compact('trades', 'type'));
    }

    /**
     * Mark user money row as viewed
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsViewed(Request $request)
    {
        $request->validate([
            'items' => 'array',
            'items.*' => 'integer',
            'type' => 'required|in:buy,sell'
        ]);

        if (empty($request->items)) {
            return response()->json([
                'message' => 'Please select at least one field'
            ], 500);
        }

        foreach ($request->items as $id) {
            $order = Order::where('order_type', $request->type)
                ->where('id', $id)->first();

            if (!empty($order)) {
                if ($order->viewed_by_admin == 0) {
                    $order->viewed_by_admin = 1;
                    $order->save();
                }
            }
        }

        $cointUnviewed = Order::where('order_type', $request->type)
            ->where('viewed_by_admin', 0)->count();

        if ($order->order_type == 'buy') {
            Cache::forever('buys_unviewed_by_admin', $cointUnviewed);
        } else if ($order->order_type == 'sell') {
            Cache::forever('sells_unviewed_by_admin', $cointUnviewed);
        }

        return response()->json([]);
    }

    public function detailsModal(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'type' => 'required|in:buy,sell'
        ]);

        $trade = Order::with('user', 'user.addresses')
            ->where('order_type', $request->type)
            ->where('id', $request->id)->first();

        if ($trade->viewed_by_admin == 0) {
            $trade->viewed_by_admin = 1;
            $trade->save();

            $cointUnviewed = Order::where('order_type', $request->type)
                ->where('viewed_by_admin', 0)->count();

            Cache::forever($request->type . 's_unviewed_by_admin', $cointUnviewed);
        }

        if ($request->type == 'sell') {
            return view('admin.trades.modals.sell-details', compact('trade'));
        }

        return view('admin.trades.modals.buy-details', compact('trade'));
    }

    /**
     * Confirm user trade by type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'type' => 'required|in:buy,sell'
        ]);

        $cryptoRequest = Order::with('user', 'user.bitcoinAddresses', 'user.ethereumAddresses')
            ->where('order_type', $request->type)
            ->where('status', Order::STATUS_WAITING)
            ->where('id', $request->id)->firstOrFail();

        if ($request->type === 'buy') {
            $settings = Settings::first();

            switch ($cryptoRequest->crypto_currency) {
                case 'BTC':
                    if (empty($cryptoRequest->user->bitcoinAddresses)) {
                        return response()->json([
                            'message' => 'Please take a look with user: ' . $cryptoRequest->user->email . ' Seems he don\'t have any available address'
                        ], 404);
                    }                
                    $adminBalance = $this->bitcoind()->getBalance('admin', 6)->get();

                    if ((float)$adminBalance < (float)$cryptoRequest->crypto_amount) {
                        return response()->json([
                            'message' => "Admin don't have enough money on your BTC account"
                        ], 400);
                    }

                    if ((float)$cryptoRequest->user->balance < (float)$cryptoRequest->fiat_amount) {
                        return response()->json([
                            'message' => "User don't have enough fiat money on his account"
                        ], 400);
                    }                
                    $txid = $this->bitcoind()->sendMany('admin', [
                        $cryptoRequest->user->addresses[0]->address => (string)number_format($cryptoRequest->crypto_amount, 8)
                    ]);
                    break;
                case 'ETH':
                    if (empty($cryptoRequest->user->ethereumAddresses)) {
                        return response()->json([
                            'message' => 'Please take a look with user: ' . $cryptoRequest->user->email . ' Seems he don\'t have any available address'
                        ], 404);
                    }

                    if(empty($request->passphrase)) {
                        return response()->json([
                            'message' => 'Please enter passphrase'
                        ], 404);
                    }

                    $from = $settings->eth_buy_address;
                    $to = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_coinbase();
                    $amount = (string)number_format($cryptoRequest->crypto_amount, 18);
                    $waiAmount = dechex($amount * pow(10, 18));
                    $gasPrice = app()->settings->ethereum_gas_price;
                    $gasPrice = dechex($gasPrice);
                    $waiAmount = dechex($amount * pow(10, 18));
                    $gasPrice = "0x$gasPrice";
                    $waiAmount = "0x$waiAmount";


                    $adminBalance = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($from, 'latest', TRUE); 
                    if ((float)$adminBalance < (float)$cryptoRequest->crypto_amount) {
                        return response()->json([
                            'message' => "Admin don't have enough money on your ETH account"
                        ], 400);
                    }
                    if ((float)$cryptoRequest->user->balance < (float)$cryptoRequest->fiat_amount) {
                        return response()->json([
                            'message' => "User don't have enough fiat money on his account"
                        ], 400);
                    }    

                    $transaction = new EthereumTransaction($from, $to, $waiAmount, "0x76c0", $gasPrice, "0x");
                    $txid = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_sendTransaction($transaction, $request->get('passphrase'));
                    break;                
            }


            $cryptoRequest->update([
                'status' => Order::STATUS_PROCESSING,
                'txid' => $txid
            ]);

            $cryptoRequest->user->updateUnviewedItemsCache('buy');

            $ordersByOrderType = Order::takeItemsByOrderType(
                $request->type, false, $cryptoRequest->user, true,
                [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING]
            );

            event(new UserOrderManageEvent([
                'order' => $cryptoRequest,
                'orders' => $ordersByOrderType,
                'user' => $cryptoRequest->user,
                'unviewed_buys' => $cryptoRequest->user->getCountUnviewedItems('buys')
            ]));

            return response()->json([
                'message' => "You successfully deposit {$cryptoRequest->crypto_amount} BTC to account: {$cryptoRequest->user->email}"
            ]);
        }

        if ($request->type === 'sell') {


            if($cryptoRequest->crypto_currency == 'BTC') {
                    $userBalance = $this->bitcoind()->getBalance($cryptoRequest->user->email, 6)->get();
                    if ((float)$userBalance < (float)$cryptoRequest->crypto_amount) {
                        return response()->json([
                            'message' => "User don't have enough money on your BTC account"
                        ], 400);
                    }
                    $settings = Settings::first();
                    $txid = $this->bitcoind()->sendMany($cryptoRequest->user->email, [
                        $settings->btc_sell_address => (string)number_format($cryptoRequest->crypto_amount, 8)
                    ]);

                    $cryptoRequest->update([
                        'status' => Order::STATUS_PROCESSING,
                        'txid' => $txid
                    ]);

                    $cryptoRequest->user->updateUnviewedItemsCache('sell');

                    $ordersByOrderType = Order::takeItemsByOrderType(
                        $request->type, false, $cryptoRequest->user, true,
                        [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING]
                    );

                    event(new UserOrderManageEvent([
                        'order' => $cryptoRequest,
                        'orders' => $ordersByOrderType,
                        'user' => $cryptoRequest->user,
                        'unviewed_sells' => $cryptoRequest->user->getCountUnviewedItems('sells')
                    ]));

                    return response()->json([
                        'message' => "You successfully bought {$cryptoRequest->crypto_amount} BTC from account: {$cryptoRequest->user->email}"
                    ]);
            } elseif ($cryptoRequest->crypto_currency == 'ETH') {


                $coinbase = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_coinbase();
                $userBalance = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($coinbase, 'latest', TRUE);
                if ((float)$userBalance < (float)$cryptoRequest->crypto_amount) {
                    return response()->json([
                        'message' => "User don't have enough money on your ETH account"
                    ], 400);
                }
                $settings = Settings::first();


                $from = $settings->eth_sell_address;
                $to = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_coinbase();
                $amount = (string)number_format($cryptoRequest->crypto_amount, 18);
                $waiAmount = dechex($amount * pow(10, 18));
                $gasPrice = app()->settings->ethereum_gas_price;
                $gasPrice = dechex($gasPrice);
                $waiAmount = dechex($amount * pow(10, 18));
                $gasPrice = "0x$gasPrice";
                $waiAmount = "0x$waiAmount";


                $transaction = new EthereumTransaction($from, $to, $waiAmount, "0x76c0", $gasPrice, "0x");
                $txid = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_sendTransaction($transaction, $request->get('passphrase'));

                    $cryptoRequest->update([
                        'status' => Order::STATUS_PROCESSING,
                        'txid' => $txid
                    ]);

                    $cryptoRequest->user->updateUnviewedItemsCache('sell');

                    $ordersByOrderType = Order::takeItemsByOrderType(
                        $request->type, false, $cryptoRequest->user, true,
                        [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING]
                    );

                    event(new UserOrderManageEvent([
                        'order' => $cryptoRequest,
                        'orders' => $ordersByOrderType,
                        'user' => $cryptoRequest->user,
                        'unviewed_sells' => $cryptoRequest->user->getCountUnviewedItems('sells')
                    ]));

                    return response()->json([
                        'message' => "You successfully bought {$cryptoRequest->crypto_amount} ETH from account: {$cryptoRequest->user->email}"
                    ]);
            }

            
        }

        return response()->json([
            'message' => 'You have requested wrong trade type'
        ], 404);
    }


    /**
     * Cancel user trade by type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'type' => 'required|in:buy,sell'
        ]);

        $cryptoRequest = Order::with('user', 'user.addresses')
            ->where('order_type', $request->type)
            ->where('status', Order::STATUS_WAITING)
            ->where('id', $request->id)->firstOrFail();

        $cryptoRequest->update([
            'status' => Order::STATUS_CANCELED
        ]);

        $ordersByOrderType = Order::takeItemsByOrderType($request->type, false, $cryptoRequest->user, true);

        $eventUserOrderManagesData = [
            'order' => $cryptoRequest,
            'orders' => $ordersByOrderType,
            'user' => $cryptoRequest->user
        ];

        if ($request->type === 'buy') {
            $cryptoRequest->user->updateUnviewedItemsCache('buy');
            $eventUserOrderManagesData['unviewed_buys'] = $cryptoRequest->user->getCountUnviewedItems('buys');
        }

        if ($request->type === 'sell') {
            $cryptoRequest->user->updateUnviewedItemsCache('sell');
            $eventUserOrderManagesData['unviewed_sells'] = $cryptoRequest->user->getCountUnviewedItems('sells');
        }

        event(new UserOrderManageEvent($eventUserOrderManagesData));

        return response()->json([
            'message' => ucfirst($request->type) . ' order request was canceled'
        ]);
    }
}
