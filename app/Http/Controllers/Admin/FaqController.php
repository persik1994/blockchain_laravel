<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::get();
        return view('admin.faqs.index', compact('faqs'));
    }

    /**
     * Get "Add gateway" modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addFaqModal() 
    {
        return view('admin.faqs.modals.add');
    }

    /**
     * Get "Edit faq" modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editFaqModal($id) 
    {
        $faqToEdit = Faq::find($id);

        if (!empty($faq)) 
        {
            return response()->json([
                'message' => 'Faq was not found! Please refresh page.'
            ], 404);
        }

        return view('admin.faqs.modals.edit', compact('faqToEdit'));
    } 


    /**
     * Add new row in faqs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $validateFieldRules = [
            'question' => 'required',
            'answer' => 'required'
        ];

        $request->validate($validateFieldRules);

        $faq = Faq::where('question', $request->question)
            ->where('answer', $request->answer)->first();

        if (!empty($faq)) {
            return response()->json([
                'message' => 'Faq ' . $request->question . ' already exist'
            ], 404);
        } else {

            $faq = Faq::create($request->all());

            return response()->json([
                'message' => 'Faq ' . $faq->question . ' was added successfully.'
            ]);
        }
    }

    /**
     * Update faq
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validateFieldRules = [
            'question' => 'required',
            'answer' => 'required'
        ];

        $request->validate($validateFieldRules);

        $faq = Faq::find($id);

        if (empty($faq)) {
            return response()->json([
                'message' => 'Faq was not found'
            ], 404);
        }

        if ($faq->question != $request->question && $faq->answer != $request->answer) {
            $faqExist = Faq::where('question', $request->question)
                ->where('answer', $request->answer)->first();

            if (!empty($faqExist)) {
                return response()->json([
                    'message' => 'Faq ' . $request->question . ' already exist'
                ], 404);
            }
        }


        $faq->update($request->all());

        return response()->json([
            'message' => 'Faq ' . $faq->question . ' was added successfully.'
        ]);
    }


    /**
     * Delete faq item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $item = Faq::find($id);

        if ($item) {
            $item->delete();

            return response()->json([
                'message' => 'Faq was deleted successfully.',
            ]);
        }

        return response()->json([
            'message' => 'Faq was not found.'
        ], 404);
    }

}
