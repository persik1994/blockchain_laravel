<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * Get private file
     *
     * @param $filepath
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getFile($filepath)
    {
        return response()->download(storage_path('app/' . $filepath), null, [], null);
    }
}
