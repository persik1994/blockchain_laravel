<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function getUnviewedCount(Request $request)
    {
        $countUnviewed = 0;
        //TODO Create this function for all types: deposit, withdraw, buy, sell
        switch ($request->type) {
            case 'transfer':
                $countUnviewed = Transfer::where('viewed_by_admin', 0)->count();
                break;
        }

        return response()->json([
            'count_unviewed' => $countUnviewed
        ]);
    }
}
