<?php

namespace App\Http\Controllers\Admin;

use App\Events\UserMoneyManageEvent;
use App\Models\Order;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserMoney;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MoneyController extends Controller
{
    /**
     * Get user deposits page
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deposits()
    {
        $userMoneyDeposits = UserMoney::with('user', 'gateway')
            ->where('transaction_type', 'deposit')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.money.deposits', compact('userMoneyDeposits'));
    }

    /**
     * Get user withdrawals page
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdrawals()
    {
        $userMoneyWithdrawals = UserMoney::with('user')
            ->where('transaction_type', 'withdraw')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.money.withdrawals', compact('userMoneyWithdrawals'));
    }

    public function depositsDetailsModal($id)
    {
        $userDeposit = UserMoney::with('user', 'gateway')
            ->where('transaction_type', 'deposit')
            ->findOrFail($id);

        if ($userDeposit->viewed_by_admin == 0) 
        {
            $userDeposit->viewed_by_admin = 1;
            $userDeposit->save();

            $cointUnviewed = UserMoney::where('transaction_type', 'deposit')->where('viewed_by_admin', 0)->count();

            Cache::forever('deposits_unviewed_by_admin', $cointUnviewed);
        }

        return view('admin.money.modals.deposits-details', compact('userDeposit'));
    }

    public function withdrawalsDetailsModal($id)
    {
        $userWithdraw = UserMoney::with('user', 'user.bankAccount')
            ->where('transaction_type', 'withdraw')
            ->findOrFail($id);

        if ($userWithdraw->viewed_by_admin == 0) {
            $userWithdraw->viewed_by_admin = 1;
            $userWithdraw->save();

            $cointUnviewed = UserMoney::where('transaction_type', 'withdraw')->where('viewed_by_admin', 0)->count();

            Cache::forever('withdraws_unviewed', $cointUnviewed);
        }

        return view('admin.money.modals.withdrawals-details', compact('userWithdraw'));
    }

    /**
     * Mark user money row as viewed
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsViewed(Request $request)
    {
        $request->validate([
            'items' => 'array',
            'items.*' => 'integer',
            'type' => 'required|in:deposit,withdraw'
        ]);

        if (empty($request->items)) {
            return response()->json([
                'message' => 'Please select at least one field'
            ], 500);
        }

        foreach ($request->items as $id) {
            $userMoney = UserMoney::where('transaction_type', $request->type)
                ->where('id', $id)->first();

            if (!empty($userMoney)) {
                if ($userMoney->viewed_by_admin == 0) {
                    $userMoney->viewed_by_admin = 1;
                    $userMoney->save();
                }
            }
        }

        $cointUnviewed = UserMoney::where('transaction_type', $request->type)->where('viewed_by_admin', 0)->count();

        if ($userMoney->transaction_type == 'deposit') {
            Cache::forever('deposits_unviewed_by_admin', $cointUnviewed);
        } else if ($userMoney->transaction_type == 'withdraw') {
            Cache::forever('withdraws_unviewed_by_admin', $cointUnviewed);
        }

        return response()->json([]);
    }

    /**
     * Confirm user money by type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'type' => 'required|in:deposit'
        ]);

        $userMoney = UserMoney::with('user')
            ->where('transaction_type', $request->type)
            ->findOrFail($request->id);

        $userMoney->user->balance = $userMoney->user->balance + $userMoney->amount;
        $userMoney->user->save();

        $userMoney->updated_at = date('Y-m-d H:i:s', time());
        $userMoney->status = UserMoney::STATUS_CONFIRMED;
        $userMoney->save();

        $userMoneyByTransactionsType = UserMoney::takeItemsByTransactionType(
            $userMoney->transaction_type, false, $userMoney->user, true,
            [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED]
        );

        $userMoney->user->updateUnviewedItemsCache('deposit');
        $userMoney->user->balance_number = number_format($userMoney->user->balance, 2) . ' ' . $userMoney->user->fiat_currency;

        event(new UserMoneyManageEvent([
            'order' => $userMoney,
            'orders' => $userMoneyByTransactionsType,
            'user' => $userMoney->user,
            'unviewed_deposits' => $userMoney->user->getCountUnviewedItems('deposits')
        ]));

        return response()->json([
            'message' => "You successfully {$request->type} {$userMoney->amount} USD to account: {$userMoney->user->email}"
        ]);
    }


    /**
     * Cancel user money by type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'type' => 'required|in:deposit,withdraw'
        ]);

        $userMoney = UserMoney::with('user')
            ->where('transaction_type', $request->type)
            ->findOrFail($request->id);

        $userMoney->status = UserMoney::STATUS_CANCELED;
        $userMoney->updated_at = date('Y-m-d H:i:s', time());
        $userMoney->save();

        $userMoneyByTransactionsType = UserMoney::takeItemsByTransactionType(
            $userMoney->transaction_type, false, $userMoney->user, true,
            [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED]
        );

        $eventUserMoneyManagesData = [
            'order' => $userMoney,
            'orders' => $userMoneyByTransactionsType,
            'user' => $userMoney->user
        ];

        if ($request->type === 'deposit') {
            $userMoney->user->updateUnviewedItemsCache('deposit');
            $eventUserMoneyManagesData['unviewed_deposits'] = $userMoney->user->getCountUnviewedItems('deposits');
        }

        if ($request->type === 'withdraw') {
            $userMoney->user->updateUnviewedItemsCache('withdraw');
            $eventUserMoneyManagesData['unviewed_withdraws'] = $userMoney->user->getCountUnviewedItems('withdraws');
        }

        event(new UserMoneyManageEvent($eventUserMoneyManagesData));

        return response()->json([
            'message' => ucfirst($request->type) . ' transaction request was canceled'
        ]);
    }

    public function uploadProofImageModal()
    {
        return view('admin.money.modals.upload-proof-image');
    }

    public function uploadProofImage($id, Request $request)
    {
        $request->validate([
            'imageProof' => 'required|file|mimes:jpg,jpeg,png,pdf'
        ]);

        $withdraw = UserMoney::with('user')
            ->where('id', $id)
            ->where('transaction_type', 'withdraw')
            ->where('status', 'waiting')
            ->first();

        if (empty($withdraw)) {
            return response()->json([
                'message' => 'Withdraw was not found. Please refresh page.'
            ]);
        }

        if (empty($withdraw->user)) {
            return response()->json([
                'message' => 'Withdraw user was not found. Please check user withdraw owner.'
            ]);
        }

        if (($withdraw->amount * (-1)) > $withdraw->user->availableAmount('USD')) {
            return response()->json([
                'message' => 'Users balance is to small to make withdraw'
            ], 404);
        }

        $time = time();
        $fileName = "payment-proof-{$time}." . $request->file('imageProof')->getClientOriginalExtension();
        $path = $request->file('imageProof')->storeAs("payment-proofs/{$withdraw->user->id}", $fileName);

        $withdraw->user->balance = $withdraw->user->balance - $withdraw->amount;
        $withdraw->user->save();

        $withdraw->updated_at = date('Y-m-d H:i:s', time());
        $withdraw->status = UserMoney::STATUS_CONFIRMED;
        $withdraw->attachment = $path;
        $withdraw->save();

        $userMoneyByTransactionsType = UserMoney::takeItemsByTransactionType(
            $withdraw->transaction_type, false, $withdraw->user, true,
            [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED]
        );

        $withdraw->user->updateUnviewedItemsCache('withdraw');

        event(new UserMoneyManageEvent([
            'order' => $withdraw,
            'orders' => $userMoneyByTransactionsType,
            'user' => $withdraw->user,
            'unviewed_withdraws' => $withdraw->user->getCountUnviewedItems('withdraws'),
            'user_current_balance' => number_format($withdraw->user->availableAmount($withdraw->user->fiat_currency), 2)
        ]));

        return response()->json([
            'message' => "You successfully {$request->type} {$withdraw->amount} USD to account: {$withdraw->user->email}"
        ]);
    }
}
