<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\UserTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Get user list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::paginate(15);

        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Get user transactions
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactionsModal(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $user = User::with([
            'transactions' => function($q) {
                $q->orderBy('id', 'desc');
            }
        ])->findOrFail($request->get('id'));

        return view('admin.users.modals.transactions', [
            'user' => $user
        ]);
    }


    /**
     * Edit user modal
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editModal(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $user = User::findOrFail($request->get('id'));

        return view('admin.users.modals.edit', [
            'user' => $user
        ]);
    }


    /**
     * Edit user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $user = User::findOrFail($request->get('id'));

        $update = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'email_verified_at' => empty($request->get('email-verified')) ? null : \DB::raw('NOW()'),
            'status' => $request->get('status'),
            'document_verified' => empty($request->get('document-verified')) ? 0 : 1,
            'mobile_verified' => empty($request->get('mobile-verified')) ? 0 : 1,
            'mobile_number' => $request->get('mobile-number'),
        ];

        if (trim($request->get('password')) != '') {
            $update['password'] = Hash::make($request->get('password'));
        }


        $user->update($update);

        return response()->json([
            'message' => 'User information updated'
        ]);
    }


    /**
     * Delete user document
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDocument(Request $request)
    {
        $request->validate([
            'type' => 'required|in:document-1,document-2',
            'id' => 'required|numeric'
        ]);

        $user = User::findOrFail($request->get('id'));

        switch ($request->get('type')) {
            case 'document-1':
                Storage::disk('verification-documents')->delete(str_replace('verification-documents/', '', $user->document_1));
                $user->update([
                    'document_1' => ''
                ]);
                break;
            case 'document-2':
                Storage::disk('verification-documents')->delete(str_replace('verification-documents/', '', $user->document_2));
                $user->update([
                    'document_2' => ''
                ]);
                break;
        }

        return response()->json([
            'message' => 'User document deleted'
        ]);
    }


    /**
     * Delete user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return response()->json([
            'message' => 'User deleted'
        ]);
    }
}
