<?php

namespace App\Http\Controllers\Account;

use App\Models\History;
use App\Models\Order;
use App\Models\UserMoney;
use App\Traits\HelperTrait;
use App\UsersLoginHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryController extends Controller
{
    use HelperTrait;

    public function index(Request $request)
    {
        if (empty($request->type)) $request->type = 'deposit';

        $historyItems = [];
        $user =  \Auth::user();

        switch ($request->type) {
            case 'deposit':
                $items = UserMoney::with('gateway')
                    ->where('uid', $user->id)
                    ->where('transaction_type', $request->type)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(15);

                foreach ($items as $item) {
                    $item->details_route = route('deposit-money.details', $item->id);
                    $item->amount = $this->convertCurrency($item->amount, 'USD', $user->fiat_currency);
                }

                $historyItems['deposit'] = $items;
                break;
            case 'withdraw':
                $items = UserMoney::with('gateway')
                    ->where('uid', $user->id)
                    ->where('transaction_type', $request->type)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(15);

                foreach ($items as $item) {
                    $item->details_route = route('withdraw-money.details', $item->id);
                    $item->amount = $this->convertCurrency((-1) * $item->amount, 'USD', $user->fiat_currency);
                }

                $historyItems['withdraw'] = $items;
                break;
            case 'buy':
            case 'sell':
                $items = Order::with('user')
                    ->where('uid', $user->id)
                    ->where('order_type', $request->type)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(15);

                foreach ($items as $item) {
                    $item->details_route = route('account.order.details', $item->id);
                }

                $historyItems[$request->type] = $items;
                break;
            case 'login':
                $items = UsersLoginHistory::with('user')
                    ->where('uid', $user->id)
                    ->paginate(15);

                $historyItems[$request->type] = $items;
                break;
        }

        $historyType = $request->type;

        return view('account.history.index', compact('historyItems', 'historyType'));
    }
}
