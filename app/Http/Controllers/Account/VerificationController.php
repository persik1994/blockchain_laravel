<?php

namespace App\Http\Controllers\Account;

use App\Helpers\NexmoMessage;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\SmsCode;
use App\Models\User;
use App\Models\UserBankAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Send user email verification
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmail()
    {
        $userId = Auth::user()->id;

        $user = User::findOrFail($userId);
        $hash = md5($user->email);

        $user->update([
            'email_hash' => $hash
        ]);

        $settings = Settings::first();

        Mail::to($user->email)->send(new EmailVerification($user, $settings));

        return response()->json([
            'message' => __('account.success_11')
        ]);
    }


    /**
     * Verify user email
     * @param $hash
     * @return \Illuminate\Http\Response
     */
    public function verifyEmail($hash)
    {
        $user = User::where('email_hash', $hash)->firstOrFail();

        $user->update([
            'email_verified_at' => \DB::raw('NOW()')
        ]);

        Session::flash('success', __('account.success_16', [
            'email' => $user->email
        ]));

        return redirect('/account/profile/edit');
    }


    /**
     * Send documents for verification
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendDocuments(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'document-1' => 'required|file|mimes:jpeg,pdf,png',
            'document-2' => 'required|file|mimes:jpeg,pdf,png',
        ], [
            'document-1.*' => __('account.error_33'),
            'document-2.*' => __('account.error_33'),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }


        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $document1FileName = "verification-document-1." . $request->file('document-1')->getClientOriginalExtension();
        $document2FileName = "verification-document-2." . $request->file('document-2')->getClientOriginalExtension();

        $document1Path = $request->file('document-1')->storeAs("verification-documents/$userId", $document1FileName);
        $document2Path = $request->file('document-2')->storeAs("verification-documents/$userId", $document2FileName);


        $user->update([
            'document_1' => $document1Path,
            'document_2' => $document2Path,
        ]);

        return response()->json([
            'message' => __('account.success_12')
        ]);
    }


    /**
     * Save new phone number
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function savePhoneNumber(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone-number' => 'required|numeric'
        ], [
            'phone-number.*' => __('account.error_37')
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $user->update([
            'mobile_number' => str_replace('+', '', $request->get('phone-number'))
        ]);

        return response()->json([
            'message' => __('account.success_15')
        ]);
    }


    /**
     * Send sms verification code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSmsCode() {
        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        if (empty($user->mobile_number)) {
            return response()->json([
                'message' => __('account.error_37')
            ], 400);
        }

        $settings = Settings::first();

        //$nexmoSms = new NexmoMessage($settings->nexmo_api_key, $settings->nexmo_api_secret);
        $code = rand(00000,99999);
        $number = '+' . $user->mobile_number;

        SmsCode::create([
            'uid' => $userId,
            'sms_code' => $code,
            'verified' => 0
        ]);

        $message = "Your code for {$settings->name} is: {$code}";
        //$nexmoSms->sendText($number, $settings->name, $message);

        return response()->json([
            'message' => __('account.success_13', ['number' => $number])
        ]);
    }


    /**
     * Verify sms code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifySmsCode(Request $request) {
        $validator = Validator::make($request->all(), [
            'sms-code' => 'required|numeric'
        ], [
            'sms-code.*' => __('account.error_34')
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        $smsCode = SmsCode::where('uid', $userId)->where('sms_code', $request->get('sms-code'))->first();

        if (empty($smsCode)) {
            return response()->json([
                'message' => __('account.error_35')
            ], 404);
        }

        $smsCode->update([
            'verified' => 1
        ]);

        $user->update([
            'mobile_verified' => 1
        ]);

        return response()->json([
            'message' => __('account.success_14')
        ]);
    }
}
