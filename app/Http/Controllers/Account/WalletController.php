<?php

namespace App\Http\Controllers\Account;

use App\Events\UpdateCryptoCurrencyEvent;
use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserAddresses;
use App\Models\UserMoney;
use App\Rules\BtcAddress;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
    
use Jcsofts\LaravelEthereum\Facade\Ethereum;
use Jcsofts\LaravelEthereum\Lib\EthereumTransaction;


class WalletController extends Controller
{

    use HelperTrait;
    use Bitcoind;



    /**
     * 
     *
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $user = Auth::user();
        $userAddresses = UserAddresses::where('uid', $user->id)
            ->where('crypto_currency', $user->crypto_currency)
            ->orderBy('id', 'desc')
            ->get();

        $settings = Settings::first();
        $userCryptoBalance = $user->getCryptoBalance();
        $transactions = [];


        return view('account.wallet.index', [
            'user' => $user,
            'settings' => $settings,    
            'transactions' => $transactions,
            'userAddresses' => $userAddresses,
            'userCryptoBalance' => $userCryptoBalance,
        ]);
    }

    /**
     * Get information about current account
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentAccountInformation()
    {
        $currentUser = \Auth::user();

        if (!empty($currentUser)) {
            $currentUserInfo = [
                'id' => $currentUser->id
            ];

            return response()->json([
                'data' => $currentUserInfo
            ]);
        }

        return response()->json([
            'message' => 'Sorry. Something went wrong. Please refresh page.'
        ]);
    }

    /**
     * Get information about current user for support
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentUserInformationSupport()
    {
        $user = \Auth::user();

        if (empty($user)) {
            return response()->json([
                'data' => null
            ]);
        }

        return response()->json([
            'data' => $user->id
        ]);
    }


    /**
     * Create new address
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAddress(Request $request)
    {
        $user = Auth::user();

        $validatorRules = [
            'label' => 'required|string'
        ];

        if ($user->crypto_currency == 'ETH') {
            $validatorRules['password'] = 'required|string|min:8|max:255|regex:/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/';
        }

        $validator = Validator::make($request->all(), $validatorRules, [
            'label.*' => __('account.error_41'),
            'password.*' => __('account.error_46')
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        switch ($user->crypto_currency) {
            case 'BTC':
                $response = $this->bitcoind()->getNewAddress($user->email);

                UserAddresses::create([
                    'uid' => $user->id,
                    'label' => $request->get('label'),
                    'crypto_currency' => 'BTC',
                    'address' => $response->get(),
                    'created' => \DB::raw('NOW()'),
                    'updated' => \DB::raw('NOW()'),
                ]);

                return response()->json([
                    'message' => 'You have new Bitcoin address'
                ]);
                break;
            case 'ETH':
                $password = $request->get('password');
                $addressUser = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_newAccount($password);

                UserAddresses::create([
                    'uid' => $user->id,
                    'label' => $request->get('label'),
                    'crypto_currency' => 'ETH',
                    'address' => $addressUser,
                    'created' => \DB::raw('NOW()'),
                    'updated' => \DB::raw('NOW()'),
                ]);

                return response()->json([
                    'message' => 'You have new Ethereum address'
                ]);
                break;
            default:
                return response()->json([
                    'message' => 'Incorrect type'
                ], 400);
                break;
        }
    }


    /**
     * Create new bitcoin address modal
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBitcoinAddressModal()
    {
        return view('account.wallet.modals.create-address');
    }


    /**
     * Get bitcoin addresses block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBitcoinAddresses()
    {
        $user = Auth::user();
        $userAddresses = UserAddresses::where('uid', $user->id)
            ->where('crypto_currency', $user->crypto_currency)
            ->orderBy('id', 'desc')
            ->get();

        switch ($user->crypto_currency) {
            case 'BTC':
                $view = 'account.wallet.btc-addresses';
                break;
            case 'ETH':
                $view = 'account.wallet.eth-addresses';
                break;
            default:
                $view = null;
                break;
        }

        return view($view, [
            'userAddresses' => $userAddresses
        ]);
    }


    /**
     * Get "Receive bitcoins" modal
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function receiveBitcoinsModal(Request $request)
    {
        $request->validate([
            'address' => ['required', new BtcAddress],
        ]);

        return view('account.wallet.modals.receive-bitcoins', [
            'address' => $request->get('address'),
        ]);
    }


    /**
     * Get "Send bitcoins" modal
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sendBitcoinsModal()
    {
        $settings = Settings::first();
        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $accountBalance = $this->bitcoind()->getBalance($user->email, 6)->get();

        $txFee = $this->bitcoind()->getWalletInfo()->get('paytxfee');
        $balance = (float)$accountBalance - (float)$txFee - (float)$settings->withdrawal_comission;
        $balance = $balance < 0 ? 0 : $balance;

        $addresses['ETH'] = UserAddresses::where('crypto_currency', 'ETH')->get();

        $gasLimit = app()['settings']->ethereum_gas_limit;
        $gasPrice = app()['settings']->ethereum_gas_price;      
        $txFeeEthereum = $gasLimit * $gasPrice / pow(10, 9);  

        return view('account.wallet.modals.send-bitcoins', [
            'balance' => $balance,
            'txFee' => $txFee,
            'user' => $user,
            'addresses' => $addresses,
            'txFeeEthereum' => $txFeeEthereum
        ]);
    }


    public function getCurrencyPrices() {
        $bitcoinPrice['USD'] = $this->getBitcoinPrice('USD');
        $bitcoinPrice['EUR'] = $this->getBitcoinPrice('EUR');
        $ethereumPrice['USD'] = $this->getEthereumPrice('USD');
        $ethereumPrice['EUR'] = $this->getEthereumPrice('EUR');

        $getCryptoCurrencyPriceChange24hBTC['USD'] = $this->getCryptoCurrencyPriceChange24h(1, 'USD');
        $getCryptoCurrencyPriceChange24hBTC['EUR'] = $this->getCryptoCurrencyPriceChange24h(1, 'EUR');
        $getCryptoCurrencyPriceChange24hETH['USD'] = $this->getCryptoCurrencyPriceChange24h(1027, 'USD');
        $getCryptoCurrencyPriceChange24hETH['EUR'] = $this->getCryptoCurrencyPriceChange24h(1027, 'EUR');

        return response()->json([
            'priceBTCUSD' => $bitcoinPrice['USD'] ? $bitcoinPrice['USD'] : '',
            'priceBTCEUR' => $bitcoinPrice['EUR'] ? $bitcoinPrice['EUR'] : '',
            'priceETHUSD' =>  $ethereumPrice['USD'] ? $ethereumPrice['USD'] : '',
            'priceETHEUR' => $ethereumPrice['EUR'] ? $ethereumPrice['EUR'] : '',
            'currencyTimeBTCUSD' => $getCryptoCurrencyPriceChange24hBTC['USD'],
            'currencyTimeBTCEUR' => $getCryptoCurrencyPriceChange24hBTC['EUR'],
            'currencyTimeETHUSD' => $getCryptoCurrencyPriceChange24hETH['USD'],
            'currencyTimeETHEUR' => $getCryptoCurrencyPriceChange24hETH['EUR']
        ]);
    }

    /**
     * Update user currencies settings
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserCurrencies(Request $request)
    {
        $request->validate([
            'crypto_currency' => 'required|in:' . implode(',', array_keys(config('currencies.crypto_currencies'))),
            'fiat_currency' => 'required:in:' . implode(',', array_keys(config('currencies.fiat_currencies')))
        ]);

        $user = \Auth::user();

        $user->crypto_currency = $request->crypto_currency;
        $user->fiat_currency = $request->fiat_currency;
        $user->save();

        return response()->json();
    }
}
