<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserBankAccount;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class SecurityController extends Controller
{

    use HelperTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $google2fa = app('pragmarx.google2fa');

        if (!empty($user->google2fa_secret)) {
            $google2faSecret = $user->google2fa_secret;
        } else {
            $google2faSecret = $google2fa->generateSecretKey();
        }

        $qrImage = $google2fa->getQRCodeInline(
            app()['settings']->name,
            $user->email,
            $google2faSecret
        );

        return view('account.security.index', [
            'user' => $user,
            'qrImage' => $qrImage,
            'google_2fa_secret' => $google2faSecret
        ]);
    }


    /**
     * Disable two factor authentication
     */
    public function disableTfa() {
        $user = Auth::user();
        $user->update([
            'google2fa_secret' => ''
        ]);

        return redirect(route('security'));
    }


    /**
     * Enable two factor authentication
     */
    public function enableTfa(Request $request) {
        $user = Auth::user();

        $user->update([
            'google2fa_secret' => $request->get('google-2fa-secret')
        ]);

        return redirect(route('security'));
    }


    /**
     * Change user password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'password' => 'required|string|min:8',
            'confirm-password' => 'required|string|min:8',
        ], [
            'password.min' => __('account.error_18'),
            'confirm-password.min' => __('account.error_18'),
        ]);

        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        $errors = [];
        if (!Hash::check($request->get('current-password'), $user->password)) {
            $errors['current-password'] = [__('account.error_17')];
        }

        if ($request->get('password') !== $request->get('confirm-password')) {
            $errors['confirm-password'] = [__('account.error_19')];
        }

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        if (!empty($errors)) {
            return response()->json([
                'errors' => $errors
            ], 400);
        }

        $user->update([
            'password' => Hash::make($request->get('password'))
        ]);

        return response()->json([
            'message' => __('account.success_6')
        ]);
    }


    /**
     * Change user pin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePin(Request $request) {
        $validator = Validator::make($request->all(), [
            'new-pin' => 'required|string|min:6',
            'confirm-pin' => 'required|string|min:6',
        ], [
            'current-pin.required' => __('account.error_1'),
            'new-pin.required' => __('account.error_1'),
            'confirm-pin.required' => __('account.error_1'),
            'new-pin.min' => __('account.error_21'),
            'confirm-pin.min' => __('account.error_21'),
        ]);


        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        $validator->sometimes('current-pin', 'required', function () use ($user) {
            return !empty($user->secret_pin);
        });

        $errors = [];
        if (!empty($user->secret_pin) && !Hash::check($request->get('current-pin'), $user->secret_pin)) {
            $errors['current-pin'] = [__('account.error_20')];
        }

        if ($request->get('new-pin') !== $request->get('confirm-pin')) {
            $errors['confirm-pin'] = [__('account.error_22')];
        }

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        if (!empty($errors)) {
            return response()->json([
                'errors' => $errors
            ], 400);
        }

        $user->update([
            'secret_pin' => Hash::make($request->get('new-pin'))
        ]);

        return response()->json([
            'message' => __('account.success_7')
        ]);
    }


    /**
     * Change user pin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removePin(Request $request) {
        $validator = Validator::make($request->all(), [
            'current-pin' => 'required|string|min:6',
        ], [
            'current-pin.required' => __('account.error_23'),
        ]);


        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);

        $errors = [];
        if (!empty($user->secret_pin) && !Hash::check($request->get('current-pin'), $user->secret_pin)) {
            $errors['current-pin'] = [__('account.error_24')];
        }


        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        if (!empty($errors)) {
            return response()->json([
                'errors' => $errors
            ], 400);
        }

        $user->update([
            'secret_pin' => ''
        ]);

        return response()->json([
            'message' => __('account.success_8')
        ]);
    }
}
