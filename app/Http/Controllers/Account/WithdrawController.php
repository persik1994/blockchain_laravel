<?php

namespace App\Http\Controllers\Account;

use App\Events\UserMoneyRequestEvent;
use App\Http\Controllers\Controller;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserBankAccount;
use App\Models\UserMoney;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class WithdrawController extends Controller
{
    use HelperTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;

        $user = User::findOrFail($userId);
        $bankAccount = UserBankAccount::where('uid', $userId)->first();
        $settings = Settings::first();
        $withdrawals = UserMoney::where('uid', \Auth::user()->id)
            ->where('transaction_type', 'withdraw')
            ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
            ->where('viewed_by_owner', 0)
            ->orderBy('updated_at', 'desc')->get();

        $withdrawalsTotalCount = UserMoney::where('uid', \Auth::user()->id)
            ->where('transaction_type', 'withdraw')->count();

        return view('account.withdraw.index', [
            'user' => $user,
            'settings' => $settings,
            'bankAccount' => $bankAccount,
            'withdrawals' => $withdrawals,
            'withdrawalsTotalCount' => $withdrawalsTotalCount
        ]);
    }


    /**
     * Send withdraw request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendRequest(Request $request)
    {
        $user = Auth::user();
        $userAvailableAmount = $user->availableAmount('USD');
        $time = time();

        $validator = Validator::make($request->all(), [
            'bank-holder-name' => 'required|string',
            'bank-account-number' => 'required|string',
            'bank-swift' => 'required|string',
            'bank-address' => 'required|string',
            'amount' => "required|numeric|min:0.01|max:$userAvailableAmount",
            'user-fiat-currency' => 'required|in:' . implode(',', array_keys(config('currencies.fiat_currencies')))
        ], [
            'bank-holder-name.*' => __('account.error_bank_holder'),
            'bank-account-number.*' => __('account.error_bank_account'),
            'bank-swift.*' => __('account.error_bank_swift'),
            'bank-address.*' => __('account.error_bank_address'),
            'amount.required' => __('account.error_9'),
            'amount.max' => __('account.error_44', ['amount' => $userAvailableAmount, 'currency' => 'USD']),
            'amount.min' =>  __('account.error_9'),
            'secret-pin.*' => __('account.error_29'),
        ]);

        $validator->sometimes('secret-pin', [
            'required',
            'string',
            function($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->secret_pin)) {
                    $fail(__('account.error_29'));
                }
            }
        ], function () use ($user) {
            return !empty($user->secret_pin);
        });

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $withdraw = UserMoney::create([
            'uid' => $user->id,
            'transaction_type' => 'withdraw',
            'status' => UserMoney::STATUS_WAITING,
            'amount' => $this->convertCurrency((float)$request->get('amount'), $request->get('user-fiat-currency'), 'USD'),
            'user_fiat_currency' => $request->get('user-fiat-currency'),
            'user_amount' => (float)$request->get('amount'),
            'gateway_id' => 0,
            'time' => $time,
            'updated_at' => date('Y-m-d H:i:s', $time),
            'u_field_1' => $request->get('bank-holder-name'),
            'u_field_2' => $request->get('bank-account-number'),
            'u_field_3' => $request->get('bank-swift'),
            'u_field_4' => $request->get('bank-address')
        ]);

        $bankAccount = UserBankAccount::where('uid', $user->id)->first();

        if (empty($bankAccount)) {
            UserBankAccount::create([
                'uid' => $user->id,
                'u_field_1' => $request->get('bank-holder-name'),
                'u_field_2' => $request->get('bank-account-number'),
                'u_field_3' => $request->get('bank-swift'),
                'u_field_4' => $request->get('bank-address'),
                'time' => $time
            ]);
        } else {
            $bankAccount->update([
                'u_field_1' => $request->get('bank-holder-name'),
                'u_field_2' => $request->get('bank-account-number'),
                'u_field_3' => $request->get('bank-swift'),
                'u_field_4' => $request->get('bank-address'),
                'time' => $time
            ]);
        }

        $cointUnviewed = UserMoney::where('transaction_type', 'withdraw')->where('viewed_by_admin', 0)->count();

        Cache::forever('withdraws_unviewed', $cointUnviewed);

        $withdraw = $withdraw->load('user');
        $withdraw->details_modal_url = route('admin.withdrawal.details.modal', $withdraw->id);
        $withdraw->proof_image_modal_url = route('admin.withdrawal.proof-image.modal', $withdraw->id);
        $withdraw->amount = number_format($withdraw->amount, 2);
        $withdraw->user_amount = number_format($withdraw->user_amount, 2);

        $depositPagination = UserMoney::where('transaction_type', 'withdraw')
            ->orderBy('updated_at', 'desc')->paginate(10);

        event(new UserMoneyRequestEvent([
            'withdraw' => $withdraw,
            'pagination' => $depositPagination
        ]));

        $withdraws = UserMoney::takeItemsByTransactionType('withdraw', 5, $user);

        return response()->json([
            'message' => __('account.success_20'),
            'withdraws' => $withdraws,
            'user' => $user
        ]);
    }

    public function details($id)
    {
        $user = Auth::user();

        $withdraw = UserMoney::with('gateway')
            ->where('transaction_type', 'withdraw')
            ->where('id', (int) $id)->first();

        if (empty($withdraw)) {
            abort(404);
        }

        $withdraw->amount = $this->convertCurrency((-1) * $withdraw->amount, 'USD', \Auth::user()->fiat_currency);

        $withdraw->time = date('Y-m-d H:i:s', $withdraw->time);
        $withdraw->updated_time = date('Y-m-d H:i:s', strtotime($withdraw->updated_at));

        $processedWithdraw = UserMoney::where('transaction_type', 'withdraw')
            ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
            ->where('id', (int) $id)->first();

        if (!empty($processedWithdraw) && $processedWithdraw->viewed_by_owner == 0) {
            $processedWithdraw->viewed_by_owner = 1;
            $processedWithdraw->save();

            $user->updateUnviewedItemsCache('withdraw');
        }

        return view('account.withdraw.details', compact('withdraw'));
    }
}
