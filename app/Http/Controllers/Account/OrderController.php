<?php

namespace App\Http\Controllers\Account;

use App\Events\UserOrderRequestEvent;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    use HelperTrait;

    public function buy() {
        $orders = Order::where('uid', \Auth::user()->id)
            ->where('order_type', 'buy')
            ->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING])
            ->where('viewed_by_owner', 0)
            ->orderBy('updated_at', 'desc')
            ->take(5)->get();

        return view('account.order.buy', compact('orders', 'ordersTotalCount'));
    }

    public function sell() {
        $orders = Order::where('uid', \Auth::user()->id)
            ->where('order_type', 'sell')
            ->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING])
            ->where('viewed_by_owner', 0)
            ->orderBy('updated_at', 'desc')
            ->take(5)->get();

        return view('account.order.sell', compact('orders', 'ordersTotalCount'));
    }

    public function details($id)
    {
        $user = Auth::user();

        $order = Order::where('uid', $user->id)
            ->where('id', (int) $id)->first();

        if (empty($order)) {
            abort(404);
        }

        $processedOrder = Order::whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_CANCELED, Order::STATUS_PROCESSING])
            ->where('id', (int) $id)->first();

        if (!empty($processedOrder) && $processedOrder->viewed_by_owner == 0) {
            $processedOrder->viewed_by_owner = 1;
            $processedOrder->save();

            $user->updateUnviewedItemsCache($processedOrder->order_type);
        }

        return view('account.order.details', compact('order'));
    }

    /**
     * Buy bitcoins
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   
    public function sendBuyRequest(Request $request)
    {
        $user = Auth::user();
        $user = User::findOrFail($user->id);
        $userAvailableAmount = (float)$user->availableAmount('USD');

        $validator = Validator::make($request->all(), [
            'strategy-type' => 'required|string|in:market,limit,stop',
            'amount' => "required|numeric|min:0.01|max:$userAvailableAmount",
            'order-crypto-currency' => 'required|in:' . implode(',', array_keys(config('currencies.crypto_currencies'))),
            'order-fiat-currency' => 'required|in:' . implode(',', array_keys(config('currencies.fiat_currencies'))),
        ], [
            'strategy-type.*' => __('account.error_45'),
            'amount.required' => __('account.error_9'),
            'amount.max' => __('account.error_28', ['amount' => $userAvailableAmount, 'currency' => 'USD']),
        ]);


        $validator->sometimes('stop-price', 'required|numeric', function ($request) {
            return $request->get('strategy-type') === 'stop';
        });

        $validator->sometimes('limit-price', 'required|numeric', function ($request) {
            return $request->get('strategy-type') === 'limit';
        });

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $strategyType = $request->get('strategy-type');
        $orderCryptoCurrency = $request->get('order-crypto-currency');
        $orderFiatCurrency = $request->get('order-fiat-currency');
        $cryptoBuyPrice = $this->getCryptoBuyPrice($orderCryptoCurrency, $orderFiatCurrency);
        $currentAmount = (float)$request->get('amount');
        $amount = $this->convertCurrency((float)$currentAmount, $orderFiatCurrency, 'USD');

//        if ($strategyType === 'market') {
            $order = Order::create([
                'uid' => $user->id,
                'strategy_type' => $strategyType,
                'order_type' => 'buy',
                'crypto_currency' => $orderCryptoCurrency,
                'crypto_amount' => convertToCrypto($amount / $cryptoBuyPrice, $orderCryptoCurrency),
                'fiat_currency' => $orderFiatCurrency,
                'fiat_amount' => $amount,
                'current_amount' => $currentAmount,
                'status' => Order::STATUS_WAITING,
                'txid' => ''
            ]);

            $cointUnviewed = Order::where('order_type', 'buy')
                ->where('viewed_by_admin', 0)
                ->count();

            Cache::forever('buys_unviewed_by_admin', $cointUnviewed);

            $orderPagination = Order::where('order_type', 'buy')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);

            $order = $order->load('user');
            $order->details_modal_url = route('admin.trades.modal.details', $order->id);
            $order->converted_crypto_amount = convertToCrypto($order->crypto_amount, $order->crypto_currency);

            event(new UserOrderRequestEvent([
                'order' => $order,
                'pagination' => $orderPagination
            ]));

            $amountReceived = $cryptoBuyPrice == 0 ? 0 : $amount / $cryptoBuyPrice;

            return response()->json([
                'message' => __('account.success_4', ['amount' => convertToCrypto($amountReceived, $orderCryptoCurrency), 'cryptocurrency' => $order->crypto_currency])
            ]);
//        }

        //TODO feature functional
        /*else {
            $targetPrice = (float)($strategyType === 'limit' ? $request->get('limit-price') : $request->get('stop-price'));

            Order::create([
                'uid' => $userId,
                'strategy_type' => $strategyType,
                'order_type' => 'buy',
                'crypto_currency' => 'BTC',
                'crypto_amount' => number_format($amount / $btcBuyPrice, 8),
                'fiat_amount' => $amount,
                'target_amount' => $targetPrice,
                'status' => Order::STATUS_WAITING
            ]);

            return response()->json([
                'message' => __('account.success_19')
            ]);
        }*/
    }


    /**
     * Sell bitcoins
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSellRequest(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $userBitcoinsAmount = (float)$user->btc_balance;

        $validator = Validator::make($request->all(), [
            'strategy-type' => 'required|string|in:market,limit,stop',
            'amount' => "required|numeric|min:0.00000001|max:$userBitcoinsAmount",
            'order-crypto-currency' => 'required|in:' . implode(',', array_keys(config('currencies.crypto_currencies'))),
            'order-fiat-currency' => 'required|in:' . implode(',', array_keys(config('currencies.fiat_currencies'))),
        ], [
            'strategy-type.*' => __('account.error_45'),
            'amount.required' => __('account.error_27'),
            'amount.max' => __('account.error_28', ['amount' => $userBitcoinsAmount, 'currency' => 'BTC']),
        ]);

        // $validator->sometimes('stop-price', 'required|numeric', function ($request) {
        //     return $request->get('strategy-type') === 'stop';
        // });

        // $validator->sometimes('limit-price', 'required|numeric', function ($request) {
        //     return $request->get('strategy-type') === 'limit';
        // });

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $strategyType = $request->get('strategy-type');
        $orderCryptoCurrency = $request->get('order-crypto-currency');
        $orderFiatCurrency = $request->get('order-fiat-currency');
        $amount = (float)$request->get('amount');

//        if ($strategyType === 'market') {
            $cryptoSellPrice = $this->getCryptoSellPrice($orderCryptoCurrency, $orderFiatCurrency);
            $receivedAmount = $amount * $cryptoSellPrice;
            $currentAmount = $this->convertCurrency($receivedAmount, $orderFiatCurrency, $orderFiatCurrency);

            $order = Order::create([
                'uid' => $userId,
                'strategy_type' => $strategyType,
                'order_type' => 'sell',
                'crypto_currency' => $orderCryptoCurrency,
                'crypto_amount' => convertToCrypto($amount, $orderCryptoCurrency),
                'fiat_currency' => $orderFiatCurrency,
                'fiat_amount' => $receivedAmount,
                'current_amount' => $currentAmount,
                'status' => Order::STATUS_WAITING,
                'txid' => ''
            ]);

            $cointUnviewed = Order::where('order_type', 'sell')
                ->where('viewed_by_admin', 0)
                ->count();

            Cache::forever('sells_unviewed_by_admin', $cointUnviewed);

            $orderPagination = Order::where('order_type', 'sell')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);

            $order = $order->load('user');
            $order->details_modal_url = route('admin.trades.modal.details', $order->id);

            event(new UserOrderRequestEvent([
                'order' => $order,
                'pagination' => $orderPagination
            ]));

            return response()->json([
                'message' => __('account.success_9', ['amount' => convertToCrypto($amount, $orderCryptoCurrency), 'cryptocurrency' => $order->crypto_currency])
            ]); 
//        }

        //TODO feature functional
        /*else {
            $targetPrice = (float)($strategyType === 'limit' ? $request->get('limit-price') : $request->get('stop-price'));

            Order::create([
                'uid' => $userId,
                'transaction_type' => 'sell_btc',
                'order_type' => $strategyType,
                'status' => 0,
                'amount' => $amount,
                'target_price' => $targetPrice,
                'time' => $time,
            ]);

            return response()->json([
                'message' => __('account.success_19')
            ]);
        }*/
    }

    /**
     * Send order request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendRequest(Request $request)
    {
        if ($request->get('order-type') == 'buy') {
            return $this->sendBuyRequest($request);
        } else {
            return $this->sendSellRequest($request);
        }
    }
}
