<?php

namespace App\Http\Controllers\Account;

use App\Helpers\AddressValidator;
use App\Http\Controllers\Controller;
use App\Mail\RequestBitcoins;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserAddresses;
use App\Models\UserBankAccount;
use App\Rules\BtcAddress;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{

    use HelperTrait;
    use Bitcoind;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;

        $user = User::findOrFail($userId);

        $bitcoinAddressesResponse = $this->bitcoind()->getAddressesByAccount($user->email);
        $bitcoinAddresses = $bitcoinAddressesResponse->get();
        $settings = Settings::first();
        $addresses['ETH'] = UserAddresses::where('crypto_currency', 'ETH')->get();

        return view('account.request.index', [
            'user' => $user,
            'settings' => $settings,
            'bitcoinAddresses' => $bitcoinAddresses,
            'addresses' => $addresses
        ]);
    }



    public function sendRequest(Request $request) {
        $validator = Validator::make($request->all(), [
            'address' => ['required', 'string'],
            'amount' => 'required|numeric',
            'email' => 'required|email',
            'notes' => 'present',
        ], [
            'address.*' => __('account.error_12'),
            'email.required' => __('account.error_13'),
            'email.email' => __('account.error_14'),
            'amount.required' => __('account.error_15'),
            'amount.numeric' => __('account.error_16'),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }


        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $settings = Settings::first();


        Mail::to($request->get('email'))->send(new RequestBitcoins([
            'settings' => $settings,
            'user' => $user,
            'amount' => formatBitcoin($request->get('amount')),
            'notes' => $request->get('notes'),
            'address' => $request->get('address'),  
        ]));

        return response()->json([
            'message' => __('account.success_5', ['email' => $request->get('email')])
        ]);
    }

}
