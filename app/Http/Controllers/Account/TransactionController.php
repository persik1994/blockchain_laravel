<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserAddresses;
use App\Models\UserBankAccount;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{

    use HelperTrait;
    use Bitcoind;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // 0.12818963 BTC

        $fromAddress = '2NAzaG3JBMUCNFpcdiB4upQo4ERHaCTzV89';
        $toAddress = '2NDTP1BWiZYVARKNVZ1WzPiT2PpogHDq1g8';
        $restAddress = '2MwQo8cDTfuYjDKZ25zFvHopUd14PK4EwaA';
        $availableAmount = 0.12818963;
        $sendAmount = 0.001;
        $taxFee = 0.001;

//        $listUnspentResponse = $this->bitcoind()->listUnspent(0, 9999999, [
//            $restAddress
//        ])->get();
//
//        dd($listUnspentResponse);

        /*if (!empty($listUnspentResponse)) {
            $createRawTransactionResponse = $this->bitcoind()->createRawTransaction([
                [
                    "txid" => $listUnspentResponse[0]['txid'],
                    "vout" => $listUnspentResponse[0]['vout'],
                ]
            ], [$toAddress => $sendAmount, $restAddress => $availableAmount - $sendAmount - $taxFee])->get();


            $signRawTransactionResponse = $this->bitcoind()->signRawTransaction($createRawTransactionResponse)->get();

            if ($signRawTransactionResponse['complete'] == true) {
                $sendRawTransactionResponse = $this->bitcoind()->sendRawTransaction($signRawTransactionResponse['hex'])->get();
                dd($sendRawTransactionResponse);
            } else {
                dd($signRawTransactionResponse);
            }
        }



        die();*/
        $userId = Auth::user()->id;

        $user = User::findOrFail($userId);
        $settings = Settings::first();


        // add pagination
        // $transactionsList = $this->bitcoind()->listTransactions($user->email, $count, $skip);
        $transactions = $this->bitcoind()->listTransactions($user->email)->get();

        return view('account.transaction.index', [
            'transactions' => $transactions,
            'settings' => $settings
        ]);
    }

    public function updateTransactions($uid)
    {
        $userAddresses = UserAddresses::where('uid', $uid)->get();

        if (!empty($userAddresses)) {
            foreach ($userAddresses as $userAddress) {

            }
        }

//                $license_query = $db->query("SELECT * FROM btc_blockio_licenses WHERE id='$get[lid]' ORDER BY id");
//                $license = $license_query->fetch_assoc();
//                $apiKey = $license['license'];
//                $pin = $license['secret_pin'];
//                $version = 2; // the API version
//                $block_io = new BlockIo($apiKey, $pin, $version);
//                $received = $block_io->get_transactions(array('type' => 'received', 'addresses' => $get[address]));
                /*if($received->status == "success") {
                    $data = $received->data->txs;
                    $dt = StdClass2array($data);
                    foreach($dt as $k=>$v) {
                        $txid = $v['txid'];
                        $time = $v['time'];
                        $amounts = $v['amounts_received'];
                        $amounts = StdClass2array($amounts);
                        foreach($amounts as $a => $b) {
                            $recipient = $b['recipient'];
                            $amount = $b['amount'];
                        }
                        $senders = $v['senders'];
                        $senders = StdClass2array($senders);
                        foreach($senders as $c => $d) {
                            $sender = $d;
                        }
                        $confirmations = $v['confirmations'];
                        $check = $db->query("SELECT * FROM btc_users_transactions WHERE uid='$uid' and txid='$txid'");
                        if($check->num_rows>0) {
                            $update = $db->query("UPDATE btc_users_transactions SET confirmations='$confirmations' WHERE uid='$uid' and txid='$txid'");
                        } else {
                            $insert = $db->query("INSERT btc_users_transactions (uid,type,recipient,sender,amount,time,confirmations,txid) VALUES ('$uid','received','$recipient','$sender','$amount','$time','$confirmations','$txid')");
                        }
                    }
                }
                $sent = $block_io->get_transactions(array('type' => 'sent', 'addresses' => $get[address]));
                if($sent->status == "success") {
                    $data = $sent->data->txs;
                    $dt = StdClass2array($data);
                    foreach($dt as $k=>$v) {
                        $txid = $v['txid'];
                        $time = $v['time'];
                        $amounts = $v['amounts_sent'];
                        $amounts = StdClass2array($amounts);
                        $recipient = '';
                        $i=1;
                        foreach($amounts as $a => $b) {
                            if($i==1) {
                                $recipient = $b['recipient'];
                                $amount = $b['amount'];
                                $i++;
                            }
                        }
                        $senders = $v['senders'];
                        $senders = StdClass2array($senders);
                        foreach($senders as $c => $d) {
                            $sender = $d;
                        }
                        $confirmations = $v['confirmations'];
                        $check = $db->query("SELECT * FROM btc_users_transactions WHERE uid='$uid' and txid='$txid'");
                        if($check->num_rows>0) {
                            $update = $db->query("UPDATE btc_users_transactions SET confirmations='$confirmations' WHERE uid='$uid' and txid='$txid'");
                        } else {
                            $insert = $db->query("INSERT btc_users_transactions (uid,type,recipient,sender,amount,time,confirmations,txid) VALUES ('$uid','sent','$recipient','$sender','$amount','$time','$confirmations','$txid')");
                        }
                    }
                }*/
            //}
        //}
    }

}
