<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function editProfile()
    {
        $user = \Auth::user();

        return view('account.profile.edit', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required|min:6'
        ]);

        $user = \Auth::user();
        $user->name = $request->name;
        $user->save();

        return response()->json([
            'message' => 'Your data was successfully updated'
        ]);
    }
}
