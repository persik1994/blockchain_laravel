<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{
    /**
     * Get file by owner
     *
     * @param $filepath
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getUserFile($filepath)
    {
        $exploadedFilePath = explode('/', $filepath);

        $fileName = end($exploadedFilePath);
        $fileDirectory = reset($exploadedFilePath);
        $fullFilePath = 'app/' . $fileDirectory . '/' . \Auth::user()->id . '/' . $fileName;

        try {
            $file = response()->download(storage_path($fullFilePath), null, [], null);
        } catch (\Exception $e) {
            abort('404');
        }

        return $file;
    }
}
