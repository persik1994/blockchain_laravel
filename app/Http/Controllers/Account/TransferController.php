<?php

namespace App\Http\Controllers\Account;

use App\Events\UserTransferRequestEvent;
use App\Http\Controllers\Controller;
use App\Models\Settings;
use App\Models\Transfer;
use App\Models\User;
use App\Rules\BtcAddress;
use App\Traits\HelperTrait;
use Denpa\Bitcoin\Traits\Bitcoind;
use App\Models\UserAddresses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Jcsofts\LaravelEthereum\Facade\Ethereum;
use Jcsofts\LaravelEthereum\Lib\EthereumTransaction;

class TransferController extends Controller
{

    use HelperTrait;
    use Bitcoind;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;

        $user = User::findOrFail($userId);
        $settings = Settings::first();

        $response = $this->bitcoind()->getWalletInfo();
        $txFee = $response->get('paytxfee');
        $addresses['ETH'] = UserAddresses::where('crypto_currency', 'ETH')->get();

        $transfers = Transfer::where('uid', $userId)
            ->whereIn('status', [1, 2])
            ->where('viewed_by_owner', 0)
            ->orderBy('time', 'asc')->get();

        return view('account.transfer.index', [
            'user' => $user,
            'settings' => $settings,
            'txFee' => $txFee,
            'addresses' => $addresses,
            'transfers' => $transfers
        ]);
    }

    public function details($id)
    {
        $user = Auth::user();

        $transfer = Transfer::where('uid', $user->id)
            ->where('id', (int) $id)->first();

        if (empty($transfer)) {
            abort(404);
        }

        $transfer->viewed_by_owner = 1;
        $transfer->save();

        $user->updateUnviewedItemsCache('transfer');

        return view('account.transfer.details', compact('transfer'));
    }


    /**
     * Make transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendRequest(Request $request)
    {

        $user = Auth::user();
        if ($user->crypto_currency == 'BTC') {

            $validator = Validator::make($request->all(), [
                'address' => ['required', 'string', new BtcAddress],
            ], [
                'address.required' => __('account.error_1')
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }
            // for Bitcoin
            $userId = Auth::user()->id;
            $user = User::findOrFail($userId);

            $accountBalance = $this->bitcoind()->getBalance($user->email, 6)->get();

            $settings = Settings::first();
            $txFee = $this->bitcoind()->getWalletInfo()->get('paytxfee');
            $balance = (float)$accountBalance - (float)$txFee - (float)$settings->withdrawal_comission;
            $balance = $balance < 0 ? 0 : $balance;

            $validator = Validator::make($request->all(), [
                'amount' => "required|numeric|min:0.00000001|max:$balance"
            ], [
                'amount.required' => __('account.error_27'),
                'amount.max' => __('account.error_30', ['amount' => formatBitcoin($balance), 'currency' => 'BTC']),
                'secret-pin.*' => __('account.error_29')
            ]);

            $validator->sometimes('secret-pin', [
                'required',
                'string',
                function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->secret_pin)) {
                        $fail(__('account.error_29'));
                    }
                }
            ], function () use ($user) {
                return !empty($user->secret_pin);
            });

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }

            $transfer = Transfer::create([
                'uid' => $userId,
                'status' => 0,
                'recipient_address' => $request->get('address'),
                'crypto_amount' => (float)$request->get('amount'),
                'crypto_currency' => $user->crypto_currency,
                'time' => \DB::raw('NOW()')
            ]);

        } elseif ($user->crypto_currency == 'ETH') {
            $validator = Validator::make($request->all(), [
                'address' => 'required|string',
                'from-address' => 'required|string',
                'passphrase' => 'required'
            ], [
                'address.required' => __('account.error_1'),
                'from-address.required' => __('account.error_1'),
                'passphrase.required' => __('account.error_1')
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }

            $user = Auth::user();
            $from = $request->get('from-address');
            $accountBalanceEthereum = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_getBalance($from, 'latest', true);
            $gasLimit = app()->settings->ethereum_gas_limit;
            $gasPrice = app()->settings->ethereum_gas_price;
            $amount = (float)$request->get('amount');
            $txFeeEthereum = $gasLimit * $gasPrice / pow(10, 9);
            $balanceEthereum = (float)$accountBalanceEthereum - (float)$txFeeEthereum - (float)app()->settings->withdrawal_comission;
            $balanceEthereum = $balanceEthereum < 0 ? 0 : $balanceEthereum;

            $gasPrice = dechex($gasPrice);
            $waiAmount = dechex($amount * pow(10, 18));
            $gasPrice = "0x$gasPrice";
            $waiAmount = "0x$waiAmount";

            $validator = Validator::make($request->all(), [
                'amount' => "required|numeric|min:0.00000001|max:$balanceEthereum"
            ], [
                'amount.required' => __('account.error_47'),
                'amount.max' => __('account.error_30', ['amount' => formatEthereum($balanceEthereum), 'currency' => 'ETH']),
                'secret-pin.*' => __('account.error_29')
            ]);

            $validator->sometimes('secret-pin', [
                'required',
                'string',
                function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->secret_pin)) {
                        $fail(__('account.error_29'));
                    }
                }
            ], function () use ($user) {
                return !empty($user->secret_pin);
            });

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }

            //var_dump($from, $request->get('address'), $waiAmount, "0x76c0", $gasPrice, $request->get('data'));
            //die();

            //TODO make it through admin approval
//            $transaction = new EthereumTransaction($from, $request->get('address'), $waiAmount, "0x76c0", $gasPrice, "");
//            $transactionHash = \Jcsofts\LaravelEthereum\Facade\Ethereum::personal_sendTransaction($transaction, $request->get('passphrase'));
            //$transactionHash = \Jcsofts\LaravelEthereum\Facade\Ethereum::eth_sendTransaction($transaction);

            $transfer = Transfer::create([
                'uid' => $user->id,
                'status' => 0,
                'recipient_address' => $request->get('address'),
                'crypto_amount' => $amount,
                'crypto_currency' => $user->crypto_currency,
                'from_address' => $from,
                'time' => \DB::raw('NOW()')
            ]);
        }

        $cointUnviewed = Transfer::where('viewed_by_admin', 0)->count();
        Cache::forever('transfers_unviewed_by_admin', $cointUnviewed);

        $transferPagination = Transfer::orderBy('id', 'desc')->paginate(10);

        $transfer->crypto_amount = convertToCrypto($transfer->crypto_amount, $transfer->crypto_currency);

        event(new UserTransferRequestEvent([
            'item' => $transfer,
            'pagination' => $transferPagination
        ]));

        return response()->json([
            'message' => __('account.success_10', [
                'amount' => convertToCrypto($request->get('amount'), $user->crypto_currency),
                'address' => $request->get('address')
            ])
        ]);

    }
}
