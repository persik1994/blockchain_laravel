<?php

namespace App\Http\Controllers\Account;

use App\Events\UserMoneyRequestEvent;
use App\Http\Controllers\Controller;
use App\Models\Gateway;
use App\Models\Settings;
use App\Models\UserMoney;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DepositController extends Controller
{

    use HelperTrait;

    /**
     * Show the application dashboard.
     *
 * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gateways = Gateway::where('allow', '1')->get();
        $settings = Settings::first();
        $deposits = UserMoney::where('uid', \Auth::user()->id)
            ->where('transaction_type', 'deposit')
            ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
            ->where('viewed_by_owner', 0)
            ->orderBy('updated_at', 'desc')->get();

        $depositsTotalCount = UserMoney::where('uid', \Auth::user()->id)
            ->where('transaction_type', 'deposit')->count();
        $firstGateway = $gateways->first();

        if (!$gateways->isEmpty()) {
            $firstGateway->gateway_information = json_decode($firstGateway->gateway_information, true);
        }

        foreach ($deposits as $deposit) {
            $deposit->amount = $this->convertCurrency($deposit->amount, 'USD', \Auth::user()->fiat_currency);
        }

        return view('account.deposit.index', [
            'gateways' => $gateways,
            'settings' => $settings,
            'deposits' => $deposits,
            'depositsTotalCount' => $depositsTotalCount,
            'firstGateway' => $firstGateway
        ]);
    }


    /**
     * Get payment info by id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getPaymentData(Request $request)
    {
        $request->validate([
            'gateway-id' => 'required|integer'
        ]);

        $gateway = Gateway::where('id', $request->get('gateway-id'))->firstOrFail();

        $gatewayInformation = json_decode($gateway->gateway_information, true);

        $gatewayFieldsInformation = [];
        foreach ($gatewayInformation as $key => $item) {
            $gatewayFieldsInformation[Gateway::$gateways[$gateway->name]['fields'][$key]] = $item;
        }

        return response()->json([
            'currency' => $gateway->	currency,
            'fields' => $gatewayFieldsInformation
        ]);
    }


    /**
     * Save user deposit amount
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function saveUserDeposit(Request $request) {
        $validator = Validator::make($request->all(), [
            'gateway-id' => 'required|integer',
            'amount' => 'required|min:0.01|numeric',
            'payment-proof' => 'required|file|mimes:jpeg,pdf,png|max:5000',
        ], [
            'gateway-id.*' => __('account.error_45'),
            'amount.*' => __('account.error_9'),
            'payment-proof.mimes' => __('account.error_11'),
            'payment-proof.size' => __('account.error_48'),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $user = Auth::user();
        $time = time();
        $fileName = "payment-proof-{$time}." . $request->file('payment-proof')->getClientOriginalExtension();
        $path = $request->file('payment-proof')->storeAs("payment-proofs/$user->id", $fileName);
        $gateway = Gateway::where('id', $request->input('gateway-id'))->first();

        if (empty($gateway)) {
            return response()->json([
                'message' => 'Gateway was not found'
            ]);
        }

        try {
           $deposit = UserMoney::create([
                'uid' => $user->id,
                'transaction_type' => 'deposit',
                'status' => UserMoney::STATUS_WAITING,
                'amount' => $this->convertCurrency($request->get('amount'), $gateway->currency, 'USD'),
                'current_amount' => $request->get('amount'),
                'gateway_id' => $request->get('gateway-id'),
                'time' => $time,
                'updated_at' => date('Y-m-d H:i:s', $time),
                'u_field_1' => '',
                'u_field_2' => '',
                'u_field_3' => '',
                'u_field_4' => '',
                'u_field_5' => '',
                'attachment' => $path
           ]);
        } catch (\Exception $exception) {
            Storage::disk('payment-proofs')->delete("$user->id/$fileName");
            throw $exception;
        }

        $cointUnviewed = UserMoney::where('transaction_type', 'deposit')->where('viewed_by_admin', 0)->count();

        Cache::forever('deposits_unviewed_by_admin', $cointUnviewed);

        $depositPagination = UserMoney::where('transaction_type', 'deposit')
            ->orderBy('updated_at', 'desc')->paginate(10);

        $deposit = $deposit->load('user', 'gateway');
        $deposit->gateway_name = \App\Models\Gateway::$gateways[$deposit->gateway->name]['name'];
        $deposit->details_modal_url = route('admin.deposit.details.modal', $deposit->id);

        event(new UserMoneyRequestEvent([
            'deposit' => $deposit,
            'pagination' => $depositPagination
        ]));

        $deposits = UserMoney::takeItemsByTransactionType('deposit', 5, \Auth::user());

        return response()->json([
            'message' => __('account.success_18'),
            'deposits' => $deposits,
            'user' => $user
        ]);
    }

    public function details($id)
    {
        $user = Auth::user();

        $deposit = UserMoney::with('gateway')
            ->where('transaction_type', 'deposit')
            ->where('id', (int) $id)->first();

        if (empty($deposit)) {
            abort(404);
        }

        $deposit->amount = $this->convertCurrency($deposit->amount, 'USD', \Auth::user()->fiat_currency);

        if (!empty($deposit->gateway)) {
            $deposit->gateway_information = json_decode($deposit->gateway->gateway_information, true);
        }

        $deposit->time = date('Y-m-d H:i:s', $deposit->time);
        $deposit->updated_time = date('Y-m-d H:i:s', strtotime($deposit->updated_at));

        $processedDeposit = UserMoney::where('transaction_type', 'deposit')
            ->whereIn('status', [UserMoney::STATUS_CONFIRMED, UserMoney::STATUS_CANCELED])
            ->where('id', (int) $id)->first();

        if (!empty($processedDeposit) && $processedDeposit->viewed_by_owner == 0) {
            $processedDeposit->viewed_by_owner = 1;
            $processedDeposit->save();

            $user->updateUnviewedItemsCache('deposit');
        }

        return view('account.deposit.details', compact('deposit'));
    }
}
