<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\UsersLoginHistory;
use Carbon\Carbon;
use Denpa\Bitcoin\Traits\Bitcoind;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    use Bitcoind;

    public function page($page)
    {
        $page = Page::where('prefix', $page)->first();

        if (empty($page)) {
            abort('404');
        }

        return view('page', compact('page'));
    }

    public function test()
    {
        //
    }
}
