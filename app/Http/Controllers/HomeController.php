<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\Support;
use App\Models\SupportChat;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Traits\HelperTrait;

use Config;
use App\Libs\Eth\Ethereum;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Validator;

use App\Events\NewTicketAdded;

class HomeController extends Controller
{

    use HelperTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function trade($tradeType) 
    {

       $user =  Auth::user();

       if (!preg_match('/^[A-Z]{3}-[A-Z]{3}$/', $tradeType)) {
           abort('404');
       }

       $tradeTypeExploded = explode('-', $tradeType);

       $cryptoCurrency = $tradeTypeExploded[0];
       $fiatCurrency = $tradeTypeExploded[1];

       if (!in_array($cryptoCurrency, array_keys(config('currencies.crypto_currencies')))) {
           abort('404');
       }

       if (!in_array($fiatCurrency, array_keys(config('currencies.fiat_currencies')))) {
           abort('404');
       }

       return view('trade', [
           'user' => $user,
           'cryptoCurrency' => $cryptoCurrency,
           'fiatCurrency' => $fiatCurrency
       ]);
    }
    
    public function buy() {
        return view('buy');
    }
    
    public function sell() {
        return view('sell');
    }
    
    public function send() {
        return view('send');
    }
    
    public function receive() {
        return view('receive');
    }

    public function faq()
    {
        $faqItems = Faq::all();

        return view('faq', compact('faqItems'));
    } 

    public function contactUs()
    {
        return view('contact-us');
    }

    public function sendContactUs(Request $request)
    {
        $request->validate([
           'name' => 'required|min:6',
           'email' => 'required|email',
           'subject' => 'required|min:6',
           'message' => 'required|min:10'
        ]);

        if (!empty(app()['settings']->supportemail)) {
            Mail::send('emails.contact-email', [
                'name' => $request->name,
                'email' =>  $request->email,
                'subject' => $request->subject,
                'userMessage' => $request->message
            ], function($message) use ($request) {
                $message->from($request->email);
                $message->to(app()['settings']->supportemail, 'Admin')
                    ->subject('From ' . app()['settings']->title . ', form contact us');
            });
        }

        return redirect()->back()->with([
            'success' => 'Thank you for contacting us. We will be soon in touch with you.'
        ]);
    }
} 
    